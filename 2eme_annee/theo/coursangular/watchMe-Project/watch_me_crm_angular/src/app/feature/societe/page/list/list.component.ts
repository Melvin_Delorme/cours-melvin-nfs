import { Component } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {
  societeLink : string = '/dashboard/societe/add';
  societeView : string = '/dashboard/societe/view';
  societeEdit : string = '/dashboard/societe/edit';

  societeTitre : string = 'Liste des sociétés';
  societeSubTitre : string = '';

  dataSociete: { [key: string]: string[] } = {
    "row1": ["data 1", "data 2", "data 3", "data 4", "data 5"],
    "row2": ["data 6", "data 7", "data 8"],
    "row3": ["data 9", "data 10", "data 11", "data 12"],
  }
}
