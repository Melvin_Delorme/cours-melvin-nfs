<div>
    <h1 style="text-align: left">Bonjour <span style="font-weight: bold"><?= $firstname ?> <?= $name ?>,</span></h1><br>
    <p>Vous avez été inscrit avec succès à notre plateforme.</p>
    <p>Votre identifiant est : <span style="background-color: rgba(255, 0, 0, 0.5); font-weight: bold"><?= $email ?></span></p>
    <p>Votre mot de passe est : <span style="background-color: rgba(255, 0, 0, 0.5); font-weight: bold"><?= $plainPassword ?></span></p>
    <p style="text-align: center; font-weight: bold; color: #FB6316; font-size: 16px">Veuillez vous connecter et changer votre mot de passe dès que possible.</p>
    <p>Cordialement,<p>
    <p style="font-weight: bold">L'équipe Watch Me</p>
</div>
