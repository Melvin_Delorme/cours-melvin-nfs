<div>
    <h1 style="text-align: left">Bonjour <span style="font-weight: bold"><?= $firstname ?> <?= $name ?>,</span></h1><br>
    <p style="text-align: center; font-weight: bold; color: #FB6316; font-size: 21px"><?= $message ?>.</p>
    <p>Conformément au RGPD, vous avez la possibilité d'en savoir plus sur la manière dont vos données personnelles sont utilisées et protégées en consultant notre politique de confidentialité.</p>
    <p>Cordialement,<p>
    <p style="font-weight: bold">L'équipe Watch Me</p>
</div>

