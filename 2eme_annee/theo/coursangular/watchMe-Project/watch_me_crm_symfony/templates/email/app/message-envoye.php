<div>
    <h1 style="text-align: left">Bonjour <span style="font-weight: bold"><?= $firstname ?> <?= $name ?>,</span></h1><br>
    <p style="text-align: center; font-weight: bold; color: #FB6316; font-size: 21px"><?= $message ?>.</p>
    <p>Nous avons enregistré les modifications que vous avez apportées à votre profil</p>
    <p>Cordialement,<p>
    <p style="font-weight: bold">L'équipe Watch Me</p>
</div>

