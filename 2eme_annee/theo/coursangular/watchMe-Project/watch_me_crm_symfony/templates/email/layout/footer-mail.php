</td>
</tr>
<tr style="background: #351441;">
    <td style="padding: 1rem 20px 20px;">
        <!-- Button : BEGIN -->
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
            <tr>
                <td class="button-td button-td-primary">
                    <a class="button-a button-a-primary" href="http://localhost:4200/" style=" background-color: #02BBE9;
                     color: #F2F2F2;
                     width: 200px;
                     border-radius: 1rem;
                     font-weight: bold;
                     box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.5);
                     padding: 0.5rem;
                     font-family: 'Philosopher', sans-serif;
                     transition: background 0.5s;">Allez sur le site</a>
                </td>
            </tr>
        </table>
        <!-- Button : END -->
    </td>
</tr>

</table>
</td>
</tr>
<!-- 1 Column Text + Button : END -->

<!-- 2 Even Columns : BEGIN -->

<!-- 2 Even Columns : END -->

<!-- Clear Spacer : BEGIN -->

<!-- Clear Spacer : END -->

<!-- 1 Column Text : BEGIN -->

<!-- 1 Column Text : END -->

</table>
<!-- Email Body : END -->

<!-- Email Footer : BEGIN -->

<!-- Email Footer : END -->


<!--[if mso]>

<![endif]-->
</div>

<!-- Full Bleed Background Section : BEGIN -->
<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #3d2e43 ;">
    <tr>
        <td>
            <div align="center" style="max-width: 600px; margin: auto;" class="email-container">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                    <td>
                <![endif]-->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td style="padding: 20px; text-align: center; font-family: 'Philosopher', sans-serif; color: #ffffff;">
                            <p style="margin: 0">Merci d'avoir choisi Watch Me !</p><br>
                            <p style="margin: 0">Votre CRM de qualité.</p>
                            <div class="reseaux" style="display: flex;padding-top: 1rem;align-items: center;flex-direction: row;justify-content: center;gap: 1rem;">
                                <a style="font-size: 30px;color: #02BBE9; transition: all .4s " href="https://www.instagram.com/" target="_blank"><i class="fa-brands lien_footer fa-instagram"></i></a>
                                <a style="font-size: 30px;color: #02BBE9; transition: all .4s  " href="https://www.facebook.com/" target="_blank"><i class="fa-brands lien_footer fa-facebook"></i></a>
                            </div>

                        </td>
                    </tr>
                </table>
                <!--[if mso]>
                <![endif]-->
            </div>
        </td>
    </tr>
</table>
<!-- Full Bleed Background Section : END -->

<!--[if mso | IE]>

<![endif]-->
</center>
</body>
</html>
