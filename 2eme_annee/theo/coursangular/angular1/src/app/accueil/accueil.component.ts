import { Component } from '@angular/core';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent {
  public text: Array<{ name: string, age: number, category: string }> = [
    { name: "shazam", age: 19, category: "adult" }
  ];

  log(event:any): void {
    console.log('toto', event);
    event.target.outerHTML = "<p>TOTO</p>"
  }
}
