import {AfterContentInit, AfterViewInit, Component} from '@angular/core';

@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.css']
})
export class LoadComponent implements AfterContentInit, AfterViewInit{

  ngAfterViewInit() {
    console.log('Page Complète Chargé')
  }
  ngAfterContentInit() {
    console.log('Html Chargé')
  }


}
