import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TachesComponent} from "./taches/taches.component";

const routes: Routes = [
  {
    path: 'taches',
    component: TachesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
