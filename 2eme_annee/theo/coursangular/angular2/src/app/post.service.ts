import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http : HttpClient) { }

  getPost() {
    return  this.http.get('http://jsonplaceholder.typicode.com/posts')
  }

  getPut(body : number) {
    return this.http.put('http://jsonplaceholder.typicode.com/posts', body)
  }
}
