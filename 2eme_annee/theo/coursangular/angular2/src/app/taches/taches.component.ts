import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {PostService} from "../post.service";

@Component({
  selector: 'app-taches',
  templateUrl: './taches.component.html',
  styleUrls: ['./taches.component.css']
})
export class TachesComponent implements OnInit{

  constructor(private postS : PostService) {
  }

  ngOnInit(){
    this.postS.getPost().subscribe(data =>{
      console.log(data);
    })

    this.postS.getPut(1).subscribe(data =>{
      console.log(data);
    })
  }


  text: string = ""
  content: string = ""

  logText(event: any) {
    this.text = event.target.value;
    console.log(this.text);
  }

  ajouterTache() {
    if (this.text.trim() !== "" && this.content.trim() !== "") {
      this.taches.push({
        name: this.text,
        content: this.content
      });
      this.text = ""; // Réinitialisez le champ de nom de la tâche
      this.content = ""; // Réinitialisez le champ de contenu de la tâche
    }
  }

  supprimerTache(index: number) {
    if (index >= 0 && index < this.taches.length) {
      this.taches.splice(index, 1);
    }
  }


  formulaire= new FormGroup({
    name : new FormControl(''),
    email: new FormControl('')
  })

  onSubmit(){
    console.log(this.formulaire.value)
  }



  taches: Array<any> = [
    {
      name: "titi",
      content: "Contenu",
      statut: "En cours"
    },
    {
      name: "Thomas",
      content: "Contenu",
      statut: "Effectué"
    },
    {
      name: "euge",
      content: "Contenu",
      statut: "En cours"
    }
  ];

}
