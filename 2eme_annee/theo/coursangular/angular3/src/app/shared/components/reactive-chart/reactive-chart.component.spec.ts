import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveChartComponent } from './reactive-chart.component';

describe('ReactiveChartComponent', () => {
  let component: ReactiveChartComponent;
  let fixture: ComponentFixture<ReactiveChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReactiveChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReactiveChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
