import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-reactive-chart',
  templateUrl: './reactive-chart.component.html',
  styleUrls: ['./reactive-chart.component.css']
})
export class ReactiveChartComponent implements OnInit{
  data: any;
  options: any;
  title: string = "Dépenses"
  values = [1, 2]


  ngOnInit() {
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');

    this.data = {
      labels: ['A', 'B', 'C'],
      datasets: [
        {
          data: this.values,
          backgroundColor: [documentStyle.getPropertyValue('--blue-500'), documentStyle.getPropertyValue('--yellow-500'), documentStyle.getPropertyValue('--green-500')],
          hoverBackgroundColor: [documentStyle.getPropertyValue('--blue-400'), documentStyle.getPropertyValue('--yellow-400'), documentStyle.getPropertyValue('--green-400')]
        }
      ]
    };

    this.options = {
      plugins: {
        legend: {
          labels: {
            usePointStyle: true,
            color: textColor
          }
        }
      }
    };
  }
}
