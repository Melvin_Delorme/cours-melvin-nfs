const Quizz = require("../models/Quizz");
const axios = require('axios');


exports.create = (req,res) => {
    if(!req.body) {
        res.status(400).send({
            message: 'Veuillez renseigner des données'
        })
    }


    axios.get('https://opentdb.com/api.php?amount=10')
        .then(response => {



            new Promise((resolve, reject) => {
                Quizz.findAll((err, data) => {
                    if (err) {
                        res.status(500).send({
                            message:  err.message || "Une erreur est survenue lors de la récupération des quizz"
                        })
                    }else{
                        resolve(data);
                    }
                });
            })
                .then(alreadyStoredQuestion => {
                    alreadyStoredQuestion = alreadyStoredQuestion.map(e => e.question);

                    response.data.results.forEach((data,index) => {
                        console.log(alreadyStoredQuestion);
                        let indexA = alreadyStoredQuestion.filter(e => e == data.question);
                        if(indexA.length > 0 ){
                            return;
                        }



                        const quizzInsert = new Quizz({
                            question: data.question
                        });

                        Quizz.create(quizzInsert, (err,data) => {

                            if(err){
                                res.status(500).send({
                                    message:  err.message || "Une erreur est survenue"
                                })
                            }
                            if(response.data.results.length -1 == index) {
                                res.send(data)
                            }

                            // Donnée renvoyer
                        })
                    })
                })






        })
        .catch(error => {
            console.log(error)
        })



    // Creation de l'object quizz



}

exports.getAll = (req,res)=>{
    Quizz.findAll((err, data) => {
        if (err) {
            res.status(500).send({
                message:  err.message || "Une erreur est survenue lors de la récupération des quizz"
            })
        }else{
            res.json(data);
        }
    });
}