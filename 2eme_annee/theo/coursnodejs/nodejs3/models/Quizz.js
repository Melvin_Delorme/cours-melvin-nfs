const sql = require('../config/db');

const Quizz = function (quizz) {
    this.question = quizz.question;
}


Quizz.create = (newQuizz , result) => {
    const { question } = newQuizz;

    sql.query(
        "INSERT INTO quizz SET ?",
        newQuizz,
        (err, res) => {
            if (err) {
                console.log("Erreur lors de la création du quizz", err);
                result(err, null);
                return;
            }
            result(null, { id: res.insertId, ...newQuizz });
        }
    );
}

Quizz.findAll = ( result ) => {
    sql.query("SELECT * FROM quizz", (err, res) => {
        if(err) {
            console.log("error", err)
            result(err,null);
            return;
        }


        result(null, res);
    })
}

module.exports = Quizz;