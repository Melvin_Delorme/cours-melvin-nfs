const express = require('express');
const router = express.Router();

const quizzController = require('../controller/quizz.controller');

router.get('/getAll', quizzController.getAll);

router.post('/postAll',quizzController.create);


module.exports = router;