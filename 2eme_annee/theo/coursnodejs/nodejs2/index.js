//Créer fichier js
// npm init
// npm i express
//npm i cors

//Require express
const express = require('express');

//Require cors
const cors = require('cors');

//Instance express
const app = express();

///
//PRECISER CONFIG API
///

//Pas de problème de cors
app.use(cors());

//Dire api utilise json
app.use(express.json());

//Dire url encoder
app.use(express.urlencoded({extended:true}));

//////////
//////////

//Récupère de la route 
const quizzRoute = require('./routes/quizz');

//Ajoute la route à l'api
app.use('/quizz',quizzRoute);


app.listen(3000, ()=>{
    console.log('Server running');
});

