//npm i mysql
const mysql = require('mysql');
// const dbConfig = require('./db.config');

const connection = mysql.createConnection({
    host : 'localhost',
    user: 'root',
    password:'',
    database:'nodebci'
});

connection.connect(error=>{
    if (error) throw error;
    console.log('Je suis connecté');
});

module.exports = connection;

