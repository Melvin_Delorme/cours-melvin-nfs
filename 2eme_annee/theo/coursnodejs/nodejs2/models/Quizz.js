const sql = require('../config/db');

const Quizz = function (quizz) {
    this.question = quizz.question;
    this.flag = quizz.flag;
    this.name = quizz.name;
    this.answer = quizz.answer;
}


Quizz.create = (newQuizz , result) => {
    const { question, flag, name, answer } = newQuizz;

    sql.query(
        "INSERT INTO quizz SET ?",
        { question: question, flag, name, answer: JSON.stringify(answer) },
        (err, res) => {
            if (err) {
                console.log("Erreur lors de la création du quizz", err);
                result(err, null);
                return;
            }
            console.log('Quizz créé', { id: res.insertId, ...newQuizz });
            result(null, { id: res.insertId, ...newQuizz });
        }
    );
}

Quizz.findAll = ( result ) => {
    sql.query("SELECT * FROM quizz", (err, res) => {
        if(err) {
            console.log("error", err)
            result(err,null);
            return;
        }

        const formattedData = res.map((quizz) => {
            return {
                ...quizz,
                answer: JSON.parse(quizz.answer),
            };
        });

        console.log('quizz crée ', formattedData);
        result(null, formattedData);
    })
}

module.exports = Quizz;