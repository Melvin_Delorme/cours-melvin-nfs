const express = require('express');
const router = express.Router();
const passport = require('passport');

const quizzController = require('../controller/quizz.controller');

router.get('/getAll',passport.authenticate('jwt', {session:false}), quizzController.getAll);

router.post('/postAll',quizzController.create);


module.exports = router;