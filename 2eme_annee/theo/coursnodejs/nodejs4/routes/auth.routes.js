const express = require('express');
const router = express.Router();
//npm i jsonwebtoken
const jwt = require('jsonwebtoken');

// npm i bcrypt
const bcrypt = require('bcryptjs');

// utilisateur connecte pour passport
const users = [];


router.post('/register', async (req,res) => {
    try {
        // venir chercher username et password
        const {username,password} = req.body;

        // hasher le password
        const hashedPassword = await bcrypt.hash(password,10);
        const newUser = {username: username, password: hashedPassword};

        users.push(newUser);

        res.status(201).json({message: 'Nickel', user: newUser})

    }catch(e) {
        console.log(e.message)
    }
})

router.post('/login' , async (req,res) => {
    try {
        const {username,password} = req.body;

        const user = users.find((u) => u.username === username);

        if(!user){
            return res.status(400).json({message: 'Invalid credentials'})
        }

        //renvoi true si les mdp correspondent
        const passwordMatching = await bcrypt.compare(password, user.password);

        if(!passwordMatching){
            return res.status(400).json({message: 'Invalid credentials'})
        }

        const token = jwt.sign({username: user.username}, process.env.JWT_SECRET, {expiresIn: "1h"});

        res.json({message: 'Connecte', token})

    }catch (e){
        console.log(e.message)
    }
})

module.exports = {router,users};