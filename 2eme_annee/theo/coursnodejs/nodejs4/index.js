//Créer fichier js
// npm init
// npm i express
//npm i cors

//Require express
const express = require('express');

// npm i dotenv permet l'acces au .env
require('dotenv').config();
//Require cors
const cors = require('cors');

//Instance express
const app = express();

const passport = require('passport');
require('./config/passport-config')(passport);





///
//PRECISER CONFIG API
///

//Pas de problème de cors
app.use(cors());

//Dire api utilise json
app.use(express.json());

//Dire url encoder
app.use(express.urlencoded({extended:true}));

app.use(passport.initialize());

//////////
//////////

//Récupère de la route 
const quizzRoute = require('./routes/quizz');
const authRoute = require('./routes/auth.routes').router;

//Ajoute la route à l'api
app.use('/quizz',quizzRoute);
app.use('/auth',authRoute);


app.listen(3000, ()=>{
    console.log('Server running');
});


/*
 API :

 Questions
    
 Categorie



 */
