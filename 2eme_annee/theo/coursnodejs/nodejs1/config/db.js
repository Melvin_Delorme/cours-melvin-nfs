const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'apinodejs1'
});

connection.connect(error => {
    if (error) throw error;
    console.log('Connecté BDD')
})

module.exports = connection;