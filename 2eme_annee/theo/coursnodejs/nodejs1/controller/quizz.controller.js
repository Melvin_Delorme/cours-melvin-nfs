const Quizz = require("../models/Quizz");
const axios = require('axios');

exports.create = (req,res) => {
    if(!req.body) {
        res.status(400).send({
            message: 'Veuillez renseigner des données'
        })
    }


    // Creation de l'object quizz
    const quizzInsert = new Quizz({
        question: req.body.question,
        r1 : req.body.r1,
        r2 : req.body.r2,
        r3 : req.body.r3,
        r4 : req.body.r4,
        goodrep : req.body.goodrep,
    })

    Quizz.create(quizzInsert, (err,data) => {


        if(err)
            res.status(500).send({
                message:  err.message || "Une erreur est arrive"
            })
        // Donnée renvoyer
        else res.send(data);
    })

}