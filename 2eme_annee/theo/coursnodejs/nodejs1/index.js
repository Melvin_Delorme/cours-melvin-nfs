const express = require('express');
const cors = require('cors');
const app = express();


app.use(cors())
// mon api utilise du json
app.use(express.json());

//mon url sera encodé
app.use(express.urlencoded({extended:true}));

const quizzRoute = require('./routes/quizz');





app.use('/quizz',quizzRoute);
app.listen(3000, () =>{
    console.log('Server running');
})


