const express = require('express');
const router = express.Router();
const quizzController = require('../controller/quizz.controller');



router.get('/getAll', (req,res) => {
    res.json('ok')
});

router.post('/postAll',quizzController.create);


module.exports = router;