const sql = require('../config/db');

const Quizz = function (quizz) {
    this.question = quizz.question;
    this.r1 = quizz.r1;
    this.r2 = quizz.r2;
    this.r3 = quizz.r3;
    this.r4 = quizz.r4;
    this.goodrep = quizz.goodrep;
}


Quizz.create = (newQuizz , result) => {
    sql.query("INSERT INTO quizz SET ?", newQuizz, (err, res) => {
        if(err) {
            console.log("error", err)
            result(err,null);
            return;
        }
        console.log('quizz crée ', {id: res.insertId, ...newQuizz});
        result(null, {id: res.insertId, ...newQuizz});
    })
}

Quizz.findAll = ( result) => {
    sql.query("SELECT * FROM quizz", (err, res) => {
        if(err) {
            console.log("error", err)
            result(err,null);
            return;
        }
        console.log('quizz crée ', res);
        result(null, res);
    })
}

module.exports = Quizz;