import 'package:flutter/material.dart';
import 'model/photo.dart';
import 'package:blog/main.dart';

class DetailPage extends StatelessWidget {
  final Photo photo;
  const DetailPage({Key? key, required this.photo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(photo.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.network(photo.url),
          const SizedBox(height: 20),
          Text(
            photo.title,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          Text('Album ID: ${photo.albumId}'),
          Text('Photo ID: ${photo.id}'),
        ],
      ),
    );
  }
}