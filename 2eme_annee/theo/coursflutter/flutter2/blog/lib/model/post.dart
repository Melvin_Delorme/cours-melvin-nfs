class Post {
  final int? userId;
  final int? id;
  final String? title;
  final String? body;

  const Post({
    this.userId,
    this.id,
    this.title,
    this.body,
  });

  factory Post.fromJson(Map<String, dynamic> json) {

    var userId = json['userId'] != null ?  json['userId'] : 0;
    var id = json['id'] != null ?  json['id'] : 0;
    var body = json['body'] != null ?  json['body'] : "";
    var title = json['title'] != null ?  json['title'] : "";

    return Post(
      body: body,
      userId: userId,
      id: id,
      title: title,
    );
  }

  Map<String, dynamic> toJson() => {
    'userId' : userId,
    'id' : id,
    'body' : body,
    'title' : title,
  };
}