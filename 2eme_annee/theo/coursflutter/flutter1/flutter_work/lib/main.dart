import 'package:flutter/material.dart';
import 'detail.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  Widget _titre = Container(
    color: Colors.red,
    height: 250,
    width: 250,
    child: Text('Titre'),
  );

  Widget _subtitle = Container(
    color: Colors.blue,
    height: 250,
    width: 250,
    child: Text('Sous titre'),
  );

  List _getList() {
    var list = [];
    String toto = '';

    for(var i = 0; i < 25; i++){
      list.add(i);
    }
    return list;
  }

  ListView _List(BuildContext context) {

    List list = _getList();

    return ListView.builder(
      shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: _titre,
            leading: Icon(Icons.list),
            subtitle: _subtitle,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DetailPage(id: "id")),
              );
            },
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text('Exaucé'),
        ),
        body: SingleChildScrollView(child: Column(
            children: [
              _List(context)
            ]
          ),
        )

    );
  }
}