import 'dart:convert';
import 'package:http/http.dart' as http;

class AnimeApi {
  static const String apiUrl = 'https://mocki.io/v1/e3cce8a6-646a-4291-8257-e47a5bc10efd';

  static Future<List<Question>> fetchQuestions() async {
    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      final List<dynamic> questionsData = data['questions'];

      return questionsData.map((json) => Question.fromJson(json)).toList();
    } else {
      throw Exception('Impossible de récupérer les questions depuis l\'API');
    }
  }
}

class Question {
  final String question;
  final List<String> options;
  final String answer;

  Question({
    required this.question,
    required this.options,
    required this.answer,
  });

  factory Question.fromJson(Map<String, dynamic> json) {
    // Mélanger la liste des options de manière aléatoire
    List<String> shuffledOptions = List<String>.from(json['options'])..shuffle();

    return Question(
      question: json['question'],
      options: shuffledOptions,
      answer: json['answer'],
    );
  }
}
