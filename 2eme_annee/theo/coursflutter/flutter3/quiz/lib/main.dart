import 'package:flutter/material.dart';
import 'anime_page.dart'; // Importez le fichier contenant la classe AnimePage

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Quiz Incroyable'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25)),
      ),
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Ink(
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: InkWell(
            onTap: () {
              // Naviguer vers la page Anime lorsque le bouton est cliqué
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AnimePage()),
              );
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Animes', style: TextStyle(fontSize: 20, color: Colors.white)),
                  Icon(Icons.play_arrow, color: Colors.white),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
