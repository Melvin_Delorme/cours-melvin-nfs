import 'package:flutter/material.dart';
import 'model/anime_api.dart';

class AnimePage extends StatefulWidget {
  @override
  _AnimePageState createState() => _AnimePageState();
}

class _AnimePageState extends State<AnimePage> {
  late List<Question> questions;
  int currentQuestionIndex = 0;
  bool isLoading = true;
  int score = 0;

  @override
  void initState() {
    super.initState();
    loadQuestions();
  }

  Future<void> loadQuestions() async {
    try {
      final List<Question> fetchedQuestions = await AnimeApi.fetchQuestions();
      setState(() {
        questions = fetchedQuestions;
        isLoading = false;
      });
    } catch (e) {
      print('Erreur lors du chargement des questions: $e');
      setState(() {
        isLoading = false;
      });
    }
  }

  void answerSelected(int selectedAnswerIndex) {
    final Question currentQuestion = questions[currentQuestionIndex];

    // Vérifie si la réponse sélectionnée est correcte
    bool isCorrect = currentQuestion.options[selectedAnswerIndex] == currentQuestion.answer;

    setState(() {
      // Passe à la question suivante
      if (currentQuestionIndex < questions.length - 1) {
        currentQuestionIndex++;

        // Incrémente le score si la réponse est correcte
        if (isCorrect) {
          score++;
        }
      } else {
        // Vous avez atteint la dernière question
        // Vous pouvez afficher un message de fin ou redémarrer le quiz, selon vos besoins
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Quiz terminé'),
              content: Text('Votre score est de $score points.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    // Réinitialiser le quiz si nécessaire
                    resetQuiz();
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      }
    });
  }

  void resetQuiz() {
    setState(() {
      currentQuestionIndex = 0;
      score = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    if (isLoading) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Anime Page'),
        ),
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    if (questions == null || questions.isEmpty) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Anime Page'),
        ),
        body: Center(
          child: Text('Aucune question disponible.'),
        ),
      );
    }

    final Question currentQuestion = questions[currentQuestionIndex];

    return Scaffold(
      appBar: AppBar(
        title: Text('Anime Page - Score: $score'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Question ${currentQuestionIndex + 1}/${questions.length}',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              currentQuestion.question,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: currentQuestion.options.length,
              itemBuilder: (context, index) {
                final String answerText = currentQuestion.options[index];

                return Container(
                  margin: EdgeInsets.symmetric(vertical: 0, horizontal: screenWidth * 0),
                  child: ElevatedButton(
                    onPressed: () => answerSelected(index),
                    child: Container(
                      padding: EdgeInsets.all(16),
                      child: Text(
                        answerText,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
