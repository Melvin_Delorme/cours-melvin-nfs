<?php

namespace App\Controller;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthController extends AbstractController
{
    private JWTTokenManagerInterface $jwtManager;

    public function __construct(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    #[Route('/login', name: 'app_login', methods: "POST")]
    public function login(UserInterface $user, UserPasswordEncoderInterface $encoder, Request $request): Response
    {
        $email = $request>$request->get('email');
        $password = $request>$request->get('password');

        if (!$encoder->isPasswordValid($user, $password)){
            return $this->json(['message' => 'Identifiants invalides'], 401);
        }

        $token = $this->jwtManager->create($user);
        return $this->json(['token'=> $token, 'username' => $user->getUsername()]);
    }

    #[Route('/register', name: 'app_register', methods: 'POST')]
    public function register(UserPasswordHasherInterface $passwordHasher, Request $request, EntityManagerInterface $em, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $data = $request->getContent();
        //Désérialisez les données JSON dans un Objet PostDTO
        $postDto = $serializer->deserialize($data, UserDTO::class, 'json');
        //Validez l'objet PostDto
        $errors = $validator->validate($postDto);
        if (count($errors) > 0) {
            return $this->json(['errors' => $errors], 400);
        }
        $user = new User();
        $user->setEmail($postDto->email);
        $hashedPassword = $passwordHasher->hashPassword($user, $postDto->password);
        $user->setPassword($hashedPassword);
        $em->persist($user);
        $em->flush();
        return $this->json(['message' => 'Utilisateur ajouté'], 201);
    }
}
