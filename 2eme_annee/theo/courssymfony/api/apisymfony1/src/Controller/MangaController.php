<?php

namespace App\Controller;

use App\Entity\Manga;
use App\MangaDTO;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/manga')]
class MangaController extends AbstractController
{
    #[Route('/', name: 'app_manga')]
    public function index(): JsonResponse
    {
        return $this->json(['get' =>"Use /get for data recuperation", 'post'=>"Use /post for data sending"]);
    }

    #[Route('/post', name: 'app_post', methods: "POST")]
    public function post(EntityManagerInterface $entityManager, Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $data = $request->getContent();

        $postDTO = $serializer->deserialize($data, MangaDTO::class, 'json');

        $errors = $validator->validate($postDTO);

        if (count($errors) > 0) {
            return $this->json(['errors' => $errors], 400);
        }

        $manga = new Manga();
        $manga->setName($postDTO->title);
        $manga->setDescription($postDTO->description);

        $entityManager->persist($manga);
        $entityManager->flush();

        return $this->json(['message' => "Manga crée"], status: 201);
    }

    #[Route('/get', name: 'app_get', methods: "GET")]
    public function get(EntityManagerInterface $entityManager): JsonResponse
    {
        $manga =$entityManager->getRepository(Manga::class)->findAll();
        return $this->json(['manga' => $manga]);
    }

}
