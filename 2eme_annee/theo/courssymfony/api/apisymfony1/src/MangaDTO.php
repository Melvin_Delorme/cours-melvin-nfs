<?php

namespace App;

use Symfony\Component\Validator\Constraints as Assert;

class MangaDTO
{
    #[Assert\NotBlank]
    public  $title;

    #[Assert\NotBlank]
    public $description;
}