<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api')]
class RegistrationController extends AbstractController
{
    private JWTTokenManagerInterface $jwtManager;

    public function __construct(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    #[Route('/register', name: 'app_register')]
    public function register(EntityManagerInterface $em, Request $request, UserPasswordHasherInterface $passwordHasher): JsonResponse
    {
        $decoded = json_decode($request->getContent());
        $email = $decoded->email;
        $plainPassword = $decoded->password;

        $user = new User();
        $hashedPassword = $passwordHasher->hashPassword(
          $user,$plainPassword
        );
        $user->setPassword($hashedPassword);
        $user->setEmail($email);
        $user->setUsername($email);
        $em->persist($user);
        $em->flush();

        return $this->json([
            'message' => 'User Created',
            'success' => true
        ]);
    }

    /*#[Route('/login', name: 'app_login', methods: ['POST'])]
    public function login(UserPasswordHasherInterface $passwordHasher, Request $request, EntityManagerInterface $em): JsonResponse
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        // Recherchez l'utilisateur par son email dans votre base de données
        $user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$user) {
            return $this->json(['message' => 'Utilisateur non trouvé'], 404);
        }
        // Vérifiez le mot de passe seulement si l'utilisateur est trouvé
        if ($passwordHasher->isPasswordValid($user, $password)) {
            // Générez un jeton JWT pour l'utilisateur
            $token = $this->jwtManager->create($user);
            return $this->json(['token' => $token, 'email' => $user->getEmail(), 'message' =>'Vous êtes connecté']);
        } else {
            return $this->json(['message' => 'Identifiants invalides'], 401);
        }
    }*/
}
