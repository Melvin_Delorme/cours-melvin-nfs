<?php

namespace App\Controller;

use App\Entity\Campagne;
use App\Entity\Personnage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/campagne', name: 'api')]
class CampagneController extends AbstractController
{
    #[Route('/add', name: 'campagne_add')]
    public function add(EntityManagerInterface $em, Request $request): JsonResponse
    {
        $decoded = json_decode($request->getContent());
        $name = $decoded->name;
        $duree = $decoded->duree;
        $difficulte = $decoded->difficulte;

        $campagne = new Campagne();
        $campagne->setName($name);
        $campagne->setDuree($duree);
        $campagne->setDifficulte($difficulte);

        $em->persist($campagne);
        $em->flush();

        return $this->json([
            'message' => 'Campagne Ajouté',
            'sucess' => true
        ]);
    }

    #[Route('/', name: 'campagne')]
    public function index(EntityManagerInterface $entityManager): JsonResponse {
        $campagnes = $entityManager->getRepository(Campagne::class)->findAll();

        $formattedCampagne = [];

        foreach ($campagnes as $campagne) {
            $formattedCampagne[] = [
                'name' => $campagne->getName(),
                'duree' => $campagne->getDuree(),
                'difficulte' => $campagne->getDifficulte(),
            ];
        }

        return $this->json(['campagne' => $formattedCampagne]);
    }
}
