<?php

namespace App\Controller;

use App\Entity\Personnage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function Webmozart\Assert\Tests\StaticAnalysis\integer;

#[Route('/api/personnage', name: 'api')]
class PersonnageController extends AbstractController
{
    #[Route('/add', name: 'personnage_add')]
    public function add(EntityManagerInterface $em, Request $request): JsonResponse
    {
        $decoded = json_decode($request->getContent());
        $name = $decoded->name;
        $class = $decoded->class;
        $stat = $decoded->stat;
        $user = $decoded->user;

        $personnage = new Personnage();
        $personnage->setName($name);
        $personnage->setClass($class);
        if (is_int($stat)) {
            $personnage->setStat($stat);
        } else {
            return $this->json(['message' => 'La stat ne peut etre que un nombre'], 401);
        }
        $personnage->setUser($user);

        $em->persist($personnage);
        $em->flush();

        return $this->json([
            'message' => 'Personnage Ajouté',
            'sucess' => true
        ]);
    }

    #[Route('/', name: 'personnage')]
    public function index(EntityManagerInterface $entityManager): JsonResponse {
        $personnages = $entityManager->getRepository(Personnage::class)->findAll();

        $formattedPersonnages = [];

        foreach ($personnages as $personnage) {
            $formattedPersonnages[] = [
                'name' => $personnage->getName(),
                'class' => $personnage->getClass(),
                'stat' => $personnage->getStat(),
            ];
        }

        return $this->json(['personnages' => $formattedPersonnages]);
    }
}
