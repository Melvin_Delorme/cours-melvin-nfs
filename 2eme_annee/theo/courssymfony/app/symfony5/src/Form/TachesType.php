<?php

namespace App\Form;

use App\Entity\Taches;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use function Sodium\add;

class TachesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('content')
            ->add('status', ChoiceType::class, [ // Ajout du champ status de type ChoiceType
                'choices' => [ // Les valeurs possibles pour le champ ENUM
                    'En cours' => 'en cours',
                    'Effectué' => 'effectue',
                    'Archivé' => 'archive',
                ],
                'label' => 'Statut', // Libellé du champ (optionnel, personnalisable)
            ])
            ->add('expirated_at')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Taches::class,
        ]);
    }
}
