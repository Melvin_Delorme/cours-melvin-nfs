<?php

namespace App\Controller;

use App\Entity\Security;
use App\Form\SecurityType;
use App\Repository\SecurityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/security')]
class SecurityController extends AbstractController
{
    #[Route('/', name: 'app_security_index', methods: ['GET'])]
    public function index(SecurityRepository $securityRepository): Response
    {
        return $this->render('security/index.html.twig', [
            'securities' => $securityRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_security_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordEncoder): Response
    {
        $security = new Security();
        $form = $this->createForm(SecurityType::class, $security);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $hashedPassword = $passwordEncoder->hashPassword($security, $security->getPassword());

            $security->setPassword($hashedPassword);

            $entityManager->persist($security);
            $entityManager->flush();

            return $this->redirectToRoute('app_security_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('security/new.html.twig', [
            'security' => $security,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_security_show', methods: ['GET'])]
    public function show(Security $security): Response
    {
        return $this->render('security/show.html.twig', [
            'security' => $security,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_security_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Security $security, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(SecurityType::class, $security);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_security_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('security/edit.html.twig', [
            'security' => $security,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_security_delete', methods: ['POST'])]
    public function delete(Request $request, Security $security, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$security->getId(), $request->request->get('_token'))) {
            $entityManager->remove($security);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_security_index', [], Response::HTTP_SEE_OTHER);
    }
}
