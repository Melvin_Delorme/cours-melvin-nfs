# BRIEF INNOWATION 2022.

> Début de l’examen 9h, fin 12h30 soit 3h30


>Je suis une agence de com qui veux réaliser un petit site avec Wordpress, je vous fournis les designs des deux pages, l’intégration, la font à utiliser ainsi que toutes les images du site.


>L’objectif est de créer un thème Wordpress sur-mesure qui devra se nommer comme votre nom (sans majuscule, ni espace, etc.). Mais ne contenir que 10 caractères maximum. Par exemple: le thème de Michel Bertolucci s’appellera «bertolucci»

```
Vous devez me créer, en plus du votre, un compte administrateur avec exactement les identifiants suivant. 
(faites le juste après l’installation pour ne pas oublier)
identifiant: weblitzer
E-mail: quidelantoine@gmail.com
mot de passe: weblitzer
```

> La page d’accueil(innow-page-home.jpg) liste la totalité des partenaires par ordre alphabétique. Lorsque vous cliquer sur un partenaire cela vous emmène vers le détail de ce partenaire.( innow-page-partener.jpg)

>La totalité du site doit être administrable en Back-office. Sauf le logo du header.

>L’utilisation des articles est interdite, car dans une version future, un blog sera intégré.

>Il vous faudra donc créer une administration pour les partenaires (custom post).

>Sur la page partenaire, au click sur la grande image, vous redirigez l’utilisateur vers une vidéo Youtube (administrable) et dans un nouvel onglet.

```
Vous devez terminer l’intégration des deux pages 
pour quelles soit adaptables sur tous les supports(Desktop, tablette, mobile). 
Nous ne fournissons pas de design pour les mobiles. 
L’important, c’est que tout reste lisible et ergonomique quelque soit le format.
```

>Vous devrez me transmettre via Wetransfert la totalité de votre travail. Je veux tous les fichiers et dossiers qui font un wordpress (tous les fichiers à la racine ainsi que le contenu de vos trois dossiers wp-admin, wp-content,wp-includes). Vous devrez aussi me fournir un export de votre BDD contenant toutes vos tables. Ceci afin de les transmettre au client final pour installation sur serveur.

##### Bonus 
 ```
 A faire si tout le reste est terminé !
Rendre la vidéo visible dans une modale au click sur la grande image dans la page détail d'un partenaire.
```