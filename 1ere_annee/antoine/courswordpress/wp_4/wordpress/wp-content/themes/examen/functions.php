
<?php


require get_template_directory() . '/inc/generale.php';
require get_template_directory() . '/inc/func.php';
require get_template_directory() . '/inc/img.php';
require get_template_directory() . '/inc/parameters.php';


require get_template_directory() . '/inc/extra/template-tags.php';
require get_template_directory() . '/inc/extra/template-functions.php';

// CUSTOM

require get_template_directory() . '/inc/custom/custom-partner.php';

