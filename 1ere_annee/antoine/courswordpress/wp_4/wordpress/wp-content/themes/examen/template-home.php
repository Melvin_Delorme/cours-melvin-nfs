<?php
/* Template Name: Homepage */
get_header();
global $web;
$metas = get_post_meta($web['page']['homepage']['id']);
//debug($metas);

$args = array(
    'post_type'   => 'partner',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$the_query = new WP_Query( $args );
?>

<section id="intro">
    <div class="wrap">
        <div class="box">
            <?php echo getImageById($metas['image_home'][0], 'introlisting', get_the_title(), 'introlisting'); ?>
        </div>
        <div class="box box_text">
            <p><?php echo $metas['texte_home'][0] ?></p>
        </div>
    </div>
</section>

<section id="partners">
    <div class="wrap">
        <!-- Il en faut 8 -->
        <?php if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) { $the_query->the_post();
        $metas_post = get_post_meta(get_the_ID()); ?>
        <div class="one_partner">

            <div class="one_partner__img shadow">
                <a href="<?php echo get_the_permalink() ?>">
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                </a>
            </div>
            <div class="one_partner__text">
                <h2><?php echo get_the_title() ?></h2>
                <h3><?php echo $metas_post['title_2'][0] ?></h3>
            </div>
        </div>
        <?php }
        } wp_reset_postdata(); ?>
    </div>
</section>

</body>
</html>

