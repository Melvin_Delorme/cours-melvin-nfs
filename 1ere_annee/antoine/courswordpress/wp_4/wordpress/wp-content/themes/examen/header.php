<?php
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INNOWATION</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;700&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>
<body  <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header>
    <div class="logo">
        <a href="<?= path('/'); ?>"><img src="<?= asset('img/logo_innow.png'); ?>" alt="Logo Innowation"></a>
    </div>
</header>
