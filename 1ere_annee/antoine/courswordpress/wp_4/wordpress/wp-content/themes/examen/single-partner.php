<?php
get_header();
?>

<div id="barorange"></div>



    <section id="jungle">
        <?php
        while ( have_posts() ) :
        the_post();
        $metas_post = get_post_meta(get_the_ID()); ?>
        <div class="wrap">
            <div class="jungle_box jungle_box_left">
                <?php echo getImageById($metas_post['img_1'][0], 'wttj', get_the_title(), 'wttj'); ?>
                <?php echo getImageById($metas_post['img_2'][0], 'ellipse', get_the_title(), 'ellipse'); ?>
                <h1><?php echo $metas_post['title_3'][0]  ?></h1>
                <p><?php echo $metas_post['title_4'][0]  ?></p>
                <p><?php echo $metas_post['title_5'][0]  ?></p>
            </div>
            <div class="jungle_box jungle_box_right">
                <h2><?php echo $metas_post['title_2'][0]  ?></h2>
                <p><?php echo $metas_post['desc_1'][0]  ?></p>
            </div>
        </div>
        <?php endwhile; // End of the loop.
        ?>
    </section>

    <section id="video">
        <?php
        while ( have_posts() ) :
            the_post();
            $metas_post = get_post_meta(get_the_ID()); ?>
        <div class="wrap2">
            <div class="relative shadow">
                <a href="#">
                    <?php echo getImageById($metas_post['img_3'][0], 'imgvideo', get_the_title(), 'imgvideo'); ?>
                    <div class="lecteur"></div>
                </a>
            </div>
        </div>
        <?php endwhile; // End of the loop.
        ?>
    </section>

</body>
</html>
