<?php

/**
 * Register a custom post type called "partner".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_partner_init() {
    $labels = array(
        'name'                  => _x( 'partners', 'Post type general name', 'innowation' ),
        'singular_name'         => _x( 'partner', 'Post type singular name', 'innowation' ),
        'menu_name'             => _x( 'PARTNER', 'Admin Menu text', 'innowation' ),
        'name_admin_bar'        => _x( 'partner', 'Add New on Toolbar', 'innowation' ),
        'add_new'               => __( 'Ajouter un partner', 'innowation' ),
        'add_new_item'          => __( 'Add New partner', 'innowation' ),
        'new_item'              => __( 'New partner', 'innowation' ),
        'edit_item'             => __( 'Edit partner', 'innowation' ),
        'view_item'             => __( 'View partner', 'innowation' ),
        'all_items'             => __( 'All partners', 'innowation' ),
        'search_items'          => __( 'Search partners', 'innowation' ),
        'parent_item_colon'     => __( 'Parent partners:', 'innowation' ),
        'not_found'             => __( 'No partners found.', 'innowation' ),
        'not_found_in_trash'    => __( 'No partners found in Trash.', 'innowation' ),
        'featured_image'        => _x( 'partner Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'innowation' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'innowation' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'innowation' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'innowation' ),
        'archives'              => _x( 'partner archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'innowation' ),
        'insert_into_item'      => _x( 'Insert into partner', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'innowation' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this partner', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'innowation' ),
        'filter_items_list'     => _x( 'Filter partners list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'innowation' ),
        'items_list_navigation' => _x( 'partners list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'innowation' ),
        'items_list'            => _x( 'partners list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'innowation' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'partner' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 45,
        'menu_icon'          => 'dashicons-hammer',
        'supports'           => array( 'title','thumbnail','excerpt' ),
    );

    register_post_type( 'partner', $args );
}

add_action( 'init', 'wpdocs_codex_partner_init' );




