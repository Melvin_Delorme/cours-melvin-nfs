<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'innowation' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'GHvB%#FhPs%[9!apSUf/S-a-!t0yw1t{>G< y3$)^O3!B:tdbs=Q-FwO|:mEw_IV' );
define( 'SECURE_AUTH_KEY',  'SHlRQr0{BdZB&+%GC@(fam9G4^Qd6L/h+!A:+}Q<DM7Zka;HMg;3l*vyvKTv|r1^' );
define( 'LOGGED_IN_KEY',    ')(9hFVe8vMnO@ GF)!bO%Z| /4[O/0b1ze_5`<k-_QX,e`2ZXv~T|!VUGzjKqr/S' );
define( 'NONCE_KEY',        ';huU(XrR|hoy0T<]EMto1| |b=ec8U3J[NOQX|U;Cu)z28Wa,A%fulup^ty.}q D' );
define( 'AUTH_SALT',        'lTAc0(p495>@|;vy6,,T-vC)pTc9]K:;CTP^O&*2N|40T=jD}R-|&P59F/Wd@#}{' );
define( 'SECURE_AUTH_SALT', '[W&i(a*6.KNyD=k)?0+~j/thLI~a7E40n8uo$ht[c=~i8sb:u-oE}g-qHe`Nc9.f' );
define( 'LOGGED_IN_SALT',   'a(!Ehd19Nyt&X{xhx=085q;KCV-g=Md}J`_6{U6#jTz=dWe-/_{cnu9{;$na_&tx' );
define( 'NONCE_SALT',       'RWQ3]ka7Axm ../no|^yc0Ol&rtTIIT|*c1RFMJ eM~|,6B,p2k0%M:~/])+dy+V' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'innow_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
