<?php
global $metaHome;
$metaHome = get_post_meta(5);
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@400;700&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="header" class="wrap">
    <div class="logo">
        <a href="<?= path('/'); ?>"><img src="<?= asset('img/page-logo.png'); ?>" alt=""></a>
    </div>
    <nav>
        <ul>
            <li class="active homelink"><a href="<?= path('/'); ?>" title="">Home</a></li>
            <li><a href="<?= path('features'); ?>" title="">Features</a></li>
            <li class="homelink"><a href="<?= path('pages'); ?>" title="">Pages</a></li>
            <li><a href="<?= path('gallery'); ?>" title="">Gallery</a></li>
            <li><a href="<?= path('blog'); ?>" title="">Blog</a></li>
            <li><a href="<?= path('contact'); ?>" title="">Contact</a></li>
<!--            --><?php //if (is_user_logged_in()) { ?>
<!--                <li><a href="" title="">Déconnection</a></li>-->
<!--            --><?php //} else { ?>
                <li><a href="<?= path('login'); ?>" title="">Login</a></li>
<!--            --><?php //} ?>
        </ul>
    </nav>
</header>
