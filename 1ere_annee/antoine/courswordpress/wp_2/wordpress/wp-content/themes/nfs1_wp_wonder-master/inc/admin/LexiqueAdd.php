<?php

if(!defined('ABSPATH')) {
    die();
}

class LexiqueAdd
{
    private $slugPage;
    public function __construct($slug)
    {
        $this->slugPage = $slug;
    }

    public function view() {
        global $wpdb;
        $errors = array();

        if(!empty($_POST['submitted'])) {
            // Faille xss
            $title = trim(strip_tags($_POST['title']));
            $content = trim(strip_tags($_POST['content']));
            // Validation
            $errors = validationText($errors, $title, 'title', 2, 100);
            $errors = validationText($errors, $content, 'content', 2, 100);
            if(count($errors) === 0 ) {
                $wpdb->insert(
                    $wpdb->prefix . 'lexique',
                    array(
                        'title'      => $title,
                        'content'    => $content,
                        'created_at' => current_time('mysql'),
                    ),
                    array('%s', '%s', '%s')
                );
                $adminUrl = admin_url('admin.php?page=' . $this->slugPage);
                echo '<script>window.location.href="'.$adminUrl.'"</script>';
            }
        }

        echo '<h1 class="wp-heading-inline">Ajouter lexique</h1>';
        echo '<hr class="wp-header-end">';

        echo '<form action="" method="post">';
        echo '<table class="form-table">';
        echo '<tbody>';
        echo '<tr>';
            echo '<th><label for="nom">Title *</label></th>';
                   echo '<td>';
                        echo '<input type="text" name="title" value="'.getPostValue('title').'" class="regular-text">';
                       echo ' <p class="error">'.viewError($errors,'title').'</p>';
                    echo '</td>';
            echo '</tr>';

        echo '<tr><th><label for="nom">Content *</label></th>';
        echo '<td>';
        echo '<textarea name="content" class="regular-text">'.getPostValue('content').'</textarea>';
        echo ' <p class="error">'.viewError($errors,'content').'</p>';
        echo '</td></tr>';

        echo '<tr><th></th><td>';
        echo '<input type="submit" name="submitted" value="Ajouter au lexique" />';
        echo '</td></tr>';
        echo '</tbody>';
        echo '</table>';
        echo '</form>';
    }

}