<?php
/* Template Name: Homepage */
global $metaHome;
get_header();

//debug($metaHome);

$args = array(
    'post_type'   => 'diapo',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$the_query = new WP_Query( $args );
?>

    <section id="page-slider" class="wrap">
        <div class="slider">
            <div class="flexslider" id="diaporama1">
                <ul class="slides">
                    <?php if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) { $the_query->the_post();
                            $img_url = get_the_post_thumbnail_url(get_the_ID(), 'img_diapo');
                            ?>
                            <li>
                                <img src="<?= $img_url; ?>" alt="<?php echo get_the_title(); ?>">
                            </li>
                        <?php }
                    } wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
        <div class="slider_text">
            <p class="lead"><?php echo web_r($metaHome, 'text_intro_1'); ?></p>
            <p><?php echo web_r($metaHome, 'text_intro_2'); ?></p>
        </div>
    </section>

    <?php
    $args = array(
        'post_type'   => 'work',
        'post_status' => 'publish',
        'posts_per_page' => 4
    );
    $the_query = new WP_Query( $args );
    ?>
    <section id="work">
        <div class="wrap">
            <h2 class="title"><?php echo web_r($metaHome, 'title_home_1'); ?></h2>
            <ul class="work-items">
                <?php if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
                        <li class="work-item">
                            <a href="<?php echo get_the_permalink(); ?>" title="">
                                <?php //the_post_thumbnail('img_work');
                                $img_url = get_the_post_thumbnail_url(get_the_ID(), 'img_work');
                                //debug($img_url);
                                ?>
                                <img src="<?= $img_url ?>" alt="">
                            </a>
                            <div class="work-item-content">
                                <h3><a href="<?php echo get_the_permalink(); ?>" title=""><?php echo get_the_title(); ?></a></h3>
                                <p><?php echo get_the_excerpt(); ?></p>
                            </div>
                        </li>
                    <?php }
                } wp_reset_postdata(); ?>
            </ul>
        </div>
    </section>


<!-- img_post-->
    <div id="halves-block">
        <div class="wrap">
            <section class="blog half-block">
                <h2 class="title"><?php echo web_r($metaHome, 'title_home_2'); ?></h2>

                <?php $args = array(
                    'post_type'   => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => 4
                );
                $the_query = new WP_Query( $args );

                ?>
                <div class="blog-flexslider flexslider" id="diapo_post">
                    <ul class="slides">
                        <?php if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                $img_url = get_the_post_thumbnail_url(get_the_ID(), 'img_post');
                                ?>
                                <li class="blog-slide">
                                    <div class="blog-slide-img">
                                        <img src="<?= $img_url; ?>" alt="<?php echo get_the_title(); ?>">
                                    </div>
                                    <h3 class="blog-slide-title"><?php echo mb_strtoupper(get_the_title()); ?></h3>
                                    <p class="blog-slide-info">
                                        Posted on: <?php echo get_the_date('F, j Y'); ?><br>
                                        Posted by: <?php echo get_the_author(); ?>
                                    </p>
                                    <div class="clear"></div>
                                    <p class="blog-slide-text"><?php echo get_the_excerpt(); ?><a href="<?php echo get_the_permalink(); ?>" title="">Read more</a></p>
                                </li>
                            <?php }
                        } wp_reset_postdata(); ?>

                    </ul>
                </div>
            </section>
            <section class="videos half-block">
                <h2 class="title"><?php echo web_r($metaHome, 'title_home_3'); ?></h2>
                <div class="video-item">
                    <iframe src="http://player.vimeo.com/video/877053?color=ef4f1d" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                </div>
            </section>
        </div>
    </div>


<?php get_footer();
