<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wonder
 */

get_header(); ?>
	<main id="primary" class="wrap site-main">
		<?php
		while ( have_posts() ) :
			the_post(); ?>
            <h2 class="title"><span class="title-focus"><?php echo get_the_title(); ?></span></h2>
            <p><?php echo nl2br(get_the_content()); ?></p>
            <?php

            $meta = get_post_meta(get_the_ID());
            //debug($meta); ?>
        <?php if(!empty($meta['annee'][0])) { ?>
            <p>Année: <?php echo $meta['annee'][0]; ?></p>
        <?php } ?>

        <?php if(!empty($meta['auteur'][0])) { ?>
            <p>Auteur: <?php echo $meta['auteur'][0]; ?></p>
        <?php } ?>
		<?php endwhile; // End of the loop.
		?>
	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
