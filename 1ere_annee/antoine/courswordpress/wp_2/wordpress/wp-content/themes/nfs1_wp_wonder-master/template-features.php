<?php
/* Template Name: Features */
$args = array(
    'post_type'   => 'book',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$the_query = new WP_Query( $args );


$args2 = array(
    'taxonomy'      => 'genre',
    'orderby'       => 'name',
    'order'         => 'ASC',
    'hide_empty'    => false,
);
$the_query_term = new WP_Term_Query($args2);
global $wpdb;
$lexiques = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}lexique ORDER BY title ASC");
//debug($lexiques);

get_header();

?>
<div class="wrap">
    <h2>Les genres</h2>
    <ul>
        <?php foreach($the_query_term->get_terms() as $term){
            //debug($term);

            ?>
        <li>
            <a href="<?php echo path('/books/genre/' . $term->slug); ?>">
                <?php echo $term->name." (".$term->count.")"; ?>
            </a>
        </li>
        <?php } ?>
    </ul>
    <h1>Books</h1>
    <?php if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
                <h2><?php echo get_the_title(); ?></h2>
                <p><a href="<?php echo get_the_permalink(); ?>">Voir plus</a></p>
        <?php }
    } wp_reset_postdata(); ?>

    <h3>Lexique</h3>
    <ul>
        <?php foreach($lexiques as $lexique) {
            echo '<li>'.$lexique->title.'</li>';
        } ?>
    </ul>
    <h3>Lexique AJAX</h3>
    <div id="app_lexique"></div>
</div>
<?php get_footer();
