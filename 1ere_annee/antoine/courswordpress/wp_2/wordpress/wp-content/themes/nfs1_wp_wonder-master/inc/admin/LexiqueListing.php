<?php
if(!defined('ABSPATH')) {
    die();
}

class LexiqueListing
{
    private $slugPage;
    public function __construct($slug)
    {
        $this->slugPage = $slug;
    }

    public function view() {
        global $wpdb;

        // si form soumis
        if(!empty($_POST['delete_lexique'])) {
            $idlexique = trim(strip_tags($_POST['id_lexique']));
            $lexiqueToDelete = $wpdb->get_row(
                    $wpdb->prepare( "SELECT id FROM {$wpdb->prefix}lexique WHERE id = %d", $idlexique)
            );
            if(!empty($lexiqueToDelete)) {
                //debug($lexiqueToDelete);
                $wpdb->delete($wpdb->prefix . 'lexique', array( 'id' => $idlexique ), array( '%d' ) );
                $adminUrl = admin_url('admin.php?page=' . $this->slugPage);
                echo '<script>window.location.href="'.$adminUrl.'"</script>';

            }
        }

        $lexiques = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}lexique ORDER BY title ASC");
        echo '<h1 class="wp-heading-inline">Lexique</h1>';
        echo '<hr class="wp-header-end">';

        echo '<table class="wp-list-table widefat fixed striped table-view-list pages">';
        echo '<thead><tr>';
            echo '<th>id</th>';
            echo '<th>title</th>';
            echo '<th>Content</th>';
            echo '<th>Date</th>';
            echo '<th>Action</th>';
        echo '</tr></thead>';

        echo '<tbody>';
        foreach($lexiques as $lexique) {
            echo '<tr>';
                echo '<td>'.$lexique->id.'</td>';
                echo '<td>'.$lexique->title.'</td>';
                echo '<td>'.$lexique->content.'</td>';
                echo '<td>'. date('d/m/Y',strtotime($lexique->created_at))  .'</td>';
                echo '<td>';
                   echo '<form method="post" action="" onsubmit="return confirm(\'Voulez vous effacer ce lexique\')">';
                   echo '<input type="hidden" name="id_lexique" value="'.$lexique->id.'"/>';
                   echo '<input type="submit" name="delete_lexique" value="Effacer">';
                   echo '</form>';
                echo '</td>';

                echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
    }


}