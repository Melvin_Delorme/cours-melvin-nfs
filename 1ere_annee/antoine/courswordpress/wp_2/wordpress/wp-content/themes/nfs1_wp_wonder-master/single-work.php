<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wonder
 */

get_header();
?>

	<main id="primary" class="wrap site-main">
		<?php



		while ( have_posts() ) :
			the_post();

            $meta = get_post_meta(get_the_ID());
            debug($meta);

//            if(!empty($meta['texte_sup'][0])) {
//                echo '<p>'.$meta['texte_sup'][0].'</p>';
//            }
            echo '<p>'. web_r($meta,'texte_sup') . '</p>';
//            if(!empty($meta['message'][0])) {
//                echo '<p>'.nl2br($meta['message'][0]).'</p>';
//            }
            echo '<p>'. nl2br(web_r($meta,'message')) . '</p>';

//            if(!empty($meta['image1'][0])) {
//                echo $meta['image1'][0];
//                $img = wp_get_attachment_image_src($meta['image1'][0],'img_work');
//                debug($img);
//                echo '<img src="'.$img[0].'" alt="" />';
//            }
            // img_work

            if(!empty($meta['image1'][0])) {
                echo getImageById($meta['image1'][0], 'img_work', get_the_title(), 'img_desk img_round');
            }


            get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation(
				array(
					'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'wonder' ) . '</span> <span class="nav-title">%title</span>',
					'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'wonder' ) . '</span> <span class="nav-title">%title</span>',
				)
			);


		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
