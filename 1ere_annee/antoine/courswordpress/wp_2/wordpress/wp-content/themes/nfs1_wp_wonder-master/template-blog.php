<?php
/* Template Name: Blog */
$args = array(
    'post_type'   => 'post',
//    'orderby'     => 'date',
//    'order'       => 'DESC',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$the_query = new WP_Query( $args );
get_header(); ?>
<div class="wrap">
    <h1>Blog</h1>
    <section id="articles">
        <?php if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
                <div class="one_article">
                    <?php $img_url = get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'); ?>
                    <a href="<?php echo get_the_permalink(); ?>">
                        <img class="" src="<?php echo $img_url; ?>" alt="<?php echo get_the_title(); ?>">
                    </a>
                    <h2><?php echo get_the_title(); ?></h2>
                    <p><?php echo get_the_excerpt(); ?></p>
                    <p><a href="<?php echo get_the_permalink(); ?>">Voir plus</a></p>
                </div>
            <?php }
        } wp_reset_postdata(); ?>
    </section>
</div>
<?php get_footer();