const form = document.querySelector('#form_contact');
const inputEmail = document.querySelector('#mail');
const inputMessage = document.querySelector('#message');
const submitButton = form.querySelector('input[type=submit]');
const errorMail = form.querySelector('#error_mail');
const errorMessage = form.querySelector('#error_message');
const successContact = document.querySelector('#contact_success');

form.addEventListener('submit',function(e) {
    e.preventDefault();
    submitButton.disabled = true;
    errorMail.innerHTML = ''
    errorMessage.innerHTML = ''
    let params = new FormData();
    params.append('action', 'submit_contact_form');
    params.append('mail', inputEmail.value);
    params.append('message', inputMessage.value);
    fetch(MYSCRIPT.ajaxUrl, {
        method: 'post',
        body: params
    }).then(function(response) {
        return response.json()
    }).then(function(data) {
        submitButton.disabled = false;
        if(data.success) {
            form.remove();
            successContact.innerHTML = 'Bravo'
        } else {
            if(data.errors.mail != null) {
                errorMail.innerHTML = data.errors.mail
            }
            if(data.errors.message != null) {
                errorMessage.innerHTML = data.errors.message
            }
        }
    });
});
