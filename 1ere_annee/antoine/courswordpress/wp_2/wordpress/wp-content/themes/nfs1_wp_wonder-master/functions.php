<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
// global
require get_template_directory() . '/inc/generale.php';
require get_template_directory() . '/inc/func.php';

require get_template_directory() . '/inc/image.php';
// admin lexique
require get_template_directory() . '/inc/admin/boot-admin-lexique.php';

// custom
require get_template_directory() . '/inc/custom/custom-book.php';
require get_template_directory() . '/inc/custom/custom-work.php';
require get_template_directory() . '/inc/custom/custom-diapohome.php';

// ajax
require get_template_directory() . '/inc/ajax/ajax-lexique.php';
require get_template_directory() . '/inc/ajax/ajax-contact.php';
require get_template_directory() . '/inc/ajax/ajax-login.php';


// extra
require get_template_directory() . '/inc/extra/template-tags.php';
require get_template_directory() . '/inc/extra/template-functions.php';
