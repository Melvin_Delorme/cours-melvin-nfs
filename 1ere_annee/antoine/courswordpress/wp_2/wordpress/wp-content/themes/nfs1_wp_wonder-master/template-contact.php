<?php
/* Template Name: Contact */

get_header(); ?>

    <div class="wrap">
        <h1>Contact</h1>

        <form id="form_contact" action="" method="post" novalidate class="wrap_form">
            <label for="mail">E-mail *</label>
            <input type="email" name="mail" id="mail">
            <span class="error" id="error_mail"></span>

            <label for="message">Message</label>
            <textarea name="message" id="message"></textarea>
            <span class="error" id="error_message"></span>

            <input type="submit" value="Envoyer">
        </form>
        <div id="contact_success"></div>
    </div>

<?php get_footer();
