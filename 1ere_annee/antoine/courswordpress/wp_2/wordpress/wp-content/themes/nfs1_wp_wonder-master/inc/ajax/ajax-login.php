<?php

add_action('wp_ajax_submit_login_form', 'submitLoginAjax');
add_action('wp_ajax_nopriv_submit_login_form', 'submitLoginAjax');


function submitLoginAjax() {
    $errors = array();
    $success = false;
    // Faille XSS
    $mail = trim(strip_tags($_POST['mail']));
    $password = trim(strip_tags($_POST['password']));
    // validation
    $errors = validationText($errors, $mail, 'mail', 2, 50);
    $errors = validationText($errors, $password, 'password', 2, 50);
    if(count($errors) === 0) {
        $success = true;

    }
    showJson(array(
        'errors' => $errors,
        'success' => $success
    ));
}