<?php
/* Template Name: Pages */

// Toutes les pages, publish, par ordre alphabétique
$args = array(
    'post_type'   => 'page',
    'orderby'     => 'title',
    'order'       => 'ASC',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$the_query = new WP_Query( $args );
// debug($the_query);
get_header(); ?>
    <div class="wrap">
        <h1>Pages</h1>
        <?php
        if ( $the_query->have_posts() ) {
            echo '<ul>';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                echo '<li><a href="'.get_the_permalink().'">' . get_the_title() . '</a></li>';
            }
            echo '</ul>';
        }
        wp_reset_postdata();
        ?>
    </div>
<?php get_footer();
