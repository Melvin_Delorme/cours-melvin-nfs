<?php

add_action('wp_ajax_get_lexique_data', 'getlexiqueAjax');
add_action('wp_ajax_nopriv_get_lexique_data', 'getlexiqueAjax');


function getlexiqueAjax() {
    global $wpdb;
    $lexiques = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}lexique ORDER BY title ASC",ARRAY_A);
    showJson($lexiques);
}


