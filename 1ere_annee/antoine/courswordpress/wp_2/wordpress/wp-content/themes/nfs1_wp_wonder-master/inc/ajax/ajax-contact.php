<?php

add_action('wp_ajax_submit_contact_form', 'submitContactAjax');
add_action('wp_ajax_nopriv_submit_contact_form', 'submitContactAjax');

function submitContactAjax() {
    $errors = array();
    $success = false;
    // Faille XSS
    $mail = trim(strip_tags($_POST['mail']));
    $message = trim(strip_tags($_POST['message']));
    // validation
    $errors = validationText($errors, $mail, 'mail', 2, 50);
    $errors = validationText($errors, $message, 'message', 2, 50);
    if(count($errors) === 0) {
        $success = true;
    }
    showJson(array(
        'errors' => $errors,
        'success' => $success
    ));
}

