<?php

/**
 * Register a custom post type called "book".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_book_init() {
    $labels = array(
        'name'                  => _x( 'Books', 'Post type general name', 'wonder' ),
        'singular_name'         => _x( 'Book', 'Post type singular name', 'wonder' ),
        'menu_name'             => _x( 'Les livres', 'Admin Menu text', 'wonder' ),
        'name_admin_bar'        => _x( 'Book', 'Add New on Toolbar', 'wonder' ),
        'add_new'               => __( 'Ajouter un livre', 'wonder' ),
        'add_new_item'          => __( 'Add New Book', 'wonder' ),
        'new_item'              => __( 'New Book', 'wonder' ),
        'edit_item'             => __( 'Edit Book', 'wonder' ),
        'view_item'             => __( 'View Book', 'wonder' ),
        'all_items'             => __( 'All Books', 'wonder' ),
        'search_items'          => __( 'Search Books', 'wonder' ),
        'parent_item_colon'     => __( 'Parent Books:', 'wonder' ),
        'not_found'             => __( 'No books found.', 'wonder' ),
        'not_found_in_trash'    => __( 'No books found in Trash.', 'wonder' ),
        'featured_image'        => _x( 'Book Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'wonder' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'wonder' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'wonder' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'wonder' ),
        'archives'              => _x( 'Book archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'wonder' ),
        'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'wonder' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'wonder' ),
        'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'wonder' ),
        'items_list_navigation' => _x( 'Books list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'wonder' ),
        'items_list'            => _x( 'Books list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'wonder' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'book' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 98,
        'menu_icon'          => 'dashicons-book-alt',
        'supports'           => array( 'title', 'editor','thumbnail','excerpt','custom-fields' ),
    );

    register_post_type( 'book', $args );
}

add_action( 'init', 'wpdocs_codex_book_init' );



/**
 * Register a 'genre' taxonomy for post type 'book', with a rewrite to match book CPT slug.
 *
 * @see register_post_type for registering post types.
 */
function wpdocs_create_book_tax_rewrite() {

    $labels = array(
        'name'              => _x( 'Genres', 'taxonomy general name', 'wonder' ),
        'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'wonder' ),
        'search_items'      => __( 'Search Genres', 'wonder' ),
        'all_items'         => __( 'All Genres', 'wonder' ),
        'parent_item'       => __( 'Parent Genre', 'wonder' ),
        'parent_item_colon' => __( 'Parent Genre:', 'wonder' ),
        'edit_item'         => __( 'Edit Genre', 'wonder' ),
        'update_item'       => __( 'Update Genre', 'wonder' ),
        'add_new_item'      => __( 'Add New Genre', 'wonder' ),
        'new_item_name'     => __( 'New Genre Name', 'wonder' ),
        'menu_name'         => __( 'Genre', 'wonder' ),
    );
    register_taxonomy( 'genre', 'book', array(
        'hierarchical' => true,
        'labels'       => $labels,
        'rewrite'      => array( 'slug' => 'books/genre' )
    ) );
}
add_action( 'init', 'wpdocs_create_book_tax_rewrite', 0 );