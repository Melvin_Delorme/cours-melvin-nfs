<?php
/* Template Name: Login */
get_header();
?>

    <h1>Login</h1>

    <form id="form_login" action="" method="post" novalidate class="wrap_form">
        <label for="mail">E-mail *</label>
        <input type="email" name="mail" id="mail">
        <span class="error" id="error_mail"></span>

        <label for="password">Mot de passe</label>
        <input type="password" name="password" id="password">
        <span class="error" id="error_password"></span>

        <input type="submit" value="Envoyer">
    </form>
    <div id="login_success"></div>


<?php get_footer();
