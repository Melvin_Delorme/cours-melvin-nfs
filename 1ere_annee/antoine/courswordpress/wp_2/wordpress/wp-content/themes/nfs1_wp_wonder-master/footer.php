<?php
global $metaHome;
// debug($metaHome); ?>

<footer class="page-footer">
    <div class="wrap">
        <div class="columns">
            <div class="column">
                <h3 class="column-title">Contact Us</h3>
                <img src="<?= asset('img/footer-logo.png'); ?>" alt="Wonder" ><br><br>
                <address>
                    <?php echo do_shortcode(nl2br(web_r($metaHome, 'adresse_footer'))); ?>
                </address>
                <ul class="social-icons">
                    <?php if(!empty($metaHome['reseau_1'][0])) { ?>
                        <li><a target="_blank" href="<?php echo web_r($metaHome, 'reseau_1'); ?>" class="facebook"></a></li>
                    <?php } ?>
                    <?php if(!empty($metaHome['reseau_2'][0])) { ?>
                        <li><a target="_blank" href="<?php echo web_r($metaHome, 'reseau_2'); ?>" class="twitter"></a></li>
                    <?php } ?>
                    <?php if(!empty($metaHome['reseau_3'][0])) { ?>
                        <li><a target="_blank" href="<?php echo web_r($metaHome, 'reseau_3'); ?>" class="dribble"></a></li>
                    <?php } ?>
                    <?php if(!empty($metaHome['reseau_4'][0])) { ?>
                        <li><a target="_blank" href="<?php echo web_r($metaHome, 'reseau_4'); ?>" class="rss"></a></li>
                    <?php } ?>

                </ul>
            </div>

            <div class="column">
                <h3 class="column-title">Latest Tweets</h3>
                <ul class="tweets">
                    <li><a href="#">@Basicoh_</a> Hey! Buy this beautiful theme right now!.</li>
                    <li><a href="#">@Basicoh_</a> Wonder Theme is out Now!. You can buy it in Wrapbootstrap.com. Really easy to configure with a wonderful design.</li>
                    <li><a href="#">@Basicoh_</a> If you are an agency, freelancer or a studio, this theme is made for you. Take a look and enjoy it.</li>
                </ul>
            </div>

            <?php
            $args = array(
                'post_type'   => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 4
            );
            $the_query = new WP_Query( $args );
            ?>
            <div class="column">
                <h3 class="column-title">Latest Posts</h3>
                <ul class="post-list">
                    <?php if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
                            <li><a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a></li>
                        <?php }
                    } wp_reset_postdata(); ?>
                </ul>
            </div>

            <div class="column">
                <h3 class="column-title">Flickr Photos</h3>
                <ul class="flickr">
                    <li><a href="#"><img src="<?= asset('img/flickr/1.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/2.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/3.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/4.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/5.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/6.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/7.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/8.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/9.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/10.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/11.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                    <li><a href="#"><img src="<?= asset('img/flickr/12.jpg'); ?>" alt="Liens vers FlickR"></a></li>
                </ul>
            </div>
        </div><!-- .columns -->

        <div class="bottom-bar">
            <p class="copyright">Copyright 2012 Wonder Theme. All rights reserved.</p>
            <nav>
                <ul class="nav">
                    <li><a href="<?= path('/'); ?>" title="">Home</a></li>&nbsp;&nbsp;|&nbsp;
                    <li><a href="<?= path('features'); ?>" title="">Features</a></li>&nbsp;&nbsp;|&nbsp;
                    <li><a href="<?= path('gallery'); ?>" title="">Gallery</a></li>&nbsp;&nbsp;|&nbsp;
                    <li><a href="<?= path('blog'); ?>" title="">Blog</a></li>&nbsp;&nbsp;|&nbsp;
                    <li><a href="<?= path('contact'); ?>" title="">Contact</a></li>
                </ul>
            </nav>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>

