const form = document.querySelector('#form_login');
const inputEmail = document.querySelector('#mail');
const inputPassword = document.querySelector('#password');
const submitButton = form.querySelector('input[type=submit]');
const errorMail = form.querySelector('#error_mail');
const errorPassword = form.querySelector('#error_password');


form.addEventListener('submit', function (e) {
    e.preventDefault();
    submitButton.disabled = true;
    errorMail.innerHtml = ''
    errorPassword.innerHtml = ''
    let params = new FormData();
    params.append('action', 'submit_login_form');
    params.append('mail', inputEmail.value);
    params.append('password', inputPassword.value);

    fetch(MYSCRIPT.ajaxUrl, {
        method: 'post',
        body: params
    }).then(function(response) {
        return response.json()
    }).then(function(data) {
        submitButton.disabled = false;
        if(data.success) {
            form.remove();
        } else {
            if(data.errors.mail != null) {
                errorMail.innerHTML = data.errors.mail
            }
            if(data.errors.password != null) {
                errorPassword.innerHTML = data.errors.password
            }
        }
    });
})