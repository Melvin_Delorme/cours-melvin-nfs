<?php

if ( ! defined( '_S_VERSION' ) ) {
    define( '_S_VERSION', '1.0.1' );
}

function wonder_setup() {
    load_theme_textdomain( 'wonder', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(
        array(
            'menu-1' => esc_html__( 'Primary', 'wonder' ),
        )
    );
}
add_action( 'after_setup_theme', 'wonder_setup' );

function wonder_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'wonder_content_width', 640 );
}
add_action( 'after_setup_theme', 'wonder_content_width', 0 );


function wonder_widgets_init() {
    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar', 'wonder' ),
            'id'            => 'sidebar-1',
            'description'   => esc_html__( 'Add widgets here.', 'wonder' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action( 'widgets_init', 'wonder_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wonder_scripts() {
    // CSS
//    if(is_front_page()) {
//    if(is_page(5)) {
    if(is_page_template( 'template-home.php' )) {
        wp_enqueue_style('flexslider-css', get_template_directory_uri() . '/asset/flexslider/flexslider.css', array(), _S_VERSION);
    }
    wp_enqueue_style( 'wonder-style', get_stylesheet_uri(), array(), _S_VERSION );

    // JS
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js',array(), _S_VERSION, true);
    if(is_page_template( 'template-home.php' )) {
        wp_enqueue_script('flexsliderjs', get_template_directory_uri() . '/asset/flexslider/jquery.flexslider.js', array('jquery'), _S_VERSION, true);
        wp_enqueue_script('homejs', get_template_directory_uri() . '/asset/js/home.js', array(), _S_VERSION, true);
    }

    if(is_page_template( 'template-features.php' )) {
        wp_enqueue_script('feature-js', get_template_directory_uri() . '/asset/js/feature.js', array(), _S_VERSION, true);
        wp_localize_script( 'feature-js', 'MYSCRIPT', array(
            'ajaxUrl' => admin_url( 'admin-ajax.php' )
        ));

    }

    if(is_page_template( 'template-contact.php' )) {
        wp_enqueue_script('contact-js', get_template_directory_uri() . '/asset/js/contact.js', array(), _S_VERSION, true);
        wp_localize_script( 'contact-js', 'MYSCRIPT', array(
            'ajaxUrl' => admin_url( 'admin-ajax.php' )
        ));

    }

    if(is_page_template( 'template-login.php' )) {
        wp_enqueue_script('login-js', get_template_directory_uri() . '/asset/js/login.js', array(), _S_VERSION, true);
        wp_localize_script( 'login-js', 'MYSCRIPT', array(
            'ajaxUrl' => admin_url( 'admin-ajax.php' )
        ));

    }

}
add_action( 'wp_enqueue_scripts', 'wonder_scripts' );


// size of excerpt
function my_excerpt_length($length){
    return 25;
}
add_filter('excerpt_length', 'my_excerpt_length');