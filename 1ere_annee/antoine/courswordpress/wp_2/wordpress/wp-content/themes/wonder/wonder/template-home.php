<?php
/* Template Name: Homepage */

$args = array(
    'post_type'   => 'work',
    'orderby'     => 'date',
    'order'       => 'DESC',
    'post_status' => 'publish',
    'posts_per_page' => 4
);
$the_query = new WP_Query( $args );

$args2 = array(
    'post_type'   => 'flex',
    'post_status' => 'publish',
    'posts_per_page' => -1
);
$the_query2 = new WP_Query( $args2 );

get_header(); ?>

    <div class="wrap">
        <section id="accueil">
            <div class="image_slide">
                <div class="flexslider" id="diapoo">
                    <ul class="slides">
                    <?php if ( $the_query2->have_posts() ) {
                        while ( $the_query2->have_posts() ) { $the_query2->the_post();
                            $img_url2 = get_the_post_thumbnail_url(get_the_ID(), 'flex'); ?>
                            <li>
                                <img src="<?= $img_url2 ?>" />
                            </li>
                        <?php }
                    } wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
            <div class="accueil_text">
                <?php
                    $metas = get_post_meta(get_the_ID());
                ?>
                <h2><?php echo $metas['titre'][0]?></h2>
                <p><?php echo $metas['description'][0]?></p>
            </div>
        </section>
        <section id="worker">
            <div class="line_work">
                <h3><span>Take a look</span> at our work</h3>
            </div>
            <ul class="works">
            <?php if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) { $the_query->the_post();
                    $img_url = get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'); ?>
                <li>
                    <a href="<?php echo get_the_permalink(); ?>">

                        <img src="<?= $img_url ?>" alt="">
                    </a>
                    <h3><?php echo get_the_title(); ?></h3>
                    <p>Description</p>
                </li>
                <?php }
                } wp_reset_postdata(); ?>
            </ul>
        </section>
        <section id="infos">
            <div class="blog">
                <div class="line_blog">
                    <h3><span>From the blog</span> all the latest news</h3>
                </div>
                <div class="awesomeblog">
                    <div class="blog1">
                        <img src="<?= asset('img/blog-1.jpg'); ?>" alt="">
                    </div>
                    <div class="blog2">
                        <h2>You are super awesome</h2>
                        <p>Posted on: Aug14,2013</p>
                        <p>Posted by: Admin</p>
                    </div>
                </div>
                <div class="blog3">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dolorum <span> inventore nam ratione repellat vitae.</span></p>
                </div>
            </div>
            <div class="video">
                <div class="line_video">
                    <h3><span>Recent video</span> created by us</h3>
                </div>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/uxkk4ukqP6A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </section>
    </div>

<?php get_footer() ?>