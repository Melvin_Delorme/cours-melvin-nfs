<?php

// global
require get_template_directory() . '/inc/generale.php';
require get_template_directory() . '/inc/func.php';

require get_template_directory() . '/inc/image.php';

// Admin Lexique

require get_template_directory() . '/inc/admin/boot-admin-lexique.php';

// Custom
require get_template_directory() . '/inc/custom/custom-book.php';
require get_template_directory() . '/inc/custom/custom-work.php';
require get_template_directory() . '/inc/custom/custom-flex.php';


// extra
require get_template_directory() . '/inc/extra/template-tags.php';
require get_template_directory() . '/inc/extra/template-functions.php';

// ajax
require get_template_directory() . '/inc/ajax/ajax-lexique.php';


