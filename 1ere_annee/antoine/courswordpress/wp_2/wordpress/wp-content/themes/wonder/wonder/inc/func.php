<?php

function debug($var,$height = 200,$fixed = false)
{
    $backt = debug_backtrace()[0];
    if($fixed) {
        echo '<pre style="position: fixed;top:0;left:0;right:0;height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 220px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    } else {
        echo '<pre style="height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 10px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    }
    echo '<p style="font-size:.85rem;">'.$backt['file'].' - '.$backt['line'].'</p>';
    print_r($var);
    echo '</pre>';
}

function path($slug) {
    return esc_url( home_url( $slug ));
}

function asset($file) {
    return get_template_directory_uri() . '/asset/img/' . $file;
}

function getImgId($id, $size) {
    $img = wp_get_attachment_image_src($id, $size);
    return '<img src="'. $img[0] .'" alt="">';
}

function getPostValue($key)
{
    if (!empty($_POST[$key])) {
        return $_POST[$key];
    }
}

function viewError($errors, $key)
{
    if (!empty($errors[$key])) {
        return $errors[$key];
    }
}

function validationText($err, $data, $keyError, $min, $max)
{
    if (!empty($data)) {
        if (mb_strlen($data) < $min) {
            $err[$keyError] = 'Veuillez renseigner plus de ' . $min . ' caractères';
        } elseif (mb_strlen($data) > $max) {
            $err[$keyError] = 'Veuillez renseigner moins de ' . $max . ' caractères';
        }
    } else {
        $err[$keyError] = 'Veuillez renseigner ce champ';
    }
    return $err;
}


function showJson($data)
{
    header('Content-type: application/json');
    $json = json_encode($data);
    if($json){
        die($json);
    }
    else {
        die('Error in json encoding');
    }
}




