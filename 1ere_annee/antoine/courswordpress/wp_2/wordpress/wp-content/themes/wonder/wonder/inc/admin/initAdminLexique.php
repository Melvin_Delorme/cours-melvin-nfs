<?php

if(!defined('ABSPATH')) {
    die();
}


class InitAdminLexique
{
    private $slugPage = 'admin-lexique';

    public function __construct()
    {
        add_action( 'admin_menu', array($this,'generatePageLexique') );
    }

    public function generatePageLexique()
    {
        add_menu_page(
            __( 'Lexique', 'wonder' ),
            'Lexique',
            'manage_options',
            $this->slugPage,
            array($this,'listingLexique'),
            'dashicons-art',
            6
        );

        add_submenu_page(
            $this->slugPage,
            'Ajouter au lexique',
            'Ajouter au lexique',
            'manage_options',
            $this->slugPage . '_add_lexique',
            array($this,'addLexique')
        );

    }

    public function listingLexique()
    {
        $listing = new LexiqueListing($this->slugPage);
        $listing->view();
    }

    public function addLexique()
    {
        $add  = new LexiqueAdd($this->slugPage);
        $add->view();
    }


}

$initAdminLexique = new InitAdminLexique();
