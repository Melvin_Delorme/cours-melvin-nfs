<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wonder
 */

get_header();
?>

    <main id="primary" class="wrap site-main">

        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', get_post_type() );

            the_post_navigation(
                array(
                    'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'wonder' ) . '</span> <span class="nav-title">%title</span>',
                    'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'wonder' ) . '</span> <span class="nav-title">%title</span>',
                )
            );

            $metas = get_post_meta(get_the_ID());

            if (!empty($metas['texte_sup'][0])) { ?>
            <p><?php echo nl2br($metas['texte_sup'][0]) ?></p>
            <?php } ?>

            <?php if (!empty($metas['message'][0])) { ?>
            <p><?php echo nl2br($metas['message'][0]) ?></p>
        <?php } ?>

            <div class="img">
            <?php if (!empty($metas['image'][0])) {
                getImgId($metas['image'][0], 'img_flex');
            } ?>
            </div>

        <?php endwhile; // End of the loop.
        ?>

    </main><!-- #main -->

<?php
// get_sidebar();
get_footer();