const app_lexique = document.querySelector('#app_lexique')

let params = new FormData();
params.append('action', 'get_lexique_data')

fetch(MYSCRIPT.ajaxUrl, {
    method: 'post',
    body: params
}).then(function(response) {
    return response.json()
}).then(function(data) {
    //console.log(data);
    data.forEach((d) => {
        const p = document.createElement('p')
        p.innerText = d.title
        app_lexique.append(p)
    })
});

