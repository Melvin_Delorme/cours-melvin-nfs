<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wonder
 */

get_header();
?>

    <!--<main id="primary" class="site-main">-->
    <div class="wrap">
        <?php
        while ( have_posts() ) :
            the_post();

            //get_template_part( 'template-parts/content', get_post_type() );

            the_post_navigation(
                array(
                    'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'wonder' ) . '</span> <span class="nav-title">%title</span>',
                    'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'wonder' ) . '</span> <span class="nav-title">%title</span>',
                )
            );

            $metas = get_post_meta(get_the_ID());
//            debug($metas);?>

            <?php if (!empty($metas['texte_sup'][0])) { ?>
            <p><?php echo $metas['texte_sup'][0] ?></p>
            <?php } ?>

            <?php if (!empty($metas['message'][0])) { ?>
            <p><?php echo $metas['message'][0] ?></p>
            <?php } ?>


        <?php endwhile; // End of the loop.
        ?>

    </div><!-- #main -->

<?php
//get_sidebar();
get_footer();