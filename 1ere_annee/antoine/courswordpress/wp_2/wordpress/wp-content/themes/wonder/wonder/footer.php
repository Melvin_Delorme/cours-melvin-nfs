<?php

$args = array(
    'post_type'   => 'post',
    'orderby'     => 'date',
    'order'       => 'DESC',
    'post_status' => 'publish',
    'posts_per_page' => 4
);
$the_query = new WP_Query( $args );
?>
<footer>
    <div class="wrap">
        <div class="footer_content">
            <div class="content content1">
                <h1>Contact US</h1>
                <img src="<?= asset('img/footer-logo.png'); ?>" alt="">
                <h2>Wonder Bunker</h2>
                <p>6064 Amazing St,</p>
                <p>Madrid, Spain</p>
                <div class="social_medias">
                    <ul>
                        <li><a href="#" class="facebook"></a></li>
                        <li><a href="#" class="twitter"></a></li>
                        <li><a href="#" class="reseau3"></a></li>
                        <li><a href="#" class="reseau4"></a></li>
                    </ul>
                </div>
            </div>
            <div class="content content2">
                <h1>Latest Tweets</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, sunt.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, sunt.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, sunt.</p>
            </div>
            <div class="content content3">
                <h1>Latest Posts</h1>
                <ul>
                    <?php if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
                    <li><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                    <?php }
                    } wp_reset_postdata(); ?>
                </ul>
            </div>
            <div class="content content4">
                <h1>Flickr Photos</h1>
                <div class="flickr">
                    <ul>
                        <li><a href="#"><img src="<?= asset('img/flickr/1.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/2.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/3.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/4.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/5.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/6.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/7.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/8.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/9.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/10.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/11.jpg'); ?>" alt=""></a></li>
                        <li><a href="#"><img src="<?= asset('img/flickr/12.jpg'); ?>" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="line_footer">
            <p>Copyright 2012 Wonder Theme. All right reserved</p>
            <ul>
                <li><a href="<?= path('/'); ?>">Home</a></li>
                <li><a href="<?= path('/features'); ?>">Features</a></li>
                <li><a href="<?= path('/pages'); ?>">Pages</a></li>
                <li><a href="<?= path('/gallery'); ?>">Gallery</a></li>
                <li><a href="<?= path('/blog'); ?>">Blog</a></li>
                <li><a href="<?= path('/contact'); ?>">Contact</a></li>
            </ul>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>