<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wp_2' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );
define('WP_POST_REVISIONS', false);

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uS1ZR/U7R(26+6f4]i|l)[1keE#=b^#>@S;Jw%M^O>x]pHxA)>9F1je/gwddgCpM' );
define( 'SECURE_AUTH_KEY',  'pU*+j-vG!iY2gu|uO^Awc2Q6s{?Gqkq,_Hi;lRO<>V-^Bg?O_g$&5 2ywq~J$(Zz' );
define( 'LOGGED_IN_KEY',    'C62p7gc>r  u)B6Ai4X)<82^XHdc$bB3Jz>X.h560nT}OA? QVjD{<d{l(/,5Y@F' );
define( 'NONCE_KEY',        '%%+j>*^1Ka6x?*Rv^/tn|zRm2_QJ]T](^W5-%FsNucZ=R}`~WtZ[Q#fiGi4*9!YD' );
define( 'AUTH_SALT',        'hw+:2hf4%,gdV^,l+NGB~y$U6i>Q9A 2OQ}B3;jW#12)#YJYL[Yh(DB@>LTK>U_S' );
define( 'SECURE_AUTH_SALT', '?>xniODqa^gEMt54nu5v%pWKrQc5%9kwHALhyx2RP{(K;_Kkgh]=s#Ly`22vn>8c' );
define( 'LOGGED_IN_SALT',   '.x!2M1R)Ay6l1_k24K;d-X@B9Pk9=0NLq<W}wwKy|8?X6NkitGNa.#|J##r{{fic' );
define( 'NONCE_SALT',       'p.NOMZu96n6g*wd08qq i*YkiU$LHYSZI2,E1J@u`uG;!,za(} cW7kg>%,Har.{' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wonder';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
