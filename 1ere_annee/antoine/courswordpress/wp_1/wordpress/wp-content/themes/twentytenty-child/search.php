<?php
get_header();
	if ( have_posts() ) { ?>
        <ul>
            <?php while ( have_posts() ) {
                the_post(); ?>
                <li>
                    <a href="<?php echo get_the_permalink();?>"><?php echo get_the_title()?></a>
                </li>
            <?php } ?>
        </ul>
	<?php }

get_footer();