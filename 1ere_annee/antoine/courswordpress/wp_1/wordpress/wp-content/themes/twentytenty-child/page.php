<?php

get_header();

if ( have_posts() ) { ?>
        <?php while ( have_posts() ) {the_post(); ?>
            <h1><?php echo get_the_title()?></h1>
            <p><?php echo nl2br(get_the_content())?></p>
        <?php }
    }

get_footer();