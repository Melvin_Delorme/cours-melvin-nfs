<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

//			get_template_part( 'template-parts/content', get_post_type() );
            ?>
                <h1 class="title"><?php echo get_the_title()?></h1>
                <div>
                    <?php echo nl2br(get_the_content())?>
                </div>
            <p><a href="<?php echo get_the_permalink();?>">Voir Plus</a></p>

            <p><?php echo get_the_date('d/m/Y')?></p>
            <p><?php echo get_the_author()?></p>
            <p><?php echo get_the_excerpt()?></p>
            <?php the_category(' ');
            echo get_the_ID();
            the_post_thumbnail('thumbnail');
            ?>
                <?php
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php
get_footer();
