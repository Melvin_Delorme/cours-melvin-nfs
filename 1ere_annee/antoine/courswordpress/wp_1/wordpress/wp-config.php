 <?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wp_1' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't/Y[2kk%!XCnr7?D7N!:jzy4epXpd_bdiVtu`L~t[=/ bKPIE~!_ZzJB`^yNzu0g' );
define( 'SECURE_AUTH_KEY',  'R&2I/9-IKNT/XXd]%f_M3>%`2?Rx.&^N8`(-FcI_ O`Y+8Vi*7hh @MQ;SE1SS$w' );
define( 'LOGGED_IN_KEY',    'f/jsvA9%3{kL$Oi?UcmE7T`su<_0x!.^3C-I|Hie}I}_%yF.pu4jlh]w0=Qi$9@v' );
define( 'NONCE_KEY',        'Qp{|%o*mN.<<]S 93c!-CMJ67/D~<`Warm5=n4p~xi>ZCg(As9]=*Z,L.>n(6@h|' );
define( 'AUTH_SALT',        '|W9@uD:Xr0;EL#SrdmQI,T*d9p/#JLxq1`$D/le#:&qJ`;ag/LFYvCdO`q=)sT_}' );
define( 'SECURE_AUTH_SALT', '31}u+u=r@m7qhMux+.]zq?E0]=]_=1@XSh Uy`1}E#6*EM0bqzO-MQ9~i&jCN>Ic' );
define( 'LOGGED_IN_SALT',   '`0I/XpmCZnys`B0=Qw0A*J_I9M8ef_vOf~,BVReI=j$sJk+I`gt}4~r,}St1_vsy' );
define( 'NONCE_SALT',       '+:poZc7.G0MWqsET,R@wNcnnaR~3}sGs`/F4dYgn(#[<4?kP<ZF<HxL6A$bn0S^5' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'test1_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
