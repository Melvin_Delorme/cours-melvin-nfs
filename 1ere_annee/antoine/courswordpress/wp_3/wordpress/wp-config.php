<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'filrouge' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l;aH?.zp//2!>7NRT;||-|M47fbiFPi KrC<Zw_7`!U[],,BQGJ[^XFYe*,MfQ<u' );
define( 'SECURE_AUTH_KEY',  'U&i/hwO3i>gJjyQQA%@+ }4;`vzy!iX_KHABSNZ84``qGh&f.-8nJq.IA=CkQV(a' );
define( 'LOGGED_IN_KEY',    '+$~:}C#W ;W?o,@K${cgW^OI<$N}#$I0d26H[Nsl&tuK gg,vBw#L:W{}T4V9WlB' );
define( 'NONCE_KEY',        'yZ!,mF@ycF?r-R[hv_yM}^C8Wzd^rx2I#YE&?*8ccN8u};z/9Fo+,-I<j.&8wqB5' );
define( 'AUTH_SALT',        ':e=AcYmg.x9UN[htevfR^/yO=++a2OKw4+OC=h~>M7j2y}y[[Sbx:MeQUb!$v1v>' );
define( 'SECURE_AUTH_SALT', 'T.1pO{R4#6OOw<G}S10_,[o@P$$RcV4}`-*oTv?6qn35UPbH9_P~>jmrEttuU%jF' );
define( 'LOGGED_IN_SALT',   '%lE=Y;5^:Y4k40(*+fUDM@po3Vg^G5_6J^%W^K*`CrCv`3WPG##*_yYQ Q}^(L*]' );
define( 'NONCE_SALT',       '!^ST)D`dvIN$@F{Q?>`G@62gV)EdIL{Gq~EzprD{pXXIG`FL&f:p}rr{;+9+r`:I' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'filr_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
