<?php

function debug($var,$height = 200,$fixed = false)
{
    $backt = debug_backtrace()[0];
    if($fixed) {
        echo '<pre style="position: fixed;top:0;left:0;right:0;height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 220px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    } else {
        echo '<pre style="height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 10px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    }
    echo '<p style="font-size:.85rem;">'.$backt['file'].' - '.$backt['line'].'</p>';
    print_r($var);
    echo '</pre>';
}

function path($slug) {
    return esc_url( home_url( $slug ));
}

function asset($file) {
    return get_template_directory_uri() . '/asset/img/' . $file;
}

function svg($file) {
    return get_template_directory_uri() . '/asset/svg/' . $file;
}

function getImgId($id, $size) {
    $img = wp_get_attachment_image_src($id, $size);?>
    <img src="<?php echo $img[0] ?>" alt="">
<?php }

function getYoutubeId($url) {
    $parts = explode('/', $url);
    return end($parts);
}
