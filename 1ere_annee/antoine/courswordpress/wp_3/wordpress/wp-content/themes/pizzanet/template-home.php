<?php /* Template Name: Homepage */
global $web;
$metas = get_post_meta($web['page']['homepage']['id']);
//debug($metas);

get_header(); ?>
<?php get_template_part('view/home/home', 'intro'); ?>
<?php get_template_part('view/home/home', 'presentation'); ?>
<?php get_template_part('view/home/home', 'preparation'); ?>
<?php get_template_part('view/home/home', 'video'); ?>
<?php get_template_part('view/home/home', 'map'); ?>
<?php get_footer() ?>