<?php
if ( ! defined( '_S_VERSION' ) ) {
    // Replace the version number of the theme on each release.
    define( '_S_VERSION', '1.0.0' );
}
function pizzanet_setup() {
    load_theme_textdomain( 'pizzanet', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    register_nav_menus(
        array(
            'menu-1' => esc_html__( 'Primary', 'pizzanet' ),
        )
    );
}
add_action( 'after_setup_theme', 'pizzanet_setup' );
function pizzanet_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'pizzanet_content_width', 640 );
}
add_action( 'after_setup_theme', 'pizzanet_content_width', 0 );
function pizzanet_widgets_init() {
    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar', 'pizzanet' ),
            'id'            => 'sidebar-1',
            'description'   => esc_html__( 'Add widgets here.', 'pizzanet' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action( 'widgets_init', 'pizzanet_widgets_init' );
function pizzanet_scripts() {

    // CSS

    if (is_page_template('template-home.php')) {
        wp_enqueue_style('flexslider-css', get_template_directory_uri() . '/asset/flexslider/flexslider.css', array(), _S_VERSION);
    }

    wp_enqueue_style( 'pizzanet-style', get_stylesheet_uri(), array(), _S_VERSION );

    //JS

    wp_deregister_script('jquery');
   wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js', array(), _S_VERSION, true);
   if (is_page_template('template-home.php')) {
       wp_enqueue_script('flex-js', get_template_directory_uri() . '/asset/flexslider/jquery.flexslider.js', array(), _S_VERSION, true);
       wp_enqueue_script('flexsider-js',get_template_directory_uri() . '/asset/js/home.js', array(), _S_VERSION, true);
   }

    if (is_page_template('template-pizza.php')) {
        wp_enqueue_script('flexsider-js',get_template_directory_uri() . '/asset/js/pizza.js', array(), _S_VERSION, true);
    }
}
add_action( 'wp_enqueue_scripts', 'pizzanet_scripts' );

