<?php
/* Template Name: Pizza */
global $web;
$metas = get_post_meta($web['page']['pizza']['id']);
//debug($metas);

get_header(); ?>
<section id="pizza">
    <div class="wrap">
        <div class="pizza_intro">
            <div class="intro_arrow1"></div>
            <div class="pizza_intro_text">
                <h2><?php echo $metas['commande_pizz_title'][0]?></h2>
            </div>
            <div class="intro_arrow2"></div>
        </div>
        <div class="pizza_intro2">
            <h2><?php echo $metas['commande_pizz_title2'][0]?></h2>
        </div>
        <ul class="all_pizzas">
            <!-- MODAL 1-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title1'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price1'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc1'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img1'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img1'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title1'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc1'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price1'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc1'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 2-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title2'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price2'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc2'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img2'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img2'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title2'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc2'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price2'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc2'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 3-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title3'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price3'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc3'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img3'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img3'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title3'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc3'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price3'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc3'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 4-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title4'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price4'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc4'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img4'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img4'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title4'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc4'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price4'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc4'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 5-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title5'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price5'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc5'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img5'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img5'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title5'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc5'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price5'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc5'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 6-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title6'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price1'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc6'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img6'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img6'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title1'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc6'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price6'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc6'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 7-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title7'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price7'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc7'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img7'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img7'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title7'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc7'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price7'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc7'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 8-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title8'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price8'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc8'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img8'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img8'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title8'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc8'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price8'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc8'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 9-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title9'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price9'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc9'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img9'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img9'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title9'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc9'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price9'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc9'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 10-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title10'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price10'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc10'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img10'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img10'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title10'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc10'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price10'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc10'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 11-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title11'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price11'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc11'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img11'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img11'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title1'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc11'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price11'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc11'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <!-- MODAL 12-->
            <li>
                <a class="open-modal-button">
                    <div class="pizza_content">
                        <div class="pizza_top">
                            <h4><?php echo $metas['all_pizza_title12'][0]?></h4>
                            <p><?php echo $metas['all_pizza_price12'][0]?></p>
                        </div>
                        <div class="pizza_bottom">
                            <p><?php echo $metas['all_pizza_desc12'][0]?></p>
                        </div>
                    </div>
                    <div class="pizza_img">
                        <?= getImgId($metas['all_pizza_img12'][0], 'img_home3'); ?>
                    </div>
                </a>
                <div class="modal">
                    <div class="modal-content">
                        <span class="close-button">&times;</span>
                        <div class="modal-content-container">
                            <div class="modal_img">
                                <?= getImgId($metas['all_pizza_img12'][0], 'img_home1'); ?>
                            </div>
                            <div class="wrap">
                                <div class="modal_inside_content">
                                    <h4><?php echo $metas['all_pizza_title12'][0]?></h4>
                                    <p class="modal_inside_content_p1"><?php echo $metas['all_pizza_indesc12'][0]?></p>
                                    <p class="modal_inside_content_p2"><?php echo $metas['all_pizza_price12'][0]?></p>
                                </div>
                                <p class="modal_inside_content_p3"><?php echo $metas['all_pizza_desc12'][0]?></p>
                                <div class="modal_inside_content2">
                                    <p class="modal_inside_content2_p1"><?php echo $metas['all_pizza_undesc1'][0]?></p>
                                    <p class="modal_inside_content2_p2">+</p>
                                </div>
                                <div class="modal_inside_content3">
                                    <p class="modal_inside_content3_p1">-</p>
                                    <p class="modal_inside_content3_p2">2</p>
                                    <p class="modal_inside_content3_p3">+</p>
                                </div>
                                <div class="modal_inside_content4">
                                    <a class="modal_inside_content4_a1" href="#">
                                        <p class="modal_inside_content4_a1_1"><?php echo $metas['all_pizza_lienleft1'][0]?></p>
                                        <p class="modal_inside_content4_a1_2">25.80€</p>
                                    </a>
                                    <a class="modal_inside_content4_a2" href="#"><?php echo $metas['all_pizza_lienright1'][0]?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<?php get_footer() ?>