<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

// global
require get_template_directory() . '/inc/generale.php';
require get_template_directory() . '/inc/func.php';
require get_template_directory() . '/inc/parameters.php';
require get_template_directory() . '/inc/image.php';


// EXTRA
require get_template_directory() . '/inc/extra/template-tags.php';
require get_template_directory() . '/inc/extra/template-functions.php';


