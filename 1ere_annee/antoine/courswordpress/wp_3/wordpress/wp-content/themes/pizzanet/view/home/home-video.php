<?php global $metas;
$youtube_url = $metas['vid_lienytb'][0];
$youtube_id = getYoutubeId($youtube_url);
?>

<section id="video">
    <div class="wrap">
        <div class="video_logo">
            <img src="<?= get_template_directory_uri() . '/asset/svg/logo.svg'?>" alt="">
        </div>
        <h1 class="video_title"><?php echo $metas['vid_title1'][0]?></h1>
        <div class="video_lecteur">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        </div>
    </div>
</section>
