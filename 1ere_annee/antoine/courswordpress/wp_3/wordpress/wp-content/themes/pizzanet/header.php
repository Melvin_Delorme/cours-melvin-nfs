<?php
global $web;
//debug($web);
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header">
        <div class="wrap">
            <div class="logo">
                <a href="<?= path($web['page']['homepage']['slug']); ?>"><img src="<?= get_template_directory_uri() . '/asset/svg/logo2.svg'?>" alt=""></a>
            </div>
            <ul class="header_link">
                <li><a href="<?= path($web['page']['pizza']['slug']); ?>">Commander</a></li>
                <li><a href="#">Point de retrait</a></li>
                <li><a href="#">FAQ</a></li>
            </ul>
            <ul class="header_buttons">
                <li class="header_button1">
                    <a href="#">
                        <!-- Svg logo -->
                        <p>Connection</p>
                    </a>
                </li>
                <li class="header_button2">
                    <a href="#">
                        <p class="header_button2_p1">3</p>
                        <h4 class="header_button2_h4_1">Mon panier</h4>
                        <p class="header_button2_p2">40.70€</p>
                    </a>
                </li>
            </ul>
        </div>
	</header><!-- #masthead -->
