<?php global $metas?>

<section id="presentation">
    <div class="wrap">
        <ul class="pres_all">
            <li>
                <div class="pres_img_1"><?= getImgId($metas['pres_img1'][0], 'img_home2'); ?></div>
                <h1><?php echo $metas['pres_title1'][0]?></h1>
                <p><?php echo $metas['pres_desc1'][0]?></p>
            </li>
            <li>
                <div class="pres_img_2"><?= getImgId($metas['pres_img2'][0], 'img_home2'); ?></div>
                <h1><?php echo $metas['pres_title2'][0]?></h1>
                <p><?php echo $metas['pres_desc2'][0]?></p>
            </li>
            <li>
                <div class="pres_img_3"><?= getImgId($metas['pres_img3'][0], 'img_home2'); ?></div>
                <h1><?php echo $metas['pres_title3'][0]?></h1>
                <p><?php echo $metas['pres_desc3'][0]?></p>
            </li>
            <li>
                <div class="pres_img_4"><?= getImgId($metas['pres_img4'][0], 'img_home2'); ?></div>
                <h1><?php echo $metas['pres_title4'][0]?></h1>
                <p><?php echo $metas['pres_desc4'][0]?></p>
            </li>
        </ul>
    </div>
</section>