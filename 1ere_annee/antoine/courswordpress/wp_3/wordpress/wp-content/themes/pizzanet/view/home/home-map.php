<?php global $metas; ?>

<section id="map">
    <div class="wrap">
        <div class="map_top">
            <div class="mt_left">
                <img src="<?php echo asset('map.jpg')?>" alt="">
            </div>
            <div class="mt_right">
                <div class="mt_right_img">
                    <img src="<?= svg('point.svg')  ?>" alt="point">
                </div>
                <h1><?php echo $metas['map_title1'][0]?></h1>
                <p><?php echo $metas['map_desc1'][0]?></p>
                <div class="mt_right_a">
                    <a href="#"><?php echo $metas['map_button1'][0]?></a>
                </div>
            </div>
        </div>
        <div class="map_bottom">
            <p><?php echo $metas['map_desc2'][0]?></p>
        </div>
    </div>
</section>