var openModalButtons = document.querySelectorAll('.open-modal-button');
var modals = document.querySelectorAll('.modal');
var closeButtons = document.querySelectorAll('.close-button');

openModalButtons.forEach(function(button) {
    button.addEventListener('click', function() {
        var modal = button.nextElementSibling;
        modal.style.display = 'block';
    });
});

closeButtons.forEach(function(button) {
    button.addEventListener('click', function() {
        var modal = button.closest('.modal');
        modal.style.display = 'none';
    });
});

modals.forEach(function(modal) {
    modal.addEventListener('click', function(e) {
        if (e.target === modal) {
            modal.style.display = 'none';
        }
    });
});
