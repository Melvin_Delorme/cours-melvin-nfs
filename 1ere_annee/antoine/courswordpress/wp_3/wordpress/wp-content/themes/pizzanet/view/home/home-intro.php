<?php global $metas?>

<section id="intro">
    <div class="wrap">
        <div class="intro_present">
            <div class="intro_arrow1"></div>
            <div class="intro_text">
                <h2><?php echo $metas['title1'][0]?></h2>
                <h2><?php echo $metas['title2'][0]?></h2>
            </div>
            <div class="intro_arrow2"></div>
        </div>
        <div class="intro_flexslider">
            <div class="if_left">
                <div class="flexslider" id="diapoo">
                    <ul class="slides">
                        <li>
                            <?= getImgId($metas['flex_img1'][0], 'img_home1'); ?>
                        </li>
                        <li>
                            <?= getImgId($metas['flex_img1'][0], 'img_home1'); ?>
                        </li>
                        <li>
                            <?= getImgId($metas['flex_img1'][0], 'img_home1'); ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="if_right">
                <div class="ifr_text">
                    <h1 class="ifr_text_h1_1"><?php echo $metas['flex_title'][0]?></h1>
                    <p class="ifr_text_p1"><?php echo $metas['flex_content1'][0]?></p>
                    <p class="ifr_text_p2"><?php echo $metas['flex_content2'][0]?></p>
                </div>
                <div class="ifr_button">
                    <a href="#"><?php echo $metas['flex_button'][0]?></a>
                </div>
            </div>
        </div>
    </div>
</section>