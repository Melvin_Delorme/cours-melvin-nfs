<?php global $metas?>

<section id="preparation">
    <div class="wrap">
        <h1><?php echo $metas['prep_title1'][0]?></h1>
        <p><?php echo $metas['prep_desc1'][0]?></p>
        <p><?php echo $metas['prep_desc2'][0]?></p>
    </div>
</section>