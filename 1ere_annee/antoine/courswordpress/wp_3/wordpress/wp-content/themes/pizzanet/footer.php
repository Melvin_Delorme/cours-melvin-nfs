    <footer id="colophon" class="site-footer">
		<div class="wrap">
            <div class="footer_top">
                <h1>Retrouvez nous sur</h1>
                <a href="#"><i class="fa-brands fa-facebook"></i></a>
            </div>
            <div class="footer_bottom">
                <ul>
                    <li><a href="#">Mentions Légales</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Conditions générales de vente</a></li>
                    <li><a href="#">Contactez nous</a></li>
                </ul>
            </div>
        </div>
	</footer><!-- #colophon -->
    <div class="under_footer">
        <div class="wrap">
            <img src="<?= get_template_directory_uri() . '/asset/svg/mastercard.svg'?>" alt="">
            <img src="<?= get_template_directory_uri() . '/asset/svg/visa.svg'?>" alt="">
        </div>
    </div>
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
