<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'ProFiles' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1(k%,)N`.=,$DEIv`;3[T6WxPbg1vf+:YXz(kXc$wqAz5imz+}bU<6y3t19.Gw28' );
define( 'SECURE_AUTH_KEY',  '_mibzN9fMk<O>6#zSg-4fm!!H{W%vI`?/?f@W6rHZF[!|0t%R3#PW]+D~7kpEJX[' );
define( 'LOGGED_IN_KEY',    'b;zEnuxk4?1pjb?|ZONk=$ (A<Dgsr,Z=}xa>4^ja}PCN5Q7g,~[nSnM,K1H2z-%' );
define( 'NONCE_KEY',        '0bbPc-N}ajJR#7u$q0aQx}nwUwV0)S5x|N)(3H%i{&K0A()E66W=JKUJ @Wgs#1x' );
define( 'AUTH_SALT',        'lv/k7[DA;t^lm3bBAydfV2P_h3]NQ@H]kjC*BJe$ ;^~uDESn]&Wom^)tc*l4@pV' );
define( 'SECURE_AUTH_SALT', 'ZVJXLA`jjwQw,WOG`tq/4{fFOAK[&v(J&9j4jtyx|7J?Q$%)2*_ax`NDtg`eeUVc' );
define( 'LOGGED_IN_SALT',   'P-U)Vo6fol{y5 I^$seltAcQD5,~NY3sJ%B,?pVhdng5_q2NFHJ,xV=TtgX/W6Ov' );
define( 'NONCE_SALT',       '~GKI4} (!>.qm}>~HTc_VQG)e.W+mMx|.i hzC!D^GS3/zOXUuc*(]a`5`(5Z#l9' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'profiles_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
