// Ici mettre le code js pour le dropdown
console.log('Dropdown');
const ingredients = [
    {title: 'Carotte', img: 'slide5.jpg', icone: 'carrot', content: 'La Carotte (Daucus carota subsp. sativus) est une plante bisannuelle de la famille des Apiacées (aussi appelées Ombellifères), largement cultivée pour sa racine pivotante charnue, comestible, de couleur généralement orangée, consommée comme légume. La carotte représente, après la pomme de terre, le principal légume-racine cultivé dans le monde2. C\'est une racine riche en carotène.'},
    {title: 'Poisson', img: 'slide4.jpg', icone: 'fish',content: 'Les poissons sont des animaux vertébrés aquatiques à branchies, pourvus de nageoires dont le corps est généralement couvert d\'écailles. On les trouve abondamment aussi bien dans les eaux douces, saumâtres et de mers : on trouve des espèces depuis les sources de montagnes (omble de fontaine, goujon) jusqu\'au plus profond des mers et océans (grandgousier, poisson-ogre). Leur répartition est toutefois très inégale : 50 % des poissons vivraient dans 17 % de la surface des océans1 (qui sont souvent aussi les plus surexploités).'},
    {title: 'Piment', img: 'slide1.jpg', icone: 'pepper',content: 'Le terme piment (vert, jaune, orange, rouge, brun, pêche ou violet) est un nom vernaculaire désignant le fruit de cinq espèces de plantes du genre Capsicum de la famille des Solanacées et de plusieurs autres taxons. Le mot désigne plus communément le fruit de ces plantes, utilisés comme condiment ou légume (en français canadien, le mot piment désigne parfois les poivrons, les autres variétés de Capsicum, au goût plus piquant, étant appelés piments forts). La notion de piment est généralement associée à la saveur de piquant.'},
    {title: 'Citron', img: 'slide3.jpg', icone: 'lemon', content: 'Le citron (ou citron jaune) est un agrume, fruit du citronnier (Citrus limon). Il existe sous deux formes : le citron doux, fruit décoratif de cultivars à jus peu ou pas acide néanmoins classé Citrus limon (L.) Burm. f. (classification de Tanaka) ; et le citron acide, le plus commun de nos jours, dont le jus a un pH d\'environ 2,5.'},
    {title: 'Crevette', img: 'slide2.jpg', icone: 'crevette', content: 'Le nom vernaculaire crevette (aussi connu comme chevrette dans certaines régions de la francophonie) est traditionnellement donné à un ensemble de crustacés aquatiques nageurs, essentiellement marins mais aussi dulcicoles, autrefois regroupés dans le sous-ordre des « décapodes nageurs », ou Natantia.'},
];

const reponse = document.querySelector('#reponse')
const dropdown = document.querySelector('#dropdown');

function changeContentDropDown(index) {
    reponse.innerHTML = '';
    const p = document.createElement('p');
    p.innerText = ingredients[index].content
    const img = document.createElement('img');
    img.alt = ingredients[index].title
    img.src = `asset/img/${ingredients[index].img}`;
    reponse.append(p)
    reponse.append(img)
}
changeContentDropDown(0);

const link = document.createElement('a');
link.href = '';
link.classList.add('btn_dropdown')
link.innerText = ingredients[0].title;

const ul = document.createElement('ul');
ingredients.forEach( function(f, index) {
    const li = document.createElement('li');
    const a = document.createElement('a');
    a.href = '';
    a.innerText = f.title
    li.appendChild(a)
    a.addEventListener('click', function(evt) {
        evt.preventDefault();
        ul.classList.remove('opened');
        link.innerText = f.title
        changeContentDropDown(index)
    })
    ul.appendChild(li);
});
link.addEventListener('click', function(evt) {
    evt.preventDefault();
    ul.classList.toggle('opened');
})
dropdown.append(link)
dropdown.append(ul)