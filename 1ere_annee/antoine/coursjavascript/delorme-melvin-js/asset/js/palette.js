// Ici mettre le code js pour la palette
console.log('Palette');
const body = document.querySelector('body');
const palette = document.querySelector('#palette');
const palette_color = document.querySelector('#palette input')
const btn_open2 = document.querySelector('#link_palette');

btn_open2.addEventListener('click', function(e) {
    e.preventDefault();
    if (palette.classList == 'opened') {
        palette.classList.remove('opened');
    } else {
        palette.classList.add('opened');
    }
});

palette_color.addEventListener('input', function (e) {
    body.style.backgroundColor = palette_color.value;
    palette.classList.remove('opened');
})


