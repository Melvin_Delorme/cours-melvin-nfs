// Ici mettre le code js pour le diaporama
console.log('Diaporama');

$(window).on('load',function() {
    $('#diaporama').flexslider({
        animation: "slide",
        animationLoop: true,
        controlNav: false,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
        directionNav: false,
        slideshowSpeed: 6000,
        animationSpeed: 3000,
        after: function(slider){
            console.log(slider)
        },
    });
});