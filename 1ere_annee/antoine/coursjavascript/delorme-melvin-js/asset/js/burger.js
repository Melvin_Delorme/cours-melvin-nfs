// Ici mettre le code js pour le menu burger
console.log('Menu Burger');

const box_burger = document.querySelector('#nav_burger');
const btn_open = document.querySelector('#link_burger');
const btn_close = box_burger.querySelector('.close');

btn_open.addEventListener('click', function(e) {
    e.preventDefault();
    box_burger.classList.add('opened');
});

btn_close.addEventListener('click', function(e) {
    e.preventDefault();
    box_burger.classList.remove('opened');
})