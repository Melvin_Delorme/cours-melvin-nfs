<?php
require('inc/pdo.php');
require('inc/fonction.php');

include('inc/header.php'); ?>

<h2>Ajax multi-surface</h2>

<button id="btn_test1">Click ici test 1</button>
<div id="js_response_test"></div>

<button id="btn_test2">Click ici test 2</button>
<div id="js_response_test2"></div>

<button id="btn_test3">Click ici test 3</button>
<div id="js_response_test3"></div>

<button id="btn_test4">Click ici test 4</button>
<div id="js_response_test4"></div>

<div id="response_norris"></div>

<div id="response_chien"></div>

<button id="btn_pixel">Get 5 images pixel</button>
<div id="js_response_pixel"></div>


<?php include('inc/footer.php');