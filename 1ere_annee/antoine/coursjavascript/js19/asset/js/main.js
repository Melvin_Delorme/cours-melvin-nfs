console.log('JS 19');
///////////////////////
// Ajax - 2
/////////////////////

//test1
const btn_test1 = document.querySelector('#btn_test1');
const response_test1 = document.querySelector('#js_response_test');

function getTest1() {
    btn_test1.disabled = true
    fetch('ajax/test1.php')
        .then(function (response) {
            console.log(response);
            return response.json();
        })
        .then(function (data) {
            console.log(data);
            response_test1.innerText = data
            btn_test1.disabled = false
        })
        .catch(function (error) {
            console.log(error);
        })
}

btn_test1.addEventListener('click', getTest1);

//test2
const btn_test2 = document.querySelector('#btn_test2');
const response_test2 = document.querySelector('#js_response_test2');

function getTest2() {
    btn_test2.disabled = true
    fetch('ajax/test2.php')
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            console.log(data);
            response_test2.innerText = data
            btn_test2.disabled = false
        })
        .catch(function (error) {
            console.log(error);
        })
}

btn_test2.addEventListener('click', getTest2);

//test3
const btn_test3 = document.querySelector('#btn_test3');
const response_test3 = document.querySelector('#js_response_test3');

function getTest3() {
    btn_test3.disabled = true
    fetch('ajax/test3.php')
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
        console.log(data);
            response_test3.innerHTML = `<h1 style="color: ${data.color1}">${data.html1}<h1>
                                        <h1 style="color: ${data.color2}">${data.html2}<h1>
                                        <h1>J'ai ${data.notes.maths} en maths<h1>
                                        <h1>J'ai ${data.notes.php} en php<h1>`
            btn_test3.disabled = false
        })
        .catch(function (error) {
            console.log(error);
        })
}

btn_test3.addEventListener('click', getTest3);

//test4
const btn_test4 = document.querySelector('#btn_test4');
const response_test4 = document.querySelector('#js_response_test4');

function getTest4() {
    btn_test4.disabled = true
    fetch('ajax/test4.php')
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            response_test4.innerHTML = '';
            console.log(data);
            data.forEach((dat) => {
                const div = document.createElement('div');
                const h2 = document.createElement('h2');
                const p = document.createElement('p');
                h2.innerText = dat.Name;
                div.appendChild(h2);
                response_test4.append(div);
            });
            btn_test4.disabled = false
        })
        .catch(function (error) {
            console.log(error);
        })
}

btn_test4.addEventListener('click', getTest4);

// API 1 => https://api.chucknorris.io/
const response_norris = document.querySelector('#response_norris');
function getBlague() {
    fetch('https://api.chucknorris.io/jokes/random')
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            console.log(data);
            response_norris.innerHTML = data.value
        })
        .catch(function(err) {
            console.log(err);
        })
}
setInterval(function () {
    getBlague();
}, 5000);


// api
// https://dog.ceo/api/breeds/image/random
// au click sur un btn => afficher une nouvelle image d'un chien

const response_chien = document.querySelector('#response_chien');
async function getChien() {
    try {
        let response = await fetch('https://dog.ceo/api/breeds/image/random');
        let data = await response.json();
        response_chien.innerHTML = `<img src="${data.message}" alt="Chien">`
    } catch (e) {
        console.log(e)
    }
}
setInterval(function () {
    getChien();
}, 5000);

// https://picsum.photos/v2/list?limit=5

const btn_pixel = document.querySelector('#btn_pixel');
const response_pixel = document.querySelector('#js_response_pixel');

async function getImages() {
    let response = await fetch('https://picsum.photos/v2/list?limit=5')
    let images = await response.json();
    console.log(images);
    response_pixel.innerHTML = '';
    images.forEach(function(i) {
        const image = document.createElement('img');
        image.src = i.download_url
        image.alt = i.author
        response_pixel.append(image)
    });
}
btn_pixel.addEventListener('click', getImages)