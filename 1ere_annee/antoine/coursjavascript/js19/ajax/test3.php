<?php
require('../inc/fonction.php');

$tab = [
    'html1' => 'hello html1',
    'html2' => 'hello html2',
    'color1' => 'red',
    'color2' => 'orange',
    'notes' => array(
        'maths' => 12,
        'php' => 15
    )
];

showJson($tab);