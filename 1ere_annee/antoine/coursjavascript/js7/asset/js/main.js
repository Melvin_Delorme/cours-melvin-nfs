console.log('JS 7')
////////////////
// Fonction
//////////////////////
function getHello()
{
    console.log('Hello World');
}
getHello();
getHello();
getHello();
// arguments
function getUser(prenom = 'Michel')
{
    console.log('Bonjour' + prenom);
}
getUser('Antoine');
getUser('Jacky');
getUser();

function multiplication(num1, num2)
{
    let result = parseFloat(num1) * parseFloat(num2);
    console.log(result.toFixed(2));
}
multiplication(12,85); // 850
multiplication(2,5); // 10

// expression de fonction

const division = function (chiffre1,chiffre2) {
    let result = parseFloat(chiffre1) / parseFloat(chiffre2)
    return result.toFixed(2);
}
let resultatDivision = division(34, 78);
console.log(resultatDivision);
// fonction flechée
const addition = (chiffre1, chiffre2) => {
    return parseFloat(chiffre1) + parseFloat(chiffre2)
}
let resultatAddition = addition(15,458);
console.log(resultatAddition);

// Object d'arguments
function ajouter()
{
    // console.log(arguments);
    // console.log(arguments[2]);
    let total = 0;
    let count = arguments.length;
    let fruits = ['Banane', 'Kiwi'];
    for(i = total;i < count;i++) {
        total += parseFloat(arguments[i]);
    }
    const data = {
        total : total,
        fruits : fruits,
        count : count
    }
    return data;
}

let data = ajouter(45,786,454,5467,78765,55456,4585,545);
console.log(data);
console.log(data.total);
console.log(data.fruits[1]);
console.log(data.count);

function calculGenerate(prenom, ...numbers)
{
    let total = 0;
    console.log(numbers);
    numbers.forEach(n) => {
        total += parseFloat(n);
}
}


