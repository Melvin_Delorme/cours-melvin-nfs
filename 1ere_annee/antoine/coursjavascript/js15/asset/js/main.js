console.log('JS 15');
///////////////////////
// Horloge
/////////////////////

let joursnum = [
    'Dimanche',
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi',
    'Samedi'
];

let monthnum = [
    'Janvier',
    'Fevrier',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Aout',
    'Septembre',
    'Obtobre',
    'Novembre',
    'Décembre'
];

setInterval(function () {
    let date = new Date();

    let hour = document.querySelector('#heure');
    let minutes = document.querySelector('#minute');
    let secondes = document.querySelector('#seconde');
    let jours = document.querySelector('#jour');
    let dateday = document.querySelector('#date');
    let month = document.querySelector('#mois');
    let year = document.querySelector('#annee');

    verif = function (value, func) {
        if (value.innerText < 10) {
            value.innerText = `0${value.innerText=func}`
        }
    }

    hour.innerText = date.getHours();
    verif(hour, date.getHours());
    minutes.innerText = date.getMinutes();
    verif(minutes, date.getMinutes());
    secondes.innerText = date.getSeconds();
    verif(secondes, date.getSeconds());
    jours.innerText = joursnum[date.getDay()];
    dateday.innerText = date.getDate();
    month.innerText = monthnum[date.getMonth()];
    year.innerText = date.getFullYear();

}, 1);