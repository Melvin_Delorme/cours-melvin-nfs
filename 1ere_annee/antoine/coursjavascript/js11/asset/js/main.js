console.log('JS 11');
///////////////////////
// Noeud
/////////////////////
const app = document.querySelector('#app');
// app.innerHTML = '<p>Bonjour a tous</p>';
// app.innerText = '<p>Bonjour a tous</p>';
// app.classList.add('good');
const img = document.createElement('img');
const imgSrc = document.createAttribute('src');
imgSrc.value= "https://picsum.photos/200/300";
img.setAttributeNode(imgSrc);

const section = document.createElement('section');

// a => ajouter a la section
    //-> href =>
    // - texte
    // target="_blank"

const a = document.createElement('a');
const aSrc = document.createAttribute('href');
const aTarget = document.createAttribute('target');

a.innerText = 'Je suis un lien';
aSrc.value = "https://pokemongolive.com/fr/";
aTarget.value = "_blank";
a.setAttributeNode(aSrc);
a.setAttributeNode(aTarget);

// p => class="para" => texte
    // => ajouter a la sestion

const p = document.createElement('p');

p.innerText = "Bonjour c'est moi";
p.classList.add('para');

section.appendChild(img);
section.appendChild(a);
section.appendChild(p);
app.append(section);
console.log(img);

///////////////////
// append, prepend, before, after
///////////////////
// const h1 = document.querySelector('h1');
// // h1.innerText = 'Nouveau titre';
// h1.append('a la fin dans le h1');
// h1.prepend('au debut dans le h1');
// h1.before('Avant le h1');
// h1.after('Après le h1');

// Supprimer un noeud
// setTimeout(function () {
//     h1.remove();
// }, 2000);

//  EXO
const articles = [
    {title: 'Mon Titre 1', excerpt: 'kllkjlkjlhjgghj   jlk jlk jklj', img: 2},
    {title: 'Mon Titre 2', excerpt: 'kllfkjdfidfsgjdf', img: 78},
    {title: 'Mon Titre 3', excerpt: 'kl lkj  lkjlhjgghj   jlk jlk jklj', img: 56},
    {title: 'Mon Titre 4', excerpt: 'kllkj lkjlhj gghj   jlk jlk jklj', img: 95},
];
// html div id app2
// Création d'une div => id "articles"
// autant de div class = one_article
//h2  => titre
// p => excerpt
// img => "https://picsum.photos/id/2/200/300";
const app2 = document.querySelector('#app2');
const divBase = document.createElement('div');
divBase.id = 'articles';
articles.forEach((article) => {
    const div = document.createElement('div');
    div.classList.add('one_article');
    // h2
    const title = document.createElement('h2');
    title.innerText = article.title;
    div.appendChild(title)
    // excerpt
    const excerpt = document.createElement('p');
    excerpt.innerText = article.excerpt
    div.appendChild(excerpt);
    // img
    const image = document.createElement('img');
    image.alt = article.title;
    image.src = `https://picsum.photos/id/${article.img}/200/300`;
    div.appendChild(image);
    divBase.appendChild(div);
})
app2.append(divBase);