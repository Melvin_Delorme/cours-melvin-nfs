console.log('JS 20');
///////////////////////
// Cocktails
/////////////////////

//Letters
allletters = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z'];
//Cocktails
const btn_letters = document.querySelector('#letters');
const response_cocktails = document.querySelector('#cocktails');

async function getCocktails(l = 'a') {
    recuCocktails = false;
    let response = await fetch('https://www.thecocktaildb.com/api/json/v1/1/search.php?f='+l);
    let cocktails = await response.json();
    response_cocktails.innerHTML ='';

    cocktails.drinks.forEach((cocktail) => {
        const h1 = document.createElement('h1');
        h1.innerText = cocktail.strDrink
        const image = document.createElement('img');
        image.src = cocktail.strDrinkThumb
        const div = document.createElement('a');
        div.id = 'cocktail';
        div.href = '#';
        div.append(h1, image);
        response_cocktails.append(div);
        recuCocktails = true;
    })
    if (recuCocktails = true) {
        console.log('Get Cocktails Sucesfull');
    } else {
        console.log('Error cocktails unloaded');
    }
}
async function getLetters() {
    allletters.forEach((l) =>{
        const a = document.createElement('a');
        a.innerText = l
        a.href = '';
        a.addEventListener('click', function (e) {
            e.preventDefault();
            getCocktails(l);
        })
        btn_letters.append(a);
    })
}
getLetters();

