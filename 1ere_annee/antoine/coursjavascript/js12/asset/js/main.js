console.log('JS 12');
///////////////////////
// Events
/////////////////////

function clickPerso() {
    const body= document.querySelector('body');
    body.style.backgroundColor = 'blue';
}

function changeColor(el,color = 'gray') {
    console.log({el});
    el.style.color = color;
}

// INFOS

const infos = document.querySelector('#infos');
console.log(infos);

infos.addEventListener('click', function () {
    console.log(event.clientX);
    console.log('Click dans la boite infos');
});

// mouseenter , mouseleave

infos.addEventListener('mouseenter', function () {
    infos.style.backgroundColor = 'green';
});

infos.addEventListener('mouseleave', function () {
    infos.style.backgroundColor = 'red';
});


// Infos 2

const paragraphes = document.querySelectorAll('#infos2 p');
console.log(paragraphes);
paragraphes.forEach((para) => {
    para.addEventListener('click', function () {
        if (para.dataset.color) {
            this.style.color = para.dataset.color;
        } else {
            this.style.color = 'red';
        }

        // this.style.color = 'red'
    });
});

// Boite

const boitemoove = document.querySelector('#boite');
console.log(boitemoove);
boitemoove.addEventListener('click', function () {
    //boitemoove.classList.toggle('open');
    if (boitemoove.classList.contains('open')) {
        boitemoove.classList.remove('open');
        boitemoove.innerText = '';
    } else {
        boitemoove.classList.add('open');
        boitemoove.innerText = 'Le texte';
    }

});

// Box

const box1 = document.querySelector('#box1');
const box2 = document.querySelector('#box2');
const box3 = document.querySelector('#box3');
box1.addEventListener('click', function () {
    console.log('Click box1');
});
box2.addEventListener('click', function (evt) {
    evt.stopPropagation();
    console.log('Click box2');
});
box3.addEventListener('click', function (evt) {
    evt.stopPropagation();
    console.log('Click box3');
});

const lien = document.querySelector('#prevent a');
lien.addEventListener('click', function (evt) {
    evt.preventDefault();
    console.log('Click sur le lien');
});

const prevent = document.querySelector('#prevent');
const form = document.querySelector('#formulaire');
form.addEventListener('submit', function (evt) {
    evt.preventDefault();
    console.log('Formulaire soumis');
    const input = document.querySelector('input');
    const ul = document.querySelector('ul');

    ul.innerText =  input.value
    input.value = '';

    prevent.append(ul)
});




