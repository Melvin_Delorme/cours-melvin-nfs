console.log('JS 10');
///////////////////////
// DOM 2
/////////////////////
const section = document.querySelector('section');
const p = section.querySelector('p');
const img = section.querySelector('img');
const a = p.querySelector('a');
const input = section.querySelector('input');

// console.log({section,p,img,a,input});
section.style.backgroundColor = 'grey';
console.log(p.childNodes);
// console.log(section.children);
p.childNodes[0].nodeValue = 'Ici le nouveau texte pour le paragraphe';
p.childNodes[1].innerText = '<span>Ici le nouveau texte du lien</span>';
// p.childNodes[1].innerHTML = '<span>Ici le nouveau texte du lien</span>';
p.childNodes[2].nodeValue = 'texte nouveau en dessous du lien';

// au bout de 2 secondes
    // a => modifier le texte du lien => modifier sa couleur => red

setTimeout(() => {
    a.innerText = 'Texte exo';
    a.style.color = 'red';
}, 2000);
console.log(a.innerHTML);

console.log({img});
console.log(img.src);
console.log(img.alt);

setTimeout(() => {
    img.src = "https://assets.pokemon.com/assets//cms2-fr-fr/img/video-games/_tiles/pokemon-unite/11172022/pokemon-unite-169.jpg"
    img.title = 'Pokemon'
}, 2000);

console.log(img.attributes);

console.log(img.getAttribute('alt'));
console.log(img.getAttribute('data'));
img.setAttribute('data', 'ici un text pour un attribut data');
console.log(img.getAttribute('data'));

console.log(img.hasAttribute('data'));
console.log(img.hasAttribute('false'));

input.focus(); // Ici je peux écrire directement dans l'input
input.value = 'Michel'; // Je donne une value à l'input
console.log(input.value); // Récupération de la value de l'input
input.blur(); // Je quitte l'input
//input.type = 'password';
input.placeholder = 'Ici votre nom';
input.disabled = true;
// input.disabled = false;
input.name = 'prenom';

// modification du css, modification de class

console.log(section.style);
console.log(section.className);
section.className += ' error';
console.log(section.getAttribute('class'));

// classList
console.log(section.classList);
// Ajouter une class à votre sélection
section.classList.add('error2');
console.log(section.classList);
// Enlever une class
section.classList.remove('dede');
section.classList.remove('dada');
console.log(section.classList);

// toggle
// section.classList.add('open');
// section.classList.toggle('open');

setTimeout(() => {
    section.classList.toggle('open');
}, 3000);

// Besoin de savoir si une boite possede une class
// contains
console.log(section.classList.contains('open')); // true

if (a.classList.contains('genial')) {
    a.classList.remove('genial');
} else {
    a.classList.add('genial');
}
console.log(a.classList.contains('genial')); // true

// DATA SET
const infos = document.querySelector('#infos');
console.log(infos);
console.log(infos.dataset.id);
console.log(infos.dataset.idCategory);
console.log(infos.dataset.prenom);
let prenom = infos.dataset.prenom;
let id = infos.dataset.id;
let idcategory = infos.dataset.idCategory;
console.log(prenom);

let html = `<p>Je suis ${prenom}, mon id est ${id} et ma categorie est : ${idcategory}</p>`;
infos.innerHTML = html;


