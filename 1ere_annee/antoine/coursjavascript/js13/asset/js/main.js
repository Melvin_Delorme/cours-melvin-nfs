console.log('JS 13');
///////////////////////
// Counter
/////////////////////

const jslink = document.querySelector('#js_link')
const counter = document.querySelector('.counter');
let countervalue = counter.innerHTML;

 jslink.addEventListener('click', function (evt) {
     evt.preventDefault();
     countervalue++;
     counter.innerHTML = countervalue;
});

// Formulaire
let formfunction = function (val1, val2, min, max) {
    const input = document.querySelector(`${val1}`);
    let span = document.querySelector(`${val2}`);

    input.addEventListener('keyup', function () {
        compteur = input.value.length;
        if (compteur < min) {
            console.log('pas assez de caracteres');
            span.innerText = `${min} caractères minimum`;
            span.className = 'error';
        } else if (compteur > max) {
            console.log('trop de caracteres');
            span.innerText = `${max} caractères maximum`;
            span.className = 'error';
        } else {
            console.log('Correct');
            span.innerText = `${compteur} caractères`;
            span.className = 'success';
        }
    });
}


formfunction('#nom', '#error_nom', 3, 70);

formfunction('#prenom', '#error_prenom', 5, 50);

formfunction('#description', '#error_description', 2, 150);





