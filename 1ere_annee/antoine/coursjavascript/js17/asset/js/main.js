console.log('JS 17');
///////////////////////
// JQUERY 2
/////////////////////

const bars = document.querySelector('.bars');
const arrow = document.querySelector('.arrow');
const ontop = document.querySelector('.ontop');

bars.addEventListener('click', function (e) {
    e.preventDefault();
    bars.className = 'bars off';
    arrow.className = 'arrow on';
    ontop.className = 'ontop on';
})

arrow.addEventListener('click', function (e) {
    e.preventDefault();
    bars.className = 'bars on';
    arrow.className = 'arrow off';
    ontop.className = 'ontop off';
})

// Flexslider
$(window).on('load',function () {
    $('#diapoow').flexslider({
        animation: "slide",
        controlNav: true,
        directionNav: false,
        after: function (slider) {
            console.log(slider)
        },
    });
});

$(window).on('load',function () {
    $('#diapoow2').flexslider({
        animation: "slide",
        controlNav: false,
        directionNav: true,
        after: function (slider) {
            console.log(slider)
        },
    });
});

$(window).on('load',function () {
    $('#diapoow3').flexslider({
        animation: "slide",
        controlNav: false,
        directionNav: true,
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5
    });
});

// Chart

const ctx = document.getElementById('myChart');

new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [90, 19, 3, 5, 2, 3],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

// Modal

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
