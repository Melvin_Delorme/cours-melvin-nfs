<?php
////////////////////////
/// BOUCLES
///////////////////////
/// WHILE / DO WHILE
$i = 0;
$num = 50;
while ($i < 10) {
    $i++;
    $num--;
}

echo $num;
do {
    $i++;
    $num--;
} while ($i < 20);
echo '<br>';
echo $num;
echo '<br>';
/////////////////////////////
/// FOR
////////////////////////////
for ($i = 0;$i <= 2000; $i++) {
    echo $i . ', ';
}
echo '<hr>';
// affichez les nombres de 1 a 100 avec un espace entre chaque
for ($i = 1;$i <= 100; $i++) {
    echo $i . ' ';
}
echo '<br>';
// affichez les nombres de 5 a 25, séparé par virgule, il ne doit pas avoir de virgule a la fin de votre chaine
for ($i = 5;$i <=25; $i++) {
    if ($i == 25) {
        echo $i;
    } else {
        echo $i . ', ';
    }
}
echo  '<br>';
// Affichez les multiples entre 15 et 120 séparés par des tirets
for ($i = 15;$i <=120; $i+=5) {
    echo $i . ' - ';
}
echo '<br>';
// Affichez les nombres pairs de 19 a 67 séparés par une balise br
for ($i = 19;$i <=67; $i++) {
    if ($i % 2 === 0)  {
        echo $i . '<br>';
    } else {
        echo ' ';
    }
}
// Affichez les années depuis votre année de naissance jusqu'a aujourd'hui. Mettre chaque année dans un p avec la class "year"
for ($i = 2002; $i <=date('Y'); $i++) {
    echo '<p class="year">';
    echo $i;
    echo '</p>';
}
// Affichez les nombres de 50 a 25 par ordre décroisant sauf le 30
for ($i = 50; $i !=24; $i--) {
    if ($i === 30) {
        echo '';
    } else {
        echo $i;
        echo '<br>';
    }
}

// Affichez les nombres de 1 a 50. Les nombres de 36 a 47 doivent etre en vert le reste sera en rouge
for ($i = 1; $i <= 50; $i ++) {
    if ($i >=36 && $i <=47) {
        $color = 'green';
        echo '<p style="color:'. $color .'";>';
        echo $i;
        echo '</p>';
    } else {
        $color = 'red';
        echo '<p style="color:'. $color .'";>';
        echo $i;
        echo '</p>';
    }
}
////////////////////////////
/// BOUCLES & TABLEAUX
///////////////////////////
$musiques = array('Basse','Guitare','Batterie','Piano','Percu', 'Xylophone', 'Voix');
echo '<pre>';
print_r($musiques);
echo '</pre>';
echo $musiques[2]; // Batterie
echo '<br>';
echo count($musiques);

echo '<ul>';
for ($i = 0; $i <count($musiques); $i++) {
    echo '<li>'. $musiques[$i] .'</li>';
}
echo '</ul>';

$fruits = array('Banane', 'Kiwi', 'Papaye', 'Tomate');
// section id fruits
    // div par fruits class fruits
        // h2

echo '<section id="fruits">';
    for ($i = 0; $i <count($fruits); $i++) {
        echo '<div class="fruits"><h2>'. $fruits[$i] .'</h2></div>';
    }
echo '</section>';

// foreach
foreach ($fruits as $fruit) {
    echo $fruit;
}

echo '<ul>';
    foreach ($musiques as $musique) {
        echo '<li>';
        echo $musique;
        echo '</li>';
    }
echo '</ul>';

$tiroirs =array('clef','monnaie','capote','piles','stylo 4 couleurs');

