<h1>Cours 1</h1>
<?php
//phpinfo();
echo 'Hello World';
echo '<p>Hello World</p>'
?>
<p>Ici un paragraphe</p>
<?php
echo '<p>Echo via php</p>';
?>
<h2><?php echo 'mon 2eme titre'; ?></h2>
<?php
////////////
// VARIABLES
////////////
$variable = 'agence immobilière';
// interdit aux maj et aux chiffres au début
// Interdits => #$%&-
// $nom est different de $nOm // les noms de variables sont sensible a la casse
?>
<h1><?php echo $variable?></h1>
<?php
/////////////////////
// TYPES DE VARIABLES
////////////////////
/// STRING , chaine de caractères
$titre = 'Mon e-shop';
// INTEGER , INT => entier;
$year = 2022;
// FLOAT
$pourcentage = 12.2;
//  BOOLEAN (true, false )
$jeSuisLeFormateur = true;
$isLoveMacron = false;
// NULL
$pasDeValeur = NULL;
// Types Composées
// Tableaux, Objects
$tableaux = array('Beau loft plein sud', 'Menagerie', 'Chalet dans les montagnes');
echo '<pre>';
print_r($tableaux);
echo '</pre>';
echo  $tableaux[1];
?>


<ul>
    <li><?php echo $tableaux[0] ?></li>
    <li><?php echo $tableaux[1] ?></li>
    <li><?php echo $tableaux[2] ?></li>
</ul>

<?php
// Comentaires
// Comentaire sur une ligne
/*Comentaire sur une ligne*/
/*
 * Commentaires sur plusieurs lignes
 */
///////////
// DEBOGAGE
///////////
// echo, print_r(), var_dump()
var_dump($tableaux);
//die('ok');
// Type Errors
    // Fatal => stoppe le script
    // WARNING => script qui continue
    // E_NOTICE => infos
/////////////////////
// SUPER GLOBAL
////////////////////
echo '<pre>';
print_r($GLOBALS);
echo '</pre>';

echo '<pre>';
print_r($_SERVER);
echo '</pre>';
///////////////////
// CONCATENATION =>
//////////////////
/// ?>
<h3><?php echo $titre ?></h3>

<?php
echo '<h3>';
echo $titre;
echo '</h3>';
//
echo '<h3>' . $titre . '</h3>';
//
$html = '';
$html .= '<h3>';
$html .= $titre;
$html .= '</h3>';
echo $html;
// exo
$chaine = 'Salut';
$chaine2 = 'Buzz';
$data1 = 'Formation';
$data2 = 'PHP';

// Avec les 3 methodes
// <p class"para">Salut Buzz! Je suis en formation PHP</p>
?>
<p class="para"><?php echo $chaine; ?> <?php echo $chaine2; ?>! Je suis en <?php echo $data1; ?> <?php echo $data2; ?></p>

<?php
echo '<p class="para">';
echo $chaine;
echo ' ';
echo $chaine2;
echo ' ! Je suis en';
echo $data1;
echo ' ';
echo $data2;
echo '</p>';

echo '<p class="para">' . $chaine . ' ' . $chaine2 . ' ! Je suis en ' . $data1 . ' ' . $data2 . '</p>';

// '' vs "", echappement
echo '<p class="dede">Bonjour dede</p>';
echo "<p class=\"dede\">Bonjour dede</p>";

///
$traitees = 'lu';
$la = 'ici';
echo '<p>Les variables sont $traitees $la</p>';
echo "<p>Les variables sont $traitees $la</p>";
echo '<p>Les variables sont '.$traitees.' '.$la.'</p>';

////////////////////////////////
/// Les conditions
///////////////////////////////
// SI / SINON SI / SINON

if(true) {
    echo 'yes';
} else {
    echo 'no';
} ?>
<br>
<?php
$mot1 = 'musique';
$mot2 = 'sport';
$mot3 = 'musique';
if ($mot1 == $mot2) {
    echo 'Les mots sont identiques';
} elseif($mot1 = $mot3) {
    echo 'Les mots sont differents';
} else {
    echo 'vous ne me verrez jamais';
}
echo $mot1 == $mot3; // true
echo $mot1 == $mot2; // false
?>
<br>
<?php
$age = 19;
if($age < 18) {
    echo 'Je suis mineur';
} else {
    echo 'Je suis majeur';
}
// Conditions Ternaires
$majeur = $age < 18 ? false : true;
echo $majeur == true ? 'Je suis majeur' : 'Je suis mineur';
?>
<hr>
<?php
// EXO
// moins de 18 ans => mineur
    // si moins de 18 ans en moins de 6 ans => en maternelle
// Sinon si plus de 65 ans => retraite
// Sinon je suis un actif

if($age < 18) {
    echo 'Je suis mineur';
    if ($age < 6) {
        echo ' en maternelle';
    }
} elseif ($age > 65) {
    echo 'Je suis a la retraite';
} else {
    echo 'Je suis actif';
}
?>
<br>
<?php
// switch
$i = 1;
switch ($i) {
    case 0:
        echo 'i egale a zero';
        break;
    case 1:
        echo 'i egale a 1';
        break;
    case 2:
        echo 'i egale a 2';
        break;
    default;
        echo 'i est plus grand que 2';
        break;
}
/////////////////////
///  Les conditions => Opérateur de comparaison
////////////////////
// $a === $b, Egal, True si $a est egale a $b
// $a === $b, Identique, True si $a est egale a $b et qu'ils sont du meme type.
    // 12 === '12' // false
    // 12 == '12' // true
// $a != $b, Different
// $a !== $b, Different ou bien ils ne sont pas du même type
// $a < $b, Plus petit que
// $a > $b, Plus grand que
// $a <= $b, Inferieur ou egale
// $a >= $b, Superieur on egale
?>
<br>
<?php

//////////////////////////////////////////////////
/// Les conditions - Opérateurs logiques
/////////////////////////////////////////////////
/// ET => &&, AND
/// OU => ||, OR
$homme = true;
$age = 19;
if($homme && $age > 18) {
    echo 'Ok';
}
$a = 42;
$b = ($a === 42);

////////////////////////////////////////////////
/// Arithmétique et incrémentation
///////////////////////////////////////////////
// -$a, Négation => opposé de $a.
// $a + $b => addition
// $a - $b => soustraction
// $a * $b => Multiplication
// $a ** $b => Exponentielle
// $a / $b => Division
// $a % $b => Modulo, Reste de la division
echo 6 % 2; //
$chiffre = 14;
if ($chiffre % 2 === 0) {
    echo 'chiffre pair';
} else {
    echo 'chiffre impair';
}
// Précedence
$calcul = 4 + 6 * 2; // 16
// Incrementation & décrémentation
$j = 1;
$j++;
echo $j;
$j+=7; //=9

// autre
$x =4;
$x++;
echo $x++;// = 5
echo  $x; // =6
