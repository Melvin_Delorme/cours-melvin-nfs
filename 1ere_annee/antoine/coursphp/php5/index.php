<?php
$tab = [];
$tab[] = array('A', 'C', 'O', 'D');
$tab[] = array('J', 'E', 'O', 'Z');
$tab[] = array('T', 'U', 'I');
$tab[] = array('A', 'P', 'R');

// Ecrivez "JUPITER" a partie de $tab

echo '<pre>';
print_r($tab);
echo '</pre>';

echo '<p>' . $tab[1][0],  $tab[2][1], $tab[3][1], $tab[2][2], $tab[2][0], $tab[1][1], $tab[3][2]  . '</p>';

$arrayDeOuf = array(
    0 => "fdjsa", 1 => 2332,
    2 => array(0 => "dsajf", "sdfsd" => array(
        0 => 9034, "kkk" => "vvv", 2390 => array(
            'dede'  => 'R2D2'
        )), array(
        0 => 1, 1 => 2, 3,
        "k"
    )
    )
);

echo '<pre>';
print_r($arrayDeOuf);
echo '</pre>';

echo '<p>' . $arrayDeOuf[2]['sdfsd'][2390]['dede'] . '</p>';
////////////////////////////////////////////////////////////
// Tableau Multidimensionnel
///////////////////////////////////////////////////////////
$paiements = array(
    array(
        'montant' => 12.55,
        'date' => '12/12/2022'
    ),
    array(
        'montant' => 14.75,
        'date' => '13/12/2022'
    ),
    array(
        'montant' => 2.25,
        'date' => '14/12/2022'
    ),
);

echo '<pre>';
print_r($paiements);
echo '</pre>';
// Affichez le montant du premier paiement
// Affichez la date du dernier paiement
// Ajoutez un nouveau paiement

echo '<p> ' . $paiements[0]['montant'] . '</p>';
echo '<p> ' . $paiements[2]['date'] . '</p>';
//$paiements[3]['montant'] = 0.5;
//$paiements[3]['date'] = '12/12/2002';
$paiements [] = array(
    'montant' => 0.5,
    'date' => '12/12/2002',
);
echo '<p> ' . $paiements[3]['montant'] . '</p>';
echo '<p> ' . $paiements[3]['date'] . '</p>';
echo '<pre>';
print_r($paiements);
echo '</pre>';

// for & foreach

echo '<section id="paiements">';
for ($i = 0; $i <count($paiements); $i++) {
    echo '<div class="paiements"><h2>'. $paiements[$i]['montant'] .'</h2></div>';
    echo '<div class="paiements"><h2>'. $paiements[$i]['date'] .'</h2></div>';
}
echo '</section>';
//////////////////////////////////////////////////////////////////////
echo '<ul>';
foreach ($paiements as $paiement) {
    echo '<li>';
    echo $paiement['montant'];
    echo ' € le ';
    echo $paiement['date'];
    echo '</li>';
}
echo '</ul>';
/////////////////////////////////////////////////////////////////////
$users = [
    ['nom' => 'Michel','mail' => 'michel@gmail.com', 'age' => 35],
    ['nom' => 'Jacky','mail' => 'jacky@gmail.com', 'age' => 45],
    ['nom' => 'Bernard','mail' => 'bernard@gmail.com', 'age' => 354],
    ['nom' => 'Janine','mail' => 'janine@gmail.com', 'age' => 2],
];

// for // foreach
// section id="users"
    // chaque user
        // div => class="one_users"
        // p => a => mailto
        // p => age => class="user_age"

echo '<section 
           style="border: solid black; background-color: darkblue; color: white; padding: 1rem; display: flex; gap: 1rem;" 
           id="user">';
for ($i = 0; $i <count($users); $i++) {
    echo '<div style="width: 25%; text-align: center; font-size: 30px; font-weight: bold; border: solid; background-color: black" class="one_users">
            <p>'. $users[$i]['nom'] .'</p>
            <a style="color: white" href="mailto:'. $users[$i]['mail'] .'">'. $users[$i]['mail'] .'</a>
            <p class="user_age">'. $users[$i]['age'] .'</p>
          </div>';
}
echo '</section>';

echo '<hr>';
//////////////////////////////////////////////////////////////////

echo '<section class="user">';
foreach ($users as $user) {
    echo '<div class="one_user">';

    echo '<p>';
    echo $user['nom'];
    echo '</p>';

    echo '<a href="mailto:'. $user['mail'] .'">';
    echo $user['mail'];
    echo '</a>';

    echo '<p class="user_age">';
    echo $user['age'];
    echo '</p>';

    echo '</div>';
}
echo '</section>';



////////////////////////////////////////////////////////////////////////////////////////////
echo '<hr>';
$jeux = array(
    array(
        'nom' => 'Undertale',
        'prix' => 447,
        'taille' => 580,
        'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, rerum.',
        'achevements' => array(
            'ici' => 'Manger une pomme',
            'la' => 'Courir vite',
            'voila' => 'Prendre une douche',
            'secondaire' => array(
                "jsp" => 123,
                "lol" => 'Ici c\'est paris',
                "pourquoi" => 'Ca fait beaucoup la non',
            ),
        ),
    ),

    array(
        'nom' => 'Pokemon',
        'prix' => 899,
        'taille' => 2,
        'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, rerum.',
    ),

    array(
        'nom' => 'Mario',
        'prix' => 526,
        'taille' => 45,
        'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, rerum.',
    ),

    array(
        'nom' => 'Resident Evil',
        'prix' => 1058,
        'taille' => 84,
        'resume' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, rerum.',
    ),

);

//////////////
/// 1 = Faire une liste en <ul> <li> qui affiche le nom, prix et resumé des jeux
/// En for et foreach
///
/// 2 = Faire la phrase suivante dans une section
/// <h1> Mario
/// <h4> qui coute 1058 € et qui a une taille de 580 go
/// <p> et qui a pour but de manger une pomme avec objectif secondaire
/// <h3> (en rouge) de prendre une douche.
///  <p> Ca fait beaucoup la non?

echo '<pre>';
print_r($jeux);
echo '</pre>';

echo '<pre>';
print_r($jeux);
echo '</pre>';

echo '<ul>';
foreach ($jeux as $jeu) {
    echo '<li>';
    echo $jeu['nom'];
    echo ' ';
    echo $jeu['prix'];
    echo '€ ';
    echo $jeu['resume'];
    echo '</li>';
}
echo '</ul>';

echo '<ul>';
for ($i = 0; $i <count($jeux); $i++) {
    echo '<li>'. $jeux[$i]['nom'] .' '. $jeux[$i]['prix'] .'€ '. $jeux[$i]['resume'] .'</li>';
}
echo '</section>';

echo '<h1>' .$jeux[2]['nom']. '</h1>';
echo '<h4> qui coute ' .$jeux[3]['prix'].' € et qui a une taille de '. $jeux[0]['taille'].' go '. '</h4>';
echo '<p> et qui a pour but de ' .$jeux[0]['achevements']['ici']. ' avec objectif secondaire</p>';
echo '<h3 style="color: red"> de ' .$jeux[0]['achevements']['voila']. '.</h3>';
echo '<p>' .$jeux[0]['achevements']['secondaire']['pourquoi'] . '</p>';


