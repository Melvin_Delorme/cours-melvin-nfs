<?php
require('inc/function.php');
require ('inc/data.php');

include('inc/header.php');?>

<section id="movies">
    <ul>
    <?php foreach ($movies as $movie) {
        echo '<li><a href="article-small.php?id=' . $movie['id'] . '">' . poster($movie) . '</a></li>';
    } ?>
    </ul>
</section>

<?php include('inc/footer.php');
