<?php
require('inc/function.php');
include('inc/data.php');

if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $goodfilm = array();
    foreach ($movies as $movie) {
        if ($id === $movie['id']) {
           $goodfilm = $movie;
        }
    }
    if(empty($goodfilm)) {
        die('404');
    }
} else {
    die('404');
}

include('inc/header.php'); ?>

<section id="movies">
    <div class="movies_img">
        <div class="movies_info">
            <h1><?php echo $goodfilm['title'] ?></h1>
            <p><?php echo $goodfilm['year'] ?></p>
            <p><?php echo $goodfilm['directors'] ?></p>
            <p><?php echo $goodfilm['rating'] ?></p>
        </div>
        <div class="poster">
            <?php echo poster($goodfilm) ?>
        </div>
    </div>


</section>

<?php include('inc/footer.php');