<?php
$url = 'https://www.php.net/';
$alt = 'Logo.php';
$width = 300;
$image = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/2560px-PHP-logo.svg.png';

// EXO
?>
<a href="<?php echo $url; ?>"><img width="<?php echo $width;?>px" src="<?php echo $image; ?>" alt="<?php echo $alt; ?>"></a>

<?php
echo '<a href="';
echo $url;
echo '">';
echo '<img width="';
echo $width;
echo 'px" src="';
echo $image;
echo '" alt="';
echo $alt;
echo '"></a>';

echo '<a href="'. $url .'"><img width="'. $width .'px" src="'. $image .'" alt="'. $alt .'"></a>';


// EXO 2
$title = 'Les fruit de bernard';
$content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus est facere illo necessitatibus nihil nostrum numquam odio quisquam repudiandae sed.';
$fruits = array('Banane', 'Kiwi', 'Papaye', 'Tomate', 'Courgette');
echo '<pre>';
print_r($fruits);
echo '</pre>';
// 3 methodes
// section id => fruit
    // h2 class title => $titre
    // p class content => $content
    // ul class "liste"
        // un li par fruit

// Methode 1
?>

<section id="fruit">
    <h2 class="title"><?php echo $title; ?></h2>
    <p class="content"><?php echo $content; ?></p>
    <ul class="liste">
        <li><?php echo $fruits[0]; ?></li>
        <li><?php echo $fruits[1]; ?></li>
        <li><?php echo $fruits[2]; ?></li>
        <li><?php echo $fruits[3]; ?></li>
        <li><?php echo $fruits[4]; ?></li>
    </ul>
</section>

<?php
// Methode 2

echo '<section id="fruit"><h2 class="title">';
echo $title;
echo '</h2><p class="content">';
echo $content;
echo '</p><ul class="liste"><li>';
echo $fruits[0];
echo '</li><li>';
echo $fruits[1];
echo '</li><li>';
echo $fruits[2];
echo '</li><li>';
echo $fruits[3];
echo '</li><li>';
echo $fruits[4];
echo '</li></ul></section>';

// Methode 3

echo '<section id="fruit">
    <h2 class="title">'. $title .'</h2>
<p class="content">'. $content .'</p>
<ul class="liste">
    <li>'. $fruits[0]. '</li>
    <li>'. $fruits[1]. '</li>
    <li>'. $fruits[2]. '</li>
    <li>'. $fruits[3]. '</li>
    <li>'. $fruits[3]. '</li>

</ul>
</section>';






