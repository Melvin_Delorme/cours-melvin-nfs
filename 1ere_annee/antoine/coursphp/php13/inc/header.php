<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="asset/css/style.css">
</head>
<body>

<header id="theheader">
<div class="wrap2">
    <div class="header_left">
        <a href="index.php"><img src="asset/img/header/logo-sfr.svg" alt=""></a>
    </div>
    <div class="header_right">
        <ul>
            <li><a href="#"><img src="asset/img/header/logo-menu1.svg" alt=""><p>Les profils</p></a></li>
            <li><a href="#"><img src="asset/img/header/logo-menu2.svg" alt=""><p>Les offres par region</p></a></li>
        </ul>
    </div>
</div>
</header>