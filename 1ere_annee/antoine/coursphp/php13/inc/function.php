<?php

function debug($tableau) {
    echo '<pre style="height:150px;overflow-y: scroll;font-size: .7rem;padding: .6rem;font-family: Consolas, Monospace; background-color: #000;color:#fff;">';
    print_r($tableau);
    echo '</pre>';
}

function cleanXss($key)
{
    return trim(strip_tags($_POST[$key]));
}


function validationText($err,$data,$keyError,$min,$max)
{
    if (!empty($data)) {
        if (mb_strlen($data) < $min) {
            $err[$keyError] = 'Veuillez renseigner plus de '.$min.' caractères';
        } elseif (mb_strlen($data) > $max) {
            $err[$keyError] = 'Veuillez renseigner moins de '.$max.' caractères';
        }
    } else {
        $err[$keyError] = 'Veuillez renseigner ce champ';
    }
    return $err;
}

function validationEmail($errors,$email,$entry = 'email')
{
    if (!empty($email)) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[$entry] = 'L\'email n\'est pas valide';
        }
    } else {
        $errors[$entry] = 'Ce champ est obligatoire';
    }
    return $errors;
}

function viewError($errors,$key)
{
    if(!empty($errors[$key])) {
        echo $errors[$key];
    }
}

function getPostValue($key, $data = '')
{
    if(!empty($_POST[$key]) ) {
        echo $_POST[$key];
    } elseif(!empty($data)) {
        echo $data;
    }
}

//function insertArticle($titre,$content,$auteur)
//{
//    global $pdo;
//    $sql = "INSERT INTO blog_articles (title, content, auteur,created_at,status)
//                VALUES (:titre,:contenu,:auteur,NOW(),'publish')";
//    $query = $pdo->prepare($sql);
//    $query->bindValue('titre', $titre, PDO::PARAM_STR);
//    $query->bindValue('contenu', $content ,PDO::PARAM_STR);
//    $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
//    $query->execute();
//    return $pdo->lastInsertId();
//}
//
//
//function updateArticle($id,$titre,$content,$auteur)
//{
//    global $pdo;
//    $sql = "UPDATE blog_articles SET title = :titre,content = :contenu,auteur = :auteur,modified_at = NOW() WHERE id = :id";
//    $query = $pdo->prepare($sql);
//    $query->bindValue('titre', $titre, PDO::PARAM_STR);
//    $query->bindValue('contenu', $content ,PDO::PARAM_STR);
//    $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
//    $query->bindValue('id',$id, PDO::PARAM_INT);
//    $query->execute();
//}
//
//function getAllPost() {
//    global $pdo;
//    $sql = "SELECT * FROM blog_articles";
//    $query = $pdo->prepare($sql);
//    $query->execute();
//    return $query->fetchAll();
//}
//
//
//function getAllPostByStatus($status = 'publish') {
//    global $pdo;
//    $sql = "SELECT * FROM blog_articles WHERE status = '$status' ORDER BY created_at DESC";
//    $query = $pdo->prepare($sql);
//    $query->execute();
//    return $query->fetchAll();
//}
//
//function getArticleById($id) {
//    global $pdo;
//    $sql = "SELECT * FROM blog_articles WHERE ID = :id";
//    $query = $pdo->prepare($sql);
//    $query->bindValue('id',$id, PDO::PARAM_INT);
//    $query->execute();
//    return $query->fetch();
//}