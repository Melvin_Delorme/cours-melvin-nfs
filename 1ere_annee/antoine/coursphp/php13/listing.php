<?php
require('inc/pdo.php');
require('inc/function.php');
$title = 'SFR - LISTING';

$sql = 'SELECT * FROM sfr_contact ORDER BY created_at DESC ';
$query = $pdo->prepare($sql);
$query->execute();
$contacts = $query->fetchAll();
include('inc/header.php'); ?>
    <section id="contact_offres">
        <div class="wrap">
            <img src="asset/img/image-fond.jpg" alt="">
        </div>
    </section>

    <section id="listing">
        <div class="wrap2">
            <?php foreach ($contacts as $contact) { ?>
                <div class="contact">
                    <a href="single.php?id=<?php echo $contact['id']; ?>">
                        <h2><?php echo $contact['email']; ?></h2>
                    </a>
                </div>
            <?php } ?>
        </div>
    </section>
<?php include('inc/footer.php');