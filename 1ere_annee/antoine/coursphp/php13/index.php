<?php
require('inc/pdo.php');
require('inc/function.php');
$title = 'SFR - HOME';
include('inc/header.php'); ?>
    <section id="home_promotion">
        <div class="wrap">
            <img src="asset/img/home/sfr-img-man-home.jpg" alt="">
        </div>
    </section>

    <section id="home_postes">
        <div class="wrap2">
            <h1>LES POSTES PAR REGION</h1>
            <p>Cliquez sur la carte pour découvrir les postes proposés par la localisation</p>
            <img src="asset/img/home/carte.svg" alt="">
        </div>
    </section>

    <section id="home_profile">
        <div class="wrap2">
            <h1>LES PROFILS</h1>
            <div class="profils_1">
                <div class="profils1_left">
                    <h2>COMMERCIAUX ITINERANTS H/F</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cum minima officiis quia quo reprehenderit ullam vel. Accusamus, adipisci amet asperiores aspernatur dolore dolores doloribus eaque esse facilis hic itaque iure laudantium libero mollitia nostrum numquam officia officiis perspiciatis possimus praesentium quod recusandae rerum saepe sequi sint sunt tempora velit. Adipisci aperiam et iusto maiores qui rem suscipit! Aliquam consequatur dolor ipsam ipsum molestiae sed. Ad adipisci asperiores aspernatur atque culpa, cum deleniti dignissimos dolor dolorum ducimus eos, esse et facilis id, impedit laborum laudantium maxime neque odit perspiciatis quisquam reprehenderit saepe soluta temporibus ut. A amet assumenda consequuntur cum enim esse fugit id illum iusto, labore laborum magni minima molestiae natus perspiciatis porro possimus, qui quia reprehenderit sequi suscipit vel velit vitae voluptate voluptatum! Atque blanditiis error maiores mollitia nobis? Dignissimos ipsa laboriosam praesentium recusandae? Animi atque cupiditate deserunt earum laboriosam laudantium maxime non pariatur recusandae vero? Esse, obcaecati!</p>
                </div>
                <div class="profils1_right">
                    <img src="asset/img/home/img-profil1.jpg" alt="">
                </div>
            </div>
            <div class="profils_2">
                <div class="profils2_left">
                    <img src="asset/img/home/img-profil2.jpg" alt="">
                </div>
                <div class="profils2_right">
                    <h2>INGENIEUR TECHNICO-COMMERCIAL H/F</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque cumque doloribus dolorum enim fugit illum labore laboriosam, maiores nobis pariatur quibusdam quos repellendus rerum sit veniam voluptas voluptatum! Adipisci amet consectetur, corporis cumque deserunt excepturi illo impedit ipsa modi molestias, neque omnis pariatur perferendis possimus quis soluta tempore voluptatem! Cumque expedita illo minima recusandae, sunt temporibus voluptatum. Aliquid dolore enim in pariatur quidem rerum saepe velit. Aliquid atque repudiandae suscipit? A aliquam aut, dicta dolor dolore earum fugit molestiae mollitia non numquam possimus quo reprehenderit, sed, sit ut? Asperiores commodi consequatur debitis deleniti eius eum excepturi exercitationem facilis harum in ipsum, itaque laborum molestiae molestias natus obcaecati omnis provident quas reprehenderit sequi similique, tenetur vel veritatis. Ab animi aut deserunt dolore dolorum explicabo fugiat impedit libero odit officia possimus quasi repellat sapiente, sunt velit. Consectetur dignissimos distinctio dolorem eius, eum expedita laudantium nesciunt nulla quos sit sunt tempora temporibus unde.</p>
                </div>
            </div>
        </div>
    </section>
<?php include('inc/footer.php');