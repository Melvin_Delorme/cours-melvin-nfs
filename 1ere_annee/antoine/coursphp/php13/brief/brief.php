<?php

// BRIEF EXAM BCI1 - HTML - CSS - PHP - SQL - n°1
// Vous avez 6h pour réaliser ce qui suit.
// Important :
// Votre examen devra m’être envoyé zippé (.zip) par mail ou we-transfert dans un dossier nommé avec votre "nom-prenom".
// Mon mail : quidelantoine@gmail.com
// Les examens reçus après 15h ne seront pas corrigés, ainsi que les dossiers sans votre nom-prenom.

/////////////////////////
// Partie 1 - Intégration HTML5/CSS3
/////////////////////////

// Réalisation d’une intégration responsive de 4 pages.

// Page d'accueil (index.php) => sfr-1-design-home.jpg
// Page contact (contact.php) => sfr-1-design-contact.jpg
// Page message (listing.php) => sfr-1-design-listing-message.jpg
// Page détail  (single.php)  => sfr-1-design-detail-message.jpg

///// HELPER
// -> Les images et les fonts nécessaires à la réalisation du site sont fournies. (css pour la font fourni à adapter à votre structure de dossier)
// -> La largeur max du site est de 1600px (comme les grandes images fournies), le contenu est cadré dans un wrap de 1140px max, la partie profil de la home dans un wrap de 976px, le formulaire est dans un wrap de 520px.
// -> Les éléments des menus du header et du footer se trouvent dans des liens qui changent d’état au survol (:hover).
// -> L’input de type submit "envoyer" doit aussi changer d’état au survol, soyez créatif.
// -> Le carré blanc sur la grande image de la page contact est un point BONUS. Essayer de le faire uniquement si tout le reste est terminé.
/////// COLOR
// rouge => #D90011
// noir  => #000000
// gris  => #D1D2D4
// Largeur du logo sfr => 170px
// Hauteur des icônes de la navigation => 50px
/////// FONT
/// font-family: 'sfrregular'; font-size: 16px; line-height: 1.3;font-weight: 400;

/////////////////////////
// Partie 2 - Développement PHP/SQL
/////////////////////////

//// Page contact (contact.php) => sfr-1-design-contact.jpg

// 	-> Le formulaire doit être traité et sécurisé par PHP.
// 	-> Vous devez vérifier et faire afficher les erreurs si elles existent en dessous de chaque champ correspondant en rouge.
// 		-> Mail renseigné et valide
// 		-> Message renseigné, min 5 caractères et max 2000 caractères. (textarea)
// 	-> Si aucune erreur, vous devez insérer dans une table contact les données reçues et ne plus faire afficher le formulaire, mais un message de succès en vert et centré.
// 			-> id (INT auto-increment Primary Key)
// 			-> email (VARCHAR 150)
// 			-> message(TEXT)
// 			-> created_at (DATETIME)
// 	-> Vous devez exporter votre table (.sql) et la joindre à votre examen pour correction. Dans un dossier "sql")

//// Page message (listing.php) => sfr-1-design-listing-message.jpg

// Ajouter un lien "Message" dans footer pour aller dans la liste des messages.
// Ici vous devez faire une request pour aller chercher tous les messages du plus récent au plus ancien. Seul l'email sera afficher, en cliquant sur l'email vous devez diriger l'utilisateur vers la page single

//// Page détail  (single.php)  => sfr-1-design-detail-message.jpg

// Cette page affiche le détail du message.
// Pour afficher un contenu avec les retours à la ligne provenant d'un textarea, utiliser la fonction nl2br()
// Afficher la date du message (created_at) au format (d/m/Y à H:i:s)

// CONSEIL
// Commencer par l'intégration de la page d'accueil.
// Puis développement (php) des pages contact, listing & detail
// Puis Terminer l'intégration de ces pages
