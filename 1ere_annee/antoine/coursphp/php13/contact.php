<?php
require('inc/pdo.php');
require('inc/function.php');
$title = 'SFR - CONTACT';

$errors = array();
$success = false;

//email, message

if(!empty($_POST['submitted'])){
    // Failles Xss
    $email = cleanXss('email');
    $message = cleanXss('message');
    // Validations
    $errors = validationEmail($errors,$email,'email');
    $errors = validationText($errors,$message,'message',2,200);

    // SQL
    if (count($errors) == 0) {
        $sql = "INSERT INTO sfr_contact (email,message,created_at)
                VALUES (:email,:message,NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->bindValue('message', $message);
        $query->execute();
        $success = true;
        header('Location: listing.php');
    }
}

include('inc/header.php'); ?>
    <section id="contact_offres">
        <div class="wrap">
            <img src="asset/img/image-fond.jpg" alt="">
            <div class="offre_title">
                <h2>LES OFFRES</h2>
                <h2>PAR REGION</h2>
            </div>
        </div>
    </section>

    <section id="contact_formulaire">
        <div class="wrap3">
            <form action="" method="post" novalidate class="wrap">
                <input class="email" type="email" id="email" name="email" placeholder="votre email" value="<?php if(!empty($_POST['email'])) {echo $_POST['email'];} ?>">
                <span class="error"><?php if(!empty($errors['email'])) {echo $errors['email'];} ?></span>

                <textarea class="message" type="text" id="message" name="message" placeholder="votre message" ><?php if(!empty($_POST['message'])) {echo $_POST['message'];} ?></textarea>
                <span class="error"><?php if(!empty($errors['message'])) {echo $errors['message'];} ?></span>

                <input type="submit" class="submit" name="submitted" value="Envoyer">
            </form>

        </div>
    </section>
<?php include('inc/footer.php');