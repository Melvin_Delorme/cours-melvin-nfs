<?php
require('inc/pdo.php');
require('inc/function.php');
$title = 'SFR - SINGLE';

if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];

    $sql = "SELECT * FROM sfr_contact WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    $contact = $query->fetch();

    if(empty($contact)) {
        die('404');
    }
} else {
    die('404');
}
include('inc/header.php'); ?>
    <section id="contact_offres">
        <div class="wrap">
            <img src="asset/img/image-fond.jpg" alt="">
        </div>
    </section>

    <section id="single">
        <div class="wrap2">
            <h1><?php echo $contact['email']; ?></h1>
            <p><?php echo nl2br($contact['message']); ?></p>
            <p class="created_at"><?php echo $contact['created_at']; ?></p>
        </div>
    </section>
<?php include('inc/footer.php');