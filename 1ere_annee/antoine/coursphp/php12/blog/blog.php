<?php
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
//require('inc/validation.php');

// PHP
$articles = getAllPostByStatus('publish');

include('inc/header.php'); ?>
<?php foreach ($articles as $article) { ?>
    <section id="blog">
        <div class="wrap">
            <h2><?= $article['title']; ?></h2>
            <p>Auteur: <?= $article['auteur']; ?></p>
            <a href="single.php?id=<?= $article['id']; ?>">Voir détail</a>
        </div>
    </section>
<?php } ?>

<?php include('inc/footer.php');
