<?php
require('../inc/pdo.php');
require('../inc/function.php');
require('../inc/request.php');


if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $post = getPostById($id);
    if (empty($post)) {
        die('404');
    } else {
        // UPDATE
        $sql = "UPDATE blog_articles SET status = 'draft' WHERE id = :id";
        $query = $pdo->prepare($sql);
        $query->bindValue('id', $id, PDO::PARAM_STR);
        $query->execute();
        header('Location: index.php');
    }
} else {
    die('404');
}