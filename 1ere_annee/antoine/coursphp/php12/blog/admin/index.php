<?php

require('../inc/pdo.php');
require('../inc/function.php');
require('../inc/request.php');

// request
$sql = 'SELECT * FROM blog_articles ORDER BY title ASC';
$query = $pdo->prepare($sql);
$query->execute();
$posts = $query->fetchAll();

include('inc/header-back.php'); ?>

    <h1>Home</h1>
    <section id="blog">
        <?php foreach ($posts as $post) { ?>
            <div class="posts">
                <h2><?php echo $post['title']; ?></h2>
                <p><?php echo $post['content']; ?></p>
                <p><?php echo $post['auteur']; ?></p>
                <p><?php $date =date_create($post['created_at']);
                    echo date_format($date,"d/m/y");?></p>
                <a href="../detail.php?id=<?php echo $post['id']; ?>">Voir plus</a>
                <p><a href="edit.php?id=<?php echo $post['id']; ?>">Editer </a></p>
                <?php if ($post['status'] == 'publish') { ?>
                    <p><a href="delete.php?id=<?php echo $post['id']; ?>">Effacer la ville du blog</a></p>
                <?php }?>
                <p><a href="hard-delete.php?id=<?php echo $post['id']; ?>">Effacer la ville definitivement</a></p>
            </div>
        <?php } ?>
    </section>

<?php include('inc/footer-back.php');