<?php

require('../inc/pdo.php');
require('../inc/function.php');
require('../inc/request.php');
require('../inc/validation.php');
$errors = array();
$sucess = false;

if(!empty($_POST['submitted'])) {

    //XSS
    $title = cleanXss('title');
    $content = cleanXss('content');
    $auteur = cleanXss('auteur');

    // Validation
    $errors = validationText($errors,$title, 'title',2,255);
    $errors = validationText($errors,$content, 'content',2,500);
    $errors = validationText($errors,$auteur, 'auteur',2,25);

    // if no error
    if (count($errors) == 0) {
        // INSERT INTO
        $sql = "INSERT INTO blog_articles (title, content, auteur,created_at,status) 
                VALUES (:title,:contenu,:auteur,NOW(),'publish')";
        $query = $pdo->prepare($sql);
        $query->bindValue('title', $title, PDO::PARAM_STR);
        $query->bindValue('contenu', $content ,PDO::PARAM_STR);
        $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
        $query->execute();
        $sucess = true;
        header('Location: index.php');
    }
}

include('inc/header-back.php'); ?>
    <h1>New Post</h1>

    <form action="" method="post" novalidate>
        <label for="title">Titre</label>
        <input type="text" id="title" name="title" value="<?php if(!empty($_POST['title'])) {echo $_POST['title'];} ?>">
        <span class="error"><?php if(!empty($errors['title'])) {echo $errors['title'];} ?></span>

        <label for="content">Contenu</label>
        <textarea type="text" id="content" name="content"><?php if(!empty($_POST['content'])) {echo $_POST['content'];} ?></textarea>
        <span class="error"><?php if(!empty($errors['content'])) {echo $errors['content'];} ?></span>

        <label for="auteur">Auteur</label>
        <input type="text" id="auteur" name="auteur" value="<?php if(!empty($_POST['auteur'])) {echo $_POST['auteur'];} ?>">
        <span class="error"><?php if(!empty($errors['auteur'])) {echo $errors['auteur'];} ?></span>

        <input type="submit" name="submitted" value="Ajouter une publication">
    </form>

<?php include('inc/footer-back.php');