<?php
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');

if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $post = getPostById($id);
    if(empty($post)) {
        die('404');
    }
} else {
    die('404');
}

/////////////////////////////////////////////////////////////
/// Comment
////////////////////////////////////////////////////////////

$errors = array();
$sucess = false;

if(!empty($_POST['submitted'])) {

    //XSS
    $content = cleanXss('content');
    $auteur = cleanXss('auteur');

    // Validation
    $errors = validationText($errors,$content, 'content',2,255);
    $errors = validationText($errors,$auteur, 'auteur',2,25);

    // if no error
    if (count($errors) == 0) {
        // INSERT INTO
        $sql = "INSERT INTO blog_comments (id_article, content, auteur,created_at,status) 
                VALUES (:id,:content,:auteur,NOW(),'new')";
        $query = $pdo->prepare($sql);
        $query->bindValue('id', $id, PDO::PARAM_STR);
        $query->bindValue('content', $content, PDO::PARAM_STR);
        $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
        $query->execute();
        $sucess = true;
    }
}

///////////////////////////////////////////
/// get comment
//////////////////////////////////////////
///
// request
$sql = "SELECT * FROM blog_comments WHERE id_article = $id ";
$query = $pdo->prepare($sql);
$query->execute();
$comments = $query->fetchAll();

include('inc/header.php'); ?>

    <div class="post">
        <h2><?php echo $post['title']; ?></h2>
        <p><?php echo $post['content']; ?></p>
        <p><?php echo $post['auteur']; ?></p>
        <p><?php echo $post['created_at']; ?></p>
    </div>

    <div class="comment">
        <h1>Ajouter un commentaire</h1>
        <form action="" method="post" novalidate>
            <label for="content">Contenu</label>
            <textarea type="text" id="content" name="content"></textarea>
            <span class="error"><?php if(!empty($errors['content'])) {echo $errors['content'];} ?></span>

            <label for="auteur">Auteur</label>
            <input type="text" id="auteur" name="auteur" value="">
            <span class="error"><?php if(!empty($errors['auteur'])) {echo $errors['auteur'];} ?></span>

            <input type="submit" name="submitted" value="Ajouter un commentaire">
        </form>
    </div>

            <div class="comments_all">
                <?php foreach ($comments as $comment) { ?>
                    <p><?php echo $comment['content']; ?></p>
                    <p><?php echo $comment['auteur']; ?></p>
                    <br>
                <?php } ?>
            </div>


<?php include('inc/footer.php');