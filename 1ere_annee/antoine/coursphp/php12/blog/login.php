<?php
session_start();
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');
$errors = [];

if (!empty($_POST['submitted'])) {
    $login = cleanXss('login');
    $password = cleanXss('password');
    $sql = "SELECT * FROM blog_users WHERE pseudo = :log OR email = :log";
    $query = $pdo->prepare($sql);
    $query->bindValue('log', $login, PDO::PARAM_STR);
    $query->execute();
    $user = $query->fetch();
    debug($user);
    if (!empty($user)) {
        if (password_verify($password, $user['password'])) {
            $_SESSION['user'] = array(
                'id' => $user['id'],
                'pseudo' => $user['pseudo'],
                'email' => $user['email'],
                'role' => $user['role'],
                'ip' => $_SERVER['REMOTE_ADDR']
            );
            header('Location: index.php');
        }else {
            $errors['login'] = 'Credentials';
        }
    } else {
        $errors['login'] = 'Credentials';
    }
}

include('inc/header.php'); ?>

    <section id="blog">
        <h1>Login</h1>
        <div class="wrap">
            <form action="" method="post" novalidate>
                <label for="login">Pseudo of Email</label>
                <input type="text" id="login" name="login" value="<?php getPostValue('login'); ?>">
                <span class="error"><?php viewError($errors, 'login'); ?></span>

                <label for="password">Mot de passe</label>
                <input type="password" id="password" name="password" value="">

                <input type="submit" name="submitted" value="Connectez-vous">
            </form>
        </div>
    </section>

<?php include('inc/footer.php');