<?php
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');
$success = false;
$errors = [];

// If form soumis
if(!empty($_POST['submitted'])) {
    // Faille XSS
    $pseudo = cleanXss('pseudo');
    $email = cleanXss('mail');
    $password = cleanXss('password');
    $password2 = cleanXss('password2');
    // Validation
    // pseudo min 3, max 140, renseigné.
    $errors = validationText($errors, $pseudo, 'pseudo', 3, 140);
    $errors = validationEmail($errors, $email, 'mail');
    // pseudo unique.
    if(empty($errors['pseudo'])) {
        $sql = "SELECT pseudo FROM blog_users WHERE pseudo = :pseudo";

        $query = $pdo->prepare($sql);
        $query->bindValue('pseudo', $pseudo, PDO::PARAM_STR);
        $query->execute();
        $verifiUserPseudo = $query->fetch();
        if (!empty($verifiUserPseudo)) {
            $errors['pseudo'] = 'Pseudo déja pris';
        }
    }
    // email => valid et renseigné,
    if(empty($errors['mail'])) {
        $sql = "SELECT email FROM blog_users WHERE email = :email";

        $query = $pdo->prepare($sql);
        $query->bindValue('mail', $email, PDO::PARAM_STR);
        $query->execute();
        $verifiUserEmail = $query->fetch();
        // email unique
        if (!empty($verifiUserEmail)) {
            $errors['mail'] = 'Email déja pris';
        }
    }

    // password
    if (!empty($password) && !empty($password2)) {
        if ($password != $password2) {
            $errors['password'] = 'vos mots de passe sont differents';
        } elseif (mb_strlen($password) < 6) {
            $errors['password'] = 'votre mdp est trop court';
        }
    } else {
        $errors['password'] = 'Veuillez renseigner les mdp';
    }
    if (count($errors) == 0) {
        $hashPassword = password_hash($password, PASSWORD_DEFAULT);
        $role = 'abonne';
        $token = generateRandomString(80);
        $sql = "INSERT INTO blog_users (pseudo,email,password,created_at,role,token)
                VALUES (:pseudo,:email,:pass, NOW(), '$role', '$token')";
        $query = $pdo->prepare($sql);
        $query->bindValue('pseudo', $pseudo,PDO::PARAM_STR);
        $query->bindValue('email', $email, PDO::PARAM_STR);
        $query->bindValue('pass', $hashPassword, PDO::PARAM_STR);
        $query->execute();
        $success = true;
        header('Location: login.php');

    }
}
debug($_POST);
debug($errors);


include('inc/header.php'); ?>

    <section id="blog">
        <h1>Register</h1>
        <div class="wrap">
            <form action="" method="post" novalidate class="wrapform">

                <label for="pseudo">Pseudo</label>
                <input type="text" id="pseudo" name="pseudo" value="<?php getPostValue('pseudo'); ?>">
                <span class="error"><?php viewError($errors, 'pseudo'); ?></span>

                <label for="mail">E-Mail</label>
                <input type="email" id="mail" name="mail" value="<?php getPostValue('mail'); ?>">
                <span class="error"><?php viewError($errors, 'mail'); ?></span>

                <label for="password">Mot de passe</label>
                <input type="password" id="password" name="password" value="">
                <span class="error"><?php viewError($errors, 'password'); ?></span>

                <label for="password2">Confirmez votre mot de passe</label>
                <input type="password" id="password2" name="password2" value="">

                <input type="submit" name="submitted" value="Inscrivez-vous">

            </form>
        </div>
    </section>

<?php include('inc/footer.php');