<?php
// Tableau Simple
$etudiants = array('Christophe','Malo','Claude','Kevin','Melvin','Axel');
// print_r
echo '<pre>';
print_r($etudiants);
echo '</pre>';
// affichez le 4eme etudiant
echo $etudiants[3];
// ajoutez un element au tableau
$etudiants[] = 'Melina';
$etudiants[] = 'Lucas';
echo '<pre>';
print_r($etudiants);
echo '</pre>';
// Modifiez les deux etudiants pour avoir leur prenom et le nom
$etudiants[3] .= ' nom de kevin';
echo '<pre>';
print_r($etudiants);
echo '</pre>';
// for // foreach

//1
echo '<section id="etudiants">';
for ($i = 0; $i <count($etudiants); $i++) {
    echo '<div class="etudiants"><h2>'. $etudiants[$i] .'</h2></div>';
}
echo '</section>';

////////////////////////////////////////////////////////////////////////

//2
echo '<ul>';
foreach ($etudiants as $etudiant) {
    echo '<li>';
    echo $etudiant;
    echo '</li>';
}
echo '</ul>';

// Construisez un tableau $nums contenant les nombres de 10 a 20 grace a une boucle for
$nums =  array();
echo '<section id="nums">';
for ($i = 10; $i <=20; $i++) {
    $nums[] = $i;
}
print_r($nums);
echo '</section>';

///////////////////////
// Tableau Assiociatif
//////////////////////
$livre = array(
    'title' => 'Le rouge et le noir',
    'annee' => 1830,
    'pages' => 640,
    'auteur' => 'Stendal'
);
echo '<pre>';
print_r($livre);
echo '</pre>';
echo '<h1>' . $livre['title'] . '</h1>';
echo '<p>' . $livre['annee'] . '</p>';
//Corrigez le nom de l'auteur
$livre['auteur']= 'Stendhal';
//Ajoutez un code isbn à votre livre 6466876
$livre['isbn']= 6466876;
$livre['prix']= 5.50;

unset($livre['annee']);

echo '<pre>';
print_r($livre);
echo '</pre>';
foreach ($livre as $key => $value) {
    echo '<p> ' . ucfirst($key) . ' : ' . $value . '</p>';
}

// tableau $me vide
$me = array();
// Ajoutez votre nom, prenom et age
$me['nom'] = 'Delorme';
$me['prenom'] = 'Melvin';
$me['age'] = 19;
// Affichez votre age a partir du tableau $me
echo $me['age'];
echo '<br>';
// Faites vous vieillir de 5 ans
$me['age'] += 5;
echo $me['age'];

// creer un tableau $mess
$mess = array();
// Ajouter un entier
$mess[] = 65543;
// Ajouter une valeur boolean
$mess[] = true;
// ajouter le tableau $me
$mess[] = $me;
// ajouter le tableau $etudiants
$mess[] = $etudiants;
// ajouter le livre
$mess = [$livre];
echo '<pre>';
print_r($mess);
echo '</pre>';

echo '<pre>';
print_r($mess[4]['auteur']);
echo '</pre>';