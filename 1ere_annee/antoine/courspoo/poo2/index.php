<?php

require_once('Class/ChaineMaster.php');

$chaine = new ChaineMaster('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet asperiores at autem consequatur corporis, cumque dolor doloribus ea error harum id ipsum iusto magnam maxime molestias mollitia nam natus nesciunt non numquam, odit officia pariatur quia quo repellendus reprehenderit similique sit soluta sunt totam velit veniam voluptatem. Ab amet, animi dolores doloribus eum fuga incidunt ipsum magni, natus quia quo saepe similique sint sunt totam ullam veniam veritatis. A ab alias aliquam, amet asperiores commodi corporis culpa eligendi et id, impedit in incidunt itaque iusto laudantium minima modi, nam odio placeat recusandae sequi sint soluta temporibus vel veritatis voluptatem.');

// Mettre en gras le texte
echo $chaine->gras();
// Mettre en italic le texte
echo $chaine->italic();
// Souligné le texte
echo $chaine->souligne();
// Mettre en majuscule
echo $chaine->majuscule();