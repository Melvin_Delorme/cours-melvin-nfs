<?php


/**
 *
 */
class ChaineMaster {
    /**
     * @var string
     */
    private $str;

    /**
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->str = $string;
    }

    /**
     * @return string
     */
    public function gras() : string
    {
        return '<strong>'.$this->str.'</strong>';
    }

    /**
     * @return string
     */
    public function italic() : string
    {
        return '<p><em>'.$this->str.'</em></p>';
    }

    /**
     * @return string
     */
    public function souligne() : string
    {
        return '<p><u>'.$this->str.'</u></p>';
    }

    /**
     * @return string
     */
    public function majuscule() : string
    {
        return '<p>'.mb_strtoupper($this->str).'</p>';
    }

}