<?php

// Héritage

 class Maman
 {
     protected $attribut = 'Bonjour';

     public function method1() : string
     {
         $str = $this->attribut;
         $str .= ' , je suis la Class Maman';
         return $str;
     }
 }

 $maman = new Maman();
 echo $maman->method1();

 class Fille extends Maman
 {
     // Surcharger une methode
     public function method1() : string
     {
         $str = $this->attribut;
         $str .= ' , je suis la Class Maman';
         return $str;
     }
     public function method2() :string
     {
         $str = $this->attribut;
         $str .= ' , je suis la Class Fille';
         return $str;
     }
 }

 $fille = new Fille();
 echo $fille->method1();
 echo $fille->method2();

// Interface

interface renderInterface {
    public function render($tag);
}
interface renderInterface2 {
    public function render2($tag);
    public function render3($tag);
}

class Dada implements renderInterface, renderInterface2 {
    public function render($tag) {
    }
    public function render2($tag) {
    }
    public function render3($tag) {
    }
}

// Static
class Debugger {
    public static function debug($tab) {
        echo '<pre>';
        print_r($tab);
        echo '</pre>';
    }
}
$fruits = array('Banane');
Debugger::debug($fruits);

class Maison {
    public $couleur = 'white';
    const HAUTEUR = 100;
    public static $espaceTerrain = 600;
    private static $nbrePiece = 12;

    public static function getNbrePiece() {
        return self::$nbrePiece;
    }

}
echo Maison::HAUTEUR;
echo '<hr>';
echo Maison::$espaceTerrain;
echo '</hr>';
echo Maison::getNbrePiece();

$maison = new Maison();
echo $maison->couleur;


// Class abstract, c'est une class que l'on ne peux pas instancier

abstract class Dodo {
    public function bonjour() {
        return 'hello world!';
    }
}

class Dudu extends Dodo {
    use Gogo, Gugu;
}

$dudu = new Dudu();
echo $dudu->bonjour();
echo $dudu->aurevoir();
echo $dudu->aurevoir2();

// Trait
trait Gogo {
    public function aurevoir() {
        return 'Bye bye world!';
    }
}

trait Gugu {
    public function aurevoir2() {
        return 'Bye bye world!';
    }
}

