<?php
require_once('Class/Form.php');
require_once('Class/Validation.php');

$errors = array();

if(!empty($_POST['submitted'])) {
    // Faille XSS
    $nom = trim(strip_tags($_POST['nom']));
    $prenom = trim(strip_tags($_POST['prenom']));
    // Validation
    $v = new Validation($errors);
    $v->validText('nom', $nom, 3, 60);
    $v->validText('prenom', $prenom, 3, 60);
    if($v->isValid()) {
        die('ok');
    }
    $errors = $v->getErrors();
}

$form = new Form($errors);

require('inc/header.php'); ?>
    <form action="" method="post">
        <?php echo $form->label('nom'); ?>
        <?php echo $form->input('nom'); ?>
        <?php echo $form->error('nom'); ?>

        <?php echo $form->widget('prenom'); // réunion de label + input + error ?>

        <?php echo $form->label('message'); ?>
        <?php echo $form->textarea('message'); ?>
        <?php echo $form->error('message'); ?>

        <?php echo $form->label('email'); ?>
        <?php echo $form->input('email', 'email'); ?>
        <?php echo $form->error('email'); ?>

        <?php echo $form->submit('Envoyer'); ?>
    </form>
<?php require('inc/footer.php');

