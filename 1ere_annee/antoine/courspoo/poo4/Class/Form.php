<?php

/**
 *
 */
class Form {

    /**
     * @var array
     */
    private array $errors;
    /**
     * @var array
     */
    private array $post;

    /**
     * @param array $errors
     * @param $method
     */
    public function __construct(array $errors, $method = 'post')
    {
        $this->errors = $errors;
        if($method === 'post') {
            $this->post = $_POST;
        } else {
            $this->post = $_GET;
        }
    }

    /**
     * @param string $name
     * @param $title
     * @return string
     */
    public function label(string $name, $title = '') : string
    {
        $theTitle = !empty($title) ? $title : ucfirst($name);
        return '<label for="'.$name.'">'.$theTitle.'</label>';
    }

    /**
     * @param string $name
     * @param string $type
     * @return string
     */
    public function input(string $name, string $type = 'text') : string
    {
        return '<input type="'.$type.'" name="'.$name.'" id="'.$name.'" value="'.$this->getValue($name).'">';
    }

    /**
     * @param string $name
     * @return string
     */
    public function textarea(string $name) : string
    {
        return '<textarea name="'.$name.'">'.$this->getValue($name).'</textarea>';
    }

    /**
     * @param string $value
     * @param string $name
     * @return string
     */
    public function submit(string $value = 'Envoyer', string $name = 'submitted') : string
    {
            return '<input type="submit" name="'.$name.'" value="'.$value.'">';
    }

    /**
     * @param $key
     * @return string
     */
    public function error($key) : string {
        if(!empty($this->errors[$key])) {
            return '<span class="error">'.$this->errors[$key].'</span>';
        }
        return '';
    }

    /**
     * @param string $name
     * @return string
     */
    public function widget(string $name) : string
    {
        $html = $this->label($name);
        $html .= $this->input($name);
        $html .= $this->error($name);
        return $html;
    }

    /**
     * @param string $name
     * @return string
     */
    private function getValue(string $name) : string {
        $value = '';
        if(!empty($this->post[$name])) {
            $value = $this->post[$name];
        }
        return $value;
    }

}