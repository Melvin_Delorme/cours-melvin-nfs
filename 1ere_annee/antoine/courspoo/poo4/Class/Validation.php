<?php


/**
 *
 */
class Validation
{
    /**
     * @var array
     */
    private array $errors;

    /**
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if(count($this->errors) === 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $keyError
     * @param $data
     * @param $min
     * @param $max
     * @return void
     */
    public function validText($keyError, $data, $min, $max)
    {
        if (!empty($data)) {
            if (mb_strlen($data) < $min) {
                $this->errors[$keyError] = 'Veuillez renseigner plus de '.$min.' caractères';
            } elseif (mb_strlen($data) > $max) {
                $this->errors[$keyError] = 'Veuillez renseigner moins de '.$max.' caractères';
            }
        } else {
            $this->errors[$keyError] = 'Veuillez renseigner ce champ';
        }
    }
}