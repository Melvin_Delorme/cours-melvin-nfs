<?php

///////////
// POO
///////////

// Déclaration d'un Object User
class User {
    // propriétés
    public $name;
    public $email;
    public $password;

}
// INSTANCIER la Class (instanciation)
$user1 = new User();
// Hydrater notre Object
$user1->name = 'Michel';
$user1->email = 'quidelantoine@gmail.com';
$user1->password = 'kjhjkhl56fdgh4g6df78';

echo '<pre>';
print_r($user1);
echo '</pre>';

echo $user1->email;
// création d'un nouveau User
$user2 = new User();
$user2->name = 'Bernard';
$user2->email = 'Bernard@gmail.fr';
$user2->password = 'gfhfghh5456';
echo '<pre>';
print_r($user2);
echo '</pre>';

echo '<p>'.$user2->name.'</p>'; // name de user2
echo '<p>'.$user1->password.'</p>';  // name de user1



class Voiture {
    private $marque;
    public $vitesse = 0;
    public $place = 4;
    public $puissance;
    private $color;

    // constructeur
    public function __construct($marque = 'Renault',$pui = 10) {
        echo 'Je viens de faire l\'instance de object de la voiture ' . $marque;
        $this->marque = $marque;
        $this->puissance = $pui;
    }

    public function accelerer(int $vit = 10) : void
    {
        $this->vitesse += $vit;
    }

    public function freiner(int $vit = 10) : void
    {
        $this->vitesse -= $vit;
        if($this->vitesse < 0) {
            //$this->vitesse = 0;
            $this->setVitesse(0);
        }
    }


    public function generateHtml() {
        $html = '<div class="one_voiture">';
            $html .= '<h2>'.$this->getMarque().'</h2>';
            $html .= '<p>Vitesse: '.$this->vitesse.'</p>';
            $html .= '<p>Place: '.$this->place.'</p>';
            $html .= '<p>Puissance: '.$this->puissance.'</p>';
            if(!empty($this->color)) {
                $html .= '<p>Couleur: '.$this->color.'</p>';
            }
        $html .= '</div>';
        return $html;
    }


    // getter & setter
    // getter marque
    public function getMarque() : string {
        return $this->marque;
    }
    // setter marque
    public function setMarque(string $marque) : void {
        $this->marque = $marque;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getVitesse(): int
    {
        return $this->vitesse;
    }

    /**
     * @param int $vitesse
     */
    public function setVitesse(int $vitesse): void
    {
        $this->vitesse = $vitesse;
    }
}
$voiture1 = new Voiture('Punto', 20);
//$voiture1->marque = 'Punto';
//echo '<strong>' . $voiture1->marque . '</strong>';
$voiture1->setMarque('BMW');
$voiture1->setColor('lime');

echo '<strong>' . $voiture1->getMarque() . '</strong>';
echo '<strong>' . $voiture1->getColor() . '</strong>';

echo '<pre>';
print_r($voiture1);
echo '</pre>';
$voiture2 = new Voiture('Ferrari',600);
$voiture3 = new Voiture();

// Course
$voiture1->accelerer(30); // ajouter un nombre de km/h à la vitesse
$voiture2->accelerer();
$voiture3->accelerer(35);

// Virage
$voiture1->freiner(12);

echo $voiture1->vitesse;

// div => class "one_voiture
    // h2  -> marque
    // p x 4   -> couleur, vitesse , puissance, place


echo $voiture1->generateHtml();
echo $voiture2->generateHtml();
echo $voiture3->generateHtml();