<?php

// Création de classes utiles à une animalerie en ligne

// src   => Animalerie

// Les classes
// Item   => correspond à tous les produits en ventes
// Animal => animaux en vente
// Food   => nourriture en vente

// Item :
//  price: le prix de vente
//  name: le nom du produit

// Les class Animal et Food elles doivent étendre Item


// Animal
// birthday: date de naissance de l'animal, au format Y-m-d
// weight: poids de l'animal


// Food
// brand: marque de la nourriture
// type: croquette, paté, graine etc ....
// expiry: date de péremption.

// création des classes
// index.php => instancier un Animal et une Food avec tous les propriétés hydrater
// print_r de chacun
// ajouter une method showPriceATI, à la class Item, afficher le prix ttc à l'ecran
// ajouter une method showExpiry, à la class Food, afficher la date de peremption au format jj/mm/YYYY
// ajouter une method showAge, à la class Animal.


// La suite ....

// Créez une nouvelle Cart
// items = array()
// total_price

// method => addItem(Object)
// method => showAll() => afficher le pdetails de tous les produits du panier