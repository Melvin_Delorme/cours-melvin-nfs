<?php

namespace Src;

class Animal extends Item {

    protected $birthday;
    protected $weight;

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    public function showAge()
    {
        $dateNaissance = $this->birthday;
        $aujourdhui = date("Y-m-d");
        $diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
        return $diff->format('%y');
    }

}