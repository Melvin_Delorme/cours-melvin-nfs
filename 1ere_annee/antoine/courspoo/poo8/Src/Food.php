<?php

namespace Src;

class Food extends Item {

    protected $brand;
    protected $type;
    protected $expiry;

    /**
     * @param mixed $brand
     */
    public function setBrand($brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @param mixed $expiry
     */
    public function setExpiry($expiry): void
    {
        $this->expiry = $expiry;
    }

    public function showExpiry()
    {
        return date('j,m,Y', strtotime($this->expiry));
    }

}