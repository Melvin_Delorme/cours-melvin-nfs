<?php

namespace Src;

class Item {

    protected $price;
    protected $name;

    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    public function showPriceATI()
    {
        return $this->price * 1.2;
    }

}