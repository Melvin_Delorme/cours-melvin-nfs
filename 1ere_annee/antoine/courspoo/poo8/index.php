<?php

spl_autoload_register();

use Src\Animal;
use Src\Item;
use Src\Food;

$animal1 = new Animal('Samba', 2000);
$animal1->setBirthday('2023-11-04');
$animal1->setWeight(26);
$food1 = new Food('Croquette master plus', 12);
$food1->setType('Croquette');
$food1->setBrand('Purina');
$food1->setExpiry('2012-12-12');

echo '<pre>';
print_r($animal1);
print_r($food1);
echo '</pre>';

echo $animal1->showPriceATI();
echo $food1->showPriceATI();
echo $food1->showExpiry();
echo $animal1->showAge();
