<?php

/**
 *
 */
class Commande {
    /**
     * @var int
     */
    private $menudujour = 11;
    /**
     * @var int
     */
    private $menuenfant = 8;
    /**
     * @var float
     */
    private $cafe = 1.8;
    /**
     * @var int[]
     */
    private $pizzas = array(
        'calzone'   => 9,
        'orientale' => 10,
        'fromage'   => 11,
        'campione'   => 7,
    );
    /**
     * @var string
     */
    private $nom;
    /**
     * @var int
     */
    private $nbreCouvert = 0;
    /**
     * @var array
     */
    private $panier = array();

    /**
     * @param string $name
     */
    public function __construct(string $name = 'no name')
    {
        $this->nom = $name;
        $this->panier = array(
            'menudujour' => 0,
            'menuenfant' => 0,
            'cafe'       => 0,
            'pizzas'     => array(
                'calzone'   => 0,
                'orientale' => 0,
                'fromage'   => 0,
                'campione'   => 0,
            )
        );
    }

    /**
     * @return void
     */
    public function debugPanier() {
        echo '<pre>';
        print_r($this->panier);
        print_r($this->nbreCouvert);
        echo '</pre>';
    }

    /**
     * @param int $nb
     * @return void
     */
    public function addCafe(int $nb = 1) : void
    {
        $this->panier['cafe'] += $nb;
    }

    /**
     * @param int $nb
     * @return void
     */
    public function addMenuEnfant(int $nb = 1) : void
    {
        $this->panier['menuenfant'] += $nb;
        $this->nbreCouvert += $nb;
    }

    /**
     * @param int $nb
     * @return void
     */
    public function addMenuJour(int $nb = 1) : void
    {
        $this->panier['menudujour'] += $nb;
        $this->nbreCouvert += $nb;
    }

    /**
     * @param string $type
     * @param int $nb
     * @return void
     */
    public function addPizza(string $type, int $nb = 1) : void
    {
        if(in_array($type, $this->getAllKeyPizzas())) {
            $this->panier['pizzas'][$type] += $nb;
            $this->nbreCouvert += $nb;
        }
    }

    /**
     * @return array
     */
    private function getAllKeyPizzas() : array {
        $keypizza = array();
        foreach($this->pizzas as $key => $value) {
            $keypizza[] = $key;
        }
        return $keypizza;
    }

    /**
     * @return float
     */
    private function calculTotal() : float
    {
        $total = 0;
        $total += $this->panier['cafe'] * $this->cafe;
        $total += $this->panier['menudujour'] * $this->menudujour;
        $total += $this->panier['menuenfant'] * $this->menuenfant;
        foreach($this->getAllKeyPizzas() as $key) {
            $total += $this->panier['pizzas'][$key] * $this->pizzas[$key];
        }
        return round($total, 2);
    }


    /**
     * @return string
     */
    public function facture() : string
    {
        $html = '<h2>Facture de la commande ' . ucfirst($this->nom) . '</h2>';
        if($this->panier['menudujour'] > 0) {
            $html .= '<p>Menu du jour: '.$this->panier['menudujour'].' x '. $this->menudujour.' => '.$this->panier['menudujour']*$this->menudujour.'$</p>';
        }
        if($this->panier['menuenfant'] > 0) {
            $html .= '<p>Menu enfant: '.$this->panier['menuenfant'].' x '. $this->menuenfant.' => '.$this->panier['menuenfant']*$this->menuenfant.'$</p>';
        }
        foreach($this->getAllKeyPizzas() as $key) {
            if($this->panier['pizzas'][$key]  > 0) {
                $html .= '<p>'.ucfirst($key).': '.$this->panier['pizzas'][$key].' x '. $this->pizzas[$key].' => '.$this->panier['pizzas'][$key]*$this->pizzas[$key].'$</p>';
            }
        }
        if($this->panier['cafe'] > 0) {
            $html .= '<p>Café: '.$this->panier['cafe'].' x '. $this->cafe.' => '.$this->panier['cafe']*$this->cafe.'$</p>';
        }
        $html .= '<p>Nombre de couvert : '.$this->nbreCouvert.'</p>';
        $html .= '<p>Total: '.$this->calculTotal().'$</p>';
        return $html;
    }
}