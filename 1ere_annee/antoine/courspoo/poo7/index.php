<?php
// creation d'un projet -> composer init
// composer install
// composer require
// composer update

require('vendor/autoload.php');

use JasonGrimes\Paginator;
use Ordifixe\Poo7\Dede;

$totalItems = 1000;
$itemsPerPage = 50;
$currentPage = 8;
$urlPattern = '/foo/page/(:num)';

$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
echo $paginator;
$dede = new Dede();

