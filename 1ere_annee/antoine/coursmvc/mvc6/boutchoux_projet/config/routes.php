<?php

$routes = array(
    array(['GET'], 'home','default','index'),

    array(['GET', 'POST'], 'contact','contact','add'),
    array(['GET', 'POST'], 'contact','contact','sendAMessage'),

    array(['GET','POST'], 'register','security','register'),
    array(['GET'],'logout','security','logout'),
    array(['GET','POST'],'login','security','login'),

    array(['GET'],'cgu','law','cgu'),
    array(['GET'],'mentionslegales','law','mentionslegales'),

    array(['GET', 'POST'],'payement','payement','pay'),

    array(['GET'],'admin','admin','index'),
    array(['GET'],'useradmin','admin','useradmin'),
    array(['GET'],'proadmin','admin','proadmin'),
    array(['GET'],'particulieradmin','admin','particulieradmin'),
    array(['GET'],'piecesproadmin','admin','piecesproadmin'),
    array(['GET'],'piecesparticulieradmin','admin','piecesparticulieradmin'),
    array(['GET'],'reservationadmin','admin','reservationadmin'),
    array(['GET'],'paiementadmin','admin','paiementadmin'),
    array(['GET'],'contactadmin','admin','contactadmin'),
    array(['GET'],'avisadmin','admin','avisadmin'),


    array(['GET','POST'],'forgotpassword','security','forgotpassword'),
    array(['GET','POST'],'resetpassword','security','resetpassword'),


    array(['GET', 'POST'],'espaceparent','parental','espaceparent'),
    array(['GET', 'POST'],'addinfoparent','parental','addinfoparent'),
    array(['GET', 'POST'],'espaceparent&child','parental','espaceparentandchild'),
);
