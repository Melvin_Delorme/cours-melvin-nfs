<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminUserModel extends AbstractModel
{
    protected static $table = 'users';

    protected $id;
    protected $nom;
    protected $prenom;
    protected $email;
    protected $created_at;
    protected $modified_at;
    protected $roles;
    protected $count_login;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return mixed
     */
    public function getCountLogin()
    {
        return $this->count_login;
    }

    public static function getAllUserByRole($role = 'Parent')
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE roles = ?",array($role),get_called_class());
    }
}