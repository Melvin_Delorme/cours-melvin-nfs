<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminContactModel extends AbstractModel
{
    protected static $table = 'contact';

    protected $id;
    protected $email;
    protected $sujet;
    protected $message;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }


}