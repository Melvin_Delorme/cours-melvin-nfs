<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminReservationModel extends AbstractModel
{
    protected static $table = 'reservation';

    protected $id;
    protected $id_children;
    protected $id_user_pro;
    protected $id_user_parent;
    protected $id_creneau;
    protected $start_at;
    protected $end_at;
    protected $status;
    protected $nbrehours;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdChildren()
    {
        return $this->id_children;
    }

    /**
     * @return mixed
     */
    public function getIdUserPro()
    {
        return $this->id_user_pro;
    }

    /**
     * @return mixed
     */
    public function getIdUserParent()
    {
        return $this->id_user_parent;
    }

    /**
     * @return mixed
     */
    public function getIdCreneau()
    {
        return $this->id_creneau;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getEndAt()
    {
        return $this->end_at;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public static function getAllReservationByStatus($status = 'en attente')
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE status = ?",array($status),get_called_class());
    }
}