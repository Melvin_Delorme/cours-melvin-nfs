<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminPieceProModel extends AbstractModel
{
    protected static $table = 'pieces_justificatifs_pro';

    protected $id;
    protected $id_parent;
    protected $nom;
    protected $pieces;
    protected $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdParent()
    {
        return $this->id_parent;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


}