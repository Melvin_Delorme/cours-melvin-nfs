<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminUser_ProModel extends AbstractModel
{
    protected static $table = 'user_pro';

    protected $id;
    protected $id_users;
    protected $type;
    protected $nom_entreprise;
    protected $email_entreprise;
    protected $description;
    protected $tarif;
    protected $adresse;
    protected $telephone;
    protected $siren;
    protected $created_at;
    protected $latitude;
    protected $longitude;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUsers()
    {
        return $this->id_users;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getNomEntreprise()
    {
        return $this->nom_entreprise;
    }

    /**
     * @return mixed
     */
    public function getEmailEntreprise()
    {
        return $this->email_entreprise;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @return mixed
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }


    public static function getAllUserByType($type)
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE type = ?",array($type),get_called_class());
    }

}