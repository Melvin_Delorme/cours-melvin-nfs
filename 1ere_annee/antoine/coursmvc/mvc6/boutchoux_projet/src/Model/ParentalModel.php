<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ParentalModel extends AbstractModel
{
    protected static $table = 'user_parents';
    protected $id;
    protected $id_user_parent;
    protected $telephone;
    protected $is_assistant;
    protected $adresse;


    /**
     * @return mixed
     */
    public function getIdUserParent()
    {
        return $this->id_user_parent;
    }

    /**
     * @param mixed $id_user_parent
     */
    public function setIdUserParent($id_user_parent): void
    {
        $this->id_user_parent = $id_user_parent;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getIsAssistant()
    {
        return $this->is_assistant;
    }

    /**
     * @param mixed $is_assistant
     */
    public function setIsAssistant($is_assistant): void
    {
        $this->is_assistant = $is_assistant;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }







    public static function findAddinfo($id_user)
    {
        return App::getDatabase()->prepare("SELECT * FROM user_parents WHERE id_users = ?", [$id_user], get_called_class(), true);
    }



    public static function insertInfoParent($id_user, $post, $latitude, $longitude)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_users, telephone, adresse, is_assistant, latitude, longitude) VALUES (?,?,?,?,?,?)",
            array($id_user, $post['telephone'], $post['adresse'], $post['checkbox'], $latitude, $longitude,)
        );
    }
}
