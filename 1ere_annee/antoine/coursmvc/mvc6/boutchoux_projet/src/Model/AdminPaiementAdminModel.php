<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminPaiementAdminModel extends AbstractModel
{
    protected static $table = 'paiement_mode';

    protected $id;
    protected $id_user_parent;
    protected $id_user_pro;
    protected $id_paimement;
    protected $nom;
    protected $prenom;
    protected $code;
    protected $cvv;
    protected $expire_at;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdUserParent()
    {
        return $this->id_user_parent;
    }

    /**
     * @return mixed
     */
    public function getIdUserPro()
    {
        return $this->id_user_pro;
    }

    /**
     * @return mixed
     */
    public function getIdPaimement()
    {
        return $this->id_paimement;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * @return mixed
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }


}