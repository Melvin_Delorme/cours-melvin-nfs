<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminPieceParticulierModel extends AbstractModel
{
    protected static $table = 'pieces_justificatifs_child';

    protected $id;
    protected $id_child;
    protected $nom;
    protected $pieces;
    protected $updated_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdChild()
    {
        return $this->id_child;
    }


    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


}