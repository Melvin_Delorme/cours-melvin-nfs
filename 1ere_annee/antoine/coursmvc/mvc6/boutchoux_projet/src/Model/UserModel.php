<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected static $table = 'users';
    protected $id;
    protected $nom;
    protected $prenom;
    protected $email;
    protected $password;
    protected $token;
    protected $roles;
    protected $created_at;
    protected $modified_at;
    protected $count_login;

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @param mixed $modified_at
     */
    public function setModifiedAt($modified_at): void
    {
        $this->modified_at = $modified_at;
    }

    /**
     * @param mixed $count_login
     */
    public function setCountLogin($count_login): void
    {
        $this->count_login = $count_login;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * @return mixed
     */
    public function getCountLogin()
    {
        return $this->count_login;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }


    public static function getUserByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE email = ?",array($email),get_called_class(), true);
    }

    public static function insertUser($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (email, password, token,nom,prenom,roles,created_at) VALUES (?,?,?,?,?,?, NOW())",
            array($post['email'], $post['password_hash'], $post['token'],$post['nom'],$post['prenom'],$post['checkbox'])
        );
    }
    public static function addLogin($id){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET count_login = count_login + 1 WHERE id = ? ", array($id));
    }

    public static function fetchToken($email)
    {
        return App::getDatabase()->prepare("SELECT email, token FROM " . self::getTable() . " WHERE email = ? ",[$email],get_called_class(),true);
    }
    public static function fetchPassword($email) {
        return App::getDatabase()->prepare("SELECT password FROM users WHERE email = :email ", ['email' => $email], get_called_class(), true);
    }

    public static function updatePassword($email, $password, $token){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET password = ?, token = ?, modified_at = NOW() WHERE email = ?", array($password, $token, $email));
    }



    public static function addOneToLoginCount($id)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET count_login = count_login + 1 WHERE id = ?",
            array($id)
        );
    }




}