<?php
namespace App\Service;

use App\Model\UserModel;
use GuzzleHttp\Client;


class Validation
{
    protected $cordonnee;
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function validationEmail($mail1){
        $error='';
        if (!empty($mail1)){
            if (!filter_var($mail1, FILTER_VALIDATE_EMAIL)){
                $error='Veuillez renseignez un email valide*';
            }
        }else{
            $error= 'Veuillez renseigner une adresse email*';
        }
        return $error;
    }

    public function existUserinbd($email){
        $error = [];
        if (!empty($email)){
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $error='Veuillez renseignez un email valide*';
            } else {
                $verifUser = UserModel::getUserByEmail($email);
                if (empty($verifUser)) {
                    $error = 'Ce compte n\'existe pas';
                }
            }
        }else{
            $error= 'Veuillez renseigner une adresse email*';
        }
        return $error;
    }

    public function existUser($email){
        $error=[];
        $existEmail=UserModel::getUserByEmail($email);
        if (!empty($existEmail)){
            $error='Cette adresse mail existe déjà';
        }
        return $error;
    }
    public function verifAllPassword($password, $password2, $min, $email){
        $errors=[];
        if(!empty($password) && !empty($password2)) {
            if($password != $password2) {
                $errors = 'Vos mots de passe sont différents*';
            } elseif(mb_strlen($password) < $min) {
                $errors = 'Votre mot de passe est trop court (min '.$min.')';
            } else {
                $verifPassword = UserModel::fetchPassword($email);
                if (password_verify($password, $verifPassword->password)) {
                    $errors = 'Votre nouveau mot de passe ne doit pas être identique à l\'ancien.';
                }
            }
        } else {
            $errors = 'Veuillez renseigner les mots de passe*';
        }
        return $errors;
    }

    public function verifPassword( $password, $password2, $min){
        $errors=[];
        if(!empty($password) && !empty($password2)) {
            if($password != $password2) {
                $errors = 'Vos mots de passe sont différents*';
            } elseif(mb_strlen($password) < $min) {
                $errors = 'Votre mot de passe est trop court(min '.$min.')';
            }
        } else {
            $errors = 'Veuillez renseigner les mots de passe*';
        }
        return $errors;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un(e) ' . $title . '.';
            }
        }
        return $error;

    }

    public function textValid2($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textonlyValid($text, $title, $min = 3, $max = 50, $empty = true)
    {
        $error = '';

        if (!empty($text)) {
            // Vérifier que la variable contient uniquement des lettres et des espaces
            if (!preg_match("/^[a-zA-Z\s]*$/", $text)) {
                $error = 'Votre ' . $title . ' ne doit contenir que des lettres et des espaces.';
            } else {
                $strtext = strlen($text);
                if ($strtext > $max) {
                    $error = 'Votre ' . $title . ' est trop long.';
                } elseif ($strtext < $min) {
                    $error = 'Votre ' . $title . ' est trop court.';
                }
            }
        } else {
            if ($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }

        return $error;
    }


    /**
     * @author Moussi Mohamed-Amir
     * @param $file
     * @param array $allowedMimeTypes
     * @param int $maxSize
     * @return string
     */
    public function fileValid($file, array $allowedMimeTypes = array('image/png','image/jpeg','image/jpg'),int $maxSize = 2000000)
    {
        $error = '';
        if($file['error'] > 0) {
            if($file['error'] != 4) {
                $error = 'Error: ' . $file['error'];
            } else {
                $error = 'Veuillez renseigner une image';
            }
        } else {
            $file_name = $file['name'];
            $file_size = $file['size'];
            $file_tmp  = $file['tmp_name'];
            $file_type = $file['type'];

            if($file_size > $maxSize || filesize($file_tmp) > $maxSize) {
                $error = 'Votre fichier est trop gros (max '. ($maxSize / 1000000) .'mo).';
            } else {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $file_tmp);
                if(!in_array($mime, $allowedMimeTypes)) {
                    $error = 'Veuillez télécharger une image du type ' . implode(' ou ', $allowedMimeTypes);
                }
            }
        }
        return $error;
    }



    /**
     * @author Moussi Mohamed-Amir
     * @param string $url
     * @return string
     */
    public function urlValid(string $url) : string
    {
        $error = '';
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            $error = 'URL invalide. Ex : https://www.google.com/';
        }
        return $error;
    }

    public function numberValid($value, $title, $min = null, $max = null, $empty = true) {

        $error = '';

        if(!empty($value)) {
            if(!is_numeric($value)) {
                $error = 'Votre ' . $title . ' doit être un nombre.';
            } elseif($min !== null && $value < $min) {
                $error = 'Votre ' . $title . ' doit être supérieur ou égal à ' . $min . '.';
            } elseif($max !== null && $value > $max) {
                $error = 'Votre ' . $title . ' doit être inférieur ou égal à ' . $max . '.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }

        return $error;
    }

    public function stringValid($value, $title, $minLength = null, $maxLength = null, $empty = true) {
        $error = '';

        if(!empty($value)) {
            if(!is_string($value)) {
                $error = 'Votre ' . $title . ' doit être une chaîne de caractères.';
            } elseif($minLength !== null && strlen($value) < $minLength) {
                $error = 'Votre ' . $title . ' doit contenir au moins ' . $minLength . ' caractères.';
            } elseif($maxLength !== null && strlen($value) > $maxLength) {
                $error = 'Votre ' . $title . ' doit contenir au maximum ' . $maxLength . ' caractères.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }

        return $error;
    }




    public function dateValid($date, $title, $empty = true)
    {
        $error = '';
        if (!empty($date)) {
            $now = new \DateTime();
            $input_date = new \DateTime($date);
            if ($input_date < $now) {
                $error = 'La ' . $title . ' ne peut pas être dans le passé.';
            }
        } else {
            if ($empty) {
                $error = 'Veuillez renseigner une ' . $title . '.';
            }
        }
        return $error;
    }

    public function phoneValid(string $phone) : string
    {
        $error = '';
        $phone = str_replace(' ', '', $phone); // Supprime les espaces éventuels
        if (!preg_match('/^0[1-9]([-. ]?[0-9]{2}){4}$/', $phone)) {
            $error = 'Numéro de téléphone invalide. Ex : 06 12 34 56 78';
        }
        if(empty($phone)){
            $error = 'Veuillez renseigner un  numéro de  téléphone .';
        }
        return $error;
    }

    /*claude*/
    public function  adresseApi($adresse){

        if (empty($adresse)) {
            $error = "Veuillez renseigner ce champ.";
        } else {

            $error = array();
            $cordonnee = array();
            $url = 'https://api-adresse.data.gouv.fr/search/?q=' . urlencode($adresse);
            // je vérifie l'adresse avec l'api
            $client = new Client();
            try {
                $response = $client->request('GET', $url);
                // je vérifie si l'adresse existe
                $body = json_decode($response->getBody(), true);
                if ($body['features'] == []) {
                    $error = "L'adresse saisie n'existe pas.";
                } else {
                    // Stocker les coordonnées de la première ville dans une variable
                    $this->cordonnee = array(
                        'cordinate1' => $body['features'][0]['geometry']['coordinates'][0],
                        'cordinate2' => $body['features'][0]['geometry']['coordinates'][1]

                    );
                }



            } catch (\GuzzleHttp\Exception\ClientException $e) {
                // je récupère les erreurs de l'api
                if (!empty($e->getResponse()->getBody())) {
                    $data2 = json_decode($e->getResponse()->getBody(), true);
                    if (empty($cordinate1)) {
                        $error = 'l\'adresse n\'existe pas';
                    }
                }
            }

        }
        return $error;

    }

    public function getCordonnee() {
        return $this->cordonnee;
    }




    public function getsiret($siret,$jeton){
        $error = array();
        $url = 'https://api.insee.fr/entreprises/sirene/V3/siren/' . urlencode($siret);
        // je chope le siret avec l'api
        $client = new Client();
        try {
            $response = $client->request('GET', $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $jeton,
                    'Accept' => 'application/json'
                ]
            ]);
            // je chope les erreurs de l'api
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if (!empty($e->getResponse()->getBody())) {
                $data = json_decode($e->getResponse()->getBody(), true);
                $error = $data['header']['message'];
            }

        }
        return $error;
    }
}
