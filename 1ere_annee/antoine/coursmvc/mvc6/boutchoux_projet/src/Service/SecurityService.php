<?php

namespace App\Service;
use App\Model\UserModel;



class SecurityService
{



    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function getUser(){
        if (!empty($_SESSION)){
            if (!empty($_SESSION['id']))
                $data= UserModel::findByColumn('id',$_SESSION['id']);


            return $data;

        }else return 'nobody connected';

    }
    private function redirection($page){
        $directory = '/';
        $data =  'http://'.$_SERVER['HTTP_HOST'] .$directory;
        return header('Location:'.$data.$page );
    }
    public function isLog(){
        if (!empty($_SESSION)){
            return true;
        }else{
            return false;
        }

    }
    public function isLogged() {
        if(!empty($_SESSION['user']['id'])) {
            if(!empty($_SESSION['user']['email'])) {
                if(!empty($_SESSION['user']['ip'])) {
                    if (!empty($_SESSION['user']['roles'])) {
                        if($_SESSION['user']['ip'] == $_SERVER['REMOTE_ADDR']) {
                            return true;
                        }

                    }
                }
            }
        }
        return false;
    }

    public function isAuthorizedRole(array $roles){

        if($this->isLogged()) {
            if (in_array($_SESSION['user']['roles'], $roles)) {
                return true;
            }
        }
        return false;
    }


}