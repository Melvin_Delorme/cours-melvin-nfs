<?php
namespace App\Service;
use Core\Helper\Mail;


class MailPerso extends Mail{

    public function sendMessage($email, $message){
        $content=$this->contentAssembly('app.email.app.content',array(
            'message'=>$message,
        ));
        $mail=new Mail();
        $mail->sendMail($content, $email);
    }

    public function sendMailRegister($email, $subject){
        $content=$this->contentAssembly('app.email.app.email-register');
        $mail= new Mail();
        $mail->sendMail($content, $email, $subject);
    }

    public function sendMailAddInfo($email, $subject){
        $content=$this->contentAssembly('app.email.app.email-addinfo');
        $mail= new Mail();
        $mail->sendMail($content, $email, $subject);
    }

    public function sendMailRegisterForgotPassword($email, $subject){
        $content=$this->contentAssembly('app.email.app.email-registerForgot');
        $mail= new Mail();
        $mail->sendMail($content, $email, $subject);
    }

    public  function sendContact($email, $content, $subject){
        $envoi=$this->contentAssembly('app.email.app.message-envoye');
        $content=$this->contentAssembly('app.email.app.send-contact',array(
            'subject'=>$subject,
            'content'=>$content,
            'email'=>$email,
        ));
        $mail= new Mail();
        $mail->sendMail($envoi, $email, 'Message envoyé !');
        $mail->sendMail($content, $email, 'Vous avez un message en attente de ' . $email . ' ', true);
    }


}
