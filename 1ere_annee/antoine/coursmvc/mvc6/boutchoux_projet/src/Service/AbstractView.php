<?php

namespace App\Service;

abstract class AbstractView
{
    // Mettre ici des méthodes public
    // Afin d'accéder au methode dans les templates via l'instance $view
    public function isLogged() {
        $security = new SecurityService();
        return $security->isLogged();
    }

    public function isAuthorizedRole(array $roles) {
        $isAuthorizedRol= new SecurityService();
        return $isAuthorizedRol->isAuthorizedRole($roles);
    }

}