<?php

namespace App\Controller;
use App\Model\UserModel;
use App\Model\ParentalModel;
use App\Service\Form;
use App\Service\MailPerso;
use App\Service\SecurityService;
use App\Service\Validation;




class SecurityController extends BaseController
{

    public function login()
    {
        if ($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->IsValid($errors)) {
                $verifUser = UserModel::getUserByEmail($post['email']);
                if (empty($verifUser)) {
                    $errors['password'] = 'Vous n\'avez pas accès à ce compte ou ce compte n\'existe pas';
                } else {
                    if (!password_verify($post['password'], $verifUser->password)) {
                        $errors['password'] = 'Vous n\'avez pas accès à ce compte';
                    } else {
                        $_SESSION['user'] = array(
                            'id' => $verifUser->id,
                            'email' => $verifUser->email,
                            'roles' => $verifUser->roles,
                            'token' => $verifUser->token,
                            'nom' => $verifUser->nom,
                            'prenom' => $verifUser->prenom,
                            'ip' => $_SERVER['REMOTE_ADDR'],
                        );
                        UserModel::addOneToLoginCount($verifUser->id);
                        if ($_SESSION['user']['roles'] == 'professionel') {
                            //redirection vers dashboard pro
                            $this->redirect('contact');
                        } elseif ($_SESSION['user']['roles'] == 'Parent') {
                            // redirection vers zone parent
                            $id_user = $_SESSION['user']['id'];
                            $result = ParentalModel::findAddinfo($id_user);
                            if (!empty($result)) {
                                $this->redirect('espaceparent&child');
                            }
                            $this->redirect('espaceparent');
                        } elseif ($_SESSION['user']['roles'] == 'Admin') {
                            $this->redirect('admin');
                        }
                    }
                }
            }
        }
        $form = new Form($errors, 'post');
        $this->render('app.security.login', array(
            'form' => $form,
        ));
    }


    public function register()
    {
        if ($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validateExistUser($v, $post);
            if (empty($post['checkbox'])) {
                $errors['checkbox'] = 'Veuillez cocher l\'une des cases*';
            }
            if ($v->isValid($errors)) {
                $post['password_hash'] = password_hash($post['password'], PASSWORD_DEFAULT);
                $post['token'] = (new SecurityService())->generateRandomString(60);
                $sendMail = new MailPerso();
                $subjet = 'Creation de votre compte Boutchoux';
                $sendMail->sendMailRegister($post['email'], $subjet);
                UserModel::insertUser($post);
                $this->redirect('login');
            }
        }
        $form = new Form($errors);
        $this->render('app.security.register', array(
            'form' => $form,
        ));
    }


    public function logout()
    {
        $_SESSION = array();
        session_destroy();
        $this->redirect('home');
    }

    public function forgotpassword(){
        if ($this->isLogged()) {
            $this->redirect('home');
        }
        $errors=[];
        $lien='';
        if (!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $v= new Validation();
            $errors['email']=$v->existUserinbd($post['email']);
            if ($v->IsValid($errors)){
                $link = UserModel::fetchToken($post['email']);
                if (!empty($link)){
                    $lien= '<a  href="resetpassword?email='.urlencode($link->email).'&token='.urlencode($link->token).'" class="lien_reset">Cliquer ici pour modifier votre mot de passe</a>';
                }
            }
        }
        $form=new Form($errors);
        $this->render('app.security.forgotpassword', array(
            'form'=>$form,
            'lien'=>$lien,
        ));
    }

    public function resetpassword()
    {
        if ($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = [];
        $email = urldecode($_GET['email']);
        $token = urldecode($_GET['token']);

        if (!empty($_GET)) {
            $verifEmail = UserModel::fetchToken($email);
            if ($verifEmail->token === $token) {
                if (!empty($_POST['submitted'])) {
                    $post = $this->cleanXss($_POST);
                    $v = new Validation();
                    $errors['password'] = $v->verifAllPassword($post['password'], $post['password2'], 4, $email);
                    if ($v->IsValid($errors)) {
                        $password = password_hash($post['password'], PASSWORD_DEFAULT);
                        $token = new SecurityService();
                        $newToken = $token->generateRandomString(60);
                        $sendMail = new MailPerso();
                        $subjet = 'Modification de votre mot de passe Boutchoux';
                        $sendMail->sendMailRegisterForgotPassword($email, $subjet);
                        UserModel::updatePassword($email, $password, $newToken);
                       $this->redirect('login');
                    }
                }
            }else{
                $this->redirect('home');
            }
        }else{
            $this->redirect('home');
        }
        $form = new Form($errors);
        $this->render('app.security.resetpassword', array(
            'form' => $form,
        ));
    }




    private function validate($v, $post){
        $error=[];
        $error['email']=$v->validationEmail($post['email']);
        if (empty($post['password'])) {
            $error['password'] = 'Veuillez saisir un mot de passe*';
        }
        return $error;
    }

    private function validateExistUser($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid2($post['nom'], 'nom',2, 20);
        $errors['prenom'] = $v->textValid2($post['prenom'], 'prenom',2, 500);
        $errors['email']=$v->validationEmail($post['email']);
        if ($errors['email']===''){
            $errors['email']=$v->existUser($post['email']);
        }
        $errors['password']=$v->verifPassword($post['password'], $post['password2'], 4);
        return $errors;
    }

}