<?php

namespace App\Controller;

use App\Model\AdminAvisModel;
use App\Model\AdminContactModel;
use App\Model\AdminPaiementAdminModel;
use App\Model\AdminPieceParticulierModel;
use App\Model\AdminPieceProModel;
use App\Model\AdminReservationModel;
use App\Model\AdminUser_ProModel;
use App\Model\AdminUserModel;
use App\Service\SecurityService;

/**
 *
 */
class AdminController extends BaseController
{
    public function index()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $users = AdminUserModel::all();
        $piecepros = AdminPieceProModel::all();
        $pieceparticuliers = AdminPieceParticulierModel::all();
        $reservations = AdminReservationModel::all();
        $reservationsvalid = AdminReservationModel::getAllReservationByStatus('valide');
        $reservationsenattente = AdminReservationModel::getAllReservationByStatus('en attente');
        $paiements = AdminPaiementAdminModel::all();
        $contacts = AdminContactModel::all();
        $avis = AdminAvisModel::all();
        $users_pros = AdminUser_ProModel::all();
        $userscreches = AdminUser_ProModel::getAllUserByType('creche');
        $usersassistantes = AdminUser_ProModel::getAllUserByType('assistante maternelle');
        $this->render("app.admin.admin",array(
            'users' => $users,
            'piecespros' => $piecepros,
            'piecesparticuliers' => $pieceparticuliers,
            'reservations' => $reservations,
            'paiements' => $paiements,
            'contacts' => $contacts,
            'avis' => $avis,
            'users_pros' => $users_pros,
            'userscreches' => $userscreches,
            'usersassistantes' => $usersassistantes,
            'reservationsvalid' => $reservationsvalid,
            'reservationsenattente' => $reservationsenattente
        ), 'admin');
    }

    public function useradmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $users = AdminUserModel::getAllUserByRole('Admin');
        $this->render("app.admin.user-admin",array(
            'users' => $users
        ), 'admin');
    }

    public function proadmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $users = AdminUserModel::getAllUserByRole('professionel');
        $this->render("app.admin.pro-admin",array(
            'users' => $users
        ), 'admin');
    }

    public function particulieradmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $users = AdminUserModel::getAllUserByRole();
        $this->render("app.admin.particulier-admin",array(
            'users' => $users
        ), 'admin');
    }

    public function piecesproadmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $pieces = AdminPieceProModel::all();
        $this->render("app.admin.piecespro-admin",array(
            'pieces' => $pieces
        ), 'admin');
    }

    public function piecesparticulieradmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $pieces = AdminPieceParticulierModel::all();
        $this->render("app.admin.piecesparticulier-admin",array(
            'pieces' => $pieces
        ), 'admin');
    }

    public function reservationadmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $reservations = AdminReservationModel::all();
        $this->render("app.admin.reservation-admin",array(
            'reservations' => $reservations
        ), 'admin');
    }

    public function paiementadmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $paiements = AdminPaiementAdminModel::all();
        $this->render("app.admin.paiement-admin",array(
            'paiements' => $paiements
        ), 'admin');
    }

    public function contactadmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $contacts = AdminContactModel::all();
        $this->render("app.admin.contact-admin",array(
            'contacts' => $contacts
        ), 'admin');
    }

    public function avisadmin()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Admin'))) {
            $this->redirect('login');
        }
        $avis = AdminAvisModel::all();
        $this->render("app.admin.avis-admin",array(
            'avis' => $avis
        ), 'admin');
    }
}
;