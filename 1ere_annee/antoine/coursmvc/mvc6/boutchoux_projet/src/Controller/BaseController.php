<?php
namespace App\Controller;


use Core\Kernel\AbstractController;
use App\Service\SecurityService;

class BaseController extends AbstractController{

    public function isLogged() {
        $security = new SecurityService();
        return $security->isLogged();
    }

}