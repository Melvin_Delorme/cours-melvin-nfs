<?php

namespace App\Controller;

use App\Model\ParentalModel;
use App\Service\Form;
use App\Service\MailPerso;
use App\Service\Validation;
use App\Service\SecurityService;



class ParentalController  extends BaseController
{


    public function espaceparent()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Parent'))) {
            $this->redirect('login');
        }
        $id_user = $_SESSION['user']['id'];
        $result = ParentalModel::findAddinfo($id_user);
        if (empty($result)) {
            $this->render("app.parent.espaceparent", array(), 'parent');
        }
        $this->render("app.parent.espaceparent", array(
            'result' => $result,
        ), 'parent');
    }



    public function addinfoparent()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Parent'))) {
            $this->redirect('login');
        }
        // Vérifier si les informations parentales ont déjà été ajoutées
        $id_user = $_SESSION['user']['id'];
        $result = ParentalModel::findAddinfo($id_user);
        if (!empty($result)) {
            $this->redirect('espaceparent');
        }
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validateInfoParent($v, $post);
            if (empty($post['checkbox'])) {
                $errors['checkbox'] = 'Veuillez cocher l\'une des cases*';
            }
            /* Adresse */
            $cordinate = $v->getCordonnee();
            $this->dump($cordinate);
            if (!empty($post['adresse'])) {
                $latitude = $cordinate['cordinate1'];
                $longitude = $cordinate['cordinate2'];
            }
            if ($v->isValid($errors)) {
                $sendMail = new MailPerso();
                $subjet = 'Ajout de vos informations à l\'espace parent Boutchoux';
                if (!empty($_SESSION)) {
                    $sendMail->sendMailAddInfo($_SESSION['user']['email'], $subjet);
                }
                ParentalModel::insertInfoParent($id_user, $post, $latitude, $longitude);
                $this->redirect('espaceparent');
            }
        }
        $form = new Form($errors);
        $this->render("app.parent.addinfoparent", array(
            'form' => $form,
        ), 'parent');
    }

    public function espaceparentandchild()
    {
        $service = new SecurityService();
        if (!$service->isAuthorizedRole(array('Parent'))) {
            $this->redirect('login');
        }
        $id_user = $_SESSION['user']['id'];
        $result = ParentalModel::findAddinfo($id_user);
        if (!empty($result)) {
            $this->render("app.parent.espaceparent&child", array(), 'parent');
        }
        $this->redirect('espaceparent');
    }





    private function validateInfoParent($v, $post)
    {
        $errors = [];
        $errors['telephone'] = $v->phonevalid($post['telephone']);
        $errors['adresse'] = $v->adresseApi($post['adresse']);
        return $errors;
    }
}
