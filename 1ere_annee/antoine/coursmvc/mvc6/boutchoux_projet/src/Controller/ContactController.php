<?php
namespace App\Controller;
use App\Model\ContactModel;
use App\Service\Form;
use App\Service\MailPerso;
use App\Service\Validation;


/**
 *
 */

class ContactController extends BaseController{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    // envoyer un message de contact
    public function sendAMessage()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            // Faille XSS.
            $post = $this->cleanXss($_POST);
            // Validation
            $errors = $this->validate($this->v, $post);
            if ($this->v->isValid($errors)) {
                $sendmail= new MailPerso();
                ContactModel::insertWithSessionEmail($post);
                if(!empty($_SESSION)){
                    $sendmail->sendContact($_SESSION['user']['email'], $post['message'], $post['sujet']);
                }else{
                    $sendmail->sendContact($post['email'], $post['message'], $post['sujet']);
                }
                 // redirection
                 $this->redirect('home');
            }
        }
        $form = new Form($errors);
        $this->render('app.contact.add', array(
            'form' => $form,
        ));
    }


    private function validate($v, $post)
    {
        $errors = [];
        if (empty($_SESSION)){
            $errors['email']=$v->validationEmail($post['email']);
        }
        $errors['sujet']=$v->textValid($post['sujet'], 'sujet', 5, 100);
        $errors['message']=$v->textValid($post['message'], 'message',4 , 1000);
        return $errors;
    }


}