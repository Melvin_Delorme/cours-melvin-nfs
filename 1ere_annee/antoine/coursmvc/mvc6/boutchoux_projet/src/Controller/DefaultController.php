<?php

namespace App\Controller;

/**
 *
 */
class DefaultController extends BaseController
{
    public function index()
    {
        $this->render('app.default.frontpage', array());
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }

}
