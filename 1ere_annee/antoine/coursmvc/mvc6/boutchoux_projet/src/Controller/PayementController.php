<?php

namespace App\Controller;
            
use App\Service\Validation;
use Core\Kernel\AbstractController;
use App\Service\Form;
use Respect\Validation\Validator as v;
use function Webmozart\Assert\Tests\StaticAnalysis\true;

class PayementController extends AbstractController
{
    private $va;

    public function __construct()
    {
        $this->va = new Validation();
    }

    private function validate($va, $post)
    {
        $errors = [];
        $errors['card-number'] = $va->stringValid($post['card-number'], 'Numero de carte', 19, 19);
        $errors['card-holders-name'] = $va->textonlyValid($post['card-holders-name'], 'Nom et un Prenom', 10, 255);
        $errors['expiry-month'] = $va->numberValid($post['expiry-month'], 'Mois correct', 1, 12);
        $errors['expiry-year'] = $va->numberValid($post['expiry-year'], 'Année correct', 1, 99);
        $errors['cvc'] = $va->stringValid($post['cvc'], 'Cvc correct', 1, 3);
        return $errors;
    }

    public function pay()
    {
        $errors = array();
        $creditcard = array();
        $errorcard = false;
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->va, $post);
            if ($this->va->IsValid($errors)) {
               $creditcard = v::creditCard()->validate($post['card-number']);
               if (!$creditcard) {
                   $errorcard = true;
               }
            }
        }
        $formcard = new Form($errors);
        $this->render("app.payement.payement",array(
            'formcard' => $formcard,
            'creditcard' => $creditcard,
            'errorcard' => $errorcard
        ), 'payment');
    }
}
