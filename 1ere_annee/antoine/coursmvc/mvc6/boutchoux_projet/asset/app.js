import './app/scss/app.scss'


/*  checkbox*/
// Vérifier que les éléments sont présents dans le document HTML avant de les sélectionner
// checkbox //
// checkbox //

const checkbox1 = document.querySelector('#checkbox1');
const checkbox2 = document.querySelector('#professionel');

if (checkbox1 && checkbox2) {
  checkbox1.addEventListener('change', () => {
    if (checkbox1.checked) {
      checkbox2.checked = false;
    }
  });

  checkbox2.addEventListener('change', () => {
    if (checkbox2.checked) {
      checkbox1.checked = false;
    }
  });
}


const map2 = document.querySelector('#map');
var map = L.map('map').setView([46.227638,  2.213749], 67);
L.tileLayer('//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
  maxZoom: 10,
  // id: 'mapbox.streets',
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);


L.popup()
    .setLatLng([ 49.433331, 1.08333, 67])
    .setContent("I am a standalone popup.")
    .openOn(map);

