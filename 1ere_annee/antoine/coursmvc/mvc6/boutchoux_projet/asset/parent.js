import './app/scss/parent.scss'

const map2 = document.querySelector('#map');
var map = L.map('map').setView([46.227638,  2.213749], 67);
L.tileLayer('//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    maxZoom: 10,
    // id: 'mapbox.streets',
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);


L.popup()
    .setLatLng([46.227638,  2.213749])
    .setContent("I am a standalone popup.")
    .openOn(map);

document.querySelector('#adresse').addEventListener('input', function() {
    let adresse = this.value;
    let url = `https://api-adresse.data.gouv.fr/search/?q=${adresse}`;
    fetch(url).then(response => response.json().then(data => console.log(data)));
});


const input = document.querySelector('#adresse');
const dropdown = document.querySelector('.dropdown');
const dropdownContent = document.querySelector('.dropdown-content');
const villeList = document.querySelector('#ville-list');
console.log(villeList)

input.addEventListener('input', function() {
    const value = this.value.trim();
    if (value.length >= 3) {
        const url = `https://api-adresse.data.gouv.fr/search/?q=${value}`;
        fetch(url)
            .then(response => response.json())
            .then(data => {
                villeList.innerHTML = '';
                data.features.forEach(feature => {
                    const ville = feature.properties.city;
                    const codePostal = feature.properties.postcode;
                    const adresse = feature.properties.name;
                    const li = document.createElement('li');
                    li.textContent = `${adresse}, (${codePostal}) ${ville} `;
                    li.addEventListener('click', function() {
                        input.value = `${adresse},(${codePostal}) ${ville} `;
                        dropdownContent.classList.remove('show');
                    });
                    villeList.appendChild(li);
                });
                dropdownContent.classList.add('show');
            });
    } else {
        dropdownContent.classList.remove('show');
    }
});

window.addEventListener('click', function(event) {
    if (dropdown.contains(event.target)) {
        dropdownContent.classList.add('show');
    } else {
        dropdownContent.classList.remove('show');
    }
});



const checkbox1 = document.querySelector('#checkbox1');
const checkbox2 = document.querySelector('#professionel');

if (checkbox1 && checkbox2) {
    checkbox1.addEventListener('change', () => {
        if (checkbox1.checked) {
            checkbox2.checked = false;
        }
    });

    checkbox2.addEventListener('change', () => {
        if (checkbox2.checked) {
            checkbox1.checked = false;
        }
    });
}

