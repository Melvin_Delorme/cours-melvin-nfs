import './admin/scss/admin.scss'
import Chart from 'chart.js/auto';


console.log('admin.js')


// SIDEBAR

window.addEventListener('scroll', function() {
    var adminBarLeft = document.getElementById('admin-barleft');
    var adminBarLeftHeight = adminBarLeft.offsetHeight;
    var windowHeight = window.innerHeight;
    var windowScroll = window.scrollY;
    var bodyHeight = document.body.offsetHeight;
    if ((windowHeight + windowScroll) >= (bodyHeight - adminBarLeftHeight)) {
        adminBarLeft.classList.add('fixed');
    } else {
        adminBarLeft.classList.remove('fixed');
    }
});


// MINIFIED BAR

const ablButton = document.querySelector('.abl_button');
const adminBarLeft = document.querySelector('.admin_barleft');
const adminContent = document.querySelector('.admin_content');

ablButton.addEventListener('click', () => {
    adminBarLeft.classList.toggle('minified');
    adminContent.classList.toggle('minified');
});

// DROPDOWN
$(document).ready(function() {
    $('.dropdown-toggle').click(function(e) {
        e.preventDefault();
        var dropdownMenu = $(this).next('.dropdown-menu');
        dropdownMenu.toggleClass('show');
        $('body').toggleClass('menu-open');
    });

    $(document).click(function(e) {
        var target = e.target;
        if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle')) {
            $('.dropdown-menu').removeClass('show');
            $('body').removeClass('menu-open');
        }
    });
});

$(document).ready(function() {
    $('.dropdown-toggle2').click(function(e) {
        e.preventDefault();
        var dropdownMenu2 = $(this).next('.dropdown-menu2');
        dropdownMenu2.toggleClass('show2');
        $('body').toggleClass('menu-open2');
    });

    $(document).click(function(e) {
        var target = e.target;
        if (!$(target).is('.dropdown-toggle2') && !$(target).parents().is('.dropdown-toggle2')) {
            $('.dropdown-menu2').removeClass('show2');
            $('body').removeClass('menu-open2');
        }
    });
});

// CHART

const ctx = document.getElementById('myChart');
const ctx2 = document.getElementById('myChart2');
const ctx3 = document.getElementById('myChart3');

new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Fev', 'Mars', 'Avr', 'Mai', 'Juin'],
        datasets: [{
            label: '# d\'inscriptions',
            data: [20, 50, 128, 133, 122, 214],
            borderWidth: 1,
            backgroundColor: ['#F68C11', '#ED166D', '#F68C11', '#ED166D', '#F68C11', '#ED166D'],
            borderRadius: {
                topLeft: 20,
                topRight: 20
            }
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

new Chart(ctx2, {
    type: 'doughnut',
    data: {
        labels: ['Demandes de reservations (particuliers)', 'Nombres de reservation effectives'],
        datasets: [{
            label: 'Reservations',
            data: [15, 13],
            backgroundColor: ['#F68C11', '#0C684B'],
        }]
    },
    options: {
        elements: {
            arc: {
                borderWidth: 0 // enlever la bordure
            }
        },
        plugins: {
            legend: {
                display: true,
                labels: {
                    color: 'white'
                }
            }
        }
    }
});



new Chart(ctx3, {
    type: 'doughnut',
    data: {
        labels: ['Nombres de factures envoyées aux particuliers et aux professionels', 'Taux de paiement des factures par les particuliers et les professionels'],
        datasets: [{
            label: 'Reservations',
            data: [62, 26],
            backgroundColor: ['#F68C11', '#ED166D'],
        }]
    }
});


