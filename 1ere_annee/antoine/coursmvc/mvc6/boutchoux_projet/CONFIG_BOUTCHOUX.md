## BRIEF RAPIDE DES FONTIONS UTILES

# Console

## CREER UN CRUD
```bash
php bin/console.php make controller
```
## GESTION DES TABLES

- `php bin/console.php Make Table` : pour créer une nouvelle table dans la base de données
- `php bin/console.php Show Table` : pour afficher une table existente
- `php bin/console.php Update Table` : pour mettre à jour et nettoyer une table dans la base de données
- `php bin/console.php Delete Table` : pour supprimer une table de la base de données

Gestion des fixtures
- `php bin/console.php Add Fixture` : pour ajouter des données dans la base de données.
- `php bin/console.php Help` : Afficher la liste des commandes disponibles.

## Créer une table

Pour créer une table, tapez la commande suivante dans la console :

```bash
php bin/console.php Make Table
```

- `php bin/console.php Show Table` : pour afficher une table dans la base de données
- `php bin/console.php Update Table` : pour mettre a jour une table existante
- `php bin/console.php Delete Table` : pour supprimer une table existante


## FIXTURE
```bash
php bin/console.php Add Fixture
```
Permet d'ajouter des Fixtures en remplissant le tableau comme ci dessous
```php 

return [
    'table' => [
    'fixture1' => [
        'columns' => [
            [
                'name' => 'name',
                'data' => 'Henry',
            ],
            [
                'name' => 'surname',
                'data' => 'Malo',
            ],
            [
                'name' => 'email',
                'data' => 'winrhy@gmail.com',
            ],
        ],
    ],
    'fixture2' => [
        'columns' => [
            [
                'name' => 'name',
                'data' => 'admin',
            ],
            [
                'name' => 'surname',
                'data' => 'admin',
            ],
            [
                'name' => 'email',
                'data' => 'admin@gmail.com',
            ],
        ],
    ],
    ],
];
```


## Utiliser l'envoi d'email
Installer:
- composer require phpmailer/phpmailer
- Mailhog pour avoir acces a cette page : <a href="http://localhost:8025/#">MailHog</a> ou seront envoyés les emails pour le test

<h3>Telecharger Mailhog</h3>
- <a href="https://github.com/mailhog/MailHog/releases/download/v1.0.1/MailHog_windows_amd64.exe">Télécharger Mailhog pour windows (64)</a>
- <a href="https://github.com/mailhog/MailHog/releases/download/v1.0.1/MailHog_linux_amd64">Télécharger Mailhog pour linux (64)</a>
- <a href="https://github.com/mailhog/MailHog/releases/tag/v1.0.1">Trouve ta version de Mailhog sur le site</a>

- <h3>L'utilisation :</h3>

Deux objets ont été créés pour ce faire :
- Mail()
- MailPerso()

Mail() est l'objet qui permet d'assembler l'email (header, template, footer) et ensuite de l'envoyer. Cet objet est en principe non-modifiable sauf si une amélioration pour l'utilisation doit être faites.

MailPerso() quant à lui, est l'objet qui permet de choisir le type de mail à envoyer.
Pour construire son email, il faudra utiliser contentAssembly. Cette méthode prend deux arguments, le template à utiliser pour l'envoi du mail, et un tableau avec les variables pour structurer le template.

Par défaut les méthodes dans cet objet sont :
- sendExemple($email, $message) qui est la méthode basique de création et d'envoi d'email, elle permet d'envoyer un message à l'adresse email renseigné.

Le header et le footer seront eux modifiables. Le template de base se situe dans template/app/email/layout.
Il faudra donc aller structurer le template pour l'envoi d'email avec les variables renseigné dans contentAssembly:

Pour modifier le style de l'email, il est possible de créer une page et faire des includes pour le visioner sans avoir besoin d'envoyer un email.
<h4 style="text-align : center; color : #ff5151;">! Les modifications du style pour un email devront forcément être à l'intérieur des balises sinon le style ne sera pas appliqué lors de la récéption !</h4>



### Créer une page
- Pour chacune des "pages" de vos applications, une méthode de contrôleur devrait être définie.
  C'est notamment pour cette raison que vous ressentirez le besoin de créer plusieurs contrôleurs,
  afin de "classer" vos méthodes, qui deviendront rapidement nombreuses.
- Ces méthodes doivent être de visibilité ```public```, et devrait normalement se terminer par l'une des actions suivantes :
```php
/* src/Controller/DefaultController.php */
public function contact()
{
	//affiche un template
	$this->render('app.default.contact');
	//affiche un template, tout en rendant des données disponibles dans celui-ci
	//template disponible dans le dossier template/app/default/contact.php
	$this->render('app.default.contact',['username' => 'michel']);
        //redirige vers une page du site
        $this->redirect('contact');
	//redirige vers un site externe
	$this->redirect('https://weblitzer.com');
	//retourne une réponse JSON
	$this->showJson([]);
	//retourne une erreur 404
	$this->Abort404();
}
```

## Model
```/src/Model```

### À quoi servent les modèles ?
- Les modèles sont les classes responsables d'exécuter <em>les requêtes à votre base de données</em>.
- Concrètement, chaque fois que vous souhaitez faire une requête à la base de données, vous devriez venir y créer une fonction qui s'en chargera (sauf si elle existe déjà dans les modèles de base du framework).</p>

### Comment créer un nouveau modèle ?
- Dans votre application, vous pourriez avoir un modèle par table MySQL (sans obligation). Chacune de ces classes devraient hériter de ```Core\Kernel\Model\```, le modèle de base du framework, qui vous fera profiter de quelques méthodes utiles pour faire les principales requêtes de base à la base de données.

Par exemple, pour créer un modèle relié à une table fictive de commentaires nommées ```Comment``` :

```php
// src/Model/CommentModel.php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CommentModel extends AbstractModel 
{
    protected static $table = 'comments';
    protected int $id;
    protected string $title;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }
    public function getTitleAndID() 
    {
        return '<p>'.$this->title.' : '.$this->id.'</p>';
    }
    public static function simpleRequest()
    {
        return App::getDatabase()->query("SELECT * FROM " . self::getTable() . " AS c",get_called_class());
    }
    public static function findOneById($id)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE id = ?",array($id),get_called_class(),true );
    }   
	public static function countPerso()
    {
        return App::getDatabase()->aggregation("SELECT COUNT(id) FROM " . self::getTable());
    }
	public static function insert($post)
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (title, created_at) VALUES (?,NOW())", array($post['title']]));
    }
}
```
### Les propriétés et méthodes héritées de AbstractModel
- Voici les propriétés et les méthodes les plus utiles, héritées du modèle de base.
- Vous devrez créer vos propres méthodes pour réaliser vos requêtes SQL plus complexes.
```php
/* Core/Kernel/AbstractModel.php */
// Récupère une ligne de la table en fonction d'un identifiant
public function all()
public function findById($id)
public function findByColumn($column,$value)
public function count()
public function delete($id)
```
