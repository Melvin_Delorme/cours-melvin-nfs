<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Framework Pédagogique MVC6</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
          integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
          crossorigin=""/>
    <?php echo $view->add_webpack_style('app'); ?>
</head>

<body>
<?php // $view->dump($view->getFlash())
?>

<header id="masthead" class="masthead">
    <div class="masthead__left">
        <a href="<?= $view->path('home'); ?>"><img class="logo_header" src="<?php echo $view->asset('img/logo_white.png') ?>" alt="header_logo"></a>
    </div>
    <ul class="masthead__right">
        <li><a class="nav_header" href="<?= $view->path('home'); ?>#squad--container">Notre equipe</a></li>
        <?php if($view->isAuthorizedRole(array('Parent'))) { ?>
            <li><a class="nav_header logout_btn" href="<?= $view->path('logout'); ?>">Deconnexion</a></li>
            <li><a class="nav_header logout_btn" href="<?= $view->path('espaceparent&child'); ?>">Mon espace Parent</a></li>
        <?php } elseif($view->isAuthorizedRole(array('professionel'))){ ?>
            <li><a class="nav_header logout_btn" href="<?= $view->path('logout'); ?>">Deconnexion</a></li>
            <li><a class="nav_header logout_btn" href="<?= $view->path('contact'); ?>">Mon espace Pro</a></li>
        <?php } elseif($view->isAuthorizedRole(array('Admin'))) { ?>
            <li><a class="nav_header logout_btn" href="<?= $view->path('logout'); ?>">Deconnexion</a></li>
            <li><a class="nav_header logout_btn" href="<?= $view->path('admin'); ?>">Admin</a></li>
       <?php } else { ?>
            <li><a class="nav_header login_btn" href="<?php echo $view->path('login')?>">Connexion</a></li>
            <li><a class="nav_header register_btn" href="<?= $view->path('register'); ?>">Inscription</a></li>
        <?php } ?>
    </ul>
</header>

<div class="container">
    <?= $content; ?>
</div>

<footer id="colophon">
    <div class="footer_wrap">
        <div class=" footer_box box1">
            <img src="<?php echo $view->asset('img/notxt_logo.png'); ?>" alt="">
            <a href="<?= $view->path('mentionslegales'); ?>">Mentions légales</a>
            <a href="<?= $view->path('cgu');?>">Conditions générales</a>
        </div>
        <div class="footer_box box2">
            <h3>Agence de Paris</h3>
            <div class="adress1">
                <p>PARIS - 1515 rue du Hameau 75015</p>
                <p>ParisTél : 01 46 05 32 88</p>
            </div>
            <div class="adress2">
                <p>PARIS - 1515 rue du Hameau 75015</p>
                <p>ParisTél : 01 46 05 32 88</p>
            </div>
        </div>
        <div class="footer_box box3">
            <h3>Agences de Boulogne</h3>
            <p>35, rue de Sèvres 92100</p>
            <p>Boulogne-Billancourt</p>
            <p>ParisTél : 01 46 05 32 88</p>
        </div>
        <div class="footer_box box4">
            <h3>Nos réseaux</h3>
            <div class="social">
                <a class="social__icon insta" href="https://www.instagram.com"><i class="fa-brands fa-instagram"></i></a>
                <a class="social__icon linkedin" href="https://www.linkedin.com"><i class="fa-brands fa-linkedin"></i></a>
                <a class="social__icon facebook" href="https://www.facebook.com"><i class="fa-brands fa-facebook"></i></a>
                <a class="social__icon twitter" href="https://twitter.com"><i class="fa-brands fa-twitter"></i></a>
            </div>
        </div>

    </div>
</footer>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<?php echo $view->add_webpack_script('app'); ?>
</body>
</html>