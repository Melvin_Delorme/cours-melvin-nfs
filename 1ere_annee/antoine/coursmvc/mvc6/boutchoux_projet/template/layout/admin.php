<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Bout'Choux Admin</title>
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
            integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
            crossorigin=""/>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>

<div class="container">
    <div class="admin_barleft" id="admin-barleft">
        <div class="abl_logo">
            <div class="abl_logo2">
                <a href="<?= $view->path('admin');?>"><img src="<?php echo $view->asset('img/logo_white.png')?>" alt="logo"></a>
            </div>
            <div class="abl_button">
                <i class="fa-solid fa-bars"></i>
            </div>
        </div>
        <div class="abl_logged">
            <div class="abl_logged_left">
                <div class="abl_logged_img">
                    <img src="<?php echo $view->asset('img/pp_admin.png')?>" alt="Photo de profil">
                </div>
            </div>
            <div class="abl_logged_right">
                <div class="abl_logged_right2">
                    <h1>Lucas Okemba</h1>
                    <h2>Administrateur</h2>
                </div>
            </div>
        </div>
        <div class="abl_bar"></div>
        <div class="abl_list">
            <ul>
                <li class="abl_list_top">
                    <a href="<?= $view->path('admin');?>">
                        <i class="fa-solid fa-gauge-simple-high"></i>
                        <h1>Dashboard</h1>
                    </a>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton">
                        <i class="fa-solid fa-user"></i>
                        <h1>Utilisateurs</h1>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="<?= $view->path('useradmin');?>">Admin</a>
                        <a class="dropdown-item" href="<?= $view->path('proadmin');?>">Professionels</a>
                        <a class="dropdown-item last-item" href="<?= $view->path('particulieradmin');?>">Parents</a>
                    </div>
                </li>


                <li class="dropdown2">
                    <a href="#" class="dropdown-toggle2" type="button" id="dropdownMenuButton2">
                        <i class="fa-solid fa-file-lines"></i>
                        <h1>Pieces Justificatives</h1>
                    </a>
                    <div class="dropdown-menu2" aria-labelledby="dropdownMenuButton2">
                        <a class="dropdown-item" href="<?= $view->path('piecesproadmin');?>">Professionels</a>
                        <a class="dropdown-item last-item" href="<?= $view->path('piecesparticulieradmin');?>">Parents</a>
                    </div>
                </li>

                <li>
                    <a href=<?= $view->path('reservationadmin');?>>
                        <i class="fa-regular fa-calendar-check"></i>
                        <h1>Réservations</h1>
                    </a>
                </li>

                <li>
                    <a href="<?= $view->path('paiementadmin');?>">
                        <i class="fa-solid fa-credit-card"></i>
                        <h1>Paiement</h1>
                    </a>
                </li>

                <li>
                    <a href="<?= $view->path('contactadmin');?>">
                        <i class="fa-solid fa-envelope"></i>
                        <h1>Contact</h1>
                    </a>
                </li>

                <li>
                    <a href="<?= $view->path('avisadmin');?>">
                        <i class="fa-solid fa-thumbs-up"></i>
                        <h1>Avis</h1>
                    </a>
                </li>
            </ul>
        </div>
        <div class="abl_link">
            <ul>
                <div class="displayed_link">
                    <li><a href="<?= $view->path('');?>">Retourner a l'accueil</a></li>
                    <li><a href="<?= $view->path('logout'); ?>">Se déconnecter</a></li>
                </div>
                <div class="undisplayed_link">
                    <li><a href="<?= $view->path('');?>"><i class="fa-solid fa-house"></i></a></li>
                    <li><a href="<?= $view->path('logout'); ?>"><i class="fa-solid fa-right-from-bracket"></i></a></li>
                </div>
            </ul>
        </div>
    </div>
    <div class="admin_content">
        <?= $content; ?>
    </div>
</div>


<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
