<?php
$textButton = 'CONNEXION';
?>


<section id="contact">
    <img src="<?php echo $view->asset('img/maman_banner.jpg') ?>" alt="">
    <div class="overlay_contact"></div>
    <div class="text_contact">
        <div class="container_contact">
            <h2>Connectez-Vous</h2>
            <p>Vous pouvez vous connecter grâce au formulaire de connexion ci-dessous en appuyant ici</p>
            <div class="top animation">
                <a href="#contact_form">
                    <i class="fa-solid fa-arrow-down"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<div id="contact_form" class="register">
    <div class="box_contact">
        <img src="<?php echo $view->asset('img/logo_inscription_transparent.png') ?>" alt="">
        <h3>Connexion</h3>
    </div>
    <div class="bar"></div>
    <form action="" method="post" novalidate class="wrapform">
        <?php echo $form->label('email'); ?>
        <?php echo $form->input('email','email', $connexion->email ?? '') ?>
        <?php echo $form->error('email'); ?>

        <?php echo $form->label('mot de passe'); ?>
        <?php echo $form->input('password','password') ?>
        <?php echo $form->error('password'); ?>
        <?php echo $form->submit('submitted', $textButton); ?>
        <a href="<?= $view->path('register'); ?>" class="lien_login"> <p> Vous n'avez pas de compte ? Inscrivez-vous ! </p></a>
        <a href="<?= $view->path('forgotpassword'); ?>" class="lien_login"> <p> Mot de passe oublié ? </p></a>
    </form>
</div><