<section id="contact">
    <img src="<?php echo $view->asset('img/maman_banner.jpg') ?>" alt="">
    <div class="overlay_contact"></div>
    <div class="text_contact">
        <div class="container_contact">
            <h2>Réinitialisation de Votre Mot de passe</h2>
            <p>Ne laissez pas un mot de passe oublié vous bloquer - réinitialisez-le dès maintenant !</p>
            <div class="top animation">
                <a href="#contact_form">
                    <i class="fa-solid fa-arrow-down"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<div id="contact_form" class="register">
    <div class="box_contact">
        <img src="<?php echo $view->asset('img/logo_inscription_transparent.png') ?>" alt="">
        <h3>Recupération du mot de passe</h3>
    </div>
    <div class="bar"></div>
    <form action="" method="post" novalidate class="wrapform">

        <?php echo $form->label('Nouveau mot de passe'); ?>
        <?php echo $form->input('password','password') ?>

        <?php echo $form->label('Confirmation du nouveau mot de passe'); ?>
        <?php echo $form->input('password2','password') ?>
        <?php echo $form->error('password'); ?>

        <?php echo $form->submit('submitted','Confirmer'); ?>
    </form>
</div>

