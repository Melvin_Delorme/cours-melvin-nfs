<section id="contact">
<img src="<?php echo $view->asset('img/maman_banner.jpg') ?>" alt="">
<div class="overlay_contact"></div>
<div class="text_contact">
    <div class="container_contact">
        <h2> Mot de passe oublié</h2>
        <p>Vous avez oublié votre mot de passe ne vous inquiétez pas, nous sommes là pour vous !</p>
        <div class="top animation">
            <a href="#contact_form">
                <i class="fa-solid fa-arrow-down"></i>
            </a>
        </div>
    </div>
</div>
</section>

<div id="contact_form" class="register">
    <div class="box_contact">
        <img src="<?php echo $view->asset('img/logo_inscription_transparent.png') ?>" alt="">
        <h3>Mot de passe oublié</h3>
    </div>
    <div class="bar"></div>
    <form action="" method="post" novalidate class="wrapform">

        <?php if(empty($lien)) {?>
        <?php echo $form->label('Email'); ?>
        <?php echo $form->input('email','email'); ?>
        <?php echo $form->error('email'); ?>
        <?php echo $form->submit('submitted','Confirmer'); ?>
        <a  href="<?= $view->path('login'); ?>"><i class="fa-solid lien_home fa-arrow-right-from-bracket"></i></a>
        <?php } else {?>
        <?= $lien; }
        ?>
    </form>
</div>
