<section id="contact">
    <img src="<?php echo $view->asset('img/maman_banner.jpg') ?>" alt="">
    <div class="overlay_contact"></div>
    <div class="text_contact">
        <div class="container_contact">
            <h2>Inscrivez-vous</h2>
            <p>Vous pouvez vous inscrire grâce au formulaire d’inscription ci dessous en cliquant ici</p>
            <div class="top animation">
                <a href="#contact_form">
                    <i class="fa-solid fa-arrow-down"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<div id="contact_form" class="register">
    <div class="box_contact">
        <img src="<?php echo $view->asset('img/logo_inscription_transparent.png') ?>" alt="">
        <h3>Inscription</h3>
    </div>
    <div class="bar"></div>
    <form action="" method="post" novalidate class="wrapform">
        <?php echo $form->label('Nom'); ?>
        <?php echo $form->input('nom','text', $register->nom ?? '') ?>
        <?php echo $form->error('nom'); ?>

        <?php echo $form->label('prenom'); ?>
        <?php echo $form->input('prenom','text', $register->prenom ?? '') ?>
        <?php echo $form->error('prenom'); ?>


        <?php echo $form->label('Email'); ?>
        <?php echo $form->input('email','email', $register->email ?? '') ?>
        <?php echo $form->error('email'); ?>


        <?php echo $form->label('mot de passe ', 'Mot de passe'); ?>
        <?php echo $form->input('password','password') ?>

        <?php echo $form->label('Confirmez votre mot de passe', 'Confirmation de mot de passe'); ?>
        <?php echo $form->input('password2','password') ?>
        <?php echo $form->error('password'); ?>

        <?php echo $form->label('Êtes-vous ?'); ?>
        <div class="register_checkbox_input">
            <div class="field">
                <?php echo $form->checkbox('checkbox','checkbox1','Parent'); ?>
                <label class="checkbox1" for="checkbox1">Parent</label>
            </div>
            <div class="field">
                <?php echo $form->checkbox('checkbox','professionel','professionel'); ?>
                <label class="professionel" for="professionel">Professionel</label>
            </div>
        </div>
        <?php echo $form->error('checkbox'); ?>

        <?php echo $form->submit('submitted','INSCRIPTION'); ?>
        <a href="<?= $view->path('login'); ?>" class="lien_login"> <p> Vous avez un compte ? Connecter-vous ! </p></a>
    </form>
</div>