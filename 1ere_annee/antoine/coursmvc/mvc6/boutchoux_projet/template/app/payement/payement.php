<section id="payment_page">
    <div class="pay_top">
        <img src="<?php echo $view->asset('img/logo_pay.png'); ?>" alt="logo">
    </div>
    <div class="pay_bottom">
        <div class="payb_left">
            <div class="payment_form">
                <div class="pay_form_top">
                    <h1>Payment Information</h1>
                    <i class="fa-solid fa-credit-card"></i>
                </div>
                <div class="pay_form_bottom">
                    <form action="" method="post" novalidate>
                        <div class="card-js">
                            <input class="card-number my-custom-class" name="card-number">
                            <input class="name" id="the-card-name-id" name="card-holders-name" placeholder="Name on card">
                            <input class="expiry-month" name="expiry-month">
                            <input class="expiry-year" name="expiry-year">
                            <input class="cvc" name="cvc">
                        </div>
                        <div class="form_errors" data-creditcard="<?php echo $creditcard ? 'true' : 'false'; ?>">
                            <?php if($formcard->error('card-number') || $formcard->error('card-holders-name') || $formcard->error('expiry-month') || $formcard->error('expiry-year') || $formcard->error('cvc')) { ?>
                                <span class="error">Informations Incorrectes</span>
                            <?php } elseif ($creditcard) { ?>
                                <span>Payement Effectué</span>
                                <div id="modal_payment">
                                    <div id="modal_payment2">
                                        <i class="fa-solid fa-face-smile-wink fa-bounce"></i>
                                        <h1>Payement Effectué</h1>
                                    </div>
                                </div>
                            <?php } elseif ($errorcard) { ?>
                                <span class="error">Carte Bleue Inexistante</span>
                            <?php } ?>
                        </div>
                        <?php echo $formcard->submit('submitted', 'Valider'); ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="payb_right">
            <img src="<?php echo $view->asset('img/child_banner2.jpg'); ?>" alt="">
            <div class="pay_underform">
                <div class="pay_underform2">
                    <!--Lien a changer-->
                    <a href="<?= $view->path(''); ?>" onclick="return confirm('Voulez-vous vraiment quitter cette page ?')"><i class="fa-solid fa-arrow-left"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>





