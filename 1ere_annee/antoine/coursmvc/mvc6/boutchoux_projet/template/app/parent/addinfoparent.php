<div id="contact_form" class="register">
    <div class="box_contact">
        <img src="<?php echo $view->asset('img/logo_inscription_transparent.png') ?>" alt="">
        <h3>Compléter vos informations</h3>
    </div>
    <div class="bar"></div>
    <form action="" method="post" novalidate class="wrapform">
        <?php echo $form->label('Telephone'); ?>
        <?php echo $form->input('telephone', 'text', $addinfoparent->telephone ?? '') ?>
        <?php echo $form->error('telephone'); ?>

        <div class="dropdown">
            <?php echo $form->label('Adresse'); ?>
            <?php echo $form->input('adresse', 'text', '', 'off') ?>
            <div class="dropdown-content">
                <ul id="ville-list"></ul>
            </div>
        </div>
        <?php echo $form->error('adresse'); ?>

        <?php echo $form->label('Êtes-vous une Assistante Marternelle ?'); ?>
        <div class="register_checkbox_input">
            <div class="field">
                <?php echo $form->checkbox('checkbox', 'checkbox1', 'oui'); ?>
                <label class="checkbox1" for="checkbox1">oui</label>
            </div>
            <div class="field">
                <?php echo $form->checkbox('checkbox', 'professionel', 'non'); ?>
                <label class="professionel" for="professionel">non</label>
            </div>
        </div>
        <?php echo $form->error('checkbox'); ?>

        <?php echo $form->submit('submitted', 'Envoyer'); ?>
    </form>
</div>