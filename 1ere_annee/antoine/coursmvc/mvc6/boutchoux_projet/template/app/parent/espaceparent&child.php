<section id="spaceparent">
    <div class="container_spaceparent">
        <h2>Votre espace parent !</h2>
        <p>Ici vous pouvez gérer vos réservations ainsi que le profil de vos enfants !</p>
    </div>
</section>

<section id="profil_child" class="wrap">
    <div class="allcard" class="wra2">
        <div class="left">
            <a class="linkAddChild" href="<?php echo $view->path('addChildren'); ?>">Ajouter un enfant</a>
            <div class="container--map">
                <div class="map" id="map"></div>
                <div class="container--txt">
                    <div class="bar"></div>
                    <h2>Regarder les crèches</h2>
                    <h2>ou assistante près de vous <i class="fa-sharp fa-solid lien fa-face-smile-wink"></i></h2>
                </div>
                <input id="search" type="text" placeholder="Saisisser une adresse">
            </div>
        </div>
        <div class="right">
            <div class="title">
                <h2>Vos enfants</h2>
                <div class="bar"></div>
            </div>
            <div class="container--listing">
                <div class="ChildProfil">
                    <img src="<?php echo $view->asset('img/icone_boy.png'); ?>" alt="icone_boy">
                    <div class="ChildProfil--txt">
                        <p>Theo Lemettais</p>
                        <p>6 ans</p>
                    </div>
                </div>
                <div class="options">
                    <a href=""><i class="fa-solid fa-trash"></i></a>
                    <a href=""><i class="fa-solid fa-pen-to-square"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container--listingReservation">
        <div class="listingTitle">
            <p>A venir</p>
            <p>Annulé</p>
            <p>Validé</p>
        </div>
        <div class="listing--data">
            <div class="a_venir">
                <p>16/03/2023 : 8h-18h / Paris - 1515 rue du Hammeau</p>
                <p>16/03/2023 : 8h-18h / Paris - 1515 rue du Hammeau</p>
            </div>
            <div class="annule">
                <p>16/03/2023 : 8h-18h / Paris - 1515 rue du Hammeau</p>
            </div>
            <div class="valide">
                <p>16/03/2023 : 8h-18h / Paris - 1515 rue du Hammeau</p>
                <p>16/03/2023 : 8h-18h / Paris - 1515 rue du Hammeau</p>
                <p>16/03/2023 : 8h-18h / Paris - 1515 rue du Hammeau</p>
            </div>
        </div>
    </div>
</section>