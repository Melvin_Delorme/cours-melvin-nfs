<div class="background-image-header">
    <img src="" alt="">
</div>

<section id="addChildren">
    <div class="bigWrap">

        <form action="" method="post" novalidate>
            <div class="form">
                <div class="childInfo">
                    <div class="left">
                        <div class="eclipse">
                            <img src="<?= $view->asset('img/childrenBoy.png') ?>" alt="">
                        </div>
                        <div class="input">
                            <?php echo $form->label('Nom'); ?>
                            <?php echo $form->input('nom') ?>
                            <?php echo $form->error('nom'); ?>

                            <?php echo $form->label('Prenom'); ?>
                            <?php echo $form->input('prenom'); ?>
                            <?php echo $form->error('prenom'); ?>

                            <?php echo $form->label('Age'); ?>
                            <?php echo $form->input('age'); ?>
                            <?php echo $form->error('age'); ?>
                        </div>
                    </div>
                    <div class="right">
                        <?php echo $form->label('Genre'); ?>
                        <?php echo $form->select('genre', array('Homme', 'Femme')) ?>
                        <?php echo $form->error('genre'); ?>

                        <?php echo $form->label('Date de naissance'); ?>
                        <?php echo $form->input('date_de_naissance', 'date'); ?>
                        <?php echo $form->error('date_de_naissance'); ?>
                    </div>
                </div>
                <div class="speMed">
                    <div class="title">
                        <h2>Spécification Médicale</h2>
                    </div>
                    <div class="input">
                        <?php echo $form->label('Renseigner le nom ici...'); ?>
                        <?php echo $form->input('speMedTitle'); ?>
                        <?php echo $form->error('speMedTitle'); ?>

                        <?php echo $form->label('Information Importante ( réaction, allergie, traitement)...'); ?>
                        <?php echo $form->textarea('speMedDesc'); ?>
                        <?php echo $form->error('speMedDesc'); ?>
                    </div>
                </div>
                <div class="speAlim">
                    <div class="title">
                        <h2>Spécification Alimentaire</h2>
                    </div>
                    <div class="input">
                        <?php echo $form->label('Renseigner le nom ici...'); ?>
                        <?php echo $form->input('speAlimTitle'); ?>
                        <?php echo $form->error('speAlimTitle'); ?>

                        <?php echo $form->label('Information Importante ( réaction, allergie, traitement)...'); ?>
                        <?php echo $form->textarea('speAlimDesc'); ?>
                        <?php echo $form->error('speAlimDesc'); ?>
                    </div>
                </div>
            </div>
            <?php echo $form->submit('submitted'); ?>
        </form>
    </div>
    </div>
</section>