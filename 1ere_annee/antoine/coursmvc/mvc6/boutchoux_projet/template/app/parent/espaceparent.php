<section id="bar_profil">
    <?php if (empty($result)) { ?>
        <div id="completer-profil">
            <a href="<?php echo $view->path('addinfoparent'); ?>">Veuillez compléter votre profil <span class="profil_span">*</span></a>
        </div>
    <?php } else { ?>
        <div id="completer-profil">
            <p>Votre profil est complet</p>
        </div>
    <?php } ?>
</section>

<section id="spaceparent">
    <div class="container_spaceparent">
        <h2>Bienvenue dans Votre espace parent !</h2>
        <?php if (!empty($result)) { ?>
            <p>Cliquez sur le lien afin d'accéder à votre espace.</p>
            <a class="lien_space" href="<?php echo $view->path('espaceparent&child'); ?>">Accédez à votre espace</a>
        <?php }else{ ?>
            <p>Veuillez compléter votre le profil afin de pouvoir accéder à votre espace.</p>
        <?php } ?>
    </div>
</section>
