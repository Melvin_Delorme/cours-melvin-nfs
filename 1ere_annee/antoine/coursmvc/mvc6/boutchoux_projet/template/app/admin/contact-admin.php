<section id="user-admin">
    <h1 class="title">Contact</h1>
    <p class=""></p>
    <table>
        <thead>
        <tr>
            <th>email</th>
            <th>sujet</th>
            <th>message</th>
            <th>Crée le</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($contacts as $contact) { ?>
            <tr>
                <td><?php echo $contact->email ?></td>
                <td><?php echo $contact->sujet ?></td>
                <td><?php echo $contact->message ?></td>
                <td><?php echo date('d/m/Y', strtotime($contact->createdat)); ?></td>
                <td><button class="edit">Modifier</button></td>
                <td><button>Supprimer</button></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</section>