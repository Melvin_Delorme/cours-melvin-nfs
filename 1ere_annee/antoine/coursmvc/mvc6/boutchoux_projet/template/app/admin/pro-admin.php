<section id="pro-admin">
        <h1 class="title">Professionnels</h1>
        <p class=""></p>
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Email</th>
                <th>Inscrit le</th>
                <th>Modifié le</th>
                <th>Nombre de connexions</th>
                <th>Modifier</th>
                <th>Bannir</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?php echo $user->nom ?></td>
                    <td><?php echo $user->prenom ?></td>
                    <td><?php echo $user->email ?></td>
                    <td><?php echo date('d/m/Y', strtotime($user->createdat)); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($user->modifiedat)); ?></td>
                    <td><?php echo $user->countlogin ?></td>
                    <td><button class="edit">Modifier</button></td>
                    <td><button>Bannir</button></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
</section>