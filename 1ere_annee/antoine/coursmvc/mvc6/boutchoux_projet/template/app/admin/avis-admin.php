<section id="user-admin">
    <h1 class="title">Avis</h1>
    <p class=""></p>
    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>message</th>
            <th>Crée le</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($avis as $avi) { ?>
            <tr>
                <td><?php echo $avi->nom ?></td>
                <td><?php echo $avi->prenom ?></td>
                <td><?php echo $avi->message ?></td>
                <td><?php echo date('d/m/Y', strtotime($avi->createdat)); ?></td>
                <td><button class="edit">Modifier</button></td>
                <td><button>Supprimer</button></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</section>