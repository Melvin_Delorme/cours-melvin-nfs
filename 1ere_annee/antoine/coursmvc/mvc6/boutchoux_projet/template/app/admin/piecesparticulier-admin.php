<section id="user-admin">
    <h1 class="title">Pieces Justificatives (Particulier)</h1>
    <p class=""></p>
    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Pièce</th>
            <th>Modifié le</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <?php foreach ($pieces as $piece) { ?>
            <tr>
                <td><?php echo $piece->nom ?></td>
                <td><?php echo $piece->pieces ?></td>
                <td><?php echo date('d/m/Y', strtotime($piece->updatedat)); ?></td>
                <td><button class="edit">Modifier</button></td>
                <td><button>Supprimer</button></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</section>