<section id="user-admin">
        <h1 class="title">Reservations</h1>
        <p class=""></p>
        <table>
            <thead>
            <tr>
                <th>Commence le</th>
                <th>Se termine le</th>
                <th>Status</th>
                <th>Nbre Heures</th>
                <th>Crée le</th>
                <th>Modifier</th>
                <th>Supprimer</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($reservations as $reservation) { ?>
                <tr>
                    <td><?php echo $reservation->startat ?></td>
                    <td><?php echo $reservation->endat ?></td>
                    <td><?php echo $reservation->status ?></td>
                    <td><?php echo $reservation->nbrehours ?></td>
                    <td><?php echo date('d/m/Y', strtotime($reservation->createdat)); ?></td>
                    <td><button class="edit">Modifier</button></td>
                    <td><button>Supprimer</button></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
</section>