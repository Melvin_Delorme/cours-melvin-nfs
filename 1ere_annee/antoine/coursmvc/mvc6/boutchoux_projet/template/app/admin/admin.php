<section id="admin_homepage">
    <div class="section_homepage1">
        <div class="sh1_left">
            <h1>Inscriptions</h1>
            <div class="sh1l_boxs">
                <div class="sh1l_box1">
                    <h1><?php echo count($userscreches) ?></h1>
                    <p>Assistantes Maternelles</p>
                </div>
                <div class="sh1l_box2">
                    <h1><?php echo count($usersassistantes) ?></h1>
                    <p>Crèches</p>
                </div>
            </div>
            <div class="sh1l_chart">
                <div>
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
        <div class="sh1_right">
            <h1>Informations professionelles</h1>
            <div class="sh1r_boxs">
                <div class="s1hr_box1">
                    <h4>Taux de verification des pieces justificatives:</h4>
                    <h1>80%</h1>
                </div>
                <div class="s1hr_box2">
                    <h4>Nombre de demandes d'exportation des données dans des formats exploitables:</h4>
                    <h1>264</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section_homepage2">
        <h1>Reservations</h1>
        <div class="sh2_all">
            <div class="sh2_left">
                <div class="sh2l_chart">
                    <div>
                        <canvas id="myChart2"></canvas>
                    </div>
                </div>
                <div class="sh2l_text">
                    <div class="sh2lt1">
                        <h2>Demandes de reservations:</h2>
                        <h3><?php echo count($reservationsenattente) ?></h3>
                    </div>
                    <div class="sh2lt2">
                        <h2>Nombres de reservations effectuées:</h2>
                        <h3><?php echo count($reservationsvalid) ?></h3>
                    </div>
                </div>
            </div>
            <div class="sh2_right">
                <div class="sh2_right2">
                    <h1>Temps de réponse moyen</h1>
                    <h2>1H</h2>
                    <h3>Precedement:</h3>
                    <div class="sh2r_bottom">
                        <div class="sh2rb1">
                            <h4>Juin</h4> <p>3J</p>
                        </div>
                        <div class="sh2rb2">
                            <h4>Mai</h4> <p>5J</p>
                        </div>
                        <div class="sh2rb3">
                            <h4>Avril</h4> <p>6J</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section_homepage3">
        <h1>Paiement</h1>
        <div class="sh3_all">
            <div class="sh3_left">
                <div class="sh3_boxs">
                    <div class="sh3_box1">
                        <h2>Nombres de factures envoyées aux particuliers et aux professionels:</h2>
                        <h1>62</h1>
                    </div>
                    <div class="sh3_box2">
                        <h2>Taux de paiement des factures par les particuliers et les professionels:</h2>
                        <h1>26</h1>
                    </div>
                </div>
            </div>
            <div class="sh3_right">
                <div class="sh3r_chart">
                    <div>
                        <canvas id="myChart3"></canvas>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>