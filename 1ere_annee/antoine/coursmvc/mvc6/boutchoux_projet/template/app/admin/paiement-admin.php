<section id="user-admin">
    <h1 class="title">Paiement</h1>
    <p class=""></p>
    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Code</th>
            <th>CVV</th>
            <th>Expire le</th>
            <th>Crée le</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($paiements as $paiement) { ?>
            <tr>
                <td><?php echo $paiement->nom ?></td>
                <td><?php echo $paiement->prenom ?></td>
                <td><?php echo $paiement->code ?></td>
                <td><?php echo $paiement->cvv ?></td>
                <td><?php echo $paiement->expireat ?></td>
                <td><?php echo date('d/m/Y', strtotime($paiement->createdat)); ?></td>
                <td><button class="edit">Modifier</button></td>
                <td><button>Supprimer</button></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</section>