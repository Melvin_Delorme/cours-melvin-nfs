<div style="background: #F5F5F5; border: 2px solid #CCCCCC; padding: 20px; border-radius: 10px;">
    <h3 style="font-size: 24px; color: #f68c11; margin-top: 0;">Nouveau message de contact<i class="fa-solid lien_contact fa-messages" style="font-size: 30px; margin-left: 10px; color: #f68c11"></i></h3>
    <p style="font-size: 14px; text-align: left; color: #333333; margin-bottom: 10px;">Bonjour,</p>
    <p style="font-size: 14px; color: #333333; text-align: left; margin-top: 0;">Vous avez reçu un nouveau message de contact de la part de <strong><?php echo $email; ?></strong>.</p>
    <ul style="list-style: none; padding: 0; margin: 0;">
        <li style="font-size: 14px; text-align: left; color: #333333; margin-bottom: 10px;"><strong>Sujet :</strong> <?php echo $subject; ?></li>
        <li style="font-size: 18px; text-align: left; color: #333333; margin-bottom: 10px;"><strong>Message :</strong></li>
        <li style="font-size: 14px; text-align: left; color: #333333;"><?php echo $content; ?></li>
    </ul>
    <p style="font-size: 14px; color: #333333; text-align: left; margin-top: 20px;">Merci de prendre en compte ce message.<br>Nous sommes reconnaissants de votre soutien continu à notre équipe.</p>
</div>


