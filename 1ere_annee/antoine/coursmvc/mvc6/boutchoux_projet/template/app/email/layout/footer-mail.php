</td>
</tr>
<tr>
    <td style="padding: 0 20px 20px;">
        <!-- Button : BEGIN -->
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
            <tr>
                <td class="button-td button-td-primary" style="border-radius: 24px; background: #6e0a34;">
                    <a class="button-a button-a-primary" href="http://localhost:3838/home" style="background:  #ed166d; font-family: 'Alkatra', cursive; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; color: #ffffff; display: block; border-radius: 24px;">Retourner sur le site</a>
                </td>
            </tr>
        </table>
        <!-- Button : END -->
    </td>
</tr>

</table>
</td>
</tr>
<!-- 1 Column Text + Button : END -->

<!-- 2 Even Columns : BEGIN -->

<!-- 2 Even Columns : END -->

<!-- Clear Spacer : BEGIN -->

<!-- Clear Spacer : END -->

<!-- 1 Column Text : BEGIN -->

<!-- 1 Column Text : END -->

</table>
<!-- Email Body : END -->

<!-- Email Footer : BEGIN -->

<!-- Email Footer : END -->


<!--[if mso]>

<![endif]-->
</div>

<!-- Full Bleed Background Section : BEGIN -->
<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #0C684B ;">
    <tr>
        <td>
            <div align="center" style="max-width: 600px; margin: auto;" class="email-container">
                <!--[if mso]>
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                <tr>
                    <td>
                <![endif]-->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td style="padding: 20px; text-align: center; font-family: 'Alkatra', cursive; font-size: 15px; line-height: 20px; color: #ffffff;">
                            <p style="margin: 0">Merci d'avoir choisi notre site !</p>
                            <p style="margin: 0">Nous prenons soin de vos petits  boutchou.</p>
                            <a style="font-size: 40px;color: #ed166d " href="https://www.instagram.com/"><i class="fa-brands animation_lien fa-instagram"></i></a>
                            <a style="font-size: 40px;color: #ed166d "   href="https://www.facebook.com/"><i class="fa-brands animation_lien fa-facebook"></i></a>
                        </td>
                    </tr>
                </table>
                <!--[if mso]>
                <![endif]-->
            </div>
        </td>
    </tr>
</table>
<!-- Full Bleed Background Section : END -->

<!--[if mso | IE]>

<![endif]-->
</center>
</body>
</html>
