<div class="backgroundImg">
    <img src="<?= $view->asset('img/introBg.png') ?>" alt="">
</div>

<section id="partenaire--container">
    <h2>VOTRE PARTENAIRE ENFANCE</h2>
    <div class="line"></div>
    <p>Disposant d’un Agrément (SAP 533475 497) et d’un certification Qualité (QUALISAP), Bout’Choux propose une approche personnalisée et adaptée au profil de chaque enfant grâce à une analyse préalable (Rythme et environnement) par nos Conseillères Enfance.</p>
</section>

<section id="partenaire--enfance">
    <div class="partenaire--img">
        <img src="<?= $view->asset('img/Break.png') ?>" alt="">
    </div>
    <div class="background--partenaire">
        <div class="partenaire--intro">
            <h2>Votre partenaire enfance</h2>
            <div class="line"></div>
        </div>
        <div class="partenaire--cards">
            <div class="card first">
                <div class="eclipse">
                    <img src="<?= $view->asset('img/cardIcon1.png') ?>" alt="">
                </div>
                <h2>ÉVEIL À DOMICILE PETITE ENFANCE</h2>
                <p>Bout’Choux s’appuie sur le savoir-faire et l’expérience de son réseau de crèches pour proposer des services d’éveil à domicile pour les enfants de moins de trois ans.</p>
            </div>
            <div class="card second">
                <div class="eclipse">
                    <img src="<?= $view->asset('img/cardIcon3.png') ?>" alt="">
                </div>
                <h2>ÉVEIL À DOMICILE PETITE ENFANCE</h2>
                <p>Bout’Choux s’appuie sur le savoir-faire et l’expérience de son réseau de crèches pour proposer des services d’éveil à domicile pour les enfants de moins de trois ans.</p>
            </div>
            <div class="card third">
                <div class="eclipse">
                    <img src="<?= $view->asset('img/cardIcon2.png') ?>" alt="">
                </div>
                <h2>ÉVEIL À DOMICILE PETITE ENFANCE</h2>
                <p>Bout’Choux s’appuie sur le savoir-faire et l’expérience de son réseau de crèches pour proposer des services d’éveil à domicile pour les enfants de moins de trois ans.</p>
            </div>
        </div>
    </div>
</section>

<section id="working">
    <div class="working--container">
        <div class="left">
            <div class="eclipse">
                <img src="<?= $view->asset('img/workingImg.png') ?>" class="workImg" alt="">
            </div>
        </div>
        <div class=" right">
            <h2>NOTRE FONCTIONNEMENT</h2>
            <div class="line"></div>
            <p>Bout’Choux s’attache au bien-être de l’enfant, en respectant son rythme et en mettant en oeuvre un environnement lui permettant de développer son autonomie et sa socialisation. Bout’Choux s’adapte à vos impératifs et vous propose des intervenants qualifiés.</p>
            <a href="<?php echo $view->path('contact')?>">Contactez-nous</a>
        </div>
    </div>
</section>

<section id="avis--container">
    <div class="bigWrap">
        <h2>LES FAMILLES QUI NOUS FONT CONFIANCES</h2>
        <div class="line"></div>
        <div class="profile">
            <img src="<?= $view->asset('img/avisImg.png') ?>" class="profilImg" alt="">
        </div>
        <div class="avis--txt">
            <p>Nous apprécions tout particulièrement les efforts d’adaptation des prestations à nos attentes, le suivi et l’accompagnement de l’intervenante par la Conseillère Enfance... Nous entamions sereinement chaque nouvelle année de partenariat avec Bout’Choux !</p>
        </div>
        <div class="avis--author">
            <p>CHARLOTTE - Maman de <span>Nicolas et de Théo - 3 et 7 ans</span></p>
        </div>
    </div>
</section>

<section id="localisation--container">
    <div class="bigWrap">
        <h2>GÉOLOCALISEZ FACILEMENT LES FAMILLES QUI CHERCHENT UNE GARDE PARTAGÉE</h2>
        <div class="line"></div>
        <p class="first--txt">
            Bout’Choux utilise un outil de géolocalisation des familles qui vous permet de visualiser rapidement les familles qui cherchent une garde partagée pour leur enfant à proximité de chez vous ou à proximité de votre lieu de travail.
        </p>
        <p class="second--txt">
            Vous pouvez visualiser, très simplement, les familles qui souhaitent partager une garde en utilisant la carte ci-dessous. Vous pouvez également utiliser l’assistant de géolocalisation, qui vous permettra de le faire à partir d’une saisie d’adresse ; pour cela il vous suffit de cliquer sur <span> le bouton ci-dessous.</span>
        </p>
        <a href="" class="geolocaliser">GÉOLOCALISEZ PAR ADRESSE</a>
        <img src="<?= $view->asset('img/mappingImg.png') ?>" alt="">
    </div>
</section>

<div class="map" id="map"></div>

<section id="squad--container">
    <div class="top-txt">
        <h2>UNE ÉQUIPE DÉDIÉE AUX SERVICES VOS ENFANTS</h2>
        <div class="line"></div>
        <p>Les conseillers enfance Bout’Choux sélectionnent pour vous l’intervenante et l’accompagne dans la réalisation des prestations d’éveil à domicile Bout’Choux, grâce au suivi pédagogique Bout’Choux :</p>
    </div>
</section>
<div class="squad--cards">
    <div class="top">
        <div class="card">
            <div class="eclipse">
                <img src="<?= $view->asset('img/avisImg.png') ?>" alt="">
            </div>
            <h2>PRINCE</h2>
            <p>CONSEILLER PETITE ENFANCE</p>
            <a href="" class="mailIcone"><img src="<?= $view->asset('img/ecmailIcon.png') ?>" alt=""></a>
        </div>
        <div class="card card--clair">
            <div class="eclipse card--sombre">
                <img src="<?= $view->asset('img/avisImg.png') ?>" alt="">
            </div>
            <h2>ARHTUR</h2>
            <p>CONSEILLER PETITE ENFANCE</p>
            <a href="" class="mailIcone"><img src="<?= $view->asset('img/ecmailIcon.png') ?>" alt=""></a>
        </div>
        <div class="card">
            <div class="eclipse">
                <img src="<?= $view->asset('img/avisImg.png') ?>" alt="">
            </div>
            <h2>CLAUDE</h2>
            <p>CONSEILLER PETITE ENFANCE</p>
            <a href="" class="mailIcone"><img src="<?= $view->asset('img/ecmailIcon.png') ?>" alt=""></a>
        </div>
    </div>
    <div class="bot">
        <div class="card card--clair">
            <div class="eclipse card--sombre">
                <img src="<?= $view->asset('img/avisImg.png') ?>" alt="">
            </div>
            <h2>MELVIN</h2>
            <p>CONSEILLER PETITE ENFANCE</p>
            <a href="" class="mailIcone"><img src="<?= $view->asset('img/ecmailIcon.png') ?>" alt=""></a>
        </div>
        <div class="card card--clair">
            <div class="eclipse card--sombre">
                <img src="<?= $view->asset('img/avisImg.png') ?>" alt="">
            </div>
            <h2>THIBAULT</h2>
            <p>CONSEILLER PETITE ENFANCE</p>
            <a href="" class="mailIcone"><img src="<?= $view->asset('img/ecmailIcon.png') ?>" alt=""></a>
        </div>
    </div>
</div>
