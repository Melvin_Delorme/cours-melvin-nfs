<section id="law_img">
    <img src="<?php echo $view->asset('img/child_banner.jpg'); ?>" alt="">
</section>
<section id="law_mention">
    <div class="law_mention_1">
        <h1>Mentions Légales</h1>
    </div>
    <div class="law_mention_2">
        <p>En vigueur au 19/03/2023 Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., il est porté à la connaissance des utilisateurs et visiteurs, ci-après l""Utilisateur", du site Bout'Choux , ci-après le "Site", les présentes mentions légales.</p>
    </div>
    <div class="law_mention_3">
        <p> La connexion et la navigation sur le Site par l’Utilisateur implique acceptation intégrale et sans réserve des présentes mentions légales. Ces dernières sont accessibles sur le Site à la rubrique « Mentions légales »</p>
    </div>
    <div class="law_mention_4">
        <p>. ARTICLE 1 - L'EDITEUR L’édition et la direction de la publication du Site est assurée par Pethyt Prince, domiciliée 10 rue du général sarrail, dont le numéro de téléphone est 0658899247, et l'adresse e-mail boutchoux@gmail.com. La personne est assujettie au Registre du Commerce et des Sociétés de Rouen sous le numéro 4685- 5-95868. Le numéro de TVA intracommunautaire est FR B 78 892862892. ci-après l'"Editeur"</p>
    </div>
    <div class="law_mention_5">
        <p>. ARTICLE 2 - L'HEBERGEUR L'hébergeur du Site est la société Bout'Choux, dont le siège social est situé au 10 rue du général sarrail , avec le numéro de téléphone : 0232306598 + adresse mail de contact</p>
    </div>
    <div class="law_mention_6">
        <p> ARTICLE 3 - ACCES AU SITE Le Site est accessible en tout endroit, 7j/7, 24h/24 sauf cas de force majeure, interruption programmée ou non et pouvant découlant d’une nécessité de maintenance. En cas de modification, interruption ou suspension du Site, l'Editeur ne saurait être tenu responsable.</p>
    </div>
    <div class="law_mention_7">
        <p>ARTICLE 4 - COLLECTE DES DONNEES Le Site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. 1 En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit : via un formulaire de contact ; </p>
    </div>
    <div class="law_mention_8">
        <p>Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du Site, sans autorisation de l’Editeur est prohibée et pourra entraînée des actions et poursuites judiciaires telles que notamment prévues par le Code de la propriété intellectuelle et le Code civil. Pour plus d’informations, se reporter aux CGU du site Bout'Choux accessible à la rubrique "CGU"</p>
    </div>
</section>
