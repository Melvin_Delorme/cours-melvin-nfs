<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('sujet'); ?>
    <?php echo $form->input('sujet','text', $contact->sujet ?? '') ?>
    <?php echo $form->error('sujet'); ?>

    <?php if (!$view->islogged()){
        echo $form->label('email');
        echo $form->input('email', 'email');
        echo $form->error('email');
    }?>


    <?php echo $form->label('message'); ?>
    <?php echo $form->textarea('message', $contact->message ?? ''); ?>
    <?php echo $form->error('message'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>

