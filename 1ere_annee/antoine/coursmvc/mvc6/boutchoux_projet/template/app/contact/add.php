<section id="contact">
    <img src="<?php echo $view->asset('img/Enfant_bg_contact.png') ?>" alt="">
    <div class="overlay_contact"></div>
    <div class="text_contact">
        <div class="container_contact">
            <h2>Nous contacter</h2>
            <p>Vous pouvez nous contacter grâce au formulaire de contact ci-dessous.
            <div class="top animation">
                <a href="#contact_form">
                    <i class="fa-solid fa-arrow-down"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<div id="contact_form">
    <div class="box_contact">
        <img src="<?php echo $view->asset('img/logo_inscription_transparent.png') ?>" alt="">
        <h3>Contact</h3>
    </div>
    <div class="bar"></div>
    <?php
    $textButton = 'Envoyer';
    include('form.php'); ?>
</div>