<?php

namespace App\Controller;

use JasonGrimes\Paginator;

class AdminController extends DefaultController {

    public function index()
    {
        $totalItems = 100;
        $itemsPerPage = 10;
        $currentPage = 1;
        $urlPattern = '/admin?page=(:num)';

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

        $this->render('app.admin.index', array(
            'burgerjs' => 'yes',
            'paginator' => $paginator,
        ), 'admin');
    }

}