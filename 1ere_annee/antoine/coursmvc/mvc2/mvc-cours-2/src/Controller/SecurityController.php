<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\SecurityService;
use App\Service\Validation;

/**
 *
 */
class SecurityController extends DefaultController
{
    public function register()
    {

        if($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors['email'] = $v->emailValid($post['email']);
            $errors['password'] = $v->validPassword($post['password'], $post['password2']);
            if($v->isValid($errors)) {
                $verifUser = UserModel::getUserByEmail($post['email']);
                if(!empty($verifUser)) {
                    $errors['email'] = 'Votre compte existe dèjà';
                }
                if ($v->isValid($errors)) {
                    $post['password_hash'] = password_hash($post['password'], PASSWORD_DEFAULT);
                    $post['token'] = (new SecurityService())->generateRandomString(60);
                    UserModel::insert($post);
                    $this->addFlash('success', 'Merci pour votre inscription');
                    $this->redirect('home');
                }
            }
        }
        $form = new Form($errors);
        $this->render('app.security.register',array(
            'form' => $form
        ));
    }

    public function login()
    {
        if($this->isLogged()) {
            $this->redirect('home');
        }
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $user = UserModel::getUserByEmail($post['email']);
            if(empty($user)) {
                $errors['email'] = 'Error Credentials';
            } else {
                if(!password_verify($post['password'], $user->password)) {
                    $errors['email'] = 'Error Credentials';
                } else {
                    // creation de la session
                    $_SESSION['user'] = array(
                        'id'     => $user->id,
                        'email'  => $user->email,
                        'ip'     => $_SERVER['REMOTE_ADDR']
                    );
                    UserModel::addOneToLoginCount($user->id);
                    $this->addFlash('success', 'Nous sommes heureux de vous revoir');
                    $this->redirect('home');
                }
            }
        }
        $form = new Form($errors);
        $this->render('app.security.login',array(
            'form' => $form
        ));
    }

    public function logout()
    {
        $_SESSION = array();
        session_destroy();
        $this->redirect('home');
    }
}