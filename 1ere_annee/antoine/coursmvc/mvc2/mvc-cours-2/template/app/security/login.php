<h2>Connexion</h2>

<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('email'); ?>
    <?php echo $form->input('email','email') ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->label('password', 'Mot de passe'); ?>
    <?php echo $form->input('password','password') ?>
    <?php echo $form->error('password'); ?>


    <?php echo $form->submit('submitted', 'Connexion'); ?>
</form>