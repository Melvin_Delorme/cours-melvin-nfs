<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('title'); ?>
    <?php echo $form->input('title'); ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('maxuser'); ?>
    <?php echo $form->input('maxuser', 'number'); ?>
    <?php echo $form->error('maxuser'); ?>

    <?php echo $form->submit('submitted', $textButton) ?>
</form>