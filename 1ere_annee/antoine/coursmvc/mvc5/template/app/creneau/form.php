<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('id_salle'); ?>
    <?php echo $form->selectEntity('id_salle', $salles, 'title', 'id'); ?>
    <?php echo $form->error('id_salle'); ?>

    <?php echo $form->label('start_at'); ?>
    <?php echo $form->input('start_at', 'date'); ?>
    <?php echo $form->error('start_at'); ?>

    <?php echo $form->label('nbrehours'); ?>
    <?php echo $form->input('nbrehours', 'number'); ?>
    <?php echo $form->error('nbrehours'); ?>

    <?php echo $form->submit('submitted', $textButton) ?>
</form>