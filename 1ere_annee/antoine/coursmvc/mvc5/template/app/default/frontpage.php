<div class="wrap2">
    <h1>Utilisateurs:</h1>
    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><?php echo $user->nom ?></td>
                        <td><?php echo $user->email ?></td>
                    </tr>
            <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th>Nom</th>
            <th>Email</th>
        </tr>
        </tfoot>
    </table>

    <h1>Salles:</h1>
    <table>
        <thead>
        <tr>
            <th>Titre</th>
            <th>Capacité</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($salles as $salle) { ?>
            <tr>
                <td><?php echo $salle->title ?></td>
                <td><?php echo $salle->maxuser ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th>Titre</th>
            <th>Capacité</th>
        </tr>
        </tfoot>
    </table>

    <h1>Créneaux:</h1>
    <table>
        <thead>
        <tr>
            <th>A partir du</th>
            <th>Disponible pendant</th>
            <th>Salle</th>
            <th> </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($creneaux as $creneau) { ?>
            <tr>
                <td><?php echo (new DateTime($creneau->startat))->format('d/m/Y') ?></td>
                <td><?php echo $creneau->nbrehours ?>h</td>
                <td><?php echo $creneau->title ?></td>
                <td><a href="<?php echo $view->path('single-creneau', array('id' => $creneau->id)) ?>">Details</a></td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th>A partir du</th>
            <th>Disponible pendant</th>
            <th>Salle</th>
            <th> </th>
        </tr>
        </tfoot>
    </table>


</div>