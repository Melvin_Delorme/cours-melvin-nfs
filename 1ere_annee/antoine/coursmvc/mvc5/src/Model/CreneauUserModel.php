<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauUserModel extends AbstractModel
{
    protected static $table = 'creneau_user';

    protected $id;
    protected $id_creneau;
    protected $id_user;
    protected $created_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdCreneau()
    {
        return $this->id_creneau;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }


    public static function insert($post, $id_creneau)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_creneau, id_user, created_at) VALUES (?, ?, NOW())",
            array($id_creneau , $post['id_user'])
        );
    }

//    public static function getAllCreneauOrderBy($column = 'start_at', $order ='DESC')
//    {
//        return App::getDatabase()->query("SELECT * FROM " .self::getTable() . " ORDER BY $column $order",get_called_class());
//    }

    public static function getAllCreneauOrderBy($column = 'start_at', $order ='DESC')
    {
        return App::getDatabase()->query("
        SELECT creneau.*, salle.title 
        FROM creneau 
        INNER JOIN salle ON creneau.id_salle = salle.id 
        ORDER BY $column $order", get_called_class());
    }

    public static function getCreneauById($id)
    {
        $creneau = App::getDatabase()->prepare("
        SELECT creneau.*, salle.title 
        FROM creneau 
        INNER JOIN salle ON creneau.id_salle = salle.id 
        WHERE creneau.id = ?
    ", [$id], get_called_class(), true);

        return $creneau;
    }


}