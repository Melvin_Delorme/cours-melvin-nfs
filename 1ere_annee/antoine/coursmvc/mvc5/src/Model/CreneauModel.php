<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrehours;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }


    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_salle, start_at, nbrehours) VALUES (?, ?, ?)",
            array($post['id_salle'], $post['start_at'], $post['nbrehours'])
        );
    }

//    public static function getAllCreneauOrderBy($column = 'start_at', $order ='DESC')
//    {
//        return App::getDatabase()->query("SELECT * FROM " .self::getTable() . " ORDER BY $column $order",get_called_class());
//    }

    public static function getAllCreneauOrderBy($column = 'start_at', $order ='DESC')
    {
        return App::getDatabase()->query("
        SELECT creneau.*, salle.title 
        FROM creneau 
        INNER JOIN salle ON creneau.id_salle = salle.id 
        ORDER BY $column $order", get_called_class());
    }

    public static function getCreneauById($id)
    {
        $creneau = App::getDatabase()->prepare("
        SELECT creneau.*, salle.title 
        FROM creneau 
        INNER JOIN salle ON creneau.id_salle = salle.id 
        WHERE creneau.id = ?
    ", [$id], get_called_class(), true);

        return $creneau;
    }


}