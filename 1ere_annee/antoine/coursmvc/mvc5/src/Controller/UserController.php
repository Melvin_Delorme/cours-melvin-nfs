<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class UserController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                UserModel::insert($post);
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.user.add', array(
            'form' => $form,
        ));
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 2, 25);
        $errors['email'] = $v->EmailValid($post['email'], 'email', 4, 30);
        return $errors;
    }

}