<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\CreneauUserModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                CreneauModel::insert($post);
                $this->redirect('');
            }
        }
        $salles = SalleModel::all();
        $form = new Form($errors);
        $this->render('app.creneau.add', array(
            'salles' => $salles,
            'form' => $form,
        ));
    }

    public function single($id) {
        $errors = array();
        $id_creneau = $id;
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validatesingle($this->v, $post);
            if ($this->v->IsValid($errors)) {
                CreneauUserModel::insert($post, $id_creneau);
                $this->redirect('');
            }
        }
        $users = UserModel::all();
        $form = new Form($errors);
        $creneau = CreneauModel::getCreneauById($id);
        if (empty($creneau)) {
            $this->Abort404();
        }
        $this->render('app.creneau.single', array(
            'creneau' => $creneau,
            'users' => $users,
            'form' => $form,
        ));
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['start_at'] = $v->dateValid($post['start_at'], 'start_at');
        $errors['nbrehours'] = $v->numberValid($post['nbrehours'], 'nbrehours', 1, 48);
        return $errors;
    }

    private function validatesingle($v, $post)
    {
        $errors = [];

        return $errors;
    }

}