<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class SalleController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                SalleModel::insert($post);
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.salle.add', array(
            'form' => $form,
        ));
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title', 2, 25);
        $errors['maxuser'] = $v->NumberValid($post['maxuser'], 'maxuser', 5, 100);
        return $errors;
    }

}