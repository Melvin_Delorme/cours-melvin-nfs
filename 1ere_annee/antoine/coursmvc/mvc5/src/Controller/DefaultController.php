<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {

        $this->render('app.default.frontpage',array(
            'users' => UserModel::getAllUserOrderBy(),
            'salles' => SalleModel::getAllSalleOrderBy(),
            'creneaux' => CreneauModel::getAllCreneauOrderBy(),
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
