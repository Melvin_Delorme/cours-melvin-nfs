<?php

$routes = array(
    array('home','default','index'),


    array('abonne','abonne','index'),
    array('add-abonne','abonne','add'),
    array('single-abonne','abonne','single', array('id')),
    array('edit-abonne','abonne','edit', array('id')),
    array('delete-abonne','abonne','delete', array('id')),

    array('product','product','index'),
    array('add-product','product','add'),
    array('single-product','product','single', array('id')),
    array('edit-product','product','edit', array('id')),
    array('delete-product','product','delete', array('id')),

    array('borrow','borrow','index'),
    array('add-borrow','borrow','add'),
    array('single-borrow','borrow','single', array('id')),
    array('edit-borrow','borrow','edit', array('id')),
    array('delete-borrow','borrow','delete', array('id')),
);









