<h1>Abonnes</h1>

<a href="<?php echo $view->path('add-abonne'); ?>">Ajouter un abonne</a>

<?php foreach ($abonnes as $abonne) { ?>
    <h1><?php echo $abonne->nom ?> <?php echo $abonne->prenom ?></h1>
    <a href="<?php echo $view->path('single-abonne', array('id' => $abonne->id)) ?>">Details</a>
    <a href="<?php echo $view->path('edit-abonne', array('id' => $abonne->id)) ?>">Editer</a>
    <a onclick="return confirm('Voulez vous supprimer?')" href="<?php echo $view->path('delete-abonne', array('id' => $abonne->id)) ?>">Effacer</a>
<?php } ?>