<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('titre'); ?>
    <?php echo $form->input('titre', 'text', $product->titre ?? ''); ?>
    <?php echo $form->error('titre'); ?>

    <?php echo $form->label('reference'); ?>
    <?php echo $form->input('reference', 'text', $product->reference ?? ''); ?>
    <?php echo $form->error('prenom'); ?>

    <?php echo $form->label('description'); ?>
    <?php echo $form->input('description', 'text', $product->description ?? ''); ?>
    <?php echo $form->error('description'); ?>

    <?php echo $form->submit('submitted', $textButton) ?>
</form>