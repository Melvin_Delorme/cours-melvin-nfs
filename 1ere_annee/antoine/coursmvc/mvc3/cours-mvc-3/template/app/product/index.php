<h1>Product</h1>

<a href="<?php echo $view->path('add-product'); ?>">Ajouter un product</a>

<?php foreach ($products as $product) { ?>
    <h1><?php echo $product->titre ?></h1>
    <a href="<?php echo $view->path('single-product', array('id' => $product->id)) ?>">Details</a>
    <a href="<?php echo $view->path('edit-product', array('id' => $product->id)) ?>">Editer</a>
    <a onclick="return confirm('Voulez vous supprimer?')" href="<?php echo $view->path('delete-product', array('id' => $product->id)) ?>">Effacer</a>
<?php } ?>