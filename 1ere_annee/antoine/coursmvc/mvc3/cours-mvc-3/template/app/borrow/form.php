<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('id_abonne')?>
    <?php echo $form->selectEntity('id_abonne', $abonnes, 'nom', 'id')?>
    <?php echo $form->error('id_abonne')?>

    <?php echo $form->label('id_product')?>
    <?php echo $form->selectEntity('id_product', $products, 'titre', 'id')?>
    <?php echo $form->error('id_product')?>

    <?php echo $form->submit('submitted', $textButton) ?>
</form>