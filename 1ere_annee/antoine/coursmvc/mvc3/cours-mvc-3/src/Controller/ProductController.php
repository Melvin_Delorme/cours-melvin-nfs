<?php

namespace App\Controller;

use App\Model\ProductModel;
use App\Service\Form;
use App\Service\Validation;

class ProductController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $this->render('app.product.index', array(
            'products' => ProductModel::all()
        ), 'admin');
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                ProductModel::insert($post);
                $this->redirect('product');
            }
        }
        $form = new Form($errors);
        $this->render('app.product.add', array(
            'form' => $form,
        ), 'admin');
    }

    public function single($id) {
        $product = $this->getProductByIdOr404($id);
        $this->render('app.product.single', array(
            'product' => $product,
        ), 'admin');
    }

    public function edit($id)
    {
        $product = $this->getProductByIdOr404($id);
        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->isValid($errors)) {
                ProductModel::update($id, $post);
                $this->redirect('single-product', array('id' => $product->id));
            }
        }
        $form = new Form($errors);
        $this->render('app.product.edit', array(
            'form' => $form,
            'product' => $product
        ), 'admin');
    }

    public function delete($id) {
        $this->getProductByIdOr404($id);
        ProductModel::delete($id);
        $this->redirect('product');
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['titre'] = $v->textValid($post['titre'], 'titre', 3, 25);
        $errors['reference'] = $v->textValid($post['reference'], 'reference', 2, 10);
        $errors['description'] = $v->textValid($post['description'], 'description', 2, 255);
        return $errors;
    }

    private function getProductByIdOr404($id)
    {
        $mod = ProductModel::findById($id);
        if (empty($mod)) {
            $this->Abort404();
        }
        return $mod;
    }

}