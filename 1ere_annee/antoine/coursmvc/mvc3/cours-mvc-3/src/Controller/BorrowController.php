<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\BorrowModel;
use App\Model\ProductModel;
use App\Service\Form;
use App\Service\Validation;

class BorrowController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $this->render('app.borrow.index', array(
            'borrows' => BorrowModel::all(),
            'abonnes' => AbonneModel::all(),
            'products' => ProductModel::all(),
        ), 'admin');
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                BorrowModel::insert($post);
                $this->redirect('borrow');
            }
        }
        $abonnes = AbonneModel::all();
        $products = ProductModel::all();
        $form = new Form($errors);
        $this->render('app.borrow.add', array(
            'form' => $form,
            'abonnes' => $abonnes,
            'products' => $products
        ), 'admin');
    }

//    public function single($id) {
//        $product = $this->getProductByIdOr404($id);
//        $this->render('app.product.single', array(
//            'product' => $product,
//        ), 'admin');
//    }
//
//    public function edit($id)
//    {
//        $product = $this->getProductByIdOr404($id);
//        $errors = [];
//        if (!empty($_POST['submitted'])) {
//            $post = $this->cleanXss($_POST);
//            $errors = $this->validate($this->v, $post);
//            if ($this->v->isValid($errors)) {
//                ProductModel::update($id, $post);
//                $this->redirect('single-product', array('id' => $product->id));
//            }
//        }
//        $form = new Form($errors);
//        $this->render('app.product.edit', array(
//            'form' => $form,
//            'product' => $product
//        ), 'admin');
//    }
//
//    public function delete($id) {
//        $this->getProductByIdOr404($id);
//        ProductModel::delete($id);
//        $this->redirect('product');
//    }

    private function validate($v, $post)
    {
        $errors = [];
        return $errors;
    }


}