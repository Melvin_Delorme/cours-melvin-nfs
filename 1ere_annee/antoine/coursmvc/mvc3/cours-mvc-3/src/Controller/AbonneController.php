<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\ContactModel;
use App\Service\Form;
use App\Service\Validation;

class AbonneController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $this->render('app.abonne.index', array(
            'abonnes' => AbonneModel::all()
        ), 'admin');
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                AbonneModel::insert($post);
                $this->redirect('abonne');
            }
        }
        $form = new Form($errors);
        $this->render('app.abonne.add', array(
            'form' => $form,
        ), 'admin');
    }

    public function single($id) {
        $abonne = $this->getAbonneByIdOr404($id);
        $this->render('app.abonne.single', array(
            'abonne' => $abonne,
        ), 'admin');
    }

    public function edit($id)
    {
        $abonne = $this->getAbonneByIdOr404($id);
        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->isValid($errors)) {
                AbonneModel::update($id, $post);
                $this->redirect('single-abonne', array('id' => $abonne->id));
            }
        }
        $form = new Form($errors);
        $this->render('app.abonne.edit', array(
            'form' => $form,
            'abonne' => $abonne
        ), 'admin');
    }

    public function delete($id) {
        $this->getAbonneByIdOr404($id);
        AbonneModel::delete($id);
        $this->redirect('abonne');
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 3, 25);
        $errors['prenom'] = $v->textValid($post['prenom'], 'prenom', 2, 25);
        $errors['email'] = $v->emailValid($post['email'], 'email', 2, 30);
        $errors['age'] = $v->numberValid($post['age'], 'age', 1, 150);
        return $errors;
    }

    private function getAbonneByIdOr404($id)
    {
        $cat = AbonneModel::findById($id);
        if (empty($cat)) {
            $this->Abort404();
        }
        return $cat;
    }

}