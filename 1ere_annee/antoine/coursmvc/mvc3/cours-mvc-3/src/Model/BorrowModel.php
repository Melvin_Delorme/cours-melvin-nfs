<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class BorrowModel extends AbstractModel
{
        protected static $table = 'borrows';

        protected $id;
        protected $id_abonne;
        protected $id_product;
        protected $date_start;
        protected $date_end;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdAbonne()
    {
        return $this->id_abonne;
    }

    /**
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }





    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_abonne, id_product, date_start) VALUES (?, ?, NOW())",
            array($post['id_abonne'], $post['id_product'])
        );
    }

    public static function selectAbonneId()
    {
        
    }

//    public static function update($id,$post)
//    {
//        App::getDatabase()->prepareInsert(
//            "UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ? WHERE id = ?",
//            array($post['titre'], $post['reference'], $post['description'], $id)
//        );
//    }

}