<?php

namespace App\Controller;

use App\Model\CategoryModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\Config;

class CategoryController extends BaseController
{
    private $statusList;
    private $v;

    public function __construct()
    {
        $this->statusList = (new Config())->get('listStatus');
        $this->v = new Validation();
    }

    // listing
    public function index() {
        $this->render('app.category.index', array(
            'categorys' => CategoryModel::all()
        ), 'admin');
    }

    // détails
    public function single($id) {
        $category = $this->getCategoryByIdOr404($id);
        $this->render('app.category.single',array(
            'category' => $category,
        ), 'admin');
    }

    // ajouter
    public function add() {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            // Faille XSS.
            $post = $this->cleanXss($_POST);
            // Validation
            //$v = new Validation();
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                CategoryModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Merci pour votre nouvelle catégorie!');
                // redirection
                $this->redirect('les-category');
            }
        }
        $form = new Form($errors);
        $this->render('app.category.add', array(
            'form' => $form,
            'statusList' => $this->statusList
        ),'admin');
    }

    // editer
    public function edit($id)
    {
        $category = $this->getCategoryByIdOr404($id);
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors))  {
                CategoryModel::update($id,$post);
                // Message flash
                $this->addFlash('success', 'Merci pour l\'édition de cette catégorie!');
                // redirection
                $this->redirect('les-category');
            }
        }
        $form = new Form($errors);
        $this->render('app.category.edit', array(
            'form' => $form,
            'category' => $category,
            'statusList' => $this->statusList
        ), 'admin');
    }
    // delete
    public function delete($id) {
        $this->getCategoryByIdOr404($id);
        CategoryModel::delete($id);
        $this->addFlash('success', 'Merci pour avoir effacé cette catégorie!');
        $this->redirect('les-category');
    }

    private function getCategoryByIdOr404($id)
    {
        $cat = CategoryModel::findById($id);
        if(empty($cat)) {
            $this->Abort404();
        }
        return $cat;
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'titre',2, 100);
        $errors['description'] = $v->textValid($post['description'], 'description',5, 500);
        $errors['status'] = $v->textValid($post['status'], 'status',5, 20);
        return $errors;
    }

    public function user() {
        $cats = CategoryModel::all();
        return $this->renderView('app.category.controller', array(
            'cats' => $cats
        ));
        return $cats;
    }
}