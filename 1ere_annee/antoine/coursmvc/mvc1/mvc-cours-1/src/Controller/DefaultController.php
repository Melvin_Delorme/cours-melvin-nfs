<?php

namespace App\Controller;

/**
 *
 */
class DefaultController extends BaseController
{
    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        //$this->dump($message);

        $fruits = array('Banane','Kiwi','Papaye');
        $this->render('app.default.frontpage',array(
            'message' => $message,
            'dede' => 'dede',
            'legumes' => $fruits
        ));
    }

    public function contact() {
        $this->render('app.default.contact');
    }

    public function mention() {
        $this->render('app.default.mention');
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
