<h1>Mes recettes</h1>

<?php //$view->dump($recettes); ?>
<p><a class="btn" href="<?php echo $view->path('add-recette'); ?>">Ajouter une recette</a></p>

<?php foreach ($recettes as $recette) {
    echo '<div><h2>'.$recette->super.'</h2>
        <a class="btn" href="' .$view->path('recette', array('id' => $recette->id)).'">Détails</a>
        <a class="btn" href="' .$view->path('edit-recette', array('id' => $recette->id)).'">Editer</a>
        <a class="btn" onclick="return confirm(\'Voulez-vous effacer ?\')" href="' .$view->path('delete-recette', array('id' => $recette->id)).'">Effacer</a>
</div>';
}