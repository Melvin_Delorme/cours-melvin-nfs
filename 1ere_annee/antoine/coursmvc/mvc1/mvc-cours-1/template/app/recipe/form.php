<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('title'); ?>
    <?php echo $form->input('title','text', $recette->title ?? '') ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('content'); ?>
    <?php echo $form->textarea('content', $recette->content ?? ''); ?>
    <?php echo $form->error('content'); ?>


    <?php echo $form->label('category'); ?>
    <?php echo $form->select('category', $categories, 'title', $recette->cat_id ?? ''); ?>
    <?php echo $form->error('category'); ?>


    <?php echo $form->submit('submitted', $textButton); ?>
</form>