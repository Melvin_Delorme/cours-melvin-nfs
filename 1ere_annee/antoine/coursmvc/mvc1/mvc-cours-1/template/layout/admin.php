<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php //$view->dump($view->getFlash());
foreach ($view->getFlash() as $flash) {
    echo '<p class="'.$flash['type'].'">'.$flash['message'].'</p>';
}

echo $view->controller(
        '\\App\\Controller\\CategoryController',
        'user'
);
?>
<header id="masthead" style="display: flex">
    <div>
        <img width="30px" src="<?= $view->asset('svg/logo.svg'); ?>" alt="">
    </div>
    
    <nav>
        <ul>
            <li><a href="<?= $view->path(''); ?>">Home</a></li>
            <li><a href="<?= $view->path('les-recettes'); ?>">Mes recettes</a></li>
            <li><a href="<?= $view->path('les-category'); ?>">les categorys</a></li>
        </ul>
    </nav>
</header>

<div class="container">
    <div class="wrap">
        <?= $content; ?>
    </div>
</div>

<footer id="colophon">
    <div class="wrap">
        <p>MVC 6 - Admin</p>
    </div>
</footer>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
