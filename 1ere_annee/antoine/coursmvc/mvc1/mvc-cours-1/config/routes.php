<?php

$routes = array(
    array('home','default','index'),
    array('contact','default','contact'),
    array('mentions-legales','default','mention'),

    array('les-recettes','recipe','index'),
    array('recette','recipe','single', array('id')),
    array('add-recette','recipe','add'),
    array('edit-recette','recipe','edit', array('id')),
    array('delete-recette','recipe','delete', array('id')),


    array('les-category','category','index'),
    array('category','category','single', array('id')),
    array('add-category','category','add'),
    array('edit-category','category','edit', array('id')),
    array('delete-category','category','delete', array('id')),
);









