<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('sujet'); ?>
    <?php echo $form->input('sujet', 'text'); ?>
    <?php echo $form->error('sujet'); ?>

    <?php echo $form->label('email'); ?>
    <?php echo $form->input('email', 'email'); ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->label('message'); ?>
    <?php echo $form->textarea('message'); ?>
    <?php echo $form->error('message'); ?>

    <?php echo $form->submit('submitted', $textButton) ?>
</form>