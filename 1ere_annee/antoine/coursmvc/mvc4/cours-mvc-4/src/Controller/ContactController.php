<?php

namespace App\Controller;

use App\Model\ContactModel;
use App\Service\Form;
use App\Service\Validation;

class ContactController extends DefaultController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $this->render('app.contact.index', array(
            'contacts' => ContactModel::getAllContactOrderBy()
        ));
    }

    public function add()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->IsValid($errors)) {
                ContactModel::insert($post);
                $this->redirect('listing-contact');
            }
        }
        $form = new Form($errors);
        $this->render('app.contact.add', array(
            'form' => $form,
        ));
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet', 3, 25);
        $errors['email'] = $v->emailValid($post['email'], 'email', 2, 30);
        $errors['message'] = $v->textValid($post['message'], 'message', 3, 255);
        return $errors;
    }

}