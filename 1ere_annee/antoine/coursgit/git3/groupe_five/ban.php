<?php
session_start();
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

include ('inc/header.php');
?>
<section id="ban">
    <div class="wrap">
        <h1>Il semblerait que vous avez été banni...</h1>
        <p><a href="contact.php"> Vous avez la possibilité de nous contacter pour résoudre le problème...</a></p>
    </div>
</section>
<?php
include ('inc/footer.php');