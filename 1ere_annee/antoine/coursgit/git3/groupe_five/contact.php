<?php
session_start();
require('inc/pdo.php');
require('inc/function.php');
require('inc/validation.php');
$errors= array();
$success=false;

if (!empty($_POST['submit'])) {
    $message = cleanXss('message');

    if (isLogged()){
    $email_Login= $_SESSION['verifLogin']['email'];
    $errors=validationEmail($errors, $email_Login);
    }else{
    $email = cleanXss('email');
    $errors = validationEmail($errors, $email);
    }

    $errors = validationText($errors, $message, 'message', 1, 500);

if (count($errors)==0) {

    $sql = "INSERT INTO contact (email, content, created_at) VALUES (:email, :message, NOW())";
    $query = $pdo->prepare($sql);
    if (isLogged()){
        $query->bindValue('email', $email_Login);
    }else{
        $query->bindValue('email', $email);
    }
    $query->bindValue('message', $message);
    $query->execute();
    $success = true;

}
}
//debug($_POST);
//debug($errors);

include('inc/header.php'); ?>
<!--<section id="contact">-->
<!--    <div class="parallax"></div>-->
<!--    <div class="wrap">-->
<!--        <div class="contact_formulaire">-->
<!--            <div class="nous_contacter">-->
<!--                <h1>Nous contacter <i class="fa-sharp fa-solid fa-phone"></i></h1>-->
<!--            </div>-->
<!--            --><?php //if ($success==true){ ?>
<!--                <h2>Message envoyé!</h2>-->
<!--            --><?php //}?>
<!--            <div class="formulaire_contact">-->
<!--                <form method="post" action="" novalidate>-->
<!---->
<!--                    --><?php //if (!isLogged()){ ?>
<!--                        <div class="contact_email input_css">-->
<!--                            <input type="email" name="email" placeholder="Votre mail">-->
<!--                            <span class="error">--><?php //if (!empty($errors['email'])){ echo $errors['email']; }  ?><!--</span>-->
<!--                        </div>-->
<!--                    --><?php //} ?>
<!--                    <div class="contact_message input_css">-->
<!---->
<!--                        <textarea name="message" placeholder="Votre message" id="contact_message"></textarea>-->
<!--                        <span class="error">--><?php //if (!empty($errors['message'])){ echo $errors['message']; }  ?><!--</span>-->
<!--                    </div>-->
<!--                    <div class="submit">-->
<!--                        <input type="submit" name="submit" id="submit" value="Envoyer">-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<div class="parallax"></div>
<section id="contact">
    <div class="wrap3">
        <div class="login_form">
            <div class="log_gauche">
                <div class="gauche_info">
                    <h2>Avis</h2>
                    <h3>Vous souhaitez donner votre avis ?</h3>
                    <i class="fa-solid fa-chevron-down"></i>
                </div>
                <?php if(isLogged()) { ?>
                    <a href="avis.php">Avis</a>
                <?php } else { ?>
                    <a href="login.php">Avis</a>
                <?php } ?>
            </div>
            <div class="log_droite">
                <h2>Nous contacter <i class="fa-sharp fa-solid fa-phone"></i></h2>
                <form method="post" action="" novalidate>

                    <?php if (!isLogged()){ ?>
                        <div class="log_email log_input">
                            <input type="email" name="email" placeholder="Votre mail">
                            <span class="error"><?php if (!empty($errors['email'])){ echo $errors['email']; }  ?></span>
                        </div>
                    <?php } ?>
                    <div class="contact_message log_input">
                        <textarea name="message" placeholder="Votre message" id="contact_message"></textarea>
                        <span class="error"><?php if (!empty($errors['message'])){ echo $errors['message']; }  ?></span>
                    </div>
                    <div class="submit">
                        <input type="submit" name="submit" id="submit" value="Envoyer">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php include('inc/footer.php'); ?>
