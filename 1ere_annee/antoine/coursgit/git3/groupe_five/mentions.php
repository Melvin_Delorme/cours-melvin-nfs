<?php
session_start();
require('inc/pdo.php');
require('inc/function.php');

if ($_SESSION['verifLogin']['status']=='draft'){
    header('Location: ban.php');
}

include('inc/header.php'); ?>
<div class="parallax"></div>
<section id="mentions">
    <div class="title">
        <h2>Mentions Légales <i class="fa-sharp fa-solid fa-scale-balanced"></i></h2>
    </div>
    <div class="wrap">

        <h3>Identification de l’éditeur :</h3>
        <p>Le contenu proposé est créé par L’entreprise VacciVite.<br>
            Forme juridique : EI<br>
            N°Siren : 760 001 287<br>
            N°Siret : 760 001 287 00075<br>
            Code CPE : 86.90F / Activités de santé humaine non classées ailleurs.
        </p>
        <h3>Directeur de la publication :</h3>
        <p class="text3">Au sens de l'article 93-2 de la loi n° 82-652 du 29 juillet 1982.
            Need For School directeur de VacciVite.
        </p>
        <h3>Prestataire de l’hébergement :</h3>
        <p>www.ovhcloud.com<br>
            OVH<br>
            RCS Lille Métropole 424 761 419 00045<br>
            Code APE 2620Z<br>
            Siège social : 2 rue Kellermann - 59100 Roubaix - France
        </p>
        <h3>Traitement des données à caractère personnel :</h3>
        <p>Vaccinvite.fr a fait l'objet d'une déclaration à la Commission Nationale de l'Informatique et des Libertés (CNIL) sous le n° 712957.
            La base de données diffusée dans la rubrique Annuaire de l'administration a fait l'objet d'une déclaration spécifique sous le n° 1546654.<br>
            <br>L'annuaire de l'administration a été autorisé par l'arrêté du 6 novembre 2000 relatif à la création d'un site sur internet intitulé vaccinvite.fr (modifié par l'arrêté du 10 août 2001).
            Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous disposez d'un droit d'accès, de modification, de rectification et de suppression des données qui vous concernent.<br>
            <br>Pour demander une modification, rectification ou suppression des données vous concernant, il vous suffit d'envoyer un courrier par voie électronique ou postale à VacciVite en justifiant de votre identité.<br>
            <br>En utilisant le site VacciVite.fr vous acceptez de respecter les conditions générales d'utilisation et de navigation ci-après définies.

            Les éléments du dossier de demande d'autorisation du carnet de vaccination électronique correspondent aux exigences du Règlement général sur la protection des données (RGPD).VacciVite est certifié HDS (Hébergeur de Données de Santé), conformément à la réglementation française en vigueur émanant de l'ANS (l’Agence du Numérique en Santé).<br>
            <br>Pour demander une modification, rectification ou suppression des données vous concernant, il vous suffit d'envoyer un courrier par voie électronique ou postale à VaccinVite en justifiant de votre identité.<br>
            <br>En utilisant le site VaccinVite.fr vous acceptez de respecter les conditions générales d'utilisation et de navigation ci-après définies.

        </p>
    </div>
</section>

<?php include('inc/footer.php'); ?>






