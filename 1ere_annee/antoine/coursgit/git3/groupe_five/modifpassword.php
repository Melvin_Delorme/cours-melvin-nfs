<?php
session_start();
if (!empty($_SESSION)) {
    header('Location: 404.php');
}
$errors=array();
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');
$email=urldecode($_GET['email']);
$token=urldecode($_GET['token']);
if (!empty($_GET)){
    $sql= "SELECT email,token FROM user WHERE email= :email";
    $query= $pdo->prepare($sql);
    $query->bindValue('email', $email);
    $query->execute();
    $verifEmail= $query->fetch();
}
if ($verifEmail['token']==$token){
    if (!empty($_POST['submit'])){
        $password = cleanXss('password');
        $password2 = cleanXss('password2');

        if(!empty($password) && !empty($password2)) {
            if($password != $password2) {
                $errors['password'] = 'Vos mots de passe sont différents*';
            } elseif(mb_strlen($password) < 6) {
                $errors['password'] = 'Votre mot de passe est trop court(min 6)';
            }
        } else {
            $errors['password'] = 'Veuillez renseigner les mots de passe*';
        }
        if (count($errors)==0){
            $hashPassword = password_hash($password, PASSWORD_DEFAULT);
            $token = generateRandomString(80);
            $sql="UPDATE user SET password=:pas, token=:tok WHERE email=:email";
            $query = $pdo->prepare($sql);
            $query->bindValue('pas', $hashPassword);
            $query->bindValue('tok', $token);
            $query->bindValue('email', $email);
            $query->execute();
            header('Location: login.php');
        }
    }
}else{
    header('Location: 404.php');
}
include('inc/header.php'); ?>

<section id="modif">
    <div class="wrap3">
        <div class="bloc_modif">
            <h1>Modification du mot de passe</h1>
            <form method="post" action="">
                <div class="pass1 pass modif">
                    <input class="pass_css" name="password" id="password" placeholder="Mot de passe" type="password">
                    <span class="errors"><?php viewError($errors, 'password'); ?></span>
                </div>
                <div class="pass2 pass modif">
                    <input class="pass_css" name="password2" id="password2" placeholder="Confirmez le mot de passe" type="password">
                    <span class="errors"><?php viewError($errors, 'password2'); ?></span>
                </div>
                <div class="submit_modif modif">
                    <input class="submit_css" type="submit" name="submit" value="Modifier">
                </div>
            </form>
        </div>
    </div>
</section>





<?php
include ('inc/footer.php');

