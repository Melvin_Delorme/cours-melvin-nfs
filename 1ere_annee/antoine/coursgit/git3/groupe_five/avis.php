<?php
session_start();
require('inc/pdo.php');
require('inc/function.php');
require('inc/validation.php');
if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}

$errors= array();
$success=false;
$userconnected = $_SESSION['verifLogin']['id'];

if (!empty($_POST['submit'])) {
    $content = cleanXss('content');
    $errors = validationText($errors, $content, 'content', 1, 180);


    if (count($errors)==0) {
        $sql = "INSERT INTO avis (user_id,content,status_avis , created_at) 
                    VALUES (:userconnected,:content,'new', NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('userconnected', $userconnected);
        $query->bindValue('content', $content);
        $query->execute();
        $success = true;
    }
}

include('inc/header.php'); ?>

<!--<section id="avis">-->
<!--    <div class="bigwrap">-->
<!--        <img src="https://cdn.pixabay.com/photo/2021/10/11/17/37/doctor-6701410_1280.jpg" alt="doctor">-->
<!--    </div>-->
<!--    <div class="wrap">-->
<!--        <div class="avis_formulaire">-->
<!--            <h1>Laisser un avis <i class="fa-sharp fa-solid fa-star"></i></h1>-->
<!--            <form method="post" action="" novalidate>-->
<!--                    <textarea name="content" placeholder="Votre message" id="content"></textarea>-->
<!--                    <span class="error">--><?php //if (!empty($errors['content'])){ echo $errors['content']; }  ?><!--</span>-->
<!--                </div>-->
<!--                <div class="submit">-->
<!--                    <input type="submit" name="submit" id="submit" value="Envoyer">-->
<!--                </div>-->
<!--            </form>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<div class="parallax"></div>
<section id="avis">
    <div class="wrap3">
        <div class="login_form">
            <div class="log_gauche">
                <div class="gauche_info">
                    <h2>Contact</h2>
                    <h3>Pour nous contacter</h3>
                    <i class="fa-solid fa-chevron-down"></i>
                </div>
                <a href="contact.php">contact</a>
            </div>
            <div class="log_droite">
                <h2>Laisser un avis <i class="fa-sharp fa-solid fa-star"></i></h2>
                <form method="post" action="" novalidate>

                    <div class="contact_message log_input">
                        <textarea name="content" placeholder="Votre avis" id="content"></textarea>
                        <span class="error"><?php if (!empty($errors['content'])){ echo $errors['content']; }  ?></span>
                    </div>
                    <div class="submit">
                        <input type="submit" name="submit" id="submit" value="Envoyer">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('inc/footer.php'); ?>
