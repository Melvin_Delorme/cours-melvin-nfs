<?php
session_start();
if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}

require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

$errors= array();
$success=false;
$vaccinations = getAllVaccin();
$user_id = $_SESSION['verifLogin']['id'];

if (!empty($_POST['submitted'])) {
    $date_todo = cleanXss('date_todo');
    $vaccin_id = cleanXss('vaccination');
    $now=time();
    $datetodo=strtotime($date_todo);
    if (!empty($date_todo)){
        if ($datetodo<=$now){
            $errors['date']='Vous devez renseigner une date valide*';
        }
    }else{
        $errors['date']='Veuillez renseigner ce formulaire*';
    }
    // Erreurs a faire

    if (count($errors) == 0){
        insertVaccinEncoursToDo($date_todo, $vaccin_id, $user_id);
        $success = true;
        header('Location: next_vaccin.php');
    }
}

include ('inc/header.php'); ?>

    <div class="parallax"></div>
    <section id="formmask">
        <div class="wrap2">
            <div class="mask">
                <h1>Ajouter un vaccin</h1>
            <img src="https://img.icons8.com/pastel-glyph/2x/protection-mask--v6.png">
            </div>
            <div class="add">
        <a href="add_done.php">Ajouter un vaccin fait</a>

        <form action="" method="post" novalidate>
            <div class="form1">
                <label for="date_todo">Date de la vaccination:</label>
                <input type="date" name="date_todo" id="date_todo" value="<?php getPostValue('date_todo'); ?>">

                <span class="error"><?php viewError($errors, 'date_todo'); ?></span>
            </div>

            <div class="form3">
                <label for="vaccination">Type de vaccin:</label>

                <span class="error"><?php viewError($errors, 'date'); ?></span>
            </div>

            <div>


                <select name="vaccination" id="vaccination">
                    <?php foreach ($vaccinations as $vaccination) { ?>
                        <option value="<?php echo ($vaccination['id']); ?>"> <?php echo ($vaccination['title']); ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form4">
                <input class="inadd" type="submit" name="submitted" value="Ajouter un vaccin">
            </div>
        </form>
            </div>
        </div>
                <div class="vacc">
                    <div class="wrap">

                <a href="vaccin.php">Vaccins effectués</a>
                <a href="next_vaccin.php">Vaccins à faire</a>
                    </div>
                </div>
    </section>

<?php include ('inc/footer.php');