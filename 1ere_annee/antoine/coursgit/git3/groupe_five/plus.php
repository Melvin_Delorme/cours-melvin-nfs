<?php
session_start();
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');
if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}
if (!empty($_GET['id']) && is_numeric($_GET['id'])){
    $idva = $_GET['vaccin_id'];
    $id=$_GET['id'];
    $infos= getInfoById($idva,$id);
    if (empty($infos)){
        die('404');
    }
}else{
    die('404');
}
if (!empty($_POST['submit'])){
    $date=$infos['date_todo'];
    $iduser=$infos['user_id'];
    $idvacc=$infos['id'];
    $dose=$infos['doses'] +1;
    $sql="UPDATE vaccin_status SET status='effectué', date_done=:date, date_todo= NULL, doses=:dose WHERE user_id=:id AND vaccin_id=:vac";
    $query= $pdo->prepare($sql);
    $query->bindValue('date',$date);
    $query->bindValue('dose',$dose);
    $query->bindValue('id',$iduser);
    $query->bindValue('vac',$idvacc);
    $query->execute();
    header('Location: vaccin.php');
}
include ('inc/header.php');
?>
<section id="infos">
    <div class="wrap2">
        <?php
        echo '<div class="plus_infos"><h1>'.$infos['title'].'</h1>';
        if (!empty($infos['date_done'])){
        echo '<p>Vous avez réalisé le vaccin "'.$infos['title'].'" le '.date('d/m/Y',strtotime($infos['date_done'])).' et avez reçu '.$infos['doses'].' dose(s)</p>';
        }
        if (!empty($infos['date_todo'])){
            $vali=strtotime($infos['date_todo']);
            $now=time();
            echo '<p class="info_user">Vous devez réaliser le vaccin "'.$infos['title'].'" le '.date('d/m/Y',strtotime($infos['date_todo'])).' pour votre ';
            if ($infos['doses']==0){
                $jour_1=$infos['doses'] +1;
                echo $jour_1.'ère dose.</p></div>';
            }else{
                $jour_1=$infos['doses'] +1;
                echo $jour_1.'ème doses</p></div>';
            }
            if ($vali <= $now){
                echo '<form method="post" action="">';
                echo '<div class="input_plus"><input type="submit" name="submit" value="Vaccin effectué !"></div>';
                echo '</form>';
            }
        }
        echo '<h2 class="info_question">Un petit peu d\'infos sur ce vaccin ?</h2>';
        echo '<p>'.$infos['content'].'</p>';

        if (!empty($infos['date_done'])){
            echo '<p><a href="modifdone.php?id='.$infos['user_id'].'&vaccin_id='.$infos['id'].'">Modifier/Supprimer le vaccin réalisé</a></p>';
        }
        if (!empty($infos['date_todo'])){
            echo '<p><a href="modiftodo.php?id='.$infos['user_id'].'&vaccin_id='.$infos['id'].'">Modifier/Supprimer le vaccin à venir</a></p>';
        }
    ?>
    </div>
</section>



<?php
include('inc/footer.php');
