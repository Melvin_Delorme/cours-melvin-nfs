<?php
session_start();
if ($_SESSION['verifLogin']['status']=='draft'){
    header('Location: ban.php');
}
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');

include ('inc/header.php');
?>
<div class="error_404">
    <h2>404</h2>
    <p><a href="index.php">Retour à la page d'accueil...</a></p>
</div>

<?php
include ('inc/footer.php');

