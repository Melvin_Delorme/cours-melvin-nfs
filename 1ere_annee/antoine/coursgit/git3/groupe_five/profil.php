<?php
session_start();

if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}

//debug($_SESSION);

require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

//$vaccin_status = getVaccinId($_SESSION['verifLogin']['id']);
//debug($vaccin_status);

$last_vacc= getVaccinProfil($_SESSION['verifLogin']['id']);
$vacc_eff=end($last_vacc);

$next_vacc=getNextVaccin($_SESSION['verifLogin']['id']);
$vacc_attente=end($next_vacc);


include ('inc/header.php');
?>
<div class="bigwrap">
    <div class="parallax"></div>
    <div class="position_h1_profil">
        <h1>Mon Profil</h1>
    </div>
</div>
<section id="profil">
    <div class="wrap">
        <div class="vacci">
            <div class="nom">
                <div class="bjr">
                    <?php
                    echo '<h1> Bonjour '.$_SESSION['verifLogin']['prenom'].' '.$_SESSION['verifLogin']['name'].'</h1>';
                    ?>
                </div>
            </div>
        </div>
        <div class="vaccins">
            <div class="vaccin1 vacc_display">
                <a href="next_vaccin.php">
                    <div class="img">
                        <img src="asset/image/calendar.svg" alt="calendrier">
                    </div>
                    <div class="nextvaccin vacc_bottom">
                        <p>Prochain vaccin à réaliser :</p>

                        <p class="vaccin_done"><?php if (!empty($vacc_attente['date_todo'])){echo $vacc_attente['title'].' A faire le : ' . date('d/m/Y',strtotime($vacc_attente['date_todo']));}else{ echo '<p>Aucun vaccin à venir !</p>';}  ?></p>

                    </div>
                </a>
            </div>

            <div class="vaccin2 vacc_display">
                <a href="vaccin.php">
                    <div class="img">
                        <img src="asset/image/edit.svg" alt="notes">
                    </div>
                    <div class="lastvaccin vacc_bottom">
                        <p>Dernier vaccin effectué :</p>
                        <p class="vaccin_done"><?php if (!empty($vacc_eff['date_done'])){echo $vacc_eff['title'].' Fait le : ' . date('d/m/Y',strtotime($vacc_eff['date_done']));}else{echo '<p>Aucun vaccin n\'a été effectué !</p>';}  ?></p>
                    </div>
                </a>
            </div>

            <div class="vaccin3 vacc_display">
                <a href="add_done.php">
                    <div class="img">
                        <img src="asset/image/plus.svg" alt="add">
                    </div>
                    <div class="addvaccin vacc_bottom">
                        <p>Ajouter un vaccin / Programmer un vaccin</p>
                    </div>
                </a>
            </div>
         </div>
    </div>
</section>



<?php
include ('inc/footer.php');
