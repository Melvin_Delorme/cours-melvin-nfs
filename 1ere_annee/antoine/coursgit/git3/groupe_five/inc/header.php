<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vaccivite</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alata&family=Fredoka:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="asset/style.css">
</head>
<body>

<header id="masthead">
    <div class="wrap">
        <div class="logo">
            <a href="index.php">
                <img src="asset/image/logo.png" alt="Logo">
            </a>
        </div>
        <nav>
            <ul class="nav">
                <?php if (isLogged()){
                    echo '<li class="res"><a href="profil.php">Profil</a></li>';
                    echo '<li class="res"><a href="vaccin.php">Vaccin</a></li>';
                    echo '<li class="res"><a href="add_done.php">Ajouter</a></li>';
                    echo '<li class="res"><a href="logout.php">Logout</a></li>';
                    if ($_SESSION['verifLogin']['status'] == 'admin'){
                        echo '<li class="res"><a href="admin">Admin</a></li>';
                    }
                }else{
                    echo '<li class="res"><a href="register.php">S\'enregistrer</a></li>';
                    echo '<li class="res"><a href="login.php">Se connecter</a></li>';
                }
                ?>
                <li class="nav_res"><i class="fa-solid fa-bars"></i></li>
            </ul>
        </nav>
    </div>
</header>
<div id="retop"></div>