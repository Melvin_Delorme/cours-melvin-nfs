<?php
function debug($tableau) {
    echo '<pre style="height:200px;overflow-y: scroll;font-size: .5rem;padding: .6rem;font-family: Consolas, Monospace; background-color: #000;color:#fff;">';
    print_r($tableau);
    echo '</pre>';
}

// FAILLE XSS
function cleanXss($key){
    return trim(strip_tags($_POST[$key]));
}
// SPAN ERROR
function viewError($errors,$key)
{
    if(!empty($errors[$key])) {
        echo $errors[$key];
    }
}
// VALUE DANS INPUT ECT
function getPostValue($key, $data = '')
{
    if(!empty($_POST[$key]) ) {
        echo $_POST[$key];
    } elseif(!empty($data)) {
        echo $data;
    }
}
function isLogged() {
    if(!empty($_SESSION['verifLogin']['id'])) {
//        if(ctype_digit($_SESSION['verifLogin']['id'])) {
        if(!empty($_SESSION['verifLogin']['name'])) {
            if(!empty($_SESSION['verifLogin']['email'])) {
                if(!empty($_SESSION['verifLogin']['status'])) {
                    if(!empty($_SESSION['verifLogin']['ip'])) {
                        if($_SESSION['verifLogin']['ip'] == $_SERVER['REMOTE_ADDR']) {
                            return true;
                        }
                    }
                }
            }
//            }
        }
    }
    return false;
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
//function pageSuivante($item, $page){
//    if (count($item)>0){
//        echo '<li><a href="index.php?page="'.$page++.'>Page suivante</a> </li>';
//    }
//}

function getMessageById($id){
    global $pdo;
    $sql = "SELECT * FROM contact WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}
function getInfoById($id_vaccin,$id){
    global $pdo;
    $sql = "SELECT V.*, S.date_done, S.date_todo, S.doses, S.user_id
                FROM vaccin V
                INNER JOIN  vaccin_status S 
                ON V.id = S.vaccin_id
                WHERE V.id= :idva AND S.user_id=:id";
    $query = $pdo->prepare($sql);
    $query->bindValue('idva', $id_vaccin, PDO::PARAM_INT);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

