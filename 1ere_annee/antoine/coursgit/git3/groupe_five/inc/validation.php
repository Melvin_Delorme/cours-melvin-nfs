<?php

function validationText($errors, $data, $keyError, $min, $max){
    if (!empty($data)){
        if (mb_strlen($data) < $min){
            $errors[$keyError]='Veuillez renseignez plus de '.$min.' caractères*';
        }elseif (mb_strlen($data)> $max){
            $errors[$keyError]='Veuillez renseignez moins de '.$max.' caractères*';
        }
    }else{
        $errors[$keyError]='Veuillez rensignez ce formulaire*';
    }
    return $errors;
}
function validationEmail($errors, $mail1, $key='email' ){
    if (!empty($mail1)){
        if (!filter_var($mail1, FILTER_VALIDATE_EMAIL)){
            $errors[$key]='Veuillez renseignez un email valide*';
        }
    }else{
        $errors[$key]= 'Veuillez rentrer une adresse email*';
    }
    return $errors;
}

//function validationText($errors, $data, $keyError, $min, $max)
//{
//    if (!empty($data)) {
//        if (mb_strlen($data) < $min) {
//            $errors[$keyError] = 'Veuillez renseignez plus de ' . $min . ' caractères*';
//        } elseif (mb_strlen($data) > $max) {
//            $errors[$keyError] = 'Veuillez renseignez moins de ' . $max . ' caractères*';
//        }
//    } else {
//        $errors[$keyError] = 'Veuillez rensignez ce formulaire*';
//    }
//    return $errors;
//
//
//}

