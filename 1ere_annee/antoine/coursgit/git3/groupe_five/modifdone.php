<?php

session_start();
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}
if (!empty($_GET['id']) && is_numeric($_GET['id'])){
    $idva = $_GET['vaccin_id'];
    $id=$_GET['id'];
    $infos= getInfoById($idva,$id);
    if (empty($infos)){
        die('404');
    }
}else{
    die('404');
}
$errors=array();
$listedoses = array(
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5
);
if (!empty($_POST['submit'])){
    $date=cleanXss('date');
    $doses = cleanXss('doses');

    $now=time();
    $vaccin_id=$infos['id'];
    $userid=$infos['user_id'];

    if (!empty($date)){
        $datedone=strtotime($date);
        if ($datedone>$now){
            $errors['date']='Vous devez renseigner une date correcte*';
        }
    }else{
        $errors['date']='Vous devez renseigner ce formulaire*';
    }
    if (count($errors) == 0){
        $sql="UPDATE vaccin_status SET date_done = :date, doses = :doses WHERE vaccin_id = $vaccin_id AND
                user_id = $userid";
        $query = $pdo->prepare($sql);
        $query->bindValue('date', $date, PDO::PARAM_STR);
        $query->bindValue('doses', $doses);
        $query->execute();
        header('Location: vaccin.php');
    }
}
include ('inc/header.php'); ?>


<section id="modifdone">
    <div class="wrap3">
        <div class="modif_form">
            <?php
            echo '<h1>Modifier '.$infos['title'].'</h1>';
            ?>
            <form method="post" action="">
                <div class="date_modifdone span">
                    <label for="date">Modification de la date</label>
                    <input type="date" name="date" id="date" value=" <?php  getPostValue('date_done'); ?> ">
                    <span class="error"><?php viewError($errors, 'date'); ?></span>
                </div>
                <div class="dose_modifdone span">
                    <label for="doses">Doses</label>
                    <select name="doses" id="doses">
                        <?php foreach ($listedoses as $key => $value) { ?>
                            <option value="<?php echo $key; ?>"<?php
                            if(!empty($_POST['doses']) && $_POST['doses'] === $key) {
                                echo ' selected';
                            }
                            ?>><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="input_done span">
                    <input type="submit" name="submit" value="Modifier">
                </div>
            </form>
        </div>
        <div class="delete_done">
            <?php
           echo '<a href="delete.php?id='.$infos['user_id'].'&vaccin_id='.$infos['id'].'">Supprimer Le vaccin</a>'
           ?>
        </div>
    </div>
</section>



<?php include ('inc/footer.php');
