<?php

session_start();
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}
if (!empty($_GET['id']) && is_numeric($_GET['id'])){
    $idva = $_GET['vaccin_id'];
    $id=$_GET['id'];
    $infos= getInfoById($idva,$id);
    if (empty($infos)){
        die('404');
    }
}else{
    die('404');
}
$errors=array();
if (!empty($_POST['submit'])){
    $date=cleanXss('date');
    $now=time();
    if (!empty($date)){
        $datedone=strtotime($date);
        if ($datedone<$now){
            $errors['date']='Vous devez renseigner une date correcte*';
        }
    }else{
        $errors['date']='Vous devez renseigner ce formulaire*';
    }
    if (count($errors)==0){
        $sql="UPDATE vaccin_status SET date_todo=:date WHERE vaccin_id=$idva AND user_id= $id";
        $query = $pdo->prepare($sql);
        $query->bindValue('date', $date, PDO::PARAM_STR);
        $query->execute();
        header('Location: next_vaccin.php');
    }
}
include ('inc/header.php');
?>
<section id="modif_todo">
    <div class="wrap3">
        <?php
        echo '<h1>Modifier la date de '.$infos['title'].'</h1>';
        ?>
        <form method="post" action="">
            <div class="date_todo">
                <label for="date">Modifier la date</label>
                <input id="date" name="date" type="date" value="<?php getPostValue('date_done');  ?>">
                <span class="error"><?php viewError($errors, 'date'); ?></span>
            </div>
            <div class="submit_todo">
                <input type="submit" name="submit" value="Modifier">
            </div>
        </form>
        <?php
        echo '<div class="delete_todo"><a href="deletetodo.php?id='.$infos['user_id'].'&vaccin_id='.$infos['id'].'">Supprimer Le vaccin</a></div>'
        ?>
    </div>
</section>
<?php
include ('inc/footer.php');

