<?php
session_start();
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}$now=time();

$page = 1;
$itemPerPage = 4;
$offset = 0;
if(!empty($_GET['page'])) {
    $page = $_GET['page'];
    $offset = ($page - 1) * $itemPerPage;
}

$vaccin_status = getVaccinIdAttente($_SESSION['verifLogin']['id']);
$count = getCount($_SESSION['verifLogin']['id']);
$all_page_non= count($count)/$itemPerPage;
$all_page=ceil($all_page_non);

include ('inc/header.php');
?>
    <div class="bigwrap">
        <div class="img_labo"></div>
        <div class="position_h1_vaccin">
            <h1>Mon carnet vaccinal</h1>
        </div>
    </div>
    <section id="vaccin">
        <div class="wrap2">
            <div class="vaccin_choix">
                <a href="vaccin.php" class="vaccin_select "><h2>Vaccins réalisés</h2></a>
                <h2 class="vaccin_select vaccin_active">Prochains vaccins</h2>
            </div>
            <div class="recherche">
                <form action="" method="get">
                    <input class="recherche_input" type="search" name="search" placeholder="Rechercher un vaccin">
                    <input class="submit_recherche" type="submit" name="rechercher">
                </form>
            </div>
            <div class="vaccin_list">
                <?php if (empty($vaccin_status)) {
                    echo '<h2>Aucun vaccin n\'a été trouvé...</h2>';
                    echo '<div><a href="next_vaccin.php">Afficher tous vos vaccins...</a></div>';
                } else {
                    foreach ($vaccin_status as $vaccin){
//                        if ($vaccin['status']=='en attente'){
//                            echo '<a href="plus.php?id='.$vaccin['vaccin_id'].'"> <div class="global" id="'.$vaccin['title'].'">';
//                            echo '<h3>'. ucfirst($vaccin['title']) .'</h3>';
//                            echo '<p> A faire le : '.date('d/m/Y',strtotime($vaccin['date_todo'])).'</p>';
//                            echo '<i class="fa-solid fa-square-check"></i>';
//                            echo '</div></a>';
                        if ($vaccin['status'] == 'en attente'){
                            echo '<a href="plus.php?id='.$vaccin['user_id'].'&vaccin_id='.$vaccin['vaccin_id'].'"> <div class="global" id="'.$vaccin['title'].'">';
                            echo '<h3>'. ucfirst($vaccin['title']) .'</h3>';
                            echo '<p> A réaliser le : '.date('d/m/Y',strtotime($vaccin['date_todo'])).'</p>';
                            $date=strtotime($vaccin['date_todo']);
                            if ($date<=$now){
                                echo '<i class="fa-regular fa-square"></i>';
                            }else{
                                echo '<i class="fa-regular fa-hourglass-half"></i>';
                            }
                            echo '</div></a>';
                        }
                    }
                    if ($vaccin['status']=='en attente'){
                        if ($page>1){
                            $pagePre = $page - 1;
                            echo '<li><p><a href="next_vaccin.php?page='.$pagePre.'">Précedent</a></p></li>';
                        }
                        if ($page < $all_page){
                            $pageSui = $page + 1;
                            echo '<li><p><a href="next_vaccin.php?page='.$pageSui.'">Suivant</a></p></li>';
                        }
                    }
                }
                ?>
            </div>
            <h4><a href="add_todo.php">Ajouter un vaccin</a></h4>
        </div>
    </section>


<?php
include ('inc/footer.php');