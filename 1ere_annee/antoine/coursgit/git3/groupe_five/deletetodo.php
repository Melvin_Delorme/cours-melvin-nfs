<?php
session_start();
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}
if (!empty($_GET['id']) && is_numeric($_GET['id'])){
    $idva = $_GET['vaccin_id'];
    $id=$_GET['id'];
    $infos= getInfoById($idva,$id);
    if (empty($infos)){
        die('404');
    }
}else{
    die('404');
}
if (!empty($_POST['submit'])){
    if (!empty($infos['date_done'])){
    $sql="UPDATE vaccin_status SET date_todo=NULL WHERE user_id= $id AND vaccin_id=$idva";
    $query = $pdo->prepare($sql);
    $query->execute();
    header('Location: vaccin.php');
    }else{
        $sql="DELETE FROM vaccin_status WHERE user_id= $id AND vaccin_id=$idva";
        $query = $pdo->prepare($sql);
        $query->execute();
        header('Location: vaccin.php');
    }
}
include ('inc/header.php'); ?>
    <section id="delete">
        <div class="wrap3">
            <?php
            echo '<h1>Voulez vous vraiment supprimer '.$infos['title'].' ?</h1>'
            ?>
            <form method="post" action="">
                <input type="submit" name="submit" value="Supprimer">
            </form>
        </div>
    </section>

<?php
include ("inc/footer.php");