<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


$vaccins = getAllVaccin();
$users = getAllUser();
$contacts = getAllContact();
$vaccins_encours = getAllVaccinEnCoursDateDone();
$usersHomme = getAllUserHomme();
$usersH = count($usersHomme);
$usersFemme = getAllUserFemme();
$usersF = count($usersFemme);
$usersAutre = getAllUserAutre();
$usersA = count($usersAutre);


?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                    </div>
                    <!-- Content Row -->
                    <div class="row">
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Utilisateurs</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($users) ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa-solid fa-user fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Vaccins</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($vaccins) ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa-solid fa-syringe fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Vaccins effectués
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?= count($vaccins_encours) ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa-solid fa-check fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                Demandes en attentes</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($contacts) ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Row -->
                    <div class="row" style="display: flex; justify-content: center">
                        <!-- Area Chart -->
                        <div class="col-xl-8 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Genre</h6>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body" style="display: flex; justify-content: center">
                                    <div class="chart-area" style="width: 100%;">
                                        <canvas id="myChart"></canvas>
                                        <script>
                                            const labels = [
                                                'Homme',
                                                'Femme',
                                                'Autre',
                                            ];

                                            const data = {
                                                labels: labels,
                                                datasets: [{
                                                    label: 'Utilisateurs par genre',
                                                    backgroundColor: [
                                                        'rgb(79, 123, 156)',
                                                        'rgb(181, 223, 255)',
                                                        'rgb(142, 192, 231)'
                                                    ],
                                                    data: [
                                                        <?php echo json_encode($usersH)?> ,
                                                        <?php echo json_encode($usersF)?>,
                                                        <?php echo json_encode($usersA)?>
                                                    ],
                                                }]
                                            };

                                            const config = {
                                                type: 'bar',
                                                data: data,
                                                options: {
                                                    scales: {
                                                        y: {
                                                            beginAtZero: true
                                                        }
                                                    }
                                                },
                                            };
                                        </script>
                                        <script>
                                            const myChart = new Chart(
                                                document.getElementById('myChart'),
                                                config
                                            );
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
<?php include ('inc/footer.php'); ?>