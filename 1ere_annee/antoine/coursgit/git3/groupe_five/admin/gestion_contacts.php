<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


$contacts = getAllContact();
?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Gestion Contact</h1>
                    <p class="mb-4">Cette table vous permet de gerer les demandes de contacts a l'aide d'une liste.</p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Demandes</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Contenu</th>
                                        <th>Date de création</th>
                                        <th>Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Email</th>
                                        <th>Contenu</th>
                                        <th>Date de création</th>
                                        <th>Supprimer</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php foreach ($contacts as $contact) { ?>
                                        <tr>
                                            <td><?= $contact['email'] ?></td>
                                            <td><?= $contact['content'] ?></td>
                                            <td><?= $contact['created_at'] ?></td>

                                            <td><a title="Supprimer definitivement" style="font-size: 1.5rem" href="deletecontact.php?id=<?= $contact['id']; ?>"><i class="fa-solid fa-trash"></i></a></td>

                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>