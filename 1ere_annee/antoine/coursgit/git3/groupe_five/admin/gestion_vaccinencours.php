<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


$vaccin_status = getAllVaccinEnCours();
//debug($vaccin_status);

?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Gestion des Vaccins en cours</h1>
                    <p class="mb-4">Cette table vous affiche la totalité des vaccins en cours ou effectués.</p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Vaccin en cours</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Utilisateur</th>
                                        <th>Vaccin</th>
                                        <th>Date en cours</th>
                                        <th>Date effectué</th>
                                        <th>Status</th>
                                        <th>Doses</th>
                                        <th>Date de création</th>
                                        <th>Dernière modification</th>
                                        <th>Modifier</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Utilisateur</th>
                                        <th>Vaccin</th>
                                        <th>Date en cours</th>
                                        <th>Date effectué</th>
                                        <th>Status</th>
                                        <th>Doses</th>
                                        <th>Date de création</th>
                                        <th>Dernière modification</th>
                                        <th>Modifier</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php foreach ($vaccin_status as $vaccin_statut) { ?>
                                        <tr>
                                            <td><?= $vaccin_statut['name'] ?></td>
                                            <td><?= $vaccin_statut['title'] ?></td>
                                            <td><?= $vaccin_statut['date_todo'] ?></td>
                                            <td><?= $vaccin_statut['date_done'] ?></td>
                                            <td><?= $vaccin_statut['status'] ?></td>
                                            <td><?= $vaccin_statut['doses'] ?></td>
                                            <td><?= $vaccin_statut['created_at'] ?></td>
                                            <td><?= $vaccin_statut['modified_at'] ?></td>

                                            <td style="display: flex; justify-content: space-between">
                                                <a title="Editer" style="font-size: 1.5rem" href="editvec.php?id=<?= $vaccin_statut['id']; ?>"><i class="fa-solid fa-pen-to-square"></i></a>
                                                <a title="Effectué" style="font-size: 1.5rem" href="donevec.php?id=<?= $vaccin_statut['id']; ?>"><i class="fa-solid fa-check"></i></a>
                                                <a title="En attente" style="font-size: 1.5rem" href="todovec.php?id=<?= $vaccin_statut['id']; ?>"><i class="fa-solid fa-hourglass-start"></i></a>
                                                <a title="Supprimer definitivement" style="font-size: 1.5rem" href="deletevec.php?id=<?= $vaccin_statut['id']; ?>"><i class="fa-solid fa-trash"></i></a>
                                            </td>

                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>