<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $vaccin = getVaccinById($id);
    if(empty($vaccin)) {
        header('location:index.php');
    }
} else {
    header('location:index.php');
}

$errors = array();
$success = false;

if (!empty($_POST['submitted'])) {
    $title = cleanXss('title');
    $content = cleanXss('content');

    $errors = validationText($errors, $title, 'title', 4, 150);
    $errors = validationText($errors, $content, 'content', 4, 500);

    if (count($errors) == 0){
        updateVaccin($id,$title,$content);
        $success = true;
        header('Location: gestion_vaccins.php#tableau');
    }
}
?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Ajouter un nouveau vaccin</h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <form style="margin: 1rem" action="" method="post" novalidate>
                        <div class="label1" style="display: flex; flex-direction: column; padding-bottom: 3rem">
                            <label for="title">Nom du vaccin</label>
                            <input type="text" name="title" placeholder="Mon Vaccin" id="title" value="<?php getPostValue('title', $vaccin['title']); ?>">
                            <span class="error"><?php viewError($errors, 'title'); ?></span>
                        </div>

                        <div class="label2" style="display: flex; flex-direction: column; padding-bottom: 3rem">
                            <label for="content">Description du vaccin</label>
                            <textarea style="max-height: 200px; min-height: 115px" name="content" id="content" cols="30" rows="10"><?php getPostValue('content', $vaccin['content']); ?></textarea>
                            <span class="error"><?php viewError($errors, 'content'); ?></span>
                        </div>

                        <div style="display: flex; justify-content: center" class="label3">
                            <input type="submit" name="submitted" value="Modifier">
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>