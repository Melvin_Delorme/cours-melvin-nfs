<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $vec = getVecById($id);
    if(empty($vec)) {
        header('location:index.php');
    }
} else {
    header('location:index.php');
}

$errors = array();
$success = false;

if (!empty($_POST['submitted'])) {
    // Faille XSS
    $date_todo = cleanXss('date_todo');
    $date_done = cleanXss('date_done');
    $doses = cleanXss('doses');

    $errors = validationText($errors, $doses, 'doses', 1, 10);

    if (count($errors) == 0){
        updateVec($id, $date_todo, $date_done, $doses);
        $success = true;
        header('Location: gestion_vaccinencours.php');
    }
}
?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Modifier un vaccin en cours</h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <form style="margin: 1rem" action="" method="post" class="wrapform" novalidate>
                        <div style="display: flex; flex-direction: column; padding-bottom: 3rem">
                            <input name="date_todo" id="date_todo" placeholder="date_todo" type="date" value="<?php getPostValue('date_todo', $vec['date_todo']); ?>">
                            <span class="errors"><?php viewError($errors, 'date_todo'); ?></span>
                        </div>

                        <div style="display: flex; flex-direction: column; padding-bottom: 3rem">
                            <input name="date_done" id="date_done" placeholder="date_done" type="date" value="<?php getPostValue('date_done', $vec['date_done']); ?>">
                            <span class="errors"><?php viewError($errors, 'date_done'); ?></span>
                        </div>

                        <div style="display: flex; flex-direction: column; padding-bottom: 3rem">
                            <input name="doses" id="doses" placeholder="doses" type="number" min="0" max="123" value="<?php getPostValue('doses', $vec['doses']); ?>">
                            <span class="errors"><?php viewError($errors, 'doses'); ?></span>
                        </div>

                        <div style="display: flex; justify-content: center" class="form_input">
                            <input name="submitted" type="submit" value="Modifier">
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>