<?php
session_start();
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}

$page = 1;
$itemPerPage = 4;
$offset = 0;
if(!empty($_GET['page'])) {
    $page = $_GET['page'];
    $offset = ($page - 1) * $itemPerPage;
}

$vaccin_status = getVaccinIdEffectue($_SESSION['verifLogin']['id']);
$count = getCount($_SESSION['verifLogin']['id']);
$all_page_non= count($count)/$itemPerPage;
$all_page=ceil($all_page_non);

include ('inc/header.php');
?>

<div class="bigwrap">
    <div class="img_labo"></div>
    <div class="position_h1_vaccin">
        <h1>Mon carnet vaccinal</h1>
    </div>
</div>
<section id="vaccin">
    <div class="wrap2">
        <div class="vaccin_choix">
            <h2 class="vaccin_select vaccin_active">Vaccins réalisés</h2>
            <a href="next_vaccin.php" class="vaccin_select vaccin_next"><h2>Prochains vaccins</h2></a>
        </div>
        <div class="recherche">
            <form action="" method="get">
                <input  class="recherche_input" type="search" name="search" placeholder="Rechercher un vaccin">
                <input class="submit_recherche" type="submit" name="rechercher">
            </form>
        </div>
        <div class="vaccin_list">
            <?php if (empty($vaccin_status)) {
                echo '<h2>Aucun vaccin n\'a été trouvé...</h2>';
                echo '<div><a href="vaccin.php">Afficher tous vos vaccins...</a></div>';
            } else {
                foreach ($vaccin_status as $vaccin){
                        echo '<a href="plus.php?id='.$vaccin['user_id'].'&vaccin_id='.$vaccin['vaccin_id'].'"> <div class="global" id="'.$vaccin['title'].'">';
                        echo '<h3>'. ucfirst($vaccin['title']) .'</h3>';
                        echo '<p> Fait le : '.date('d/m/Y',strtotime($vaccin['date_done'])).'</p>';
                        echo '<i class="fa-solid fa-check"></i>';
                        echo '</div></a>';

                }
                if ($vaccin['status']=='effectué'){
                    if ($page>1){
                        $pagePre = $page - 1;
                        echo '<li><a href="vaccin.php?page='.$pagePre.'">Précedent</a></li>';
                    }
                    if ($page < $all_page){
                        $pageSui = $page + 1;
                        echo '<li><a href="vaccin.php?page='.$pageSui.'">Suivant</a></li>';
                    }
                }
            }

            ?>
        </div>
        <h4><a href="add_done.php">Ajouter un vaccin</a></h4>
    </div>
</section>

<?php
include ('inc/footer.php');