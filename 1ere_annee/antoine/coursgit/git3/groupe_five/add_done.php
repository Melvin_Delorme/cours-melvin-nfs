<?php
session_start();
if (empty($_SESSION)){
    header('Location: 404.php');
}else{
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}

require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
require ('inc/request.php');

$errors= array();
$success=false;
$vaccinations = getAllVaccin();
$user_id = $_SESSION['verifLogin']['id'];
$listedoses = array(
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5
);

if (!empty($_POST['submitted'])) {
    $date_done = cleanXss('date_done');
    $vaccin_id = cleanXss('vaccination');
    $doses = cleanXss('doses');
    $now=time();

//    echo $vaccin_id;
    if (!empty($date_done)){
        $datedone=strtotime($date_done);
        if ($datedone>$now){
            $errors['date']='Vous devez renseigner une date correcte*';
        }
    }else{
        $errors['date']='Vous devez renseigner ce formulaire*';
    }
    // Erreurs a faire
    if (count($errors) == 0){
        insertVaccinEncoursDone($date_done, $doses, $vaccin_id, $user_id);
        $success = true;
        header('Location: vaccin.php');

    }
}

include ('inc/header.php'); ?>

<div class="parallax"></div>
<section id="formmask">

    <div class="wrap2">
        <div class="mask">
            <h1>Ajouter un vaccin</h1>
            <img src="https://img.icons8.com/pastel-glyph/2x/protection-mask--v6.png">
        </div>
        <div class="add">
            <a href="add_todo.php">Programmer un vaccin</a>

            <form action="" method="post" novalidate>

                <div class="form1">
                    <label for="date_done">Date de la vaccination:</label>
                    <input type="date" name="date_done" id="date_done" value="<?php getPostValue('date_done'); ?>">
                    <span class="error"><?php viewError($errors, 'date'); ?></span>
                </div>
<!--            <div class="form1">-->
<!--                <label for="date_done">Date de la vaccination:</label>-->
<!--                <input type="date" name="date_done" id="date_done" value="--><?php //getPostValue('date_done'); ?><!--">-->
<!--                <span class="error">--><?php //viewError($errors, 'date'); ?><!--</span>-->
<!--            </div>-->



                <div class="form2">
                    <label for="vaccination">Type de vaccin:</label>
                    <select name="vaccination" id="vaccination">
                        <?php foreach ($vaccinations as $vaccination) { ?>
                            <option value="<?php echo ($vaccination['id']); ?>"> <?php echo ($vaccination['title']); ?></option>
                        <?php } ?>
                    </select>
                </div>

                <label for="doses">Doses</label>
                <select name="doses" id="doses">
                    <?php foreach ($listedoses as $key => $value) { ?>
                        <option value="<?php echo $key; ?>"<?php
                        if(!empty($_POST['doses']) && $_POST['doses'] === $key) {
                            echo ' selected';
                        }
                        ?>><?php echo $value; ?></option>
                    <?php } ?>
                </select>
                <input class="inadd" type="submit" name="submitted" value="Ajouter un vaccin">
        </div>
        </form>
    </div>
    </div>
    <div class="vacc">
        <div class="wrap">
            <a href="vaccin.php">Vaccins effectués</a>
            <a href="next_vaccin.php">Vaccins à faire</a>
        </div>
    </div>
</section>


<?php include ('inc/footer.php');
