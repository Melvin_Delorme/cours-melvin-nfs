<?php
session_start();
if (!empty($_SESSION)){
    header('Location: 404.php');
}
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');
$errors= array();
$genres=array(
    'femme'=>'Femme',
    'homme'=>'Homme',
    'autre'=>'Autre'
);

if(!empty($_POST['submitted'])) {
    // Faille XSS
    $name = cleanXss('name');
    $prenom= cleanXss('prenom');
    $email = cleanXss('email');
    $genre= cleanXss('genre');
    $age= cleanXss('age');
    $password = cleanXss('password');
    $password2 = cleanXss('password2');

    //validation de chaque champ => errors

    $errors = validationText($errors, $name, 'name', 3, 150);
    $errors= validationText($errors, $prenom, 'prenom', 2, 150);
    $errors = validationEmail($errors, $email);

    if (empty($errors['email'])){
        $sql= "SELECT email FROM user WHERE email= :email";
        $query= $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->execute();
        $verifEmail= $query->fetch();
        if (!empty($verifEmail)){
            $errors['email']='Cette adresse mail existe déjà';
        }
        debug($verifEmail);
    }
    if(!empty($genre)) {
        if(!array_key_exists($genre, $genres)) {
            $errors['genre'] = "N'existe pas";
        }
    } else {
        $errors['genre'] = 'Veuillez renseigner un genre';
    }
    if (!empty($age)){
       if ($age >= 123){
           $errors['age']= 'Âge non-valable*';
       }
    }else{
        $errors['age']='Veuillez renseigner un âge*';
    }
    if(!empty($password) && !empty($password2)) {
        if($password != $password2) {
            $errors['password'] = 'Vos mots de passe sont différents*';
        } elseif(mb_strlen($password) < 6) {
            $errors['password'] = 'Votre mot de passe est trop court(min 6)';
        }
    } else {
        $errors['password'] = 'Veuillez renseigner les mots de passe*';
    }
    if(count($errors) == 0) {
        // INSERT INTO
        $hashPassword = password_hash($password, PASSWORD_DEFAULT);
        $token = generateRandomString(80);
        $sql = "INSERT INTO user (name, prenom, email,age,genre,password,status, token,created_at) VALUES (:name, :pre,:em,:age,:genre,:pass,'new', '$token',NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('name', $name, PDO::PARAM_STR);
        $query->bindValue('pre', $prenom , PDO::PARAM_STR);
        $query->bindValue('em', $email, PDO::PARAM_STR);
        $query->bindValue('age', $age, PDO::PARAM_STR);
        $query->bindValue('genre', $genre, PDO::PARAM_STR);
        $query->bindValue('pass', $hashPassword, PDO::PARAM_STR);
        $query->execute();
        header('Location: login.php');
    }
}
//if no error
//INSERT INTO

include('inc/header.php'); ?>
<section id="regis">

        <div class="wrap3">

            <div class="form_regis">

                <div class="form_enregistrer">
                    <h2 class="title">S'inscrire</h2>
                    <form action="" method="post" class="wrapform" novalidate>
                        <div class="form_nomPrenom">
                            <div class="form_display">
                                <input name="name" id="name" placeholder="Nom" type="text">
                                <span class="errors"><?php viewError($errors, 'name'); ?></span>
                            </div>
                            <div class="form_prenom form_display">
                                <input type="text" id="prenom" name="prenom" placeholder="Prénom">
                                <span class="errors"><?php viewError($errors, 'prenom');  ?></span>
                            </div>
                        </div>

                        <div class="form_ageGenre">
                            <div class="form_age form_display">
                                <input name="age" id="age" placeholder="Age" type="number" min="0" max="123">
                                <span class="errors"><?php viewError($errors, 'age'); ?></span>
                            </div>

                            <div class="form_genre form_display">
                                <select name="genre" id="genre">
                                    <option value="">_sélectionnez un genre_</option>
                                    <?php foreach ($genres as $key => $value) { ?>
                                        <option value="<?php echo $key; ?>"<?php
                                        if(!empty($_POST['genre']) && $_POST['genre'] === $key) {
                                            echo ' selected';
                                        }
                                        ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                                <span class="error"><?php if(!empty($errors['genre'])) { echo $errors['genre']; } ?></span>
                            </div>
                        </div>

                        <div class="form_email form_display">
                            <input name="email" id="email" placeholder="Email" type="email">
                            <span class="errors"><?php viewError($errors, 'email'); ?></span>
                        </div>

                        <div class="form_password form_display">
                            <input name="password" id="password" placeholder="Mot de passe" type="password">
                            <span class="errors"><?php viewError($errors, 'password'); ?></span>
                        </div>

                        <div class="form_password2 form_display">
                            <input name="password2" id="password2" placeholder="Confirmez le mot de passe" type="password">
                            <span class="errors"><?php viewError($errors, 'password2'); ?></span>
                        </div>

                        <div class="form_submit">
                            <input name="submitted" type="submit" value="s'inscrire">
                        </div>
                    </form>
                </div>
                <div class="connecter">
                    <h2>Se connecter</h2>
                    <h3>Déjà inscrit ?</h3>
                    <i class="fa-solid fa-chevron-down"></i>
                    <div class="connecter_submit">
                        <a href="login.php">Se connecter</a>
                    </div>
                </div>
                <div>
        </div>

</section>
<?php include('inc/footer.php');