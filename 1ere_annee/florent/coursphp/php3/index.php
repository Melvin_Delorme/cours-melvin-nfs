<?php
class Trames {
    private $date;
    private $type;
    private $ipSource;
    private $ipDest;
    private $portSource;
    private $portDest;

    function __construct(){

    }

    public function getTrames(){
        return json_decode(file_get_contents("trames.json"), true);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getIpSource()
    {
        return $this->ipSource;
    }

    public function getIpDest()
    {
        return $this->ipDest;
    }

    public function getPortSource()
    {
        return $this->portSource;
    }

    public function getPortDest()
    {
        return $this->portDest;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setIpSource($ipSource)
    {
        $this->ipSource = $ipSource;
    }

    public function setIpDest($ipDest)
    {
        $this->ipDest = $ipDest;
    }

    public function setPortSource($portSource)
    {
        $this->portSource = $portSource;
    }

    public function setPortDest($portDest)
    {
        $this->portDest = $portDest;
    }
}

$trames = new Trames();
$tramesArray = $trames->getTrames();

$trames->setDate($tramesArray['date']);
$trames->setType($tramesArray['protocol']['name']);
$trames->setPortSource($tramesArray['protocol']['ports']['from']);
$trames->setPortDest($tramesArray['protocol']['ports']['dest']);

$addr = new AdresseIP();

$ipSource = str_split($tramesArray['ip']['from'], 2);
$ipDest = str_split($tramesArray['ip']['dest'], 2);

$ipSource = $addr->loopInArrayIP($ipSource);
$ipDest = $addr->loopInArrayIP($ipDest);

$sourceIsOk = $addr->checkIfIpIsGood($ipSource);
$destIsOk = $addr->checkIfIpIsGood($ipDest);

if($sourceIsOk){
    $trames->setIpSource($ipSource);
}

if($destIsOk){
    $trames->setIpDest($ipDest);
}

var_dump($trames);
//explode de l'ip qui est sous le format c0a8014a pour avoir 4 morceaux (c0, a8, 01 et 4a) pour envoyer ça morceau par morceau dans hex_to_dec();
// 11) AVANT d'utiliser setIpSource et le setIpDest (en gros, avant insertion des IP dans l'objet), convertir l'IP hexa en IP décimale
// - ASTUCE : utiliser la classe AdresseIP créée en début de semaine, elle a une fonction hex_to_dec de prête
// 12) AVANT inversion toujours, vérifier si l'IP est au bon format avec la fonction checkIfIpIsGood() de la classe AdresseIP, toujours
// 13) Ajoutez avec les setters, les IP dans l'objet

// - ATTENTION : Je ne veux voir aucun code autre les instanciations, les echo/var_dump() et certains appels de fonctions en dehors des classes

// LE RESULTAT DOIT ÊTRE LE VAR_DUMP DE L'EXO AVANT LA POSE IDENTIQUE SAUF POUR LES IPs QUI DOIVENT ÊTRE AU FORMAT IP (4 octets donc, sous le forma W.X.Y.Z)
