<?php

class Personnage {
    private $nom;
    private $niveau = 1;
    private $pv = 10;
    private $pm = 5;
    private $type;
    private $arme;

    function __construct($nom){
        $this->nom = $nom;
    }

    public function getNom(){
        return $this->nom;
    }
    public function getNiveau(){
        return $this->niveau;
    }
    public function getPV(){
        return $this->pv;
    }
    public function getPM(){
        return $this->pm;
    }
    public function getType(){
        return $this->type;
    }
    public function getArme(){
        return $this->arme;
    }
    public function setNom($value){
        $this->nom = $value;
    }
    public function setNiveau($value){
        $this->niveau = $value;
    }
    public function setPV($value){
        $this->pv = $value;
    }
    public function setPM($value){
        $this->pm = $value;
    }
    public function setType($value){
        $this->type = $value;
    }
    public function setArme($value){
        $this->arme = $value;
    }
    public function clearValues($obj){
        unset($obj->nom);
        unset($obj->niveau);
        unset($obj->pv);
        unset($obj->pm);
        unset($obj->type);
    }
    public function attaquer($cible){
        $degat = $this->getArme()->getDegat();
        $cible->setPV($cible->getPV() - $degat);
    }
}

class Guerrier extends Personnage {
    private $ratioPV = 1.9;
    private $ratioPM = 0.2;

    function __construct(){
    }

    public function getRatioPV(){
        return $this->ratioPV;
    }
    public function getRatioPM(){
        return $this->ratioPM;
    }

    public function setRatioPV($value){
        $this->ratioPV = $value;
    }
    public function setRatioPM($value){
        $this->ratioPM = $value;
    }
}


class Archer extends Personnage {
    private $ratioPV = 1.1;
    private $ratioPM = 0.8;

    function __construct(){
    }

    public function getRatioPV(){
        return $this->ratioPV;
    }
    public function getRatioPM(){
        return $this->ratioPM;
    }

    public function setRatioPV($value){
        $this->ratioPV = $value;
    }
    public function setRatioPM($value){
        $this->ratioPM = $value;
    }
}


class Mage extends Personnage {
    private $ratioPV = 0.6;
    private $ratioPM = 1.5;

    function __construct(){
    }

    public function getRatioPV(){
        return $this->ratioPV;
    }
    public function getRatioPM(){
        return $this->ratioPM;
    }

    public function setRatioPV($value){
        $this->ratioPV = $value;
    }
    public function setRatioPM($value){
        $this->ratioPM = $value;
    }
}

class Jeu {
    function __construct(){
    }

    public function init($classes, $perso){
        echo "Bonjour et bienvenue dans ce jeu vidéo ! Choisissez votre classe : \n";
        echo "1. Guerrier\n";
        echo "2. Archer\n";
        echo "3. Mage\n";
        $choixClasse = rtrim(fgets(STDIN));
        if($choixClasse == 1){
            echo "Vous êtes un guerrier !";
            $classe = $classes[0];
        }elseif($choixClasse == 2){
            echo "Vous êtes un archer !";
            $classe = $classes[1];
        }elseif($choixClasse == 3){
            echo "Vous êtes un mage !";
            $classe = $classes[2];
        }else{
            echo "Vous avez écrit n'importe quoi !";
        }
        echo "\n";
        $perso->setType($classe);
        $classePerso = $perso->getType();
        $perso->clearValues($classePerso);
        $pv = $perso->getPV() * $classePerso->getRatioPV(); // 10 * 1.1 = 11
        $pm = $perso->getPM() * $classePerso->getRatioPM(); // 5 * 0.8 = 4
        $perso->setPV($pv);
        $perso->setPM($pm);
    }

    public function choix($persoQuiJoue, $cible, $tourJoueur){
        if($tourJoueur === true){
            echo $cible->getNom()." vous attaque...\n";
            echo "1. Lui maraver sa tronche \n";
            echo "2. Fuir comme un gros lâche \n";
            echo "3. Appeler la police \n";
            $choixAttaque = rtrim(fgets(STDIN));
            if($choixAttaque == 1){
                $persoQuiJoue->attaquer($cible);
            }
        }else{
            $persoQuiJoue->attaquer($cible);
        }

    }
}

class Arme {
    private $nom;
    private $degat;

    function __construct($nom, $degat){
        $this->nom = $nom;
        $this->degat = $degat;
    }

    public function getNom(){
        return $this->nom;
    }
    public function getDegat(){
        return $this->degat;
    }

    public function setNom($value){
        $this->nom = $value;
    }
    public function setDegat($value){
        $this->degat = $value;
    }
}

$tesson = new Arme("Tesson de bouteille", 1);
$epee = new Arme("Épée", 5);

$guerrier = new Guerrier();
$archer = new Archer();
$mage = new Mage();

$perso = new Personnage("Florian");
$perso->setArme($epee);
$ennemi = new Personnage("Un mec bourré");
$ennemi->setArme($tesson);
$game = new Jeu();
$game->init([$guerrier, $archer, $mage], $perso);

while($ennemi->getPV() > 0){
    $game->choix($perso, $ennemi, true);
    echo "Il n'a plus que ".$ennemi->getPV()." PV !\n";
    echo "Il vous attaque !\n";
    $game->choix($ennemi, $perso, false);
    echo "Vous n'avez plus que ".$perso->getPV()." PV !\n";
}