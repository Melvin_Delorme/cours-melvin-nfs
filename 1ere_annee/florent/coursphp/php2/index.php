<?php
// En PHP
// 1) Créer une fonction qui converti 8 bits en octet (Par exemple : 10101000 => 168)
// 2) Créer une fonction qui converti 1 octet en 8 bits (Par exemple : 168 => 10101000)
// INTERDICTION D'UTILISER LES FONCTIONS PHP TOUTES FAITES POUR CONVERTIR !
function bits_to_byte($bits){
    $byte = 0; // VALEUR FINALE À RETOURNER
    $correspondance = [128, 64, 32, 16, 8, 4, 2, 1]; // TABLEAU DE CORRESPONDANCE
    for($i = 0; $i < count($correspondance); $i++){
        // On ne peut pas count une STRING. Car une string n'est pas COUNTABLE
        // On peut donc mettre à la place : count($correspondance) OU BIEN simplement 8
        // Car un octet fait et fera toujours 8 bits.
        if($bits[$i] === "1"){ // triple égal vérifie l'équivalence de la valeur ET de son type
            $byte = $byte + $correspondance[$i]; // ON ADDITIONNE
        }
    }
    return $byte;
}

echo bits_to_byte("10101000"); // ON MET SOUS FORME DE STRING SINON ON NE PEUT PAS BOUCLER DEDANS
echo "\n";

function byte_to_bits($byte){ // Vaut 168 au départ par exemple
    $bits = ""; // Nos futurs 8 bits à 1 ou à 0
    $correspondance = [128, 64, 32, 16, 8, 4, 2, 1];
    for($i = 0; $i < count($correspondance); $i++){
        if($correspondance[$i] <= $byte){ // Au premier tour, 128 est bien inférieur ou égal à 168
            $bits .= "1"; // On concatène un 1
            $byte = $byte - $correspondance[$i]; // On soustrait 128 de 168 (ce qui fait 40), sinon
            // Toutes les valeurs suivantes passeront aussi dans le if ce qui générera toujours 11111111
        }else{ // S'il ne l'est pas, on concatène un 0
            $bits .= "0";
        }
        echo $bits."\n";
    }
    return $bits;
}
$byte = 168;
echo "Résultat de ".$byte." en binaire => ".byte_to_bits($byte);
echo "\n";

//////////////////////////////////////////////////////////////////////////
// CORRECTION JUSQU'À EXO 8
class AdresseIP {
    function __construct(){}

    public function bits_to_byte($bits){
        $byte = 0; // VALEUR FINALE À RETOURNER
        $correspondance = [128, 64, 32, 16, 8, 4, 2, 1]; // TABLEAU DE CORRESPONDANCE
        for($i = 0; $i < count($correspondance); $i++){
            // On ne peut pas count une STRING. Car une string n'est pas COUNTABLE
            // On peut donc mettre à la place : count($correspondance) OU BIEN simplement 8
            // Car un octet fait et fera toujours 8 bits.
            if($bits[$i] === "1"){ // triple égal vérifie l'équivalence de la valeur ET de son type
                $byte = $byte + $correspondance[$i]; // ON ADDITIONNE
            }
        }
        return $byte;
    }

    public function byte_to_bits($byte){ // Vaut 168 au départ par exemple
        $bits = ""; // Nos futurs 8 bits à 1 ou à 0
        $correspondance = [128, 64, 32, 16, 8, 4, 2, 1];
        for($i = 0; $i < count($correspondance); $i++){
            if($correspondance[$i] <= $byte){ // Au premier tour, 128 est bien inférieur ou égal à 168
                $bits .= "1"; // On concatène un 1
                $byte = $byte - $correspondance[$i]; // On soustrait 128 de 168 (ce qui fait 40), sinon
                // Toutes les valeurs suivantes passeront aussi dans le if ce qui générera toujours 11111111
            }else{ // S'il ne l'est pas, on concatène un 0
                $bits .= "0";
            }
            echo $bits."\n";
        }
        return $bits;
    }

    public function hex_to_dec($hexa){ // $hexa = "2F" par exemple, donc un tableau de caractères
        $correspondance = ["0" => 0, "1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7, "8" => 8, "9" => 9, "A" => 10, "B" => 11, "C" => 12, "D" => 13, "E" => 14, "F" => 15];
        $dizaine = $hexa[0]; // à la case 0 du tableau, on a "2"
        $unite = $hexa[1]; // à la case 1 du tableau, on a "F"
        return $correspondance[$dizaine] * 16 + $correspondance[$unite]; // 2 * 16 + 15
    }

    public function dec_to_hex($dec){ // $dec = "254" par exemple, donc un tableau de caractères
        $correspondance = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'];
        $dizaine = $correspondance[floor($dec / 16)]; // 15.8.... QU'on arrondit (donc 15)
        $unite = $correspondance[$dec % 16]; // 14
        return $dizaine.$unite; // FE
    }

    public function convert2rgb($hexa){ #FFFF2D -> 255,255,45
        // - découper en 3 le code hexadécimal (FF, FF et 2D)
        $hex1 = $hexa[1].$hexa[2];
        $hex2 = $hexa[3].$hexa[4];
        $hex3 = $hexa[5].$hexa[6];
        // - utiliser une des fonctions de l'exo 7 qui fonctionne déjà et qui transforme en décimal votre bloc de 2 caractères hexadécimaux
        $dec1 = $this->hex_to_dec($hex1);
        $dec2 = $this->hex_to_dec($hex2);
        $dec3 = $this->hex_to_dec($hex3);
        // - Concaténer une string + les décimaux pour afficher dans le terminal rgb(255,255,45)
        echo "rgb(".$dec1.",".$dec2.",".$dec3.")";
    }

}

$adresse = new AdresseIP();

echo "\n";