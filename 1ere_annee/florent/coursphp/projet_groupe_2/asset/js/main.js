console.log('Project de groupe 2');
///////////////////////
// Projet de groupe 2
/////////////////////

const darkModeSwitch = document.getElementById('dark-mode-switch');
const accueil_background2 = document.getElementById('accueil_background2');

darkModeSwitch.addEventListener('change', (event) => {
    if (event.target.checked) {
        document.body.classList.add('dark-mode');
        accueil_background2.className = 'accueil_background2-darkmode';
    } else {
        document.body.classList.remove('dark-mode');
        accueil_background2.className = 'accueil_background2-light';
    }
});

///////////////////////////////
// MODAL

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

////////////////////////////////////////////////////////////////
// ANIMATIONS

const wwd1 = document.querySelector('.wwd_1');  // select the .wwd_1 element

const observer = new IntersectionObserver((entries) => {
    if (entries[0].isIntersecting) {  // if the element is intersecting with the viewport
        wwd1.classList.add('visible');  // add the 'visible' class
    }
});

observer.observe(wwd1);  // start observing the element

const wwd2 = document.querySelector('.wwd_2');  // select the .wwd_2 element

const observer2 = new IntersectionObserver((entries) => {
    if (entries[0].isIntersecting) {  // if the element is intersecting with the viewport
        wwd2.classList.add('visible');  // add the 'visible' class
    }
});

observer2.observe(wwd2);  // start observing the element


const teamList = document.querySelector('.team_list');  // select the .team_list element

const observer3 = new IntersectionObserver((entries) => {
    if (entries[0].isIntersecting) {  // if the element is intersecting with the viewport
        teamList.classList.add('visible');  // add the 'visible' class
    }
});

observer3.observe(teamList);  // start observing the element

const contactForm = document.querySelector('#contact_us form');  // select the #contact_us form element

const observer4 = new IntersectionObserver((entries) => {
    if (entries[0].isIntersecting) {  // if the element is intersecting with the viewport
        contactForm.classList.add('visible');  // add the 'visible' class
    }
});

observer4.observe(contactForm);  // start observing the element

