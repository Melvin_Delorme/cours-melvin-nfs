document.querySelector('form').addEventListener('submit', function(e) {
    e.preventDefault();

    // récupère les données du formulaire
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;

    // prépare les données à envoyer au script PHP
    const formData = new FormData();
    formData.append('submitted', 1);
    formData.append('email', email);
    formData.append('password', password);

    // envoie la requête AJAX
    fetch('lg.php', {
        method: 'POST',
        body: formData
    })
        .then((response) => response.json()) // récupère la réponse en format JSON
        .then((data) => {
            // réinitialise les erreurs
            document.querySelector('#error-email').innerHTML = '';
            document.querySelector('#error-password').innerHTML = '';

            // affiche les erreurs
            // if (data.errors.email) {
            //     document.querySelector('#error-email').value = data.errors.email;
            // }
            // if (data.errors.password) {
            //     document.querySelector('#error-password').value = data.errors.password;
            // }

            // redirige vers l'espace membre si la connexion a réussi
            if (!data.errors) {
                window.location = 'index2.php';
            }
        })
        .catch((error) => {
            console.log(error);
        });
});





