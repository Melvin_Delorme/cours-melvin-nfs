<?php
session_start();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Project de groupe 2</title>
    <link rel="stylesheet" href="asset/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Alegreya+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com/%22%3E<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin><link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
<header>
    <div class="header_left">
        <a href="index.php"><img src="asset/img/logo_simple.png" alt="logo"></a>
    </div>
    <ul class="header_center">
       <li><a href="#accueil">Accueil</a></li>
       <li><a href="#whatwedo">Qu'est ce qu'on fait?</a></li>
        <li><a href="#ourteam">Notre equipe</a></li>
    </ul>
    <div class="header_right">
        <div class="hr_topbutton">
            <!--      SI CONNECTE      -->
            <!--<a href="">Deconnection</a>-->
            <a href="inscription.php">Inscription</a>

            <!--    SI CONNECTE    -->
            <!--<li><a href="">Graphs</a></li>-->
            <li><a href="login.php">Connection</a></li>
        </div>
        <div class="hr_bottombutton">
            <label class="switch">
                <input type="checkbox" id="dark-mode-switch">
                <span class="slider round"></span>
            </label>
        </div>
    </div>
</header>