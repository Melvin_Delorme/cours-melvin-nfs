
let isDarkMode = false;

// Charger l'état du mode sombre ou light à partir de localStorage ou de cookies
const darkModeFromStorage = localStorage.getItem('darkMode');
if (darkModeFromStorage === 'true') {
    isDarkMode = true;
}

function toggleDarkMode() {
    isDarkMode = !isDarkMode;
    updateDarkMode();
}

function updateDarkMode() {
    const body = document.getElementById('content-wrapper');
    const switchButton = document.getElementById('dark-mode-switch');
    if (isDarkMode) {
        body.classList.add('dark-mode');
        switchButton.checked = true;
        // Enregistrer l'état du mode sombre dans localStorage ou dans un cookie
        localStorage.setItem('darkMode', 'true');
    } else {
        body.classList.remove('dark-mode');
        switchButton.checked = false;
        // Enregistrer l'état du mode light dans localStorage ou dans un cookie
        localStorage.setItem('darkMode', 'false');
    }
    updateStyles();
}

function updateStyles() {
    const mode = document.querySelector('.h3');
    const graphContent = document.querySelectorAll('.txt_graph');
    const card = document.querySelectorAll('.card');
    const white = document.querySelector('.bg-white');
    const cardInter = document.querySelectorAll('.card-header');
    const cardFooter = document.querySelectorAll('.card-footer');
    const footer = document.querySelectorAll('.sticky-footer');
    const colorMode = document.querySelectorAll('.color_mode');
    const table = document.querySelectorAll('th');

    if (isDarkMode) {
        white.style.backgroundColor = '#0e174e';
        mode.style.color = 'white';
        card.forEach(function (card) {
            card.classList.add('card_ensemble');
        });
        cardInter.forEach(function (cardHea) {
            cardHea.style.backgroundColor = '#0c0a4c';
            cardHea.style.borderBottom = '0';
        });
        graphContent.forEach(function (content) {
            content.style.color = 'white';
        });
        cardFooter.forEach(function (cardFoot) {
            cardFoot.style.backgroundColor = '#0c0a4c';
            cardFoot.style.borderBottom = '0';
        });
        footer.forEach(function (foot) {
            foot.style.backgroundColor = '#0c0a4c';
            foot.style.color = 'white';
        });
        colorMode.forEach(function (mode) {
            mode.style.color = 'white';
        });
        table.forEach(function (table) {
            table.style.color = 'cornflowerblue';
        });
    } else {
        white.style.backgroundColor = 'white';
        mode.style.color = '#5a5c69';
        card.forEach(function (card) {
            card.classList.remove('card_ensemble');
        });
        cardInter.forEach(function (cardHea) {
            cardHea.style.backgroundColor = '#f8f9fc';
            cardHea.style.borderBottom = '1px solid #e3e6f0';
        });
        graphContent.forEach(function (content) {
            content.style.color = '';
        });
        cardFooter.forEach(function (cardFoot) {
            cardFoot.style.backgroundColor = '#f8f9fc';
            cardFoot.style.borderBottom = '1px solid #e3e6f0';
        });
        footer.forEach(function (foot) {
            foot.style.backgroundColor = '';
            foot.style.color = '';
        });
        colorMode.forEach(function (mode) {
            mode.style.color = '';
        });
        table.forEach(function (table) {
            table.style.color = '';
        });
    }
}


const darkModeSwitch = document.getElementById('dark-mode-switch');
darkModeSwitch.addEventListener('change', toggleDarkMode);
updateDarkMode();

    const darkMode = document.getElementById('dark-mode-switch');
const h3 = document.querySelector('h1');

darkMode.addEventListener('change', (event) => {
    const body  = document.getElementById('content-wrapper');
    if (event.target.checked) {
        body.classList.add('dark-mode');
        h3.style.color = 'white';
    } else {
        body.classList.remove('dark-mode');
        h3.style.color = '';
    }
});

