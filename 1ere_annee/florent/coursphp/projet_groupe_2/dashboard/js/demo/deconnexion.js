const deconnexion = document.querySelector('.deconnexion');

deconnexion.addEventListener('click', function (evt) {
    evt.preventDefault();
    fetch('index.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: 'action=logout'
    }).then(function (response) {
        if (response.ok) {
            window.location.href = 'inc/deconnect.php';
        } else {
            alert('There was an error logging out.');
        }
    });
});