// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

let ctx = document.getElementById('myPieChart');
let trames_Nombres = [];
let trames_Double = [];
fetch('inc/requetDiff.php')
    .then(function(response){
      return response.json();
    })
    .then(function(data){
      for (let i = 0; i < data.length; i++){
        trames_Nombres.push(data[i].nombre);
        trames_Double.push(data[i].name + ' ' + data[i].status);
      }

      let chart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: trames_Double,
          datasets: [{
            label: 'Nombre',
            data: trames_Nombres,
            backgroundColor: 'rgba(46,89,217,0.37)',
            borderColor: 'rgba(23,166,115,0.4)',
            borderWidth: 1
          },]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    });