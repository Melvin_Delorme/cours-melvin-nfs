let ttl_status = [];
let ttl_nmb = [];
fetch('inc/requet_bar.php')
    .then(function (response){
      return response.json();
    })
    .then(function (data){
      for (let i = 0; i < data.length; i++){
        console.log(data)
        ttl_nmb.push(data[i].nombre);
        ttl_status.push(data[i].status);
      }
      const ctx = document.getElementById('myBarChart');
      new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ttl_status,
          datasets: [{
            label: '#of_votes',
            data: ttl_nmb,
            backgroundColor: [
              'rgba(78,115,223,0.46)',
              'rgba(28,200,138,0.47)',
              'rgba(54,185,204,0.38)',
              'rgba(35,7,117,0.36)',
            ],
            borderColor: [
              'rgba(78,115,223,0.46)',
              'rgba(28,200,138,0.47)',
              'rgba(54,185,204,0.38)',
              'rgba(35,7,117,0.36)',
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
    });
