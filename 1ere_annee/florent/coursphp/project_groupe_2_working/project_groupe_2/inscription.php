<?php
include('inc/header.php');
require ('inc/function.php');

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="asset/css/style.css">
    <title>Inscription</title>
</head>
<body>
<div id="inscriptionForm">
    <!--    <div class="side_right_image">-->
    <!--        <img src="asset/img/img_inscription 1.png" alt="">-->
    <!--    </div>-->
    <div class="form-container">
        <div class="imgLogo">
            <img src="asset/img/GL_BAL_3 1.png" alt="">
        </div>
        <form action="" id="input_form" method="post" >
            <input type="text" placeholder="Nom" name="fullname" id="fullname">
            <span class="error" id="error_fullname"></span>
            <input type="text" placeholder="Prenom" name="surname" id="surname">
            <span class="error" id="error_surname"></span>
            <input type="text" placeholder="Email" name="mail" id="mail">
            <span class="error" id="error_mail"></span>
            <input type="password" placeholder="Mots de passe" name="password" id="password">
            <span class="error" id="error_password"></span>
            <input type="password" placeholder="mots de passe confirmartion" name="password2" id="password2" onkeyup="validateEmail()">
            <span class="error" id="error_password2"></span>
            <input type="button" value="Inscription" id="submit">
            <a style="color: white" href="login.php">Deja un compte...?</a>
        </form>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    const submit = document.querySelector('#submit');
    const input = document.querySelector('#input_form');
    console.log(input[3])
    submit.addEventListener('click',function (evt){

        let data = input[0].value.trim()
        let data1 = input[1].value.trim()
        let data2 = input[2].value.trim()
        let data3 = input[3].value.trim()
        let data4 = input[4].value.trim()

        if (data == '' && data1 ==''){
            evt.preventDefault();
            console.log('formulaire non  soumis');
        } else if (data2 == ''){
            console.log( 'formulaire non soumis');
        }else if (data3 == ''){
            console.log( 'formulaire non soumis');
        } else if (data4 == ''){
            console.log('formulaire non sousmis')
        }
        else {
            $.ajax({
                url:'http://localhost/js/Projet_trames_reseaux/project_groupe_2/server.php',
                type:'POST',
                data: $("#input_form").serialize(),
                success:function (response){



                }
            })

            console.log('formulaire soumis')
            window.location.href="login.php";

        }
    })



    //erreurs
    function validationInputs(id,min,max){
        const input = document.querySelector('#' + id);
        const error = document.querySelector('#error_' + id)
        const password = document.querySelector('#password')
        const passW2 = document.querySelector('#password2')
        const errorPassW = document.querySelector('#error_password2')
        input.addEventListener('blur', function (){
            let inputValue = input.value.length
            let text = '';
            let classError = 'error';
            if (input.value!=''){
                if (inputValue < min){
                    text =  'Veuillez renseigner min '+min+' caractères';
                }else if (inputValue>max){
                    text = 'Veuillez renseigner max '+max+' caractères';
                } else if (password.value != passW2.value){
                    errorPassW.innerHTML = "sont diff";
                } else if (passW2.value != password.value ){
                    errorPassW.innerHTML = "les mots de passe sont different";
                }
                else{

                    text = '&#9989;';
                    classError = 'success';
                }
            } else {
                text = 'Veuillez renseigner ce champ';
            }

            error.innerHTML = text;
            error.className = classError
        })
    }
    validationInputs('fullname',1,70);
    validationInputs('surname',1,50);
    function validateEmail(){
        const inputPw = document.querySelector('#mail')
        const errorEmail = document.querySelector('#error_mail')
        inputPw.addEventListener('keyup',()=>{
            if(!inputPw.value.match(/^[A-Za-z\._\-0-9]*[@][A-Za-z]*[\.][a-z]{2,4}$/)){
                errorEmail.innerHTML = "svp mettez un email valide";
                return false;
            }
            errorEmail.innerHTML = "&#9989;";
            return true;
        })

    }
    validateEmail('mail')
    validationInputs('password',6,50);
    validationInputs('password2',6,50);

</script>
</body>
</html>

