

let trames_nombres = [];
let trames_Name = [];
fetch('inc/requet.php')
    .then(function (response){
      return response.json();
    })
    .then(function (data){
      for (let i = 0; i < data.length; i++){
        trames_nombres.push(data[i].number);
        trames_Name.push(data[i].name);
      }
      const ctx = document.getElementById('myDoughnutChart');
      new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: trames_Name,
          datasets: [{
            label: '# of Votes',
            data: trames_nombres,
            backgroundColor: ['rgba(78,115,223,0.46)', 'rgba(28,200,138,0.47)', 'rgba(54,185,204,0.38)','rgba(35,7,117,0.36)' ],
            hoverBackgroundColor: ['rgba(46,89,217,0.37)', 'rgba(23,166,115,0.4)', 'rgba(44,159,175,0.46)'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            borderWidth: 1,
          }]
        },
        options: {
          scales: {
            y: {
              width_: 100,
              height_: 100,
              beginAtZero: true
            }
          }
        }
      });
    });



