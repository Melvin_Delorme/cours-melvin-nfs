<?php
require ('../../inc/pdo.php');

$user_id = $_GET['id'];

$sql = "DELETE FROM user WHERE id = :id";
$stmt = $pdo->prepare($sql);
$stmt->bindValue(':id', $user_id, PDO::PARAM_INT);
$stmt->execute();

// Redirect to the home page
header('Location: index.php');
exit;
?>


