<?php
require('inc/function.php');
//debug($_SESSION);

include('inc/header.php');
?>

<div id="container_login">
    <div class="form_login">
        <form id="login-form" method="post">
            <div class="form-group">
                <!--                <label for="email">Email</label>-->
                <input type="email" name="email" id="email" placeholder="Email" required>
                <span id="error-email"></span><br>
            </div>
            <div class="form-group">
                <!--                <label for="password">Mot de passe</label>-->
                <input type="password" name="password" id="password" placeholder="mots de passe" required>
                <span id="error-password"></span><br></br>
            </div>
            <input type="hidden" name="submitted" value="1">
            <input type="submit" value="Envoyer">
        </form>
    </div>
</div>
<script>
    document.querySelector('form').addEventListener('submit', function(e) {
        e.preventDefault();

        // récupère les données du formulaire
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;

        // prépare les données à envoyer au script PHP
        const formData = new FormData();
        formData.append('submitted', 1);
        formData.append('email', email);
        formData.append('password', password);

        // envoie la requête AJAX
        fetch('lg.php', {
            method: 'POST',
            body: formData
        })
            .then(function (response){
                return response.json()
        }) // récupère la réponse en format JSON
            .then((data) => {
                // réinitialise les erreurs
                document.querySelector('#error-email').innerHTML = '';
                document.querySelector('#error-password').innerHTML = '';

                // affiche les erreurs
                // if (data.errors.email) {
                //     document.querySelector('#error-email').value = data.errors.email;
                // }
                // if (data.errors.password) {
                //     document.querySelector('#error-password').value = data.errors.password;
                // }

                // redirige vers l'espace membre si la connexion a réussi
                if (!data.errors) {
                    window.location = 'dashboard/index.php';
                }
            })
            .catch((error) => {
                console.log(error);
            });
    });






</script>