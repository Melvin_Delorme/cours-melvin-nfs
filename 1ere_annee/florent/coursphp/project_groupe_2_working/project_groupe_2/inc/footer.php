<footer>
    <div class="footer_left">
        <a href="index.php"><img src="asset/img/logo.png" alt="logo"></a>
    </div>
    <div class="footer_center">
        <h2><a href="mentions_legales.php">Mentions Legales</a></h2>
        <h3>2022 copyright all right reserved.</h3>
    </div>
    <ul class="footer_right">
        <div class="fr_top">
            <li><a href=""><i class="fa-brands fa-twitter"></i></a></li>
            <li><a href=""><i class="fa-brands fa-facebook-f"></i></a></li>
        </div>
        <div class="fr_bottom">
            <li><a href=""><i class="fa-brands fa-youtube"></i></a></li>
            <li><a href=""><i class="fa-brands fa-instagram"></i></a></li>
        </div>
    </ul>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="asset/js/main.js"></script>

</body>
</html>