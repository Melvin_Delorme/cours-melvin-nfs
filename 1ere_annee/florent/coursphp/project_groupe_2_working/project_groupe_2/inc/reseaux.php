<?php
require('function.php');
require('pdo.php');

function getTrames(){
    return json_decode(file_get_contents("../trames.json"), true);
}
$trames = getTrames();

for($i = 0; $i < count($trames);$i++) {
    $date = $trames[$i]["date"];
    $flagscode = $trames[$i]["flagscode"];
    $identification =  $trames[$i]["identification"];
    $ttl = $trames[$i]["ttl"];
    $name = $trames[$i]["protocol_name"];
    $dest = $trames[$i]["protocol_portsdest"];
    $ipSource =  $trames[$i]["ip_from"];
    $status = $trames[$i]["status"];
    $octer1 =  $ipSource[0]. $ipSource[1];
    $octer2 =  $ipSource[2]. $ipSource[3];
    $octer3 =  $ipSource[4]. $ipSource[5];
    $octer4 =  $ipSource[6]. $ipSource[7];
    $adresseip1 = hex_to_dec($octer1);
    $adresseip2 = hex_to_dec($octer2);
    $adresseip3 = hex_to_dec($octer3);
    $adresseip4 = hex_to_dec($octer4);
    $adresse_ip = $adresseip1. '.'. $adresseip2. '.' .$adresseip3. '.'. $adresseip4;
    echo 'la date est '. ' : ' .$date;
    echo 'flagscode '. ' ' .$flagscode;
    echo 'ttl '. ' : ' .$ttl;
    echo ' le name  est '. ' ' .$name;
    echo ' le dest  est '. ' ' .$dest;
    $user_id='';
    echo 'l\'identifcation est '. ' ' .$identification;
    echo 'votre adresse ip est '. ' ' .$adresse_ip;
    if ($i >= 0 AND $i <= 249 ){
        $user_id = 1 ;
        $sql = "INSERT INTO trames(date,flagscode,identification,ttl,protocol_name,protocol_portsdest,ip_from,status, user_id)
    VALUES (:date,:flagscode,:identification,:ttl,:protocol_name,:protocol_portsdest,:ip_from,:status, :user_id)";
        $query = $pdo->prepare($sql);
        $query->bindValue('date', $date, PDO::PARAM_STR);
        $query->bindValue('identification', $identification, PDO::PARAM_STR);
        $query->bindValue('status', $flagscode, PDO::PARAM_STR);
        $query->bindValue('ttl', $ttl, PDO::PARAM_STR);
        $query->bindValue('dest', $dest, PDO::PARAM_STR);
        $query->bindValue('from_', $name, PDO::PARAM_STR);
        $query->bindValue('name', $date, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
    }
     if ($i >= 250 AND $i <= 499 ){
        $user_id = 2 ;
        $sql = "INSERT INTO trames(date,flagscode,identification,ttl,protocol_name,protocol_portsdest,ip_from,status, user_id)
    VALUES (:date,:flagscode,:identification,:ttl,:protocol_name,:protocol_portsdest,:ip_from,:status, :user_id)";
        $query = $pdo->prepare($sql);
        $query->bindValue('adresse_ip', $adresse_ip, PDO::PARAM_STR);
        $query->bindValue('identification', $identification, PDO::PARAM_STR);
        $query->bindValue('status', $flagscode, PDO::PARAM_STR);
        $query->bindValue('ttl', $ttl, PDO::PARAM_STR);
        $query->bindValue('dest', $dest, PDO::PARAM_STR);
        $query->bindValue('from_', $name, PDO::PARAM_STR);
        $query->bindValue('name', $date, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
    }
     if ($i >= 500 AND $i <= 749 ){
        $user_id = 3 ;
        $sql = "INSERT INTO trames(date,flagscode,identification,ttl,protocol_name,protocol_portsdest,ip_from,status, user_id)
    VALUES (:date,:flagscode,:identification,:ttl,:protocol_name,:protocol_portsdest,:ip_from,:status, :user_id)";
        $query = $pdo->prepare($sql);
        $query->bindValue('adresse_ip', $adresse_ip, PDO::PARAM_STR);
        $query->bindValue('identification', $identification, PDO::PARAM_STR);
        $query->bindValue('status', $flagscode, PDO::PARAM_STR);
        $query->bindValue('ttl', $ttl, PDO::PARAM_STR);
        $query->bindValue('dest', $dest, PDO::PARAM_STR);
        $query->bindValue('from_', $name, PDO::PARAM_STR);
        $query->bindValue('name', $date, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
    }
     if ($i >= 750 AND $i <= 1000 ){
        $user_id = 4 ;
        $sql = "INSERT INTO trames(date,flagscode,identification,ttl,protocol_name,protocol_portsdest,ip_from,status, user_id)
    VALUES (:date,:flagscode,:identification,:ttl,:protocol_name,:protocol_portsdest,:ip_from,:status, :user_id)";
        $query = $pdo->prepare($sql);
        $query->bindValue('adresse_ip', $adresse_ip, PDO::PARAM_STR);
        $query->bindValue('identification', $identification, PDO::PARAM_STR);
        $query->bindValue('status', $flagscode, PDO::PARAM_STR);
        $query->bindValue('ttl', $ttl, PDO::PARAM_STR);
        $query->bindValue('dest', $dest, PDO::PARAM_STR);
        $query->bindValue('from_', $name, PDO::PARAM_STR);
        $query->bindValue('name', $date, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
    }
}

