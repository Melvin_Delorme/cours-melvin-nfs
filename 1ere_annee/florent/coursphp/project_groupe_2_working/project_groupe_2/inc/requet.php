<?php
function showJson($data){
    header("Content-type: application/json");
    $json = json_encode($data, JSON_PRETTY_PRINT);
    if ($json){
        die($json);
    }else{
        die('error in json encoding');
    }
}

require ('pdo.php');
    $sql = "SELECT name, COUNT(*) nombre FROM `trames` GROUP BY name";
$query = $pdo->prepare($sql);
$query->execute();
$trames = $query->fetchAll();
showJson($trames);