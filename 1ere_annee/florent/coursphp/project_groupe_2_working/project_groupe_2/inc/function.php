<?php


function debug($tableau) {
    echo '<pre style="height:150px;overflow-y: scroll;font-size: .7rem;padding: .6rem;font-family: Consolas, Monospace; background-color: #000;color:#fff;">';
    print_r($tableau);
    echo '</pre>';
}

function redirectNotFound()
{
    header("HTTP/1.0 404 Not Found");
    header('Location: 404.php');
}

function cleanXss($key)
{
    return trim(strip_tags($_POST[$key]));
}

function hex_to_dec($hexa){ // $hexa = "2F" par exemple, donc un tableau de caractères
    $correspondance = ["0" => 0, "1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7, "8" => 8, "9" => 9, "A" => 10, "B" => 11, "C" => 12, "D" => 13, "E" => 14, "F" => 15];
    $dizaine = $hexa[0]; // à la case 0 du tableau, on a "2"
    $unite = $hexa[1]; // à la case 1 du tableau, on a "F"
    return $correspondance[$dizaine] * 16 + $correspondance[$unite]; // 2 * 16 + 15
}

function dec_to_hex($dec){ // $dec = "254" par exemple, donc un tableau de caractères
    $correspondance = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'];
    $dizaine = $correspondance[floor($dec / 16)]; // 15.8.... QU'on arrondit (donc 15)
    $unite = $correspondance[$dec % 16]; // 14
    return $dizaine.$unite; // FE
}

function showJson($data) {
    header("Content-type: application/json");
    $json = json_encode($data, JSON_PRETTY_PRINT);
    if ($json) {
        die($json);
    } else {
        die('error in json encoding');
    }
}

function isLogged()
{
    if (!empty($_SESSION['user']['id'])) {
        if (is_numeric($_SESSION['user']['id'])) {
            /*if(!empty($_SESSION['user']['nom'])){
                if (!empty($_SESSION['user']['prenom'])) {*/
            if (!empty($_SESSION['user']['email'])) {
                return true;
            }
            /* }*/
            /* }*/
        }
        return false;
    }
}