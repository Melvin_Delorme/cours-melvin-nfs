<?php
require('inc/pdo.php');
require('inc/function.php');

$errors = array();
$success = false;

//email, message

if(!empty($_POST['submitted'])){
    // Failles Xss
    $email = cleanXss('email');
    $message = cleanXss('message');
    // Validations
    $errors = validationEmail($errors,$email,'email');
    $errors = validationText($errors,$message,'message',5,150);

    // SQL
    if (count($errors) == 0) {
        $sql = "INSERT INTO contact (email,message)
                VALUES (:email,:message)";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->bindValue('message', $message);
        $query->execute();
        $success = true;
    }
}

include('inc/header.php'); ?>
    <section id="accueil">
        <div class="accueil_background"></div>
        <div id="accueil_background2" class="accueil_background2-light"></div>
        <div class="accueil_left">
            <div class="accueil_left2">
                <div class="accueil_ltop">
                    <h2>Des utilisateurs extraordinaires méritent</h2>
                    <h1>Un site extraordinaire</h1>
                    <h4>La seule plateforme dont vous aurez besoin pour gérer la sécurité de vos donées trames réseau : des graphiques intégrées, simples et appréciées par des millions d'utilisateurs satisfaits.</h4>
                </div>
                <ul class="accueil_lbottom">
                    <li><a href="#whatwedo">Qu'est ce qu'on fait?</a></li>
                    <li><a href="#ourteam">Notre equipe</a></li>
                </ul>
            </div>
        </div>
        <div class="accueil_right">
            <img src="asset/img/ordinateur.png" alt="Ordinateur">
            <div class="accueil_modal">
                <div id="myModal" class="modal">
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <iframe id="video"  width="100%" height="100%" src="https://www.youtube.com/embed/PLmrHvsEkV4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    </div>
                </div>
                <i id="myBtn" class="fa-solid fa-play"></i>
            </div>
        </div>
    </section>

    <section id ="whatwedo">
        <h2>Qu'est-ce qu'on fait</h2>
        <div class="wwd_all">
            <div class="wwd_1">
                <div class="wwd_1_1">
                    <div class="wwd_1_1_top">
                        <i class="fa-solid fa-globe"></i>
                        <h4>Competitive exchange rates</h4>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda consectetur cupiditate dicta distinctio ipsa, itaque molestiae mollitia omnis optio possimus quae quos ut voluptatibus.</p>
                </div>

                <div class="wwd_1_2">
                    <div class="wwd_1_2_top">
                        <i class="fa-solid fa-scale-balanced"></i>
                        <h4>No hidden fees</h4>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda consectetur cupiditate dicta distinctio ipsa, itaque molestiae mollitia omnis optio possimus quae quos ut voluptatibus.</p>
                </div>
            </div>
            <div class="wwd_2">
                <div class="wwd_2_1">
                    <div class="wwd_2_1_top">
                        <i class="fa-solid fa-bolt"></i>
                        <h4>Transfer are instant</h4>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda consectetur cupiditate dicta distinctio ipsa, itaque molestiae mollitia omnis optio possimus quae quos ut voluptatibus.</p>
                </div>

                <div class="wwd_2_2">
                    <div class="wwd_2_2_top">
                        <i class="fa-solid fa-mobile"></i>
                        <h4>Mobile notifications</h4>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda consectetur cupiditate dicta distinctio ipsa, itaque molestiae mollitia omnis optio possimus quae quos ut voluptatibus.</p>
                </div>
            </div>
        </div>
        <div class="vid_losange2"></div>
    </section>

    <section id="ourteam">
        <h2>Notre equipe</h2>
        <ul class="team_list">
            <li>
                <img src="asset/img/melvin.jpg" alt="photo de profil">
                <h4>Melvin</h4>
            </li>
            <li>
                <img src="asset/img/axel.jpg" alt="photo de profil">
                <h4>Axel</h4>
            </li>
            <li>
                <img src="asset/img/kevin.jpg" alt="photo de profil">
                <h4>Kevin</h4>
            </li>
            <li>
                <img src="asset/img/lendoly.jpg" alt="photo de profil">
                <h4>Lendoly</h4>
            </li>
            <li>
                <img src="asset/img/prince.jpg" alt="photo de profil">
                <h4>Prince</h4>
            </li>
        </ul>
        <div class="vid_losange3"></div>
    </section>

    <section id="contact_us">
        <div class="contact_background"></div>
        <div class="contact_all">
            <div class="contact_left">
                <img src="https://odoocdn.com/openerp_website/static/src/img/2020/home/screens-mockup.png" alt="Contact img">
            </div>
            <div class="contact_right">
                <div class="contact_right_all">
                    <h1>Nous contacter</h1>
                    <form id="form_contact" action="#contact_us" method="post">
                        <div class="form_all">
                            <div class="form_email">
                                <input class="email" type="text" id="email" name="email" placeholder="vous@example.fr" value="<?php if ($success == true) {echo ("");} elseif(!empty($_POST['email'])) {echo $_POST['email'];} ?>">
                                <span class="error"><?php if(!empty($errors['email'])) {echo $errors['email'];} ?></span>
                            </div>

                            <div class="form_description">
                                <textarea class="message" type="text" id="message" name="message" placeholder="message" ><?php if ($success == true) {echo ("");} elseif(!empty($_POST['message'])) {echo $_POST['message'];} ?></textarea>
                                <span class="error"><?php if(!empty($errors['message'])) {echo $errors['message'];} ?></span>
                            </div>

                            <div class="form_valid">
                                <input id="submit_contact" type="submit" name="submitted" value="envoyer">
                            </div>

                            <div class="form_sucess">
                                <?php if ($success == true) {?>
                                    <h1>Merci pour votre message!</h1>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php include('inc/footer.php');