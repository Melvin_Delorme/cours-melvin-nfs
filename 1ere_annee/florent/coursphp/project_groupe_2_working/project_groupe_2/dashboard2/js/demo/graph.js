const darkModeSwitch = document.getElementById('content');
const graph = document.querySelector('.h3');

darkModeSwitch.addEventListener('change', (event) => {
    const body  = document.getElementById('content-wrapper');
    if (event.target.checked) {
        body.classList.add('dark-mode');
        graph.style.color = 'white';
    } else {
        body.classList.remove('dark-mode');
        graph.style.color = '#5a5c69';
    }
});