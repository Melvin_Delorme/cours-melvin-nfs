// Area Chart
let trames_status = {
  failed: 0,
  unknown: 0,
  pending: 0,
  success: 0
};
fetch('inc/requet2.php')
    .then(function (response){
      return response.json();
    })
    .then(function (data){
      console.log(data)
      console.log(data.status)
      data.forEach(function(trame) {
        if (trame.status in trames_status) {
          trames_status[trame.status]++;
        }
      });
      const ctx = document.getElementById('myAreaChart');
      new Chart(ctx, {
        type: 'polarArea',
        data: {
          labels: ['failed', 'unknown', 'pending', 'success' ],
          datasets: [{
            label: '# of Votes',
            data: [trames_status.failed, trames_status.unknown, trames_status.pending, trames_status.success],
            backgroundColor: ['#c41415', '#1b3164', '#4e73df','#17a673' ],
            hoverBackgroundColor: ['#8a0f10', '#1d3e8f', '#2e59d9', '#0d6c2e'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
    });


const darkModeSwitch = document.getElementById('dark-mode-switch');
const h3 = document.querySelector('h1');

darkModeSwitch.addEventListener('change', (event) => {
  const body  = document.getElementById('content-wrapper');
  if (event.target.checked) {
    body.classList.add('dark-mode');
    h3.style.color = 'white';
  } else {
    body.classList.remove('dark-mode');
    h3.style.color = '';
  }
});


