window.onload = function() {
    var animateDivs = document.getElementsByClassName("animate-div");
    for (var i = 0; i < animateDivs.length; i++) {
        animateDivs[i].style.animation = "slideInFromRight 0.5s ease-in-out both";
        animateDivs[i].style.animationDelay = (i * 0.1) + "s";
    }
}
