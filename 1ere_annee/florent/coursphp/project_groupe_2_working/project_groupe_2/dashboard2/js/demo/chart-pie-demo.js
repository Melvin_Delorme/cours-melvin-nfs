// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';


let ctx = document.getElementById('myBarChart');
let trames_Nombres = [];
let trames_Double = [];
fetch('inc/requetDiff.php')
    .then(function(response){
      return response.json();
    })
    .then(function(data){
      for (let i = 0; i < data.length; i++){
        trames_Nombres.push(data[i].nombre);
        trames_Double.push(data[i].name + ' ' + data[i].status);
      }

      let chart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: trames_Double,
          datasets: [{
            label: 'Nombre',
            data: trames_Nombres,
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
          }, /*{
            label: 'Status',
            data: trames_Status,
            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc','#230775FF' ],
            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            borderWidth: 1
          }*/]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    });