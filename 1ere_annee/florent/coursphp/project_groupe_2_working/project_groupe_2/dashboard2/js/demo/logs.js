const darkModeSwitch = document.getElementById('content');
const graph = document.querySelector('.h3');
const text_logs = document.querySelector('.txt-graph');

darkModeSwitch.addEventListener('change', (event) => {
    const body  = document.getElementById('content-wrapper');
    if (event.target.checked) {
        body.classList.add('dark-mode');
        text_logs.style.color = 'white';
        graph.style.color = 'white';

    } else {
        body.classList.remove('dark-mode');
        text_logs.style.color = '#5a5c69';
        graph.style.color = '#5a5c69';
    }
});


let divBase = document.querySelector('.logs')
console.log(divBase)
function getLog(){
    fetch('inc/requet_logs.php')
        .then(function (response){
            return response.json();
        })
        .then(function (data){
            console.log(data);
            for (let i = 0; i < data.length; i++){
                divBase.innerHTML += `L'adresse ip <span style="color: green;">${data[i].adresse_ip}</span> de type <span style="color: green;">${data[i].name}</span> à envoyer un paquet de type <span style="color: green;">${data[i].status}</span> à l'adresse ip <span style="color: green;">${data[i+10].adresse_ip}</span> de type <span style="color: green;">${data[i+10].name}.<br>`;
                divBase.classList.add('log-text');

            }
        })
        .catch(function (err){
            console.log(err);
        })
}
getLog();