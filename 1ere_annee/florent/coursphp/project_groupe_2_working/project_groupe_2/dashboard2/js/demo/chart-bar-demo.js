// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';


let trames_nombres = [];
let trames_Name = [];
fetch('inc/requet.php')
    .then(function (response){
      return response.json();
    })
    .then(function (data){
      for (let i = 0; i < data.length; i++){
        trames_nombres.push(data[i].number);
        trames_Name.push(data[i].name);
      }
      const ctx = document.getElementById('myChart');
      new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: trames_Name,
          datasets: [{
            label: '# of Votes',
            data: trames_nombres,
            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc','#230775FF' ],
            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
    });

